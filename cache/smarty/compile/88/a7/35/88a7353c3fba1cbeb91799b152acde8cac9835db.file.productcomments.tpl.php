<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 11:12:21
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/productcomments//productcomments.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14709756845ac23a4543be63-35907147%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '88a7353c3fba1cbeb91799b152acde8cac9835db' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/productcomments//productcomments.tpl',
      1 => 1522527998,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14709756845ac23a4543be63-35907147',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'comments' => 0,
    'comment' => 0,
    'cookie' => 0,
    'seller' => 0,
    'too_early' => 0,
    'allow_guestss' => 0,
    'allow_guests' => 0,
    'product' => 0,
    'productcomment_cover_image' => 0,
    'mediumSize' => 0,
    'criterions' => 0,
    'criterion' => 0,
    'id_product_comment_form' => 0,
    'productcomments_controller_url' => 0,
    'moderation_active' => 0,
    'productcomments_url_rewriting_activated' => 0,
    'secure_key' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac23a45493b98_67566490',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac23a45493b98_67566490')) {function content_5ac23a45493b98_67566490($_smarty_tpl) {?>
<div id="idTab5">
    <div id="product_comments_block_tab">
        <?php if ($_smarty_tpl->tpl_vars['comments']->value) {?>
            <?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
?>
                <?php if ($_smarty_tpl->tpl_vars['comment']->value['content']) {?>
               <div class="comment comment-list" itemprop="review" itemscope itemtype="https://schema.org/Review">
                        <div class="customer_comment">
                            
                                 <?php if ($_smarty_tpl->tpl_vars['comment']->value['title']=="Pregunta") {?>
                                     <div class="comment_author_infos ">
                                      <meta itemprop="datePublished" content="<?php echo substr(htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['date_add'], ENT_QUOTES, 'UTF-8', true),0,10);?>
" />
                                       <em><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['date_add'], ENT_QUOTES, 'UTF-8', true),'full'=>0),$_smarty_tpl);?>
</em>
                                     </div>
                                 <?php }?>
                                 <?php if ($_smarty_tpl->tpl_vars['comment']->value['title']!="Pregunta") {?>
                                     <div class="comment_author_infos seller_post">
                                     <em><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('class'=>"comment_author_infos seller_post",'date'=>htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['date_add'], ENT_QUOTES, 'UTF-8', true),'full'=>0),$_smarty_tpl);?>
</em>
                                     </div>
                                 <?php }?> 
                          </div>
            
  
                <div class="comment_details "> 
                            <?php if ($_smarty_tpl->tpl_vars['comment']->value['title']=="Pregunta") {?>
                                <div class="comment_details">
                                    <span itemprop="reviewBody"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['content'], ENT_QUOTES, 'UTF-8', true);?>
</span>
                                        <ul>
                                            <?php if ($_smarty_tpl->tpl_vars['comment']->value['total_advice']>0) {?>
                                                <li> <?php echo smartyTranslate(array('s'=>'%1$d out of %2$d people found this review useful.','sprintf'=>array($_smarty_tpl->tpl_vars['comment']->value['total_useful'],$_smarty_tpl->tpl_vars['comment']->value['total_advice']),'mod'=>'productcomments'),$_smarty_tpl);?>

                                                </li>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['cookie']->value->id_customer==$_smarty_tpl->tpl_vars['seller']->value->id_customer) {?>
                                      <?php if (!$_smarty_tpl->tpl_vars['comment']->value['customer_advice']) {?>
                                                     <button id="submitNewMessage" name="submitMessage" type="submit" class="btn btn-effect open-comment-form" href="#new_comment_form">
                                                         <?php echo smartyTranslate(array('s'=>'Responder','mod'=>'productcomments'),$_smarty_tpl);?>

                                                     </button>&nbsp;
                                                 <?php }?>
                                            <?php }?>
                                        </ul>
                                </div>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['comment']->value['title']!="Pregunta") {?>
                                <div class="comment_author_infos seller_post ">
                                    <span itemprop="reviewBody" class="comment_author_infos seller_post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['content'], ENT_QUOTES, 'UTF-8', true);?>
</span>
                                        <ul>
                                            <?php if ($_smarty_tpl->tpl_vars['comment']->value['total_advice']>0) {?>
                                                <li> <?php echo smartyTranslate(array('s'=>'%1$d out of %2$d people found this review useful.','sprintf'=>array($_smarty_tpl->tpl_vars['comment']->value['total_useful'],$_smarty_tpl->tpl_vars['comment']->value['total_advice']),'mod'=>'productcomments'),$_smarty_tpl);?>

                                                </li>
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['cookie']->value->id_customer==$_smarty_tpl->tpl_vars['seller']->value->id_customer) {?>
                                                 <?php if (!$_smarty_tpl->tpl_vars['comment']->value['customer_advice']) {?>
                                                    
                                                 <?php }?>
                                            <?php }?>
                                        </ul>
                                </div>
                            <?php }?>
              </div>


 

                <?php }?>
            <?php } ?>


            <?php if ((!$_smarty_tpl->tpl_vars['too_early']->value&&($_smarty_tpl->tpl_vars['cookie']->value->id_customer!=$_smarty_tpl->tpl_vars['seller']->value->id_customer||$_smarty_tpl->tpl_vars['allow_guestss']->value))) {?>
                                   <p class="align_center">
                                 <button id="new_comment_tab_btn" type="submit" class="btn btn-effect open-comment-form" href="#new_comment_form">
                                                         <?php echo smartyTranslate(array('s'=>'Write your review!','mod'=>'productcomments'),$_smarty_tpl);?>

                                                     </button>&nbsp;
                                             </p>
            

               <?php }?>
              <?php } else { ?> 
              <?php if ((!$_smarty_tpl->tpl_vars['too_early']->value&&($_smarty_tpl->tpl_vars['cookie']->value->id_customer!=$_smarty_tpl->tpl_vars['seller']->value->id_customer||$_smarty_tpl->tpl_vars['allow_guests']->value))) {?>
                <p class="align_center">
                   <button id="new_comment_tab_btn" type="submit" class="btn btn-effect open-comment-form" href="#new_comment_form">
                    <?php echo smartyTranslate(array('s'=>'Be the first to write your review!','mod'=>'productcomments'),$_smarty_tpl);?>

                     </button>&nbsp;
             
                </p>
            <?php } else { ?>
            <p class="align_center"><?php echo smartyTranslate(array('s'=>'No customer reviews for the moment.','mod'=>'productcomments'),$_smarty_tpl);?>
</p>
            <?php }?>
        <?php }?>
    </div> <!-- #product_comments_block_tab -->

<div> </div>
</div>

<!-- Fancybox -->
<div style="display: none;">
    <div id="new_comment_form">
        <form id="id_new_comment_form" action="#">
            <h3 class="page-heading">
                <?php echo smartyTranslate(array('s'=>'Write a review','mod'=>'productcomments'),$_smarty_tpl);?>

            </h3>
            <div class="">
                <?php if (isset($_smarty_tpl->tpl_vars['product']->value)&&$_smarty_tpl->tpl_vars['product']->value) {?>
                    <div class="product clearfix">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['productcomment_cover_image']->value;?>
" height="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['height'];?>
" width="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['width'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" />
                        <div class="product_desc">
                            <p class="product_name">
                                <strong><?php echo $_smarty_tpl->tpl_vars['product']->value->name;?>
</strong>
                            </p>
                            <?php echo $_smarty_tpl->tpl_vars['product']->value->description_short;?>

                        </div>
                    </div>
                <?php }?>
                
                    <?php if (count($_smarty_tpl->tpl_vars['criterions']->value)>0) {?>
                        <ul id="criterions_list">
                        <?php  $_smarty_tpl->tpl_vars['criterion'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['criterion']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['criterions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->key => $_smarty_tpl->tpl_vars['criterion']->value) {
$_smarty_tpl->tpl_vars['criterion']->_loop = true;
?>
                            <li>
                                <label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['criterion']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
:</label>
                                <div class="star_content">
                                    <input class="star not_uniform" type="radio" name="criterion[<?php echo round($_smarty_tpl->tpl_vars['criterion']->value['id_product_comment_criterion']);?>
]" value="1" />
                                    <input class="star not_uniform" type="radio" name="criterion[<?php echo round($_smarty_tpl->tpl_vars['criterion']->value['id_product_comment_criterion']);?>
]" value="2" />
                                    <input class="star not_uniform" type="radio" name="criterion[<?php echo round($_smarty_tpl->tpl_vars['criterion']->value['id_product_comment_criterion']);?>
]" value="3" />
                                    <input class="star not_uniform" type="radio" name="criterion[<?php echo round($_smarty_tpl->tpl_vars['criterion']->value['id_product_comment_criterion']);?>
]" value="4" checked="checked" />
                                    <input class="star not_uniform" type="radio" name="criterion[<?php echo round($_smarty_tpl->tpl_vars['criterion']->value['id_product_comment_criterion']);?>
]" value="5" />
                                </div>
                                <div class="clearfix"></div>
                            </li>
                                <?php } ?>
                        </ul>
                    <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['cookie']->value->id_customer==$_smarty_tpl->tpl_vars['seller']->value->id_customer) {?>

                                        <input id="comment_title" class="form-control" name="title" type="text" value="Respuesta" style="visibility:hidden"/>
                                         <p>Respuesta</p>
                                         <textarea id="content" class="form-control" name="content" placeholder="<?php echo smartyTranslate(array('s'=>'Respuesta.....','mod'=>'productcomments'),$_smarty_tpl);?>
"></textarea>
                                         <?php }?>
                                        
                                      <?php if ($_smarty_tpl->tpl_vars['cookie']->value->id_customer!=$_smarty_tpl->tpl_vars['seller']->value->id_customer) {?>
                                    
                                    
                                        
                    <input id="comment_title" class="form-control" name="title" type="text" value="Pregunta" placeholder="<?php echo smartyTranslate(array('s'=>'Title..','mod'=>'productcomments'),$_smarty_tpl);?>
"  style="visibility:hidden" />
                                     <label for="content"><?php echo smartyTranslate(array('s'=>'Comment:','mod'=>'productcomments'),$_smarty_tpl);?>
 <sup class="required">*</sup></label>
                     <textarea id="content" class="form-control" name="content" placeholder="<?php echo smartyTranslate(array('s'=>'Consulta.....','mod'=>'productcomments'),$_smarty_tpl);?>
"></textarea>
<label>
                            <?php echo smartyTranslate(array('s'=>'Your name:','mod'=>'productcomments'),$_smarty_tpl);?>
 <sup class="required">*</sup>
                        </label>
                        <input id="commentCustomerName" class="form-control" name="customer_name" type="text" placeholder="<?php echo smartyTranslate(array('s'=>'Deje su nombre o un nick...','mod'=>'productcomments'),$_smarty_tpl);?>
"/>
                                    <?php }?>
                                      
                    <div id="new_comment_form_footer">
                        <input id="id_product_comment_send" name="id_product" type="hidden" value='<?php echo $_smarty_tpl->tpl_vars['id_product_comment_form']->value;?>
' />
                        
                        <p class="fr">
                            <button id="submitNewMessage" name="submitMessage" type="submit" class="btn btn-default">
                                <span><?php echo smartyTranslate(array('s'=>'Submit','mod'=>'productcomments'),$_smarty_tpl);?>
</span>
 
                            </button>&nbsp;
                            <?php echo smartyTranslate(array('s'=>'or','mod'=>'productcomments'),$_smarty_tpl);?>
&nbsp;
                            <a class="closefb" href="#">
                                <?php echo smartyTranslate(array('s'=>'Cancel','mod'=>'productcomments'),$_smarty_tpl);?>

                            </a>
                        </p>
                        <div class="clearfix"></div>
                    </div> <!-- #new_comment_form_footer -->
                </div>
            </div>
        </form><!-- /end new_comment_form_content -->
    </div>
</div>
<!-- End fancybox -->
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('productcomments_controller_url'=>addcslashes($_smarty_tpl->tpl_vars['productcomments_controller_url']->value,'\'')),$_smarty_tpl);?>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('moderation_active'=>$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['boolval'][0][0]->boolval($_smarty_tpl->tpl_vars['moderation_active']->value)),$_smarty_tpl);?>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('productcomments_url_rewrite'=>$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['boolval'][0][0]->boolval($_smarty_tpl->tpl_vars['productcomments_url_rewriting_activated']->value)),$_smarty_tpl);?>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('secure_key'=>$_smarty_tpl->tpl_vars['secure_key']->value),$_smarty_tpl);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('addJsDefL', array('name'=>'confirm_report_message')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'confirm_report_message'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smartyTranslate(array('s'=>'Are you sure that you want to report this comment?','mod'=>'productcomments','js'=>1),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'confirm_report_message'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('addJsDefL', array('name'=>'productcomment_added')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_added'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smartyTranslate(array('s'=>'Your comment has been added!','mod'=>'productcomments','js'=>1),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_added'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('addJsDefL', array('name'=>'productcomment_added_moderation')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_added_moderation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smartyTranslate(array('s'=>'Your comment has been added and will be available once approved by a moderator.','mod'=>'productcomments','js'=>1),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_added_moderation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('addJsDefL', array('name'=>'productcomment_title')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_title'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smartyTranslate(array('s'=>'New comment','mod'=>'productcomments','js'=>1),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_title'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('addJsDefL', array('name'=>'productcomment_ok')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_ok'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smartyTranslate(array('s'=>'OK','mod'=>'productcomments','js'=>1),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'productcomment_ok'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>
