<?php /* Smarty version Smarty-3.1.19, created on 2018-04-03 14:29:33
         compiled from "C:\wamp64\www\ps16_automasterstore\themes\jms_deermarket\modules\jmspagebuilder\views\templates\hook\addoncontactinfo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11942678345ac3b9fda65656-96836758%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2788fbaca761adadab35290720e9f110902c30c2' => 
    array (
      0 => 'C:\\wamp64\\www\\ps16_automasterstore\\themes\\jms_deermarket\\modules\\jmspagebuilder\\views\\templates\\hook\\addoncontactinfo.tpl',
      1 => 1520824129,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11942678345ac3b9fda65656-96836758',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'addon_title' => 0,
    'addon_desc' => 0,
    'box_class' => 0,
    'address' => 0,
    'phone' => 0,
    'email' => 0,
    'opentime' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac3b9fda7bc17_01295943',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac3b9fda7bc17_01295943')) {function content_5ac3b9fda7bc17_01295943($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>
<div class="addon-title">
	<h3><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['addon_title']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h3>
</div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['addon_desc']->value) {?>
<p class="addon-desc"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['addon_desc']->value,100,' '), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
<?php }?>
<div class="contact-info<?php if ($_smarty_tpl->tpl_vars['box_class']->value) {?> <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['box_class']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>">
	<div class="contact-box">
			<ul>
		       	<?php if ($_smarty_tpl->tpl_vars['address']->value!='') {?>
		       		<li>
		       			<i class="fa fa-home" aria-hidden="true"></i>
		       			<span style="margin-left:7px;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['address']->value, ENT_QUOTES, 'UTF-8', true);?>
</span> 
		       		</li>
		       	<?php }?>
		       	<?php if ($_smarty_tpl->tpl_vars['phone']->value!='') {?>
					<li>
						<i class="fa fa-phone" aria-hidden="true"></i>
						<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['phone']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
					</li>
				<?php }?>
			   	<?php if ($_smarty_tpl->tpl_vars['email']->value!='') {?>
			   		<li>
			   			<i class="fa fa-envelope" aria-hidden="true"></i>
			   			<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true);?>
</span> 
			   		</li>
			   	<?php }?>
			   	<?php if ($_smarty_tpl->tpl_vars['opentime']->value!='') {?>
			   		<li>
			   			<i class="fa fa-clock-o" aria-hidden="true"></i>
			   			<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['opentime']->value, ENT_QUOTES, 'UTF-8', true);?>
</span> 
			   		</li>
			   	<?php }?>
	   	 	</ul>
	</div>
</div>
<?php }} ?>
