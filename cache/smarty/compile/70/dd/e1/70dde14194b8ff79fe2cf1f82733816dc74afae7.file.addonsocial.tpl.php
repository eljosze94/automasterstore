<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 11:36:31
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addonsocial.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1205099085ac23fef173942-35323775%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70dde14194b8ff79fe2cf1f82733816dc74afae7' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addonsocial.tpl',
      1 => 1521393992,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1205099085ac23fef173942-35323775',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'addon_title' => 0,
    'addon_desc' => 0,
    'facebook_url' => 0,
    'twitter_url' => 0,
    'linkedin_url' => 0,
    'youtube_url' => 0,
    'pinterest_url' => 0,
    'instagram_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac23fef186421_78136724',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac23fef186421_78136724')) {function content_5ac23fef186421_78136724($_smarty_tpl) {?>

<div id="social_block">
    <?php if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>
    <div class="addon-title">
        <h3><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['addon_title']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h3>
    </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['addon_desc']->value) {?>
    <p class="addon-desc"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['addon_desc']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
    <?php }?> 
    <ul class="find-us">
        <?php if ($_smarty_tpl->tpl_vars['facebook_url']->value!='') {?>
        <li class="divider">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facebook_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="facebook">
            <span class="fa fa-facebook"></span>
            </a>
        </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['twitter_url']->value!='') {?>
        <li class="divider">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['twitter_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="twitter">
            <span class="fa fa-twitter"></span>
            </a>
        </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['linkedin_url']->value!='') {?>
        <li class="divider">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['linkedin_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="linkedin">
            <span class="fa fa-linkedin"></span>
            </a>
        </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['youtube_url']->value!='') {?>
        <li class="divider">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['youtube_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="youtube">
            <span class="fa fa-youtube"></span>
            </a>
        </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pinterest_url']->value!='') {?>
        <li class="divider">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pinterest_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="pinterest">
            <span class="fa fa-pinterest"></span>
            </a>
        </li>
        <?php }?>  
        <?php if ($_smarty_tpl->tpl_vars['instagram_url']->value!='') {?>
        <li class="divider">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instagram_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="instagram">
            <span class="fa fa-instagram"></span>
            </a>
        </li>
        <?php }?>
       
        <li class="divider">
            <a href="mailto:contacto@automasterchile.cl?Subject=Consulta" target="_top" class="envelope">
            <span class="fa fa-envelope"></span>
            </a>
        </li>
       
    </ul>
</div>

<?php }} ?>
