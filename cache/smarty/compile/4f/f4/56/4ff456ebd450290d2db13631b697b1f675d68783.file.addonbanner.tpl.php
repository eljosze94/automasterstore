<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 10:58:36
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addonbanner.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13682790995ac2370cac88f4-23290667%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ff456ebd450290d2db13631b697b1f675d68783' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addonbanner.tpl',
      1 => 1519076592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13682790995ac2370cac88f4-23290667',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'banner_link' => 0,
    'target' => 0,
    'box_class' => 0,
    'banner' => 0,
    'root_url' => 0,
    'alt_text' => 0,
    'html_content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac2370cad3ee2_31628417',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac2370cad3ee2_31628417')) {function content_5ac2370cad3ee2_31628417($_smarty_tpl) {?>
<a href="<?php if ($_smarty_tpl->tpl_vars['banner_link']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['banner_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>#<?php }?>"<?php if ($_smarty_tpl->tpl_vars['target']->value=='new window') {?> target="_blank"<?php }?>>
	<div class="jms-banner<?php if ($_smarty_tpl->tpl_vars['box_class']->value) {?> <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['box_class']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>">
			<?php if ($_smarty_tpl->tpl_vars['banner']->value) {?>
				<div class="img-banner">
						<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['root_url']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['alt_text']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
				</div>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['html_content']->value) {?>
				<?php echo $_smarty_tpl->tpl_vars['html_content']->value;?>

			<?php }?>
	</div>	
</a>
<?php }} ?>
