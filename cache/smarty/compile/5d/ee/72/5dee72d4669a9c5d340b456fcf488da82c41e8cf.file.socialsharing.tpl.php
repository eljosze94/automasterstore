<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 11:12:21
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/socialsharing/socialsharing.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21446127255ac23a45396b55-90566872%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5dee72d4669a9c5d340b456fcf488da82c41e8cf' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/socialsharing/socialsharing.tpl',
      1 => 1520546424,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21446127255ac23a45396b55-90566872',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'PS_SC_TWITTER' => 0,
    'PS_SC_FACEBOOK' => 0,
    'PS_SC_GOOGLE' => 0,
    'PS_SC_PINTEREST' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac23a4539cf78_92703516',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac23a4539cf78_92703516')) {function content_5ac23a4539cf78_92703516($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['PS_SC_TWITTER']->value||$_smarty_tpl->tpl_vars['PS_SC_FACEBOOK']->value||$_smarty_tpl->tpl_vars['PS_SC_GOOGLE']->value||$_smarty_tpl->tpl_vars['PS_SC_PINTEREST']->value) {?>
	<div class="social-wrap">
		<div class="socialsharing_product">
			<span class="title"><?php echo smartyTranslate(array('s'=>'Compartir','mod'=>'socialsharing'),$_smarty_tpl);?>
: </span>
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_FACEBOOK']->value) {?>
				<button data-type="facebook" type="button" class="btn-facebook social-sharing">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</button>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_TWITTER']->value) {?>
				<button data-type="twitter" type="button" class="btn-twitter social-sharing">
					<i class="fa fa-twitter"></i>
				</button>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_GOOGLE']->value) {?>
				<button data-type="google-plus" type="button" class="btn-google-plus social-sharing">
					<i class="fa fa-google-plus" aria-hidden="true"></i>
				</button>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['PS_SC_PINTEREST']->value) {?>
				<button data-type="pinterest" type="button" class="btn-pinterest social-sharing">
					<i class="fa fa-pinterest"></i>
				</button>
			<?php }?>
		</div>
	</div>
<?php }?><?php }} ?>
