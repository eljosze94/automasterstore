<?php /* Smarty version Smarty-3.1.19, created on 2018-03-30 20:05:32
         compiled from "/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17072304355abec2bc9ead53-28730041%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0358834faea797c922f41ca0e5abaeeb456672d9' => 
    array (
      0 => '/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/footer.tpl',
      1 => 1497108760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17072304355abec2bc9ead53-28730041',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abec2bc9f27b8_68643626',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abec2bc9f27b8_68643626')) {function content_5abec2bc9f27b8_68643626($_smarty_tpl) {?>

<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceFooter'),$_smarty_tpl);?>


<ul class="footer_links clearfix">
    <li>
        <a class="btn btn-default button" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <span>
                <i class="icon-chevron-left fa fa-chevron-left"></i> <?php echo smartyTranslate(array('s'=>'Back to your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </a>
    </li>
    <li>
        <a class="btn btn-default button" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
            <span>
                <i class="icon-chevron-left  fa fa-chevron-left"></i> <?php echo smartyTranslate(array('s'=>'Back to your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </a>
    </li>
</ul>   <?php }} ?>
