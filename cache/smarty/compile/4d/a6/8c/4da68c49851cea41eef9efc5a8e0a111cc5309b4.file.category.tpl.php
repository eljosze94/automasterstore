<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 08:12:40
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/category.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5243112545ac210288d2dc2-97685203%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4da68c49851cea41eef9efc5a8e0a111cc5309b4' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/category.tpl',
      1 => 1519076591,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5243112545ac210288d2dc2-97685203',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac210288dc6a1_25066359',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac210288dc6a1_25066359')) {function content_5ac210288dc6a1_25066359($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php if (isset($_smarty_tpl->tpl_vars['category']->value)) {?>
	<?php if ($_smarty_tpl->tpl_vars['category']->value->id&&$_smarty_tpl->tpl_vars['category']->value->active) {?>
		<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
			<div class="filters-panel">
				<div class="row">
					<div class="col-lg-7 col-md-6 col-sm-6 col-xs-12 left">
						<div class="product-sort">
							<?php echo $_smarty_tpl->getSubTemplate ("./product-sort.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						</div>
						<div class="nbr-page">
							<?php echo $_smarty_tpl->getSubTemplate ("./nbr-product-page.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						</div>
					</div>
					<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 right">
						<div class="view-mode clearfix hidden-sm hidden-xs">
							<a class="view-grid" href="#">
								<span class="fa fa-th"></span>
							</a> 
							<a class="view-list" href="#">
								<span class="fa fa-th-list"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
			
			<?php echo $_smarty_tpl->getSubTemplate ("./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0);?>


			<div class="filters-panel bottom clearfix">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="pagination-block">
							<?php echo $_smarty_tpl->getSubTemplate ("./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						</div>
					</div>
				</div>
			</div>
			
		<?php }?>
	<?php } elseif ($_smarty_tpl->tpl_vars['category']->value->id) {?>
		<p class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'This category is currently unavailable.'),$_smarty_tpl);?>
</p>
	<?php }?>
<?php }?>
<?php }} ?>
