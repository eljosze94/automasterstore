<?php /* Smarty version Smarty-3.1.19, created on 2018-03-30 20:05:30
         compiled from "/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/selleraccount.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4980119165abec2ba637345-84222496%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d9ba54b392adbeaaa5ba794156b4f0a73ae4528' => 
    array (
      0 => '/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/selleraccount.tpl',
      1 => 1508758894,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4980119165abec2ba637345-84222496',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'show_import_product' => 0,
    'seller_link' => 0,
    'show_edit_seller_account' => 0,
    'show_orders' => 0,
    'show_manage_orders' => 0,
    'show_manage_carriers' => 0,
    'show_contact' => 0,
    'mesages_not_readed' => 0,
    'show_dashboard' => 0,
    'show_seller_invoice' => 0,
    'total_funds' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abec2ba66fc15_66928163',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abec2ba66fc15_66928163')) {function content_5abec2ba66fc15_66928163($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?>
    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
        <?php echo smartyTranslate(array('s'=>'Your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

    </a>
    <span class="navigation-pipe">
        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

    </span>
    <span class="navigation_page">
        <?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

    </span>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceHeader'),$_smarty_tpl);?>


<div class="row">
    <ul id="sellermenu">
        <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <a title="<?php echo smartyTranslate(array('s'=>'Add product','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','addproduct',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-plus fa fa-plus"></i>
                <span><?php echo smartyTranslate(array('s'=>'Add product','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li>  
        <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <a title="<?php echo smartyTranslate(array('s'=>'Products','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-th-list fa fa-list"></i>
                <span><?php echo smartyTranslate(array('s'=>'Products','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li> 
        <?php if ($_smarty_tpl->tpl_vars['show_import_product']->value==1) {?>
            <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a title="<?php echo smartyTranslate(array('s'=>'Import and export products','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','csvproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <i class="icon-arrow-up  fa fa-arrow-up"></i>
                    <span><?php echo smartyTranslate(array('s'=>'Import and export products','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                </a>
            </li>
        <?php }?>
        <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <a title="<?php echo smartyTranslate(array('s'=>'View my seller profile','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller_link']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-user fa fa-user"></i>
                <span><?php echo smartyTranslate(array('s'=>'Seller profile','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li>
        <?php if ($_smarty_tpl->tpl_vars['show_edit_seller_account']->value==1) {?>
            <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a title="<?php echo smartyTranslate(array('s'=>'Edit your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','editseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <i class="icon-user fa fa-edit"></i>
                    <span><?php echo smartyTranslate(array('s'=>'Edit seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                </a> 
            </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['show_orders']->value==1) {?>
            <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a title="<?php echo smartyTranslate(array('s'=>'History commissions','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerhistorycommissions',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <i class="icon-list-ol fa fa-list"></i>
                    <span><?php echo smartyTranslate(array('s'=>'History commissions','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                </a>
            </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['show_manage_orders']->value==1) {?>
            <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a title="<?php echo smartyTranslate(array('s'=>'Manage Orders','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','orders',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <i class="icon-money fa fa-money"></i>
                    <span><?php echo smartyTranslate(array('s'=>'Orders','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                </a>
            </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['show_manage_carriers']->value==1) {?>
            <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a title="<?php echo smartyTranslate(array('s'=>'Carriers and shipping cost','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','carriers',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <i class="icon-truck fa fa-truck"></i>
                    <span><?php echo smartyTranslate(array('s'=>'Carriers','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                </a>
            </li>
        <?php }?>     
        <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <a title="<?php echo smartyTranslate(array('s'=>'Payment','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerpayment',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-credit-card fa fa-credit-card"></i>
                <span><?php echo smartyTranslate(array('s'=>'Payment','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li>
        <?php if ($_smarty_tpl->tpl_vars['show_contact']->value==1) {?>
        <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <a title="<?php echo smartyTranslate(array('s'=>'Messages','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellermessages',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-envelope fa fa-envelope-o"></i>
                <span><?php echo smartyTranslate(array('s'=>'Messages','mod'=>'jmarketplace'),$_smarty_tpl);?>
 (<?php echo intval($_smarty_tpl->tpl_vars['mesages_not_readed']->value);?>
)</span>
            </a>
        </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['show_dashboard']->value==1) {?>
            <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a title="<?php echo smartyTranslate(array('s'=>'Dashboard','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','dashboard',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <i class="icon-tachometer fa fa-tachometer"></i>
                    <span><?php echo smartyTranslate(array('s'=>'Dashboard','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                </a>
            </li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['show_seller_invoice']->value==1) {?>
            <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a title="<?php echo smartyTranslate(array('s'=>'Withdraw money','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerinvoice',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <i class="icon-money fa fa-money"></i>
                    <span><?php echo smartyTranslate(array('s'=>'Withdraw money','mod'=>'jmarketplace'),$_smarty_tpl);?>
<br/>(<strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['total_funds']->value, ENT_QUOTES, 'UTF-8', true);?>
</strong>)</span>
                </a>
            </li>
        <?php }?>
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceMenu'),$_smarty_tpl);?>

    </ul>
</div>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceAfterMenu'),$_smarty_tpl);?>

<?php }} ?>
