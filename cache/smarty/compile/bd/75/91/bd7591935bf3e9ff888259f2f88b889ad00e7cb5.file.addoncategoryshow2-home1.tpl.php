<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 10:58:36
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addoncategoryshow2-home1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15020234095ac2370cadde26-12958077%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bd7591935bf3e9ff888259f2f88b889ad00e7cb5' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addoncategoryshow2-home1.tpl',
      1 => 1519240282,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15020234095ac2370cadde26-12958077',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'addon_title' => 0,
    'categories' => 0,
    'category' => 0,
    'link' => 0,
    'categoryLink' => 0,
    'id_view_all' => 0,
    'viewAllLink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac2370caeb1b9_59532798',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac2370caeb1b9_59532798')) {function content_5ac2370caeb1b9_59532798($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>
    <div class="addon-title">
        <h3><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['addon_title']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h3>
    </div>
<?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['categories']->value)&&$_smarty_tpl->tpl_vars['categories']->value) {?>
    <div class="cat-box">
        <ul class="categories-list">
            <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['category']->key;
?>
                <?php $_smarty_tpl->tpl_vars['categoryLink'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getcategoryLink($_smarty_tpl->tpl_vars['category']->value['id_category'],$_smarty_tpl->tpl_vars['category']->value['link_rewrite']), null, 0);?>
                    <li>
                        <a class="cat-name" href="<?php echo $_smarty_tpl->tpl_vars['categoryLink']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value['name'];?>
</a>
                    </li>
            <?php } ?>
        </ul>
        <?php if ($_smarty_tpl->tpl_vars['id_view_all']->value) {?>
            <?php $_smarty_tpl->tpl_vars['viewAllLink'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getcategoryLink($_smarty_tpl->tpl_vars['id_view_all']->value,'#'), null, 0);?>
            <a class="view-all-btn" href="<?php echo $_smarty_tpl->tpl_vars['viewAllLink']->value;?>
">
            <?php echo smartyTranslate(array('s'=>'VER TODO','mod'=>'jmspagebuilder'),$_smarty_tpl);?>

            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
        </a>
        <?php }?>
    </div>
    <?php } else { ?>
        <p><?php echo smartyTranslate(array('s'=>'No categories','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</p>
    <?php }?><?php }} ?>
