<?php /* Smarty version Smarty-3.1.19, created on 2018-03-30 20:05:32
         compiled from "/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/sellerproducts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11021881475abec2bc8c4b53-00212434%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65a0fc6d71cf37b9ad8c46d5d8bda29735acd446' => 
    array (
      0 => '/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/sellerproducts.tpl',
      1 => 1519753584,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11021881475abec2bc8c4b53-00212434',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'show_menu_top' => 0,
    'ps_version' => 0,
    'link' => 0,
    'navigationPipe' => 0,
    'confirmation' => 0,
    'moderate' => 0,
    'show_menu_options' => 0,
    'products' => 0,
    'search_query' => 0,
    'order_by' => 0,
    'order_way' => 0,
    'num_products' => 0,
    'errors' => 0,
    'show_active_product' => 0,
    'show_delete_product' => 0,
    'product' => 0,
    'img_prod_dir' => 0,
    'lang_iso' => 0,
    'modules_dir' => 0,
    'show_edit_product' => 0,
    'num_pages' => 0,
    'current_page' => 0,
    'foo' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abec2bc99adc9_86626343',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abec2bc99adc9_86626343')) {function content_5abec2bc99adc9_86626343($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['show_menu_top']->value==1) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("./selleraccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
    <div class="row">
        <div class="col-md-12 jmarketplace_breadcrumb">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
                <?php echo smartyTranslate(array('s'=>'Your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </a>
            <span class="navigation-pipe">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

            </span>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </a>
            <span class="navigation-pipe">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

            </span>
            <span class="navigation_page">
                <?php echo smartyTranslate(array('s'=>'Your products','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span> 
        </div>
    </div>
<?php } else { ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo smartyTranslate(array('s'=>'Your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </a>
        <span class="navigation-pipe">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

        </span>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </a>
        <span class="navigation-pipe">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

        </span>
        <span class="navigation_page">
            <?php echo smartyTranslate(array('s'=>'Your products','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </span>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?> 
<?php }?>

<?php if (isset($_smarty_tpl->tpl_vars['confirmation']->value)&&$_smarty_tpl->tpl_vars['confirmation']->value) {?>
    <?php if ($_smarty_tpl->tpl_vars['moderate']->value) {?>
        <div class="alert alert-success" style="float:left; width: 100%;">
            <?php echo smartyTranslate(array('s'=>'Your product has been successfully saved. It is pending approval.','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
        </div>
    <?php } else { ?>
        <div class="alert alert-success" style="float:left; width: 100%;">
            <?php echo smartyTranslate(array('s'=>'Your product has been successfully saved.','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
        </div>
    <?php }?>
<?php }?>        

<div class="row">
    <div class="column col-xs-12 col-sm-12 col-md-3"<?php if ($_smarty_tpl->tpl_vars['show_menu_options']->value==0) {?> style="display:none;"<?php }?>>
        <?php echo $_smarty_tpl->getSubTemplate ("./sellermenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
    
    <div class="col-xs-12 <?php if ($_smarty_tpl->tpl_vars['show_menu_options']->value==1) {?>col-sm-12 col-md-9<?php }?>">
        <?php if ($_smarty_tpl->tpl_vars['products']->value&&count($_smarty_tpl->tpl_vars['products']->value)>0||$_smarty_tpl->tpl_vars['search_query']->value!='') {?>
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-lg-8">
                        <h4 class="box-title"><?php echo smartyTranslate(array('s'=>'Order and Search','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                        <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std">
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                    <label><?php echo smartyTranslate(array('s'=>'Order by','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <select name="orderby" class="form-control">
                                        <option value="name"><?php echo smartyTranslate(array('s'=>'Product name','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                        <option value="date_add"<?php if ($_smarty_tpl->tpl_vars['order_by']->value=='date_add') {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Date add','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                        <option value="date_upd"<?php if ($_smarty_tpl->tpl_vars['order_by']->value=='date_upd') {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Date update','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                        <option value="active"<?php if ($_smarty_tpl->tpl_vars['order_by']->value=='active') {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Status','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-5">
                                    <label><?php echo smartyTranslate(array('s'=>'Order way','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <select name="orderway" class="form-control">
                                        <option value="desc"<?php if ($_smarty_tpl->tpl_vars['order_way']->value=='desc') {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Descending','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                        <option value="asc"<?php if ($_smarty_tpl->tpl_vars['order_way']->value=='asc') {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Ascending','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-3" style="margin-top: 31px;">
                                    <button type="submit" name="submitOrder" class="btn btn-primary">
                                        <span><?php echo smartyTranslate(array('s'=>'Order','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-12 col-md-12 col-lg-4">
                        <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std">
                            <div class="input-group input-group-sm search-seller-products">
                                <input type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true);?>
" name="search_query" id="search_seller_product_query" class="form-control">
                                <span class="input-group-btn">
                                  <button type="submit" class="btn btn-primary btn-flat"><?php echo smartyTranslate(array('s'=>'Search','mod'=>'jmarketplace'),$_smarty_tpl);?>
</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php }?>
        
        <div class="box">
            <h1 class="page-subheading">
                <?php echo smartyTranslate(array('s'=>'Your products','mod'=>'jmarketplace'),$_smarty_tpl);?>
 (<?php echo intval($_smarty_tpl->tpl_vars['num_products']->value);?>
)
                <div class="form-group pull-right">
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','addproduct',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" class="btn btn-default button button-small">
                        <span><i class="icon-plus fa fa-plus"></i><?php echo smartyTranslate(array('s'=>'Add new product','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
                    </a>
                </div> 
            </h1>
            <?php if ($_smarty_tpl->tpl_vars['ps_version']->value!='1.7') {?>
                <?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&$_smarty_tpl->tpl_vars['errors']->value) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                <?php }?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['products']->value&&count($_smarty_tpl->tpl_vars['products']->value)>0) {?>
                <form id="form-products" method="post" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                    <div class="table-responsive">
                        <table id="order-list" class="table table-bordered footab">
                            <thead>
                                <tr>
                                    <?php if ($_smarty_tpl->tpl_vars['show_active_product']->value==1||$_smarty_tpl->tpl_vars['show_delete_product']->value==1) {?><th class="first_item text-center hidden-xs"></th><?php }?>
                                    <th class="item text-center"><?php echo smartyTranslate(array('s'=>'Image','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                    <th class="item"><?php echo smartyTranslate(array('s'=>'Product name','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                    <th class="item"><?php echo smartyTranslate(array('s'=>'Date add','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                    <th class="item"><?php echo smartyTranslate(array('s'=>'Date update','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                    <th class="item text-center"><?php echo smartyTranslate(array('s'=>'Status','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                    <th class="item text-center"><?php echo smartyTranslate(array('s'=>'Actions','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                                <tr>
                                    <?php if ($_smarty_tpl->tpl_vars['show_active_product']->value==1||$_smarty_tpl->tpl_vars['show_delete_product']->value==1) {?>
                                        <td class="first_item text-center">
                                            <input name="productBox[]" value="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" class="ck not_uniform" type="checkbox">
                                        </td>
                                    <?php }?>
                                    <td class="item text-center">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['id_image']) {?>
                                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'small_default'), ENT_QUOTES, 'UTF-8', true);?>
" />
                                        <?php } else { ?>
                                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_prod_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lang_iso']->value, ENT_QUOTES, 'UTF-8', true);?>
-default-small_default.jpg" />
                                        <?php }?>
                                    </td>
                                    <td class="item">
                                        <strong><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</strong><br/>
                                        <small><?php echo smartyTranslate(array('s'=>'Price','mod'=>'jmarketplace'),$_smarty_tpl);?>
: <?php if ($_smarty_tpl->tpl_vars['product']->value['price_without_reduction']>$_smarty_tpl->tpl_vars['product']->value['price']) {?><span class="price_without_reduction"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['regular_price'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span><?php }?> <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['final_price'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small><br/>
                                        <small><?php echo smartyTranslate(array('s'=>'Quantity','mod'=>'jmarketplace'),$_smarty_tpl);?>
: <?php echo intval($_smarty_tpl->tpl_vars['product']->value['real_quantity']);?>
 <?php if ($_smarty_tpl->tpl_vars['product']->value['real_quantity']==1) {?><?php echo smartyTranslate(array('s'=>'unit','mod'=>'jmarketplace'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'units','mod'=>'jmarketplace'),$_smarty_tpl);?>
<?php }?></small>
                                    </td>
                                    <td class="item"><i class="icon-calendar fa fa-calendar"></i> - <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['product']->value['date_add'],'full'=>0),$_smarty_tpl);?>
 - <i class="icon-time fa fa-clock-o"></i> <?php echo substr(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['date_add'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'),11,5);?>
</td>
                                    <td class="item"><i class="icon-calendar fa fa-calendar"></i> - <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['product']->value['date_upd'],'full'=>0),$_smarty_tpl);?>
 - <i class="icon-time fa fa-clock-o"></i> <?php echo substr(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['date_upd'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'),11,5);?>
</td>
                                    <td class="item text-center">
                                        <?php if ($_smarty_tpl->tpl_vars['show_active_product']->value==1) {?>
                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?>
                                                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['active_product_link'], ENT_QUOTES, 'UTF-8', true);?>
">
                                                    <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.6'||$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                        <i class="icon-check fa fa-check" title="<?php echo smartyTranslate(array('s'=>'Disable','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
"></i>
                                                    <?php } else { ?>
                                                        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/status_green.png" title="<?php echo smartyTranslate(array('s'=>'Disable','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" />
                                                    <?php }?>
                                                </a>
                                            <?php } else { ?> 
                                                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['active_product_link'], ENT_QUOTES, 'UTF-8', true);?>
">
                                                    <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.6'||$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                        <i class="icon-times fa fa-times" title="<?php echo smartyTranslate(array('s'=>'Enable','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
"></i>
                                                    <?php } else { ?>
                                                        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/status_red.png" title="<?php echo smartyTranslate(array('s'=>'Enable','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" />
                                                    <?php }?>

                                                </a>
                                            <?php }?>
                                        <?php } else { ?>
                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?>
                                                <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.6'||$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                    <i class="icon-check fa fa-check" title="<?php echo smartyTranslate(array('s'=>'Active','mod'=>'jmarketplace'),$_smarty_tpl);?>
"></i>
                                                <?php } else { ?>
                                                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/status_green.png" title="<?php echo smartyTranslate(array('s'=>'Active','mod'=>'jmarketplace'),$_smarty_tpl);?>
" />
                                                <?php }?>

                                            <?php } else { ?> 
                                                <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.6'||$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                    <i class="icon-times fa fa-times" title="<?php echo smartyTranslate(array('s'=>'Pending approval','mod'=>'jmarketplace'),$_smarty_tpl);?>
"></i>
                                                <?php } else { ?>
                                                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/status_red.png" title="<?php echo smartyTranslate(array('s'=>'Pending approval','mod'=>'jmarketplace'),$_smarty_tpl);?>
" />
                                                <?php }?>
                                            <?php }?>
                                        <?php }?>
                                    </td>
                                    <td class="item text-center">
                                        <a class="btn btn-primary btn-xs btn-view" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product']), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'View','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
                                            <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.6'||$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                <i class="icon-eye fa fa-eye"></i> <?php echo smartyTranslate(array('s'=>'View','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                            <?php } else { ?>
                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/view.gif" />
                                            <?php }?>
                                        </a><br/>
                                        <?php if ($_smarty_tpl->tpl_vars['show_edit_product']->value==1) {?>
                                            <a class="btn btn-primary btn-xs btn-edit" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['edit_product_link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
">
                                                <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.6'||$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                    <i class="icon-pencil fa fa-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                <?php } else { ?>
                                                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/edit.gif" />
                                                <?php }?>
                                            </a><br/>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['show_delete_product']->value==1) {?>
                                            <a class="btn btn-primary btn-xs delete_product" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['delete_product_link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" onclick="return confirm('¿Estás seguro de borrar este producto?');">
                                                <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.6'||$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                    <i class="icon-trash-o fa fa-trash-o"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                <?php } else { ?>
                                                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/remove.gif" />
                                                <?php }?> 
                                            </a>
                                        <?php }?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row" style="margin-top:15px;">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        <?php if ($_smarty_tpl->tpl_vars['show_active_product']->value==1||$_smarty_tpl->tpl_vars['show_delete_product']->value==1) {?>
                            <div id="bulk_actions" class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <?php echo smartyTranslate(array('s'=>'Bulk actions','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a id="check_all" href="#"><i class="icon-check-sign fa fa-check-sign"></i> <?php echo smartyTranslate(array('s'=>'Select all','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></li>
                                    <li><a id="uncheck_all" href="#"><i class="icon-check-empty fa fa-check-empty"></i> <?php echo smartyTranslate(array('s'=>'Unselect all','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></li>
                                    <?php if ($_smarty_tpl->tpl_vars['show_active_product']->value==1) {?>
                                        <li role="separator" class="divider"></li>
                                        <li><a class="bulk_all" id="submitBulkenableSelectionproduct" href="#"><i class="icon-power-off fa fa-power-off text-success"></i> <?php echo smartyTranslate(array('s'=>'Enable selection','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></li>
                                        <li><a class="bulk_all" id="submitBulkdisableSelectionproduct" href="#"><i class="icon-power-off fa fa-power-off text-danger"></i> <?php echo smartyTranslate(array('s'=>'Disable selection','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></li>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['show_delete_product']->value==1) {?>
                                        <li role="separator" class="divider"></li>
                                        <li><a class="bulk_all" id="submitBulkdeleteSelectionproduct" href="#"><i class="icon-trash-o fa fa-trash-o"></i> <?php echo smartyTranslate(array('s'=>'Delete selected','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></li>
                                    <?php }?>
                                </ul>
                            </div>
                         <?php }?>
                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['num_pages']->value>1) {?>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <div class="dataTables_paginate paging_simple_numbers pull-right">
                            <ul class="pagination">
                                <?php if ($_smarty_tpl->tpl_vars['current_page']->value<=$_smarty_tpl->tpl_vars['num_pages']->value) {?>
                                    <li class="paginate_button next">
                                        <a tabindex="0" data-dt-idx="<?php echo $_smarty_tpl->tpl_vars['current_page']->value-intval(1);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
?orderby=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_by']->value, ENT_QUOTES, 'UTF-8', true);?>
&orderway=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_way']->value, ENT_QUOTES, 'UTF-8', true);?>
&page=<?php echo $_smarty_tpl->tpl_vars['current_page']->value-intval(1);?>
"><?php echo smartyTranslate(array('s'=>'Previus','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                    </li>
                                <?php }?>
                                <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['num_pages']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['num_pages']->value)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = 1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
                                    <li class="paginate_button<?php if ($_smarty_tpl->tpl_vars['current_page']->value==$_smarty_tpl->tpl_vars['foo']->value) {?> active<?php }?>">
                                        <a tabindex="0" data-dt-idx="<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
?orderby=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_by']->value, ENT_QUOTES, 'UTF-8', true);?>
&orderway=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_way']->value, ENT_QUOTES, 'UTF-8', true);?>
&page=<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
"><?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
</a>
                                    </li>
                                <?php }} ?>  
                                <?php if ($_smarty_tpl->tpl_vars['current_page']->value<$_smarty_tpl->tpl_vars['num_pages']->value) {?>
                                    <li class="paginate_button next">
                                        <a tabindex="0" data-dt-idx="<?php echo $_smarty_tpl->tpl_vars['current_page']->value+intval(1);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
?orderby=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_by']->value, ENT_QUOTES, 'UTF-8', true);?>
&orderway=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_way']->value, ENT_QUOTES, 'UTF-8', true);?>
&page=<?php echo $_smarty_tpl->tpl_vars['current_page']->value+intval(1);?>
"><?php echo smartyTranslate(array('s'=>'Next','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                    </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                    <?php }?>
                </div>
                </form>
            <?php } else { ?>
                <p class="alert alert-info"><?php echo smartyTranslate(array('s'=>'There are not products.','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </p>
            <?php }?>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ("./footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("./varstoscript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
