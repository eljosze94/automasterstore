<?php /* Smarty version Smarty-3.1.19, created on 2018-03-30 19:39:37
         compiled from "/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/editseller.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17895060215abebca98e6f54-87048636%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be6448cc57fea6205f50eb2c2c8528649020ce90' => 
    array (
      0 => '/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/editseller.tpl',
      1 => 1521127543,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17895060215abebca98e6f54-87048636',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'show_menu_top' => 0,
    'ps_version' => 0,
    'link' => 0,
    'navigationPipe' => 0,
    'show_menu_options' => 0,
    'confirmation' => 0,
    'moderate' => 0,
    'errors' => 0,
    'seller' => 0,
    'show_shop_name' => 0,
    'show_cif' => 0,
    'show_language' => 0,
    'languages' => 0,
    'language' => 0,
    'show_phone' => 0,
    'show_fax' => 0,
    'show_address' => 0,
    'show_country' => 0,
    'countries' => 0,
    'country' => 0,
    'show_state' => 0,
    'show_postcode' => 0,
    'show_city' => 0,
    'show_description' => 0,
    'show_logo' => 0,
    'photo' => 0,
    'img_ps_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abebca9961b08_39776404',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abebca9961b08_39776404')) {function content_5abebca9961b08_39776404($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['show_menu_top']->value==1) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("./selleraccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
    <div class="row">
        <div class="col-md-12 jmarketplace_breadcrumb">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
                <?php echo smartyTranslate(array('s'=>'Your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </a>
            <span class="navigation-pipe">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

            </span>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </a>
            <span class="navigation-pipe">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

            </span>
            <span class="navigation_page">
                <?php echo smartyTranslate(array('s'=>'Edit your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </div>
    </div>
<?php } else { ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo smartyTranslate(array('s'=>'Your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </a>
        <span class="navigation-pipe">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

        </span>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </a>
        <span class="navigation-pipe">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

        </span>
        <span class="navigation_page">
            <?php echo smartyTranslate(array('s'=>'Edit your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </span>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>   
<?php }?>

<div class="row">
    <div class="column col-xs-12 col-sm-12 col-md-3"<?php if ($_smarty_tpl->tpl_vars['show_menu_options']->value==0) {?> style="display:none;"<?php }?>>
        <?php echo $_smarty_tpl->getSubTemplate ("./sellermenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
    
    <div class="col-xs-12 <?php if ($_smarty_tpl->tpl_vars['show_menu_options']->value==1) {?>col-sm-12 col-md-9<?php }?>">
        <div class="box">
            <h1 class="page-subheading"><?php echo smartyTranslate(array('s'=>'Edit your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h1>
            <?php if (isset($_smarty_tpl->tpl_vars['confirmation']->value)&&$_smarty_tpl->tpl_vars['confirmation']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['moderate']->value) {?>
                    <p class="alert alert-success"><?php echo smartyTranslate(array('s'=>'Your seller account has been successfully edited. It is pending approval.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                <?php } else { ?>
                    <p class="alert alert-success"><?php echo smartyTranslate(array('s'=>'Your seller account has been successfully edited.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                <?php }?>
            <?php } else { ?>
                <?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&$_smarty_tpl->tpl_vars['errors']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['ps_version']->value!='1.7') {?>
                        <?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&$_smarty_tpl->tpl_vars['errors']->value) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                        <?php }?>
                    <?php }?>
                <?php }?>
                <p class="info-title"><?php echo smartyTranslate(array('s'=>'Edit your seller account.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','editseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std" enctype="multipart/form-data">
                    <fieldset>
                        <div class="required form-group">
                            <label for="name" class="required"><?php echo smartyTranslate(array('s'=>'Seller name','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                            <input class="is_required validate form-control" type="text" id="name" name="name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="64" required />
                        </div>
                        <?php if ($_smarty_tpl->tpl_vars['show_shop_name']->value==1) {?>
                            <div class="form-group">
                                <label for="shop"><?php echo smartyTranslate(array('s'=>'Shop name','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="shop" id="shop" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->shop, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="64" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_cif']->value==1) {?>
                            <div class="required form-group">
                                <label for="cif"><?php echo smartyTranslate(array('s'=>'CIF/NIF','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="cif" id="cif" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->cif, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="16" />
                            </div>
                        <?php }?>
                        <div class="required form-group">
                            <label for="email" class="required"><?php echo smartyTranslate(array('s'=>'Seller email','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                            <input class="is_required validate form-control" type="text" id="email" name="email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->email, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="64" required />
                        </div>
                        <?php if ($_smarty_tpl->tpl_vars['show_language']->value==1) {?>
                            <div class="form-group">
                                <label for="seller_lang"><?php echo smartyTranslate(array('s'=>'Language','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <select name="id_lang">
                                    <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                        <option value="<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if ($_smarty_tpl->tpl_vars['seller']->value->id_lang==$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
 </option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_phone']->value==1) {?>
                            <div class="required form-group">
                                <label for="phone"><?php echo smartyTranslate(array('s'=>'Phone','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="phone" id="phone" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->phone, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="32" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_fax']->value==1) {?>
                            <div class="form-group">
                                <label for="fax"><?php echo smartyTranslate(array('s'=>'Fax','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="fax" id="fax" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->fax, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="32" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_address']->value==1) {?>
                            <div class="form-group">
                                <label for="address"><?php echo smartyTranslate(array('s'=>'Address','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="address" id="address" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->address, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_country']->value==1) {?>
                            <div class="form-group">
                                <label><?php echo smartyTranslate(array('s'=>'Country','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <select name="country">
                                    <option value=""><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                    <?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value) {
$_smarty_tpl->tpl_vars['country']->_loop = true;
?>
                                        <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['country']->value['name']==$_smarty_tpl->tpl_vars['seller']->value->country) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_state']->value==1) {?>
                            <div class="form-group">
                                <label for="state"><?php echo smartyTranslate(array('s'=>'State','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                <select id="id_ciudad" class="form-control" name="id_ciudad">
                                <option value="68" selected="selected">Santiago</option>
                                </select>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_postcode']->value==1) {?>
                            <div class="form-group">
                                <label for="postcode"><?php echo smartyTranslate(array('s'=>'Postal Code','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="postcode" id="postcode" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->postcode, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="12" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_city']->value==1) {?>
                            <div class="form-group">
                                <label for="city"><?php echo smartyTranslate(array('s'=>'City','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="city" id="city" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller']->value->city, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_description']->value==1) {?>
                            <div class="form-group">
                                <label for="description"><?php echo smartyTranslate(array('s'=>'Description','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <textarea name="description" id="description" cols="40" rows="7"><?php echo $_smarty_tpl->tpl_vars['seller']->value->description;?>
 </textarea>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_logo']->value==1) {?>    
                            <div class="form-group">
                                <label for="fileUpload"><?php echo smartyTranslate(array('s'=>'Logo or photo','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <!--<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />-->
                                <input type="file" name="sellerImage" id="fileUpload" class="form-control" />
                                <?php if (isset($_smarty_tpl->tpl_vars['photo']->value)) {?>
                                    <p><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_ps_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
sellers/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['photo']->value, ENT_QUOTES, 'UTF-8', true);?>
" width="70" height="80" /></p>
                                <?php }?>
                            </div>
                        <?php }?>  
                        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceFormAddSeller'),$_smarty_tpl);?>

                        <div class="form-group">
                            <button type="submit" name="submitEditSeller" class="btn btn-default button button-medium">
                                <span><?php echo smartyTranslate(array('s'=>'Save','mod'=>'jmarketplace'),$_smarty_tpl);?>
<i class="icon-chevron-right right"></i></span>
                            </button>
                        </div>
                    </fieldset>
                </form>
            <?php }?>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ("./footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
</div>   

<?php }} ?>
