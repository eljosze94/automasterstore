<?php /* Smarty version Smarty-3.1.19, created on 2018-03-29 14:59:04
         compiled from "/home/h3hued5u4248/public_html/modules/jmsblog/views/templates/admin/jmsblog_post/filter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7312158085abd296872deb6-09643597%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c06f6c3078b0aa63aeca7edb95168810ecdc26b' => 
    array (
      0 => '/home/h3hued5u4248/public_html/modules/jmsblog/views/templates/admin/jmsblog_post/filter.tpl',
      1 => 1519076592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7312158085abd296872deb6-09643597',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'categories' => 0,
    'category' => 0,
    'filter_category_id' => 0,
    'filter_state' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abd296873e0e9_95302876',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abd296873e0e9_95302876')) {function content_5abd296873e0e9_95302876($_smarty_tpl) {?>
<script type="text/javascript">
$( document ).ready(function() {
	$( "#filter_category_id" ).change(function() {
		filterchange();
	});
	$( "#filter_state" ).change(function() {
		filterchange();
	});
});
function filterchange(){
	var category_id = $( "#filter_category_id" ).val();
	var state = $( "#filter_state" ).val();
	var url = "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminJmsblogPost'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&configure=jmsblog&filter_category_id=" + category_id + "&filter_state=" + state;
	url = url.replace('&amp;','&');
	window.location = url;
}
</script>
<div class="jms-blog-filter">
	<span><?php echo smartyTranslate(array('s'=>'Category Filter','mod'=>'jmsblog'),$_smarty_tpl);?>
</span>
	<select id="filter_category_id">
		<option value="0"><?php echo smartyTranslate(array('s'=>'Select Category','mod'=>'jmsblog'),$_smarty_tpl);?>
</option>
		<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
		<option <?php if ($_smarty_tpl->tpl_vars['category']->value['category_id']==$_smarty_tpl->tpl_vars['filter_category_id']->value) {?>selected<?php }?> value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category_id'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
		<?php } ?>
	</select>
	
	<span><?php echo smartyTranslate(array('s'=>'State Filter','mod'=>'jmsblog'),$_smarty_tpl);?>
</span>
	<select id="filter_state">
		<option <?php if ($_smarty_tpl->tpl_vars['filter_state']->value==-1) {?>selected<?php }?> value="-1"><?php echo smartyTranslate(array('s'=>'Select Status','mod'=>'jmsblog'),$_smarty_tpl);?>
</option>		
		<option <?php if ($_smarty_tpl->tpl_vars['filter_state']->value==1) {?>selected<?php }?> value="1"><?php echo smartyTranslate(array('s'=>'Enabled','mod'=>'jmsblog'),$_smarty_tpl);?>
</option>		
		<option <?php if ($_smarty_tpl->tpl_vars['filter_state']->value==0) {?>selected<?php }?> value="0"><?php echo smartyTranslate(array('s'=>'Disabled','mod'=>'jmsblog'),$_smarty_tpl);?>
</option>
	</select>
</div><?php }} ?>
