<?php /* Smarty version Smarty-3.1.19, created on 2018-04-03 14:29:32
         compiled from "C:\wamp64\www\ps16_automasterstore\themes\jms_deermarket\modules\jmspagebuilder\views\templates\hook\productbox.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20175156545ac3b9fcdb6881-53850056%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dbf6386647a13ea39d8d26cac5b864b6267c02c1' => 
    array (
      0 => 'C:\\wamp64\\www\\ps16_automasterstore\\themes\\jms_deermarket\\modules\\jmspagebuilder\\views\\templates\\hook\\productbox.tpl',
      1 => 1519249790,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20175156545ac3b9fcdb6881-53850056',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product' => 0,
    'jpb_phover' => 0,
    'link' => 0,
    'specific_prices' => 0,
    'comparator_max_item' => 0,
    'PS_CATALOG_MODE' => 0,
    'restricted_country_mode' => 0,
    'priceDisplay' => 0,
    'currency' => 0,
    'add_prod_display' => 0,
    'static_token' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac3b9fce04ba5_37878013',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac3b9fce04ba5_37878013')) {function content_5ac3b9fce04ba5_37878013($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'C:\\wamp64\\www\\ps16_automasterstore\\tools\\smarty\\plugins\\modifier.date_format.php';
?>
<div class="product-box" itemscope itemtype="http://schema.org/Product">
	<div class="preview product-colors" data-id-product="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8', true);?>
">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" class="product-image <?php if ($_smarty_tpl->tpl_vars['jpb_phover']->value=='image_swap') {?>image_swap<?php } else { ?>image_blur<?php }?>" data-id-product="<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" itemprop="url">
			<img class="img-responsive product-img1" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'large_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image" />
		</a>
		<?php if ($_smarty_tpl->tpl_vars['product']->value['specific_prices']) {?>
                                <?php $_smarty_tpl->tpl_vars['specific_prices'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['specific_prices'], null, 0);?>
									<?php if ($_smarty_tpl->tpl_vars['specific_prices']->value['reduction_type']=='percentage'&&($_smarty_tpl->tpl_vars['specific_prices']->value['from']==$_smarty_tpl->tpl_vars['specific_prices']->value['to']||(smarty_modifier_date_format(time(),'%Y-%m-%d %H:%M:%S')<=$_smarty_tpl->tpl_vars['specific_prices']->value['to']&&smarty_modifier_date_format(time(),'%Y-%m-%d %H:%M:%S')>=$_smarty_tpl->tpl_vars['specific_prices']->value['from']))) {?>
                                    <span class="price-percent-reduction">-<?php echo $_smarty_tpl->tpl_vars['specific_prices']->value['reduction']*floatval(100);?>
%</span>
									<?php }?>
                            <?php }?>
		<div class="action-btn">
				<a class="add-to-bookmark addToWishlist product-btn btn-hover" href="#" onclick="WishlistCart('wishlist_block_list', 'add', '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8', true);?>
', false, 1); return false;" data-id-product="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Agregar a lista de deseados','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
">
					<i class="fa fa-heart" aria-hidden="true"></i>
				</a>
				<?php if (isset($_smarty_tpl->tpl_vars['comparator_max_item']->value)&&$_smarty_tpl->tpl_vars['comparator_max_item']->value) {?>
					<a class="add_to_compare product-btn btn-hover" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
 " data-id-product="<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" title="<?php echo smartyTranslate(array('s'=>'Agregar a lista de comparación','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
">
						<i class="fa fa-exchange" aria-hidden="true"></i>
						<i class="fa fa-check" aria-hidden="true"></i>
					</a>
				<?php }?>
		</div>
		<a href="#" data-link="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" class="quick-view product-btn hidden-xs btn-hover">
			<?php echo smartyTranslate(array('s'=>'VISTA RÁPIDA','mod'=>'jmspagebuilder'),$_smarty_tpl);?>

		</a>
	</div>
	<div class="product-info">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" class="product-name">
			<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>

		</a>
		<?php if (!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
			<div class="content_price clearfix" itemscope itemtype="http://schema.org/Offer">
				<?php if ($_smarty_tpl->tpl_vars['product']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?>
					<span class="price new" itemprop="price">
						<?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?>
							<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price']),$_smarty_tpl);?>

						<?php } else { ?>
							<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']),$_smarty_tpl);?>

						<?php }?>
					</span>
				<?php }?>
				<?php if (isset($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']>0) {?>
					<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"old_price"),$_smarty_tpl);?>

						<span class="price old">
							<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['product']->value['price_without_reduction']),$_smarty_tpl);?>

						</span>
				<?php }?>
				<meta itemprop="priceCurrency" content="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->iso_code, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
			</div>
		<?php }?>
		<?php $_smarty_tpl->_capture_stack[0][] = array('displayProductListReviews', null, null); ob_start(); ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListReviews','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
				<?php if (($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['product']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['product']->value['minimal_quantity']==1&&$_smarty_tpl->tpl_vars['product']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
					<?php if (($_smarty_tpl->tpl_vars['product']->value['quantity']>0||$_smarty_tpl->tpl_vars['product']->value['allow_oosp'])) {?>
						<a class="product-btn cart-button ajax_add_to_cart_button btn-effect" data-id-product="<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart'), ENT_QUOTES, 'UTF-8', true);?>
?qty=1&amp;id_product=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
&amp;add" title="<?php echo smartyTranslate(array('s'=>'Agregar al carro','mod'=>'jmsproductfilter'),$_smarty_tpl);?>
">
								<span class="fa fa-shopping-cart" aria-hidden="true"></span>
								<span class="text"><?php echo smartyTranslate(array('s'=>'AGREGAR AL CARRO','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</span>
								<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
								<i class="fa fa-check" aria-hidden="true"></i>
						</a>							
					<?php } else { ?>
						<a href="#" class="product-btn cart-button btn-default ajax_add_to_cart_button disable btn-effect" title="<?php echo smartyTranslate(array('s'=>'Out of Stock','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
">
							<span class="fa fa-shopping-cart" aria-hidden="true"></span>
							<span class="text"><?php echo smartyTranslate(array('s'=>'AGREGAR AL CARRO','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</span>
							<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
							<i class="fa fa-check" aria-hidden="true"></i>
						</a>
					<?php }?>									
				<?php }?>
	</div>
</div><?php }} ?>
