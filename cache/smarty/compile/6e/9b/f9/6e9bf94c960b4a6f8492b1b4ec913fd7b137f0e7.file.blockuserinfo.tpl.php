<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 11:36:30
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/blockuserinfo/blockuserinfo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10240830105ac23fee17d370-46343190%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e9bf94c960b4a6f8492b1b4ec913fd7b137f0e7' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/blockuserinfo/blockuserinfo.tpl',
      1 => 1519140191,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10240830105ac23fee17d370-46343190',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'logged' => 0,
    'link' => 0,
    'cookie' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac23fee197112_69631009',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac23fee197112_69631009')) {function content_5ac23fee197112_69631009($_smarty_tpl) {?>

<!-- Block user information module HEADER -->
	<div class="btn-group compact-hidden user-info">
		<?php if ($_smarty_tpl->tpl_vars['logged']->value) {?>
			<a class="btn-xs dropdown-toggle login account" data-toggle="dropdown" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
					<span class="text">
						<?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>

<i aria-hidden="true" class="fa fa-angle-down"></i>
					</span>
				</a>
				<ul role="menu" class="dropdown-menu">
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" class="account" rel="nofollow">
							<?php echo smartyTranslate(array('s'=>'Mi Cuenta','mod'=>'blockuserinfo'),$_smarty_tpl);?>

						</a>
					</li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('compare',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Compare page','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
							<?php echo smartyTranslate(array('s'=>'Comparación','mod'=>'blockuserinfo'),$_smarty_tpl);?>

						</a>
					</li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
							<?php echo smartyTranslate(array('s'=>'Pagar','mod'=>'blockuserinfo'),$_smarty_tpl);?>

						</a>
					</li>
					<?php if ($_smarty_tpl->tpl_vars['logged']->value) {?>
						<li>
							<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,"mylogout");?>
" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
								<?php echo smartyTranslate(array('s'=>'Salir','mod'=>'blockuserinfo'),$_smarty_tpl);?>

							</a>
						</li>
					<?php }?>
				</ul>
		<?php } else { ?>
			<a title="Login & Register" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true);?>
" rel="nofollow" class="btn-xs">
				<span class="text">
					<?php echo smartyTranslate(array('s'=>'Iniciar Sesión','mod'=>'blockuserinfo'),$_smarty_tpl);?>

					
				</span>
			</a>
		<?php }?>
	</div>
<?php }} ?>
