<?php /* Smarty version Smarty-3.1.19, created on 2018-03-31 00:48:55
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/my-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20722345905abf05274334a8-61647256%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2112c492e8d32c27a4de9b24c3d33948cd26708c' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/my-account.tpl',
      1 => 1522347311,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20722345905abf05274334a8-61647256',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'account_created' => 0,
    'has_customer_an_address' => 0,
    'link' => 0,
    'returnAllowed' => 0,
    'voucherAllowed' => 0,
    'HOOK_CUSTOMER_ACCOUNT' => 0,
    'force_ssl' => 0,
    'base_dir_ssl' => 0,
    'base_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abf052747f751_04999498',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abf052747f751_04999498')) {function content_5abf052747f751_04999498($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'My account'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<h1 class="page-heading"><?php echo smartyTranslate(array('s'=>'My account'),$_smarty_tpl);?>
</h1>

<div class="row addresses-lists">
    <div class="col-sm-12">
        <?php if (isset($_smarty_tpl->tpl_vars['account_created']->value)) {?>
            <p class="alert alert-success fa fa-check">
                <?php echo smartyTranslate(array('s'=>'Your account has been created.'),$_smarty_tpl);?>

            </p>
        <?php }?>
        <p class="info-account"><?php echo smartyTranslate(array('s'=>'Welcome to your account. Here you can manage all of your personal information and orders.'),$_smarty_tpl);?>
</p>
    </div>
	<ul class="myaccount-link-list">
        <?php if ($_smarty_tpl->tpl_vars['has_customer_an_address']->value) {?>
        <li class="col-sm-6">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('address',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Add my first address'),$_smarty_tpl);?>
">
                <span class="fa fa-plus"><?php echo smartyTranslate(array('s'=>' Agregar mi primera dirección'),$_smarty_tpl);?>
</span>
            </a>
        </li>
        <?php }?>
        <li class="col-sm-6">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('history',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
">
                <span class="fa fa-list"><?php echo smartyTranslate(array('s'=>' HISTORIAL DE PEDIDOS Y DETALLES'),$_smarty_tpl);?>
</span>
            </a>
        </li>
        <?php if ($_smarty_tpl->tpl_vars['returnAllowed']->value) {?>
            <li class="col-sm-6">
                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-follow',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Merchandise returns'),$_smarty_tpl);?>
">
                    <span><?php echo smartyTranslate(array('s'=>'My merchandise returns'),$_smarty_tpl);?>
</span>
                </a>
            </li>
        <?php }?>
        <li class="col-sm-6">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
">
                <span class="fa fa-credit-card"><?php echo smartyTranslate(array('s'=>' Mis Pagos'),$_smarty_tpl);?>
</span>
            </a>
        </li>
        <li class="col-sm-6">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
">
                <span class="fa fa-location-arrow"><?php echo smartyTranslate(array('s'=>' Mis Direcciones'),$_smarty_tpl);?>
</span>
            </a>
        </li>
        <li class="col-sm-6">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('identity',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
">
                <span class="fa fa-user"><?php echo smartyTranslate(array('s'=>' Mi información Personal'),$_smarty_tpl);?>
</span>
            </a>
        </li>
    </ul>
</div>
<?php if ($_smarty_tpl->tpl_vars['voucherAllowed']->value||isset($_smarty_tpl->tpl_vars['HOOK_CUSTOMER_ACCOUNT']->value)&&$_smarty_tpl->tpl_vars['HOOK_CUSTOMER_ACCOUNT']->value!='') {?>
        <ul class="myaccount-link-list">
            <?php if ($_smarty_tpl->tpl_vars['voucherAllowed']->value) {?>
                <li class="col-sm-6 menu-tienda"">
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('discount',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
">
                        <span><?php echo smartyTranslate(array('s'=>'My vouchers'),$_smarty_tpl);?>
</span>
                    </a>
                </li>
            <?php }?>
            <?php echo $_smarty_tpl->tpl_vars['HOOK_CUSTOMER_ACCOUNT']->value;?>

        </ul>
    </div>
<?php }?>
</div>
<ul class="footer_links clearfix">
    <li class="col-sm-12 btn-inicio-mi-tienda">
        <a class="btn btn-default button button-small" href="<?php if (isset($_smarty_tpl->tpl_vars['force_ssl']->value)&&$_smarty_tpl->tpl_vars['force_ssl']->value) {?><?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
<?php }?>" title="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
">
            <span><?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</span>
        </a>
    </li>
</ul>
<?php }} ?>
