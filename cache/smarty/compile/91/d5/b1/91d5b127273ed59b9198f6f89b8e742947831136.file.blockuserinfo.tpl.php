<?php /* Smarty version Smarty-3.1.19, created on 2018-04-03 14:29:31
         compiled from "C:\wamp64\www\ps16_automasterstore\themes\jms_deermarket\modules\blockuserinfo\blockuserinfo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17817963125ac3b9fb25fa85-74904770%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '91d5b127273ed59b9198f6f89b8e742947831136' => 
    array (
      0 => 'C:\\wamp64\\www\\ps16_automasterstore\\themes\\jms_deermarket\\modules\\blockuserinfo\\blockuserinfo.tpl',
      1 => 1519140191,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17817963125ac3b9fb25fa85-74904770',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'logged' => 0,
    'link' => 0,
    'cookie' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac3b9fb2784b4_75228079',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac3b9fb2784b4_75228079')) {function content_5ac3b9fb2784b4_75228079($_smarty_tpl) {?>

<!-- Block user information module HEADER -->
	<div class="btn-group compact-hidden user-info">
		<?php if ($_smarty_tpl->tpl_vars['logged']->value) {?>
			<a class="btn-xs dropdown-toggle login account" data-toggle="dropdown" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
					<span class="text">
						<?php echo $_smarty_tpl->tpl_vars['cookie']->value->customer_firstname;?>

<i aria-hidden="true" class="fa fa-angle-down"></i>
					</span>
				</a>
				<ul role="menu" class="dropdown-menu">
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" class="account" rel="nofollow">
							<?php echo smartyTranslate(array('s'=>'Mi Cuenta','mod'=>'blockuserinfo'),$_smarty_tpl);?>

						</a>
					</li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('compare',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Compare page','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
							<?php echo smartyTranslate(array('s'=>'Comparación','mod'=>'blockuserinfo'),$_smarty_tpl);?>

						</a>
					</li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order',true);?>
" title="<?php echo smartyTranslate(array('s'=>'View my customer account','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
							<?php echo smartyTranslate(array('s'=>'Pagar','mod'=>'blockuserinfo'),$_smarty_tpl);?>

						</a>
					</li>
					<?php if ($_smarty_tpl->tpl_vars['logged']->value) {?>
						<li>
							<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('index',true,null,"mylogout");?>
" title="<?php echo smartyTranslate(array('s'=>'Log me out','mod'=>'blockuserinfo'),$_smarty_tpl);?>
" rel="nofollow">
								<?php echo smartyTranslate(array('s'=>'Salir','mod'=>'blockuserinfo'),$_smarty_tpl);?>

							</a>
						</li>
					<?php }?>
				</ul>
		<?php } else { ?>
			<a title="Login & Register" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true);?>
" rel="nofollow" class="btn-xs">
				<span class="text">
					<?php echo smartyTranslate(array('s'=>'Iniciar Sesión','mod'=>'blockuserinfo'),$_smarty_tpl);?>

					
				</span>
			</a>
		<?php }?>
	</div>
<?php }} ?>
