<?php /* Smarty version Smarty-3.1.19, created on 2018-04-01 12:23:52
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmsblog/views/templates/front/post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5459526615ac0f988cd6fe1-13134823%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5a7fb466287f59bbc51fd50869255a4a99b8ce73' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmsblog/views/templates/front/post.tpl',
      1 => 1519311437,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5459526615ac0f988cd6fe1-13134823',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'post' => 0,
    'jmsblog_setting' => 0,
    'catparams' => 0,
    'comments' => 0,
    'msg' => 0,
    'cerrors' => 0,
    'cerror' => 0,
    'image_baseurl' => 0,
    'comment' => 0,
    'logged' => 0,
    'customer' => 0,
    'module_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac0f988d3daf8_17612411',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac0f988d3daf8_17612411')) {function content_5ac0f988d3daf8_17612411($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/h3hued5u4248/public_html/tools/smarty/plugins/modifier.date_format.php';
?>
<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<div class="single-blog">
	<div class="blog-post">
		<?php $_smarty_tpl->tpl_vars['catparams'] = new Smarty_variable(array('category_id'=>$_smarty_tpl->tpl_vars['post']->value['category_id'],'slug'=>$_smarty_tpl->tpl_vars['post']->value['category_alias']), null, 0);?>	
		<?php if ($_smarty_tpl->tpl_vars['post']->value['link_video']&&$_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_MEDIA']) {?>
			<div class="post-video">
				<?php echo $_smarty_tpl->tpl_vars['post']->value['link_video'];?>

			</div>
		
		<?php }?>
		<h1 class="title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
</h1>
		<ul class="post-meta">
			<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_CATEGORY']) {?>
				<li>
					<span>
						<?php echo smartyTranslate(array('s'=>'En:','mod'=>'jmsblog'),$_smarty_tpl);?>
 
						<a href="<?php echo jmsblog::getPageLink('jmsblog-category',$_smarty_tpl->tpl_vars['catparams']->value);?>
">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['category_name'], ENT_QUOTES, 'UTF-8', true);?>

						</a>
					</span>
				</li>
			<?php }?>
			<li>
				<span><?php echo smarty_modifier_date_format(htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['created'], ENT_QUOTES, 'UTF-8', true),"%B %e, %Y");?>
</span>
			</li>
			<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_COMMENTS']) {?>
				<li>
					<span><?php echo count($_smarty_tpl->tpl_vars['comments']->value);?>
<?php echo smartyTranslate(array('s'=>' Comentarios','mod'=>'jmsblog'),$_smarty_tpl);?>
</span>
				</li>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_VIEWS']) {?>
				<li>
					<span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['views'], ENT_QUOTES, 'UTF-8', true);?>
 <?php echo smartyTranslate(array('s'=>'Visitas','mod'=>'jmsblog'),$_smarty_tpl);?>
</span>
				</li>
			<?php }?>
		</ul>
		<div class="post-fulltext">
			<?php echo $_smarty_tpl->tpl_vars['post']->value['fulltext'];?>
	
		</div>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_SOCIAL_SHARING']==1) {?>
		<div class="social-sharing">
		
		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "a6f949b3-864b-44c5-b0ec-4140186ad958", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		
		<span class='st_sharethis_large' displayText='ShareThis'></span>

		<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_FACEBOOK']) {?>
			<span class='st_facebook_large' displayText='Facebook'></span>
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_TWITTER']) {?>
			<span class='st_twitter_large' displayText='Tweet'></span>
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_GOOGLEPLUS']) {?>
			<span class='st_googleplus_large' displayText='Google +'></span>
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_LINKEDIN']) {?>
			<span class='st_linkedin_large' displayText='LinkedIn'></span>
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_PINTEREST']) {?>
			<span class='st_pinterest_large' displayText='Pinterest'></span>
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_SHOW_EMAIL']) {?>
			<span class='st_email_large' displayText='Email'></span>
		<?php }?>
		</div>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_COMMENT_ENABLE']) {?>	
		<div id="comments">
			<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_FACEBOOK_COMMENT']==0) {?>
				<?php if ($_smarty_tpl->tpl_vars['msg']->value==1) {?>
				<div class="success">
					<?php echo smartyTranslate(array('s'=>'Your comment submited','mod'=>'jmsblog'),$_smarty_tpl);?>
 ! <?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_AUTO_APPROVE_COMMENT']==0) {?> <?php echo smartyTranslate(array('s'=>'Please waiting approve from Admin','mod'=>'jmsblog'),$_smarty_tpl);?>
.<?php }?>
				</div>
				<?php }?>
				<?php if (count($_smarty_tpl->tpl_vars['cerrors']->value)>0) {?>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['cerror'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cerror']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cerrors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cerror']->key => $_smarty_tpl->tpl_vars['cerror']->value) {
$_smarty_tpl->tpl_vars['cerror']->_loop = true;
?>
						<li class="error"><?php echo $_smarty_tpl->tpl_vars['cerror']->value;?>
</li>
					<?php } ?>	
					</ul>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['comments']->value) {?>
					<ol class="comment-list ">
						<h1 class="page-heading"><?php echo smartyTranslate(array('s'=>'Comentarios','mod'=>'jmsblog'),$_smarty_tpl);?>
</h1>
						<?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['comments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['comment']->key;
?>
						<li>
							<article>
							<div class="left-content">
								<div class="user-image">
									<img class="attachment-widget wp-post-image img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_baseurl']->value, ENT_QUOTES, 'UTF-8', true);?>
user.png" />
								</div>
							</div>
								<div class="comment-info">
									<a class="comment-date" href="#comments">
										<time><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['comment']->value['time_add'],"%e %B %Y");?>
</time>
									</a>
									<p class="customer-name">
										<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['customer_site'], ENT_QUOTES, 'UTF-8', true);?>
" class="url"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['customer_name'], ENT_QUOTES, 'UTF-8', true);?>
</a>
									</p>
									<p class="comement-text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value['comment'], ENT_QUOTES, 'UTF-8', true);?>
</p>
								</div>
							</article>
						</li>
						<?php } ?>
					</ol>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_ALLOW_GUEST_COMMENT']||(!$_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_ALLOW_GUEST_COMMENT']&&$_smarty_tpl->tpl_vars['logged']->value)) {?>	
				<div class="commentForm">
					<form id="commentForm" enctype="multipart/form-data" method="post" action="index.php?fc=module&module=jmsblog&controller=post&post_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['post_id'], ENT_QUOTES, 'UTF-8', true);?>
&action=submitComment">
						<div class="row">
							<div class="col-sm-12">
								<h1 class="page-heading"><?php echo smartyTranslate(array('s'=>'Deja un cometario','mod'=>'jmsblog'),$_smarty_tpl);?>
</h1>
								<p class="h-info"><?php echo smartyTranslate(array('s'=>'Tu dirección de email no será publicada','mod'=>'jmsblog'),$_smarty_tpl);?>
.</p>
							</div>
						</div>	
						
							
								<div class="form-group">
									<label for="comment_name"><?php echo smartyTranslate(array('s'=>'Nombre','mod'=>'jmsblog'),$_smarty_tpl);?>
<sup class="required">*</sup></label>
									<input id="customer_name" class="form-control" name="customer_name" type="text" value="<?php echo $_smarty_tpl->tpl_vars['customer']->value['firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['customer']->value['lastname'];?>
" required />
								</div>	
							
							
								<div class="form-group">
									<label for="comment_title"><?php echo smartyTranslate(array('s'=>'Email','mod'=>'jmsblog'),$_smarty_tpl);?>
<sup class="required">*</sup></label>
									<input id="comment_title" class="form-control" name="email" type="text" value="<?php echo $_smarty_tpl->tpl_vars['customer']->value['email'];?>
" required />
								</div>
							
						
						<div class="form-group">
							<label for="comment_title"><?php echo smartyTranslate(array('s'=>'Website','mod'=>'jmsblog'),$_smarty_tpl);?>
</label>
							<input id="customer_site" class="form-control" name="customer_site" type="text" value=""/>
						</div>
						<div class="form-group">
							<label for="content"><?php echo smartyTranslate(array('s'=>'Comentario','mod'=>'jmsblog'),$_smarty_tpl);?>
<sup class="required">*</sup></label>
							<textarea id="comment" class="form-control" name="comment" rows="2" required></textarea>
						</div>
						<div id="new_comment_form_footer">
							<input id="item_id_comment_send" name="post_id" type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['post_id'], ENT_QUOTES, 'UTF-8', true);?>
" />
							<input id="item_id_comment_reply" name="post_id_comment_reply" type="hidden" value="" />
							<p class="">
								<button id="submitComment" class="btn btn-default" name="submitComment" type="submit"><?php echo smartyTranslate(array('s'=>'Comentario','mod'=>'jmsblog'),$_smarty_tpl);?>
</button>
							</p>
						</div>
					</form>
					<script>
					$("#commentForm").validate({
					  rules: {		
						customer_name: "required",		
						email: {
						  required: true,
						  email: true
						}
					  }
					});
					</script>
				</div>
				<?php }?>
				<?php if (!$_smarty_tpl->tpl_vars['jmsblog_setting']->value['JMSBLOG_ALLOW_GUEST_COMMENT']&&!$_smarty_tpl->tpl_vars['logged']->value) {?>
					<?php echo smartyTranslate(array('s'=>'Porfavor inicia sesión para comentar','mod'=>'jmsblog'),$_smarty_tpl);?>

				<?php }?>
			<?php } else { ?>
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['module_dir']->value)."comment_facebook.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
		
			<?php }?>
		</div>
	<?php }?>

</div><?php }} ?>
