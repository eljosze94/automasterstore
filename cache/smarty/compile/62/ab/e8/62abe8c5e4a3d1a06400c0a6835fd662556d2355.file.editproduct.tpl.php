<?php /* Smarty version Smarty-3.1.19, created on 2018-03-30 19:59:42
         compiled from "/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/editproduct.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1630294745abec15ecbdcb7-12839187%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62abe8c5e4a3d1a06400c0a6835fd662556d2355' => 
    array (
      0 => '/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/front/editproduct.tpl',
      1 => 1510142766,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1630294745abec15ecbdcb7-12839187',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'show_menu_top' => 0,
    'ps_version' => 0,
    'link' => 0,
    'navigationPipe' => 0,
    'id_product' => 0,
    'id_lang' => 0,
    'product' => 0,
    'show_menu_options' => 0,
    'errors' => 0,
    'form_edit' => 0,
    'languages' => 0,
    'language' => 0,
    'img_lang_dir' => 0,
    'show_tabs' => 0,
    'show_price' => 0,
    'show_tax' => 0,
    'show_meta_keywords' => 0,
    'show_meta_title' => 0,
    'show_meta_desc' => 0,
    'show_categories' => 0,
    'show_suppliers' => 0,
    'show_manufacturers' => 0,
    'show_width' => 0,
    'show_height' => 0,
    'show_depth' => 0,
    'show_weight' => 0,
    'show_shipping_product' => 0,
    'show_quantity' => 0,
    'show_minimal_quantity' => 0,
    'show_available_now' => 0,
    'show_available_later' => 0,
    'show_available_date' => 0,
    'show_attributes' => 0,
    'show_images' => 0,
    'show_features' => 0,
    'show_virtual' => 0,
    'show_reference' => 0,
    'show_isbn' => 0,
    'show_ean13' => 0,
    'show_upc' => 0,
    'show_available_order' => 0,
    'show_show_price' => 0,
    'show_online_only' => 0,
    'show_condition' => 0,
    'show_pcondition' => 0,
    'show_desc_short' => 0,
    'show_desc' => 0,
    'seller_commission' => 0,
    'fixed_commission' => 0,
    'show_wholesale_price' => 0,
    'sign' => 0,
    'show_unit_price' => 0,
    'show_offer_price' => 0,
    'specific_price' => 0,
    'final_price_tax_excl' => 0,
    'taxes' => 0,
    'tax' => 0,
    'final_price_tax_incl' => 0,
    'show_commission' => 0,
    'tax_commission' => 0,
    'show_on_sale' => 0,
    'show_link_rewrite' => 0,
    'categoryTree' => 0,
    'categories_string' => 0,
    'categories_selected' => 0,
    'category' => 0,
    'suppliers' => 0,
    'supplier' => 0,
    'show_new_suppliers' => 0,
    'manufacturers' => 0,
    'manufacturer' => 0,
    'show_new_manufacturers' => 0,
    'carriers' => 0,
    'carrier' => 0,
    'show_manage_carriers' => 0,
    'real_quantity' => 0,
    'show_availability' => 0,
    'out_of_stock' => 0,
    'attribute_groups' => 0,
    'ag' => 0,
    'first_options' => 0,
    'option' => 0,
    'attributes' => 0,
    'attribute' => 0,
    'max_images' => 0,
    'max_dimensions' => 0,
    'images' => 0,
    'image' => 0,
    'imageIds' => 0,
    'imageType' => 0,
    'imageTitle' => 0,
    'foo' => 0,
    'image_not_available' => 0,
    'features' => 0,
    'feature' => 0,
    'val' => 0,
    'is_virtual' => 0,
    'display_filename' => 0,
    'filename' => 0,
    'modules_dir' => 0,
    'attachment_maximun_size' => 0,
    'virtual_product_name' => 0,
    'virtual_product_nb_downloable' => 0,
    'virtual_product_expiration_date' => 0,
    'virtual_product_nb_days' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abec15f1ab010_34613851',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abec15f1ab010_34613851')) {function content_5abec15f1ab010_34613851($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['show_menu_top']->value==1) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("./selleraccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
    <div class="row">
        <div class="col-md-12 jmarketplace_breadcrumb">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
                <?php echo smartyTranslate(array('s'=>'Your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </a>
            <span class="navigation-pipe">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

            </span>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </a>
            <span class="navigation-pipe">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

            </span>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <?php echo smartyTranslate(array('s'=>'Your products','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </a>
            <span class="navigation-pipe">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

            </span>
            <span class="navigation_page">
                <?php echo smartyTranslate(array('s'=>'Edit product','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php if (isset($_smarty_tpl->tpl_vars['id_product']->value)&&$_smarty_tpl->tpl_vars['id_product']->value) {?>"<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name[$_smarty_tpl->tpl_vars['id_lang']->value], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>
            </span>
        </div>
    </div>
<?php } else { ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo smartyTranslate(array('s'=>'Your account','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </a>
        <span class="navigation-pipe">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

        </span>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </a>
        <span class="navigation-pipe">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

        </span>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','sellerproducts',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo smartyTranslate(array('s'=>'Your products','mod'=>'jmarketplace'),$_smarty_tpl);?>

        </a>
        <span class="navigation-pipe">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['navigationPipe']->value, ENT_QUOTES, 'UTF-8', true);?>

        </span>
        <span class="navigation_page">
            <?php echo smartyTranslate(array('s'=>'Edit product','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php if (isset($_smarty_tpl->tpl_vars['id_product']->value)&&$_smarty_tpl->tpl_vars['id_product']->value) {?>"<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name[$_smarty_tpl->tpl_vars['id_lang']->value], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>
        </span>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>  
<?php }?>

<div class="row">
    <div class="column col-xs-12 col-sm-12 col-md-3"<?php if ($_smarty_tpl->tpl_vars['show_menu_options']->value==0) {?> style="display:none;"<?php }?>>
        <?php echo $_smarty_tpl->getSubTemplate ("./sellermenu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
    
    <div class="col-xs-12 <?php if ($_smarty_tpl->tpl_vars['show_menu_options']->value==1) {?>col-sm-12 col-md-9<?php }?>">
        <div class="box">
            <h1 class="page-subheading"><?php echo smartyTranslate(array('s'=>'Edit product','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php if (isset($_smarty_tpl->tpl_vars['id_product']->value)&&$_smarty_tpl->tpl_vars['id_product']->value) {?>"<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name[$_smarty_tpl->tpl_vars['id_lang']->value], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?></h1>
            <?php if (isset($_smarty_tpl->tpl_vars['errors']->value)&&$_smarty_tpl->tpl_vars['errors']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['ps_version']->value!='1.7') {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                <?php }?>
            <?php }?>
            <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_edit']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std" enctype="multipart/form-data">
                <input type="hidden" name="id_product" id="id_product" value="<?php echo intval($_smarty_tpl->tpl_vars['product']->value->id);?>
" />
                <?php if (count($_smarty_tpl->tpl_vars['languages']->value)>1) {?>
                    <div class="fixed">
                        <div><?php echo smartyTranslate(array('s'=>'Language','mod'=>'jmarketplace'),$_smarty_tpl);?>
</div>
                        <div class="lang_selector">
                            <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                    <span<?php if ($_smarty_tpl->tpl_vars['id_lang']->value==$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> class="selected"<?php }?> title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
"  data="<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8', true);?>
</span>
                                <?php } else { ?>
                                    <img class="flag<?php if ($_smarty_tpl->tpl_vars['id_lang']->value==$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> selected<?php }?>" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_lang_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
.jpg" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" data="<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" />
                                <?php }?>
                            <?php } ?>
                        </div> 
                    </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['show_tabs']->value==1) {?>
                    <div id="jmarketplace-tabs" class="row">
                        <div class="col-lg-3">
                            <div class="list-group">
                                <a href="#information" class="list-group-item active" data-toggle="tab"><?php echo smartyTranslate(array('s'=>'Information','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php if ($_smarty_tpl->tpl_vars['show_price']->value==1||$_smarty_tpl->tpl_vars['show_tax']->value==1) {?>
                                    <a href="#prices" class="list-group-item" data-toggle="tab"><?php echo smartyTranslate(array('s'=>'Price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_meta_keywords']->value==1||$_smarty_tpl->tpl_vars['show_meta_title']->value==1||$_smarty_tpl->tpl_vars['show_meta_desc']->value==1) {?>
                                    <a href="#seo" class="list-group-item" data-toggle="tab"><?php echo smartyTranslate(array('s'=>'SEO','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_categories']->value==1||$_smarty_tpl->tpl_vars['show_suppliers']->value==1||$_smarty_tpl->tpl_vars['show_manufacturers']->value==1) {?>
                                    <a href="#associations" class="list-group-item" data-toggle="tab"><?php echo smartyTranslate(array('s'=>'Associations','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>
                                <?php if (($_smarty_tpl->tpl_vars['show_width']->value==1||$_smarty_tpl->tpl_vars['show_height']->value==1||$_smarty_tpl->tpl_vars['show_depth']->value==1||$_smarty_tpl->tpl_vars['show_weight']->value==1||$_smarty_tpl->tpl_vars['show_shipping_product']->value==1)) {?>
                                    <a href="#shipping" class="list-group-item" data-toggle="tab" id="shipping_tab"<?php if ($_smarty_tpl->tpl_vars['product']->value->is_virtual==1) {?> style="display:none;"<?php }?>><?php echo smartyTranslate(array('s'=>'Shipping','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?> 
                                <?php if ($_smarty_tpl->tpl_vars['show_quantity']->value==1||$_smarty_tpl->tpl_vars['show_minimal_quantity']->value==1||$_smarty_tpl->tpl_vars['show_available_now']->value==1||$_smarty_tpl->tpl_vars['show_available_later']->value==1||$_smarty_tpl->tpl_vars['show_available_date']->value==1) {?>
                                    <a href="#quantities" class="list-group-item" data-toggle="tab"><?php echo smartyTranslate(array('s'=>'Quantities','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_attributes']->value==1) {?>
                                    <a href="#combinations" class="list-group-item" data-toggle="tab" id="combinations_tab"<?php if ($_smarty_tpl->tpl_vars['show_attributes']->value==1&&$_smarty_tpl->tpl_vars['product']->value->is_virtual==1) {?> style="display:none;"<?php }?>><?php echo smartyTranslate(array('s'=>'Combinations','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_images']->value==1) {?>
                                    <a href="#images" class="list-group-item" data-toggle="tab"><?php echo smartyTranslate(array('s'=>'Images','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_features']->value==1) {?>
                                    <a href="#features" class="list-group-item" data-toggle="tab"><?php echo smartyTranslate(array('s'=>'Features','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_virtual']->value==1) {?>
                                    <a href="#virtualproduct" class="list-group-item" data-toggle="tab" id="virtual_product_tab"<?php if ($_smarty_tpl->tpl_vars['product']->value->is_virtual==0) {?> style="display:none;"<?php }?>><?php echo smartyTranslate(array('s'=>'Virtual product','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                <?php }?>          
                                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceFormAddProductTab'),$_smarty_tpl);?>

                            </div>
                        </div>
                        <div class="tab-content col-lg-9">
                            <div class="tab-pane active panel" id="information">
                                <?php if ($_smarty_tpl->tpl_vars['show_virtual']->value==1) {?>
                                    <div class="form-group">
                                        <label class="control-label"><?php echo smartyTranslate(array('s'=>'Type','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <div>
                                            <div class="radio">
                                                <label for="simple_product">
                                                    <input type="radio" checked="checked" value="0" id="simple_product" name="type_product">
                                                    <?php echo smartyTranslate(array('s'=>'Standard product','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label for="virtual_product">
                                                    <input type="radio" value="2" id="virtual_product" name="type_product"<?php if ($_smarty_tpl->tpl_vars['product']->value->is_virtual==1) {?> checked="checked"<?php }?>>
                                                    <?php echo smartyTranslate(array('s'=>'Virtual product (services, booking, downloadable products, etc.)','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                </label>
                                            </div>
                                        </div>    
                                    </div> 
                                <?php }?>
                                <div class="required form-group">
                                    <label for="product_name" class="required"><?php echo smartyTranslate(array('s'=>'Product name','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                        <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control product_name input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="name_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="name_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp7=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name[$_tmp7], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                                    <?php } ?> 
                                </div>

                                <?php if ($_smarty_tpl->tpl_vars['show_reference']->value==1) {?>
                                    <div class="form-group">
                                        <label for="reference">
                                            <?php echo smartyTranslate(array('s'=>'Reference','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" type="text" name="reference" id="reference" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->reference, ENT_QUOTES, 'UTF-8', true);?>
"  maxlength="32" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_isbn']->value==1) {?>
                                    <div class="form-group">
                                        <label for="isbn">
                                            <?php echo smartyTranslate(array('s'=>'ISBN','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" type="text" name="isbn" id="isbn" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->isbn, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="32" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_ean13']->value==1) {?>
                                    <div class="form-group">
                                        <label for="ean13">
                                            <?php echo smartyTranslate(array('s'=>'Ean13','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" type="text" name="ean13" id="ean13" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->ean13, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="13" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_upc']->value==1) {?>
                                    <div class="form-group">
                                        <label for="upc">
                                            <?php echo smartyTranslate(array('s'=>'UPC','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" data-validate="isName" type="text" name="upc" id="upc" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->upc, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="12" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_available_order']->value==1||$_smarty_tpl->tpl_vars['show_show_price']->value==1||$_smarty_tpl->tpl_vars['show_online_only']->value==1) {?>
                                    <label for="options"><?php echo smartyTranslate(array('s'=>'Options','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <?php if ($_smarty_tpl->tpl_vars['show_available_order']->value==1) {?>
                                        <div class="form-group">
                                            <p class="checkbox">
                                                <input type="checkbox" value="1" id="available_for_order" name="available_for_order"<?php if ($_smarty_tpl->tpl_vars['product']->value->available_for_order==1) {?> checked="checked"<?php }?>>
                                                <label for="available_for_order"><?php echo smartyTranslate(array('s'=>'Available for order','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            </p>
                                        </div>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['show_show_price']->value==1) {?>
                                        <div class="form-group">
                                            <p class="checkbox">
                                                <input type="checkbox" value="1" id="show_product_price" name="show_product_price"<?php if ($_smarty_tpl->tpl_vars['product']->value->show_price==1) {?> checked="checked"<?php }?>>
                                                <label for="show_price"><?php echo smartyTranslate(array('s'=>'Show price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            </p>
                                        </div>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['show_online_only']->value==1) {?>
                                        <div class="form-group">
                                            <p class="checkbox">
                                                <input type="checkbox" value="1" id="online_only" name="online_only"<?php if ($_smarty_tpl->tpl_vars['product']->value->online_only==1) {?> checked="checked"<?php }?>>
                                                <label for="online_only"><?php echo smartyTranslate(array('s'=>'Online only (not sold in your retail store)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            </p>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_condition']->value==1) {?>
                                    <div class="form-group">
                                        <label><?php echo smartyTranslate(array('s'=>'Condition','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <select id="condition" name="condition">
                                            <option<?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='new') {?> selected="selected"<?php }?> value="new"><?php echo smartyTranslate(array('s'=>'New','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                            <option<?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='used') {?> selected="selected"<?php }?> value="used"><?php echo smartyTranslate(array('s'=>'Used','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                            <option<?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='refurbished') {?> selected="selected"<?php }?> value="refurbished"><?php echo smartyTranslate(array('s'=>'Refurbished','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                        </select>
                                    </div>
                                    <?php if ($_smarty_tpl->tpl_vars['show_pcondition']->value==1) {?>
                                        <div class="form-group">
                                            <p class="checkbox">
                                                <input type="checkbox" value="1" id="show_product_condition" name="show_product_condition"<?php if ($_smarty_tpl->tpl_vars['product']->value->show_condition==1) {?> checked="checked"<?php }?>>
                                                <label for="show_product_condition"><?php echo smartyTranslate(array('s'=>'Show condition on the product page','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            </p>
                                        </div>
                                    <?php }?>     
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_desc_short']->value==1) {?>
                                    <div class="form-group">
                                        <label for="short_description"><?php echo smartyTranslate(array('s'=>'Short description','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <div id="short_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" class="short_description input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?>>
                                                <textarea name="short_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" cols="40" rows="7"><?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp8=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['product']->value->description_short[$_tmp8];?>
</textarea> 
                                            </div>
                                        <?php } ?> 
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_desc']->value==1) {?>
                                    <div class="form-group">
                                        <label for="description"><?php echo smartyTranslate(array('s'=>'Description','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <div id="description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" class="description input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?>>
                                                <textarea name="description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" cols="40" rows="7"><?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp9=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['product']->value->description[$_tmp9];?>
</textarea> 
                                            </div>
                                        <?php } ?> 
                                    </div>
                                <?php }?>
                            </div>
                            <div class="tab-pane panel" id="prices">
                                <?php if ($_smarty_tpl->tpl_vars['show_price']->value==1) {?>
                                    <input type="hidden" name="seller_commission" id="seller_commission" value="<?php echo floatval($_smarty_tpl->tpl_vars['seller_commission']->value);?>
" />
                                    <input type="hidden" name="fixed_commission" id="fixed_commission" value="<?php echo floatval($_smarty_tpl->tpl_vars['fixed_commission']->value);?>
" />
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['show_wholesale_price']->value==1) {?>    
                                        <div class="required form-group">
                                            <label for="wholesale_price"><?php echo smartyTranslate(array('s'=>'Wholesale price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            <div class="input-group">
                                                <input class="form-control" data-validate="isNumber" type="text" name="wholesale_price" id="wholesale_price" value="<?php echo floatval($_smarty_tpl->tpl_vars['product']->value->wholesale_price);?>
" maxlength="10" />
                                                <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                            </div>
                                            <p class="help-block"><?php echo smartyTranslate(array('s'=>'The cost price is the price you paid for the product. Do not include the tax. It should be lower than the net sales price: the difference between the two will be your margin.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                        </div>
                                    <?php }?>
                                    <div class="form-group">
                                        <label for="price"><?php echo smartyTranslate(array('s'=>'Price (tax excl.)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <div class="input-group">
                                            <input class="form-control" data-validate="isNumber" type="text" name="price" id="price" value="<?php echo floatval($_smarty_tpl->tpl_vars['product']->value->price);?>
" maxlength="10" />
                                            <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                        </div>
                                    </div>
                                        
                                    <?php if ($_smarty_tpl->tpl_vars['show_unit_price']->value==1) {?>  
                                        <div class="row">
                                            <div class="required form-group col-md-6">
                                                <label for="unit_price"><?php echo smartyTranslate(array('s'=>'Unit price (tax excl.)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                                <div class="input-group">
                                                    <input class="form-control" data-validate="isNumber" type="text" name="unit_price" id="unit_price" value="<?php if ($_smarty_tpl->tpl_vars['product']->value->unit_price_ratio>0) {?><?php echo floatval(Tools::ps_round(($_smarty_tpl->tpl_vars['product']->value->price/$_smarty_tpl->tpl_vars['product']->value->unit_price_ratio),2));?>
<?php } else { ?>0<?php }?>"  maxlength="10" />
                                                    <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                                </div>
                                                <p class="help-block"><?php echo smartyTranslate(array('s'=>'If your country\'s pricing laws or regulations require mandatory informations about the base price of a unit, fill in the base price here (for example, price per kg, per liter, per meter).','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="required form-group col-md-6">
                                                <label for="unit_price"><?php echo smartyTranslate(array('s'=>'Unity','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                                <div class="input-group">
                                                    <input class="form-control" type="text" name="unity" id="unity"<?php if ($_smarty_tpl->tpl_vars['product']->value->unity!='') {?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->unity, ENT_QUOTES, 'UTF-8', true);?>
"<?php } else { ?> placeholder="<?php echo smartyTranslate(array('s'=>'Per kilo, per litre','mod'=>'jmarketplace'),$_smarty_tpl);?>
"<?php }?> maxlength="255" />
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>    
                                        
                                    <?php if ($_smarty_tpl->tpl_vars['show_offer_price']->value==1) {?>    
                                        <div class="required form-group">
                                            <label for="offer_price"><?php echo smartyTranslate(array('s'=>'Offer price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            <div class="input-group">
                                                <input class="form-control" data-validate="isNumber" type="text" name="specific_price" id="specific_price" value="<?php if (isset($_smarty_tpl->tpl_vars['specific_price']->value)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['final_price_tax_excl']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>0<?php }?>" maxlength="10" />
                                                <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                            </div>
                                                <p class="help-block"><small><?php echo smartyTranslate(array('s'=>'Leave 0 if no offer. The offer price must be lower than the price.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</small></p>
                                        </div>  
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['show_tax']->value==1) {?>
                                        <div class="form-group">
                                            <label><?php echo smartyTranslate(array('s'=>'Tax','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            <select id="id_tax" name="id_tax">
                                                <option value="0"><?php echo smartyTranslate(array('s'=>'no tax','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                                <?php  $_smarty_tpl->tpl_vars['tax'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['taxes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax']->key => $_smarty_tpl->tpl_vars['tax']->value) {
$_smarty_tpl->tpl_vars['tax']->_loop = true;
?>
                                                    <option value="<?php echo intval($_smarty_tpl->tpl_vars['tax']->value['id_tax_rules_group']);?>
" data="<?php echo floatval($_smarty_tpl->tpl_vars['tax']->value['rate']);?>
"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->id_tax_rules_group)&&$_smarty_tpl->tpl_vars['product']->value->id_tax_rules_group==$_smarty_tpl->tpl_vars['tax']->value['id_tax_rules_group']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <?php }?>
                                    <div class="form-group"<?php if ($_smarty_tpl->tpl_vars['show_tax']->value==0) {?>  style="display:none;"<?php }?>>
                                        <label for="price"><?php echo smartyTranslate(array('s'=>'Price (tax incl.)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <div class="input-group">
                                            <input class="form-control" data-validate="isNumber" type="text" name="price_tax_incl" id="price_tax_incl" value="<?php echo floatval($_smarty_tpl->tpl_vars['final_price_tax_incl']->value);?>
" readonly="readonly" />
                                            <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                        </div>
                                    </div>
                                    <?php if ($_smarty_tpl->tpl_vars['show_commission']->value==1) {?>    
                                        <div class="form-group">
                                            <label for="commission"><?php echo smartyTranslate(array('s'=>'Commission for you','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            <div class="input-group">
                                                <input class="form-control" data-validate="isNumber" type="text" name="commission" id="commission" value="<?php if ($_smarty_tpl->tpl_vars['tax_commission']->value==1) {?><?php echo (($_smarty_tpl->tpl_vars['final_price_tax_incl']->value*$_smarty_tpl->tpl_vars['seller_commission']->value)/100)-floatval($_smarty_tpl->tpl_vars['fixed_commission']->value);?>
<?php } else { ?><?php echo (($_smarty_tpl->tpl_vars['final_price_tax_excl']->value*$_smarty_tpl->tpl_vars['seller_commission']->value)/100)-floatval($_smarty_tpl->tpl_vars['fixed_commission']->value);?>
<?php }?>" readonly="readonly" />
                                                <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                            </div>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_on_sale']->value==1) {?>
                                    <div class="form-group">
                                        <p class="checkbox">
                                            <input type="checkbox" value="1" id="on_sale" name="on_sale"<?php if ($_smarty_tpl->tpl_vars['product']->value->on_sale==1) {?> checked="checked"<?php }?>>
                                            <label for="on_sale"><?php echo smartyTranslate(array('s'=>'Display the "on sale" icon on the product page, and in the text found within the product listing.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        </p>
                                    </div>
                                <?php }?>
                            </div>
                            <div class="tab-pane panel" id="seo">
                                <h4><?php echo smartyTranslate(array('s'=>'Search Engine Optimization','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                <?php if ($_smarty_tpl->tpl_vars['show_meta_keywords']->value==1) {?>
                                    <div class="form-group">
                                        <label for="meta_keywords"><?php echo smartyTranslate(array('s'=>'Meta keywords (Every keyword separate by coma, ex. key1, key2, key3...)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control meta_keywords input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="meta_keywords_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="meta_keywords_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp10=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->meta_keywords[$_tmp10], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="255" />
                                        <?php } ?> 
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_meta_title']->value==1) {?>
                                    <div class="form-group">
                                        <label for="meta_title"><?php echo smartyTranslate(array('s'=>'Meta title','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control meta_title input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="meta_title_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="meta_title_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp11=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->meta_title[$_tmp11], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                                        <?php } ?> 
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_meta_desc']->value==1) {?>
                                    <div class="form-group">
                                        <label for="meta_description"><?php echo smartyTranslate(array('s'=>'Meta description','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control meta_description input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="meta_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="meta_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp12=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->meta_description[$_tmp12], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="255" />
                                        <?php } ?> 
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_link_rewrite']->value==1) {?>
                                    <div class="form-group">
                                        <label for="link_rewrite"><?php echo smartyTranslate(array('s'=>'Friendly URL','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control link_rewrite input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="link_rewrite_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="link_rewrite_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp13=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->link_rewrite[$_tmp13], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                                        <?php } ?> 
                                    </div>
                                <?php }?>
                            </div>
                            <div class="tab-pane panel" id="associations">
                                <?php if ($_smarty_tpl->tpl_vars['show_categories']->value==1) {?>
                                    <div class="form-group">
                                        <div class="category_search_block">
                                            <label><?php echo smartyTranslate(array('s'=>'Categories','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            <input name="search_tree_category" id="search_tree_category" type="text" class="search_category" placeholder="<?php echo smartyTranslate(array('s'=>'Search category','mod'=>'jmarketplace'),$_smarty_tpl);?>
" autocomplete="off">
                                            <div id="category_suggestions"></div>    
                                            <div class="checkok"></div>    
                                        </div>
                                        <?php echo $_smarty_tpl->tpl_vars['categoryTree']->value;?>
 
                                    </div>
                                    <p><?php echo smartyTranslate(array('s'=>'This product is associated with','mod'=>'jmarketplace'),$_smarty_tpl);?>
:</strong> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['categories_string']->value, ENT_QUOTES, 'UTF-8', true);?>
</p>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_categories']->value==1) {?>
                                    <div id="category_default" class="form-group">
                                        <label><?php echo smartyTranslate(array('s'=>'Category default','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <select id="id_category_default" name="id_category_default">
                                            <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories_selected']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                                                <option value="<?php echo intval($_smarty_tpl->tpl_vars['category']->value['id_category']);?>
"<?php if (($_smarty_tpl->tpl_vars['category']->value['id_category']==$_smarty_tpl->tpl_vars['product']->value->id_category_default)) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_suppliers']->value==1) {?>
                                    <div class="form-group">
                                        <label><?php echo smartyTranslate(array('s'=>'Supplier','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <select name="id_supplier">
                                            <option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                            <?php  $_smarty_tpl->tpl_vars['supplier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['supplier']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['suppliers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['supplier']->key => $_smarty_tpl->tpl_vars['supplier']->value) {
$_smarty_tpl->tpl_vars['supplier']->_loop = true;
?>
                                                <option value="<?php echo intval($_smarty_tpl->tpl_vars['supplier']->value['id_supplier']);?>
"<?php if ($_smarty_tpl->tpl_vars['product']->value->id_supplier==$_smarty_tpl->tpl_vars['supplier']->value['id_supplier']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['supplier']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_new_suppliers']->value==1) {?>
                                    <div class="form-group"><a id="open_new_supplier" href="#"><?php echo smartyTranslate(array('s'=>'Add new supplier','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></div>
                                    <div id="content_new_supplier" class="form-group" style="display:none;">
                                        <label><?php echo smartyTranslate(array('s'=>'New supplier','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" data-validate="isName" type="text" name="new_supplier" id="new_supplier" maxlength="64" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_manufacturers']->value==1) {?>
                                    <div class="form-group">
                                        <label><?php echo smartyTranslate(array('s'=>'Manufacturer','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <select name="id_manufacturer">
                                            <option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                            <?php  $_smarty_tpl->tpl_vars['manufacturer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['manufacturer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['manufacturers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['manufacturer']->key => $_smarty_tpl->tpl_vars['manufacturer']->value) {
$_smarty_tpl->tpl_vars['manufacturer']->_loop = true;
?>
                                                <option value="<?php echo intval($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer']);?>
"<?php if ($_smarty_tpl->tpl_vars['product']->value->id_manufacturer==$_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_new_manufacturers']->value==1) {?>
                                    <div class="form-group"><a id="open_new_manufacturer" href="#"><?php echo smartyTranslate(array('s'=>'Add new manufacturer','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></div>
                                    <div id="content_new_manufacturer" class="form-group" style="display:none;">
                                        <label><?php echo smartyTranslate(array('s'=>'New manufacturer','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" data-validate="isName" type="text" name="new_manufacturer" id="new_manufacturer" maxlength="64" />
                                    </div>
                                <?php }?>
                            </div>
                            <?php if (!$_smarty_tpl->tpl_vars['product']->value->is_virtual) {?>
                                <div class="tab-pane panel" id="shipping">
                                    <?php if ($_smarty_tpl->tpl_vars['show_width']->value==1) {?>
                                        <div class="form-group">
                                            <label for="width">
                                                <?php echo smartyTranslate(array('s'=>'Width (cm)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                            </label>
                                            <input class="form-control" data-validate="isNumber" type="text" name="width" id="width" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->width, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                        </div>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['show_height']->value==1) {?>
                                        <div class="form-group">
                                            <label for="height">
                                                <?php echo smartyTranslate(array('s'=>'Height (cm)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                            </label>
                                            <input class="form-control" data-validate="isNumber" type="text" name="height" id="height" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->height, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                        </div>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['show_depth']->value==1) {?>
                                        <div class="form-group">
                                            <label for="depth">
                                                <?php echo smartyTranslate(array('s'=>'Depth (cm)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                            </label>
                                            <input class="form-control" data-validate="isNumber" type="text" name="depth" id="depth" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->depth, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                        </div>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['show_weight']->value==1) {?>
                                        <div class="form-group">
                                            <label for=weight">
                                                <?php echo smartyTranslate(array('s'=>'Weight (kg)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                            </label>
                                            <input class="form-control" data-validate="isNumber" type="text" name="weight" id="weight" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->weight, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                        </div>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['show_shipping_product']->value==1) {?>                   
                                        <h4><?php echo smartyTranslate(array('s'=>'Select delivery method','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                        <?php if (isset($_smarty_tpl->tpl_vars['carriers']->value)&&$_smarty_tpl->tpl_vars['carriers']->value) {?>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th><?php echo smartyTranslate(array('s'=>'Delivery service name','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                                            <th><?php echo smartyTranslate(array('s'=>'Delivery speed','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                                            <th><?php echo smartyTranslate(array('s'=>'Tick to enable for this product','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php  $_smarty_tpl->tpl_vars['carrier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carrier']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['carriers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carrier']->key => $_smarty_tpl->tpl_vars['carrier']->value) {
$_smarty_tpl->tpl_vars['carrier']->_loop = true;
?>
                                                            <tr>
                                                                <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</td>
                                                                <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['delay'], ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['carrier']->value['is_free']==1) {?> - <?php echo smartyTranslate(array('s'=>'Shipping free!','mod'=>'jmarketplace'),$_smarty_tpl);?>
<?php }?></td>
                                                                <td>
                                                                    <input type="checkbox" name="carriers[]" value="<?php echo intval($_smarty_tpl->tpl_vars['carrier']->value['id_reference']);?>
"<?php if ($_smarty_tpl->tpl_vars['carrier']->value['checked']==1) {?> checked="checked"<?php }?> />
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-group">
                                                <label for="additional_shipping_cost">
                                                    <?php echo smartyTranslate(array('s'=>'Additional shipping cost','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                                </label>
                                                <input class="form-control" type="text" name="additional_shipping_cost" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->additional_shipping_cost, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                            </div>
                                        <?php } else { ?>
                                             <?php if ($_smarty_tpl->tpl_vars['show_manage_carriers']->value==1) {?>
                                                 <p>
                                                     <?php echo smartyTranslate(array('s'=>'First you must create at least one carrier.','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                                     <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','addcarrier',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Create your first carrier now','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                                 </p>
                                             <?php }?>
                                        <?php }?>  
                                    <?php }?>
                                </div>
                            <?php }?>
                            <div class="tab-pane panel" id="quantities">
                                <?php if ($_smarty_tpl->tpl_vars['show_quantity']->value==1) {?>
                                    <div class="form-group">
                                        <label for="quantity">
                                            <?php echo smartyTranslate(array('s'=>'Quantity','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" data-validate="isNumber" type="text" name="quantity" id="quantity" value="<?php echo intval($_smarty_tpl->tpl_vars['real_quantity']->value);?>
" maxlength="10" />
                                    </div>
                                <?php }?> 
                                <?php if ($_smarty_tpl->tpl_vars['show_minimal_quantity']->value==1) {?>
                                    <div class="form-group">
                                        <label for="minimal_quantity"><?php echo smartyTranslate(array('s'=>'Minimal quantity','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" data-validate="isNumber" type="text" name="minimal_quantity" id="quantity"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->minimal_quantity)) {?> value="<?php echo intval($_smarty_tpl->tpl_vars['product']->value->minimal_quantity);?>
"<?php } else { ?> value="1"<?php }?> maxlength="10" />
                                    </div>
                                <?php }?> 
                                <?php if ($_smarty_tpl->tpl_vars['show_availability']->value==1) {?>
                                    <div class="form-group">
                                        <label class="control-label"><?php echo smartyTranslate(array('s'=>'Availability preferences (Behavior when out of stock)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <div>
                                            <div class="radio">
                                                <label for="deny_orders">
                                                    <input type="radio" value="0" id="deny_orders" name="out_of_stock"<?php if ((isset($_smarty_tpl->tpl_vars['out_of_stock']->value)&&$_smarty_tpl->tpl_vars['out_of_stock']->value==0)) {?> checked="checked"<?php }?>>
                                                    <?php echo smartyTranslate(array('s'=>'Deny orders','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label for="allow_orders">
                                                    <input type="radio" value="1" id="allow_orders" name="out_of_stock"<?php if ((isset($_smarty_tpl->tpl_vars['out_of_stock']->value)&&$_smarty_tpl->tpl_vars['out_of_stock']->value==1)) {?> checked="checked"<?php }?>>
                                                    <?php echo smartyTranslate(array('s'=>'Allow orders','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                </label>
                                            </div>     
                                            <div class="radio">
                                                <label for="default_behavior">
                                                    <input type="radio" value="2" id="default_behavior" name="out_of_stock"<?php if ((isset($_smarty_tpl->tpl_vars['out_of_stock']->value)&&$_smarty_tpl->tpl_vars['out_of_stock']->value==2)) {?> checked="checked"<?php }?>>
                                                    <?php echo smartyTranslate(array('s'=>'Use default behavior (Deny orders)','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                </label>
                                            </div>  
                                        </div>    
                                    </div> 
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_available_now']->value==1) {?>
                                    <div class="form-group">
                                        <label for="available_now"><?php echo smartyTranslate(array('s'=>'Available now','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control product_name input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" type="text" id="available_now_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="available_now_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->available_now[intval($_smarty_tpl->tpl_vars['language']->value['id_lang'])])) {?> value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp14=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->available_now[$_tmp14], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?> maxlength="255" />
                                        <?php } ?> 
                                    </div>
                                <?php }?> 
                                <?php if ($_smarty_tpl->tpl_vars['show_available_later']->value==1) {?>
                                    <div class="form-group">
                                        <label for="available_later"><?php echo smartyTranslate(array('s'=>'Available later','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                            <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control product_name input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" type="text" id="available_later_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="available_later_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->available_later[intval($_smarty_tpl->tpl_vars['language']->value['id_lang'])])) {?> value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp15=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->available_later[$_tmp15], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?> maxlength="255" />
                                        <?php } ?> 
                                    </div>
                                <?php }?> 
                                <?php if ($_smarty_tpl->tpl_vars['show_available_date']->value==1) {?>
                                    <div class="form-group">
                                        <label for="available_date"><?php echo smartyTranslate(array('s'=>'Available date','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" type="text" name="available_date" id="available_date" value="<?php if (isset($_smarty_tpl->tpl_vars['product']->value->available_date)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->available_date, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>0000-00-00<?php }?>" maxlength="10" />
                                    </div>
                                <?php }?> 
                            </div>
                            <?php if ($_smarty_tpl->tpl_vars['show_attributes']->value==1&&!$_smarty_tpl->tpl_vars['product']->value->is_virtual) {?>  
                                <div class="tab-pane panel" id="combinations">
                                    <h4><?php echo smartyTranslate(array('s'=>'Attributes','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                    <?php if (isset($_smarty_tpl->tpl_vars['attribute_groups']->value)&&$_smarty_tpl->tpl_vars['attribute_groups']->value) {?>
                                        <div class="row" style="margin-bottom:15px;">
                                            <div class="form-group col-md-5">
                                                <label><?php echo smartyTranslate(array('s'=>'Attribute','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                                <select id="attribute_group" name="attribute_group">
                                                    <option value="0" selected="selected"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                                    <?php  $_smarty_tpl->tpl_vars['ag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['attribute_groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ag']->key => $_smarty_tpl->tpl_vars['ag']->value) {
$_smarty_tpl->tpl_vars['ag']->_loop = true;
?>
                                                        <option value="<?php echo intval($_smarty_tpl->tpl_vars['ag']->value['id_attribute_group']);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ag']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label><?php echo smartyTranslate(array('s'=>'Value','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                                <select id="attribute" name="attribute">
                                                    <option value="0" selected="selected"><?php echo smartyTranslate(array('s'=>'-- Choose attribute --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                                    <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['first_options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                                                        <option value="<?php echo intval($_smarty_tpl->tpl_vars['option']->value['id_attribute']);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <button id="button_add_combination" onclick="add_attr();" class="btn btn-default btn-block" type="button"><i class="icon-plus-sign-alt"></i> <?php echo smartyTranslate(array('s'=>'Add','mod'=>'jmarketplace'),$_smarty_tpl);?>
</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-12">
                                                <select class="form-control col-lg-12" multiple="multiple" name="attribute_combination_list[]" id="product_att_list"></select>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <button  onclick="add_combination()" class="btn btn-default btn-block" type="button"><i class="icon-plus-sign-alt"></i> <?php echo smartyTranslate(array('s'=>'Save combination','mod'=>'jmarketplace'),$_smarty_tpl);?>
</button>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <h4><?php echo smartyTranslate(array('s'=>'Combinations','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                                <div class="table-responsive">
                                                    <table class="table" id="table-combinations-list">
                                                        <thead>
                                                            <tr class="nodrag nodrop">
                                                                <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Attribute - value','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                                <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Combination reference','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                                <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Impact price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                                <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Impact weight','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                                <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Quantity','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if (isset($_smarty_tpl->tpl_vars['attributes']->value)&&$_smarty_tpl->tpl_vars['attributes']->value) {?>
                                                                <?php  $_smarty_tpl->tpl_vars['attribute'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attribute']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['attributes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attribute']->key => $_smarty_tpl->tpl_vars['attribute']->value) {
$_smarty_tpl->tpl_vars['attribute']->_loop = true;
?>
                                                                    <tr id="combination_<?php echo intval($_smarty_tpl->tpl_vars['attribute']->value['id_product_attribute']);?>
" class="highlighted odd selected-line">
                                                                        <td class="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attribute']->value['attribute_designation'], ENT_QUOTES, 'UTF-8', true);?>
</td>
                                                                        <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attribute']->value['reference'], ENT_QUOTES, 'UTF-8', true);?>
" name="combination_reference[]"></td>
                                                                        <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo floatval($_smarty_tpl->tpl_vars['attribute']->value['price']);?>
" name="combination_price[]"></td>
                                                                        <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo floatval($_smarty_tpl->tpl_vars['attribute']->value['weight']);?>
" name="combination_weight[]"></td>
                                                                        <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo floatval($_smarty_tpl->tpl_vars['attribute']->value['quantity']);?>
" name="combination_qty[]"></td>
                                                                        <td>
                                                                            <input type="hidden" class="form-control col-md-2" value="<?php echo intval($_smarty_tpl->tpl_vars['attribute']->value['id_product_attribute']);?>
" name="id_product_attributes[]">
                                                                            <input type="hidden" name="attributes[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attribute']->value['attribute_designation'], ENT_QUOTES, 'UTF-8', true);?>
" />
                                                                            <a class="edit btn btn-default" data="<?php echo intval($_smarty_tpl->tpl_vars['attribute']->value['id_product_attribute']);?>
" onclick="delete_combination(this)">
                                                                                <i class="icon-minus-sign-alt"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            <?php }?>  
                                                        </tbody>
                                                    </table> 
                                                </div>
                                            </div> 
                                        </div>
                                    <?php }?> 
                                    <div class="clear"></div>
                                </div>
                            <?php }?>
                            <div class="tab-pane panel" id="images">
                                <?php if ($_smarty_tpl->tpl_vars['show_images']->value==1) {?>
                                    <div class="form-group">
                                        <label for="fileUpload"><?php echo smartyTranslate(array('s'=>'Images','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <p><?php echo smartyTranslate(array('s'=>'You can upload up to','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['max_images']->value);?>
 <?php echo smartyTranslate(array('s'=>'images.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                        <p><?php echo smartyTranslate(array('s'=>'The optimal size of the images is','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max_dimensions']->value, ENT_QUOTES, 'UTF-8', true);?>
</p><br/>
                                        <?php if (isset($_smarty_tpl->tpl_vars['images']->value)) {?>
                                            <div class="">
                                                <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['thumbnails']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['thumbnails']['iteration']++;
?>
                                                    <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']<=$_smarty_tpl->tpl_vars['max_images']->value) {?>
                                                        <?php $_smarty_tpl->tpl_vars['imageIds'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value->id)."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
                                                        <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
                                                            <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['legend'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
                                                        <?php } else { ?>
                                                            <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name[$_smarty_tpl->tpl_vars['id_lang']->value], ENT_QUOTES, 'UTF-8', true), null, 0);?>
                                                        <?php }?>
                                                        <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                            <?php $_smarty_tpl->tpl_vars['imageType'] = new Smarty_variable('large_default', null, 0);?>
                                                        <?php } else { ?>
                                                            <?php $_smarty_tpl->tpl_vars['imageType'] = new Smarty_variable('thickbox_default', null, 0);?>
                                                        <?php }?>
                                                        <div class="row upload_image">
                                                            <div id="contentUploadPreview<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
" class="col-xs-12 col-md-3" data="<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
">
                                                                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite[$_smarty_tpl->tpl_vars['id_lang']->value],$_smarty_tpl->tpl_vars['imageIds']->value,$_smarty_tpl->tpl_vars['imageType']->value), ENT_QUOTES, 'UTF-8', true);?>
" class="fancybox">
                                                                    <img class="img-responsive fancybox" id="uploadPreview<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite[$_smarty_tpl->tpl_vars['id_lang']->value],$_smarty_tpl->tpl_vars['imageIds']->value,'medium_default'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" height="150" width="150" />
                                                                </a>
                                                                <a class="delete_product_image btn btn-default" href="#" data="<?php echo intval($_smarty_tpl->tpl_vars['image']->value['id_image']);?>
">
                                                                    <i class="icon-trash fa fa-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                                </a>
                                                            </div>

                                                            <div class="col-xs-12 col-md-9">
                                                                <div class="form-group">
                                                                    <label>
                                                                        <?php echo smartyTranslate(array('s'=>'Image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>

                                                                        <?php if ($_smarty_tpl->tpl_vars['image']->value['cover']==1) {?>
                                                                            <i class="icon-check-sign icon-2x"></i> <?php echo smartyTranslate(array('s'=>'Cover image','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                                        <?php }?>
                                                                    </label>
                                                                    <input class="form-control not_uniform" id="uploadImage<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
" type="file" name="images[<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
]" onchange="previewImage(<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
);" />
                                                                </div>
                                                                <div class="form-group">             
                                                                    <label for="legend"><?php echo smartyTranslate(array('s'=>'Legend image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
</label>
                                                                    <input class="form-control" type="text" name="legends[<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                <?php } ?>

                                                <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['max_images']->value+1 - (count($_smarty_tpl->tpl_vars['images']->value)+1) : count($_smarty_tpl->tpl_vars['images']->value)+1-($_smarty_tpl->tpl_vars['max_images']->value)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = count($_smarty_tpl->tpl_vars['images']->value)+1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
                                                    <div class="row upload_image">
                                                        <div class="col-xs-12 col-md-3">
                                                            <div class="preview">
                                                                <img class="img-responsive" id="uploadPreview<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
" width="150" height="150" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_not_available']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 col-md-9">
                                                            <div class="form-group">
                                                                <label>
                                                                    <?php echo smartyTranslate(array('s'=>'Image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>

                                                                    <?php if ($_smarty_tpl->tpl_vars['foo']->value==1) {?>
                                                                        <i class="icon-check-sign icon-2x"></i> <?php echo smartyTranslate(array('s'=>'Cover image','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                                    <?php }?>
                                                                </label>
                                                                <input class="form-control not_uniform" id="uploadImage<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
" type="file" name="images[<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
]" onchange="previewImage(<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
);" />
                                                            </div>
                                                            <div class="form-group">             
                                                                <label for="legend"><?php echo smartyTranslate(array('s'=>'Legend image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
</label>
                                                                <input class="form-control" type="text" name="legends[<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
]" value="" maxlength="128" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }} ?> 
                                            </div>
                                        <?php }?>

                                    </div>
                                <?php }?>
                            </div>
                            <?php if ($_smarty_tpl->tpl_vars['show_features']->value==1) {?>     
                                <div class="tab-pane panel" id="features">          
                                    <h4><?php echo smartyTranslate(array('s'=>'Features','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                    <?php if (isset($_smarty_tpl->tpl_vars['features']->value)&&$_smarty_tpl->tpl_vars['features']->value) {?>
                                        <div class="row">
                                            <?php  $_smarty_tpl->tpl_vars['feature'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feature']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feature']->key => $_smarty_tpl->tpl_vars['feature']->value) {
$_smarty_tpl->tpl_vars['feature']->_loop = true;
?>
                                                <?php if (isset($_smarty_tpl->tpl_vars['feature']->value['featureValues'])&&$_smarty_tpl->tpl_vars['feature']->value['featureValues']) {?>
                                                    <div class="form-group col-lg-12">
                                                        <label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</label>
                                                        <select name="feature_value_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
">
                                                            <option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                                            <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['featureValues']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                                                                <option value="<?php echo intval($_smarty_tpl->tpl_vars['option']->value['id_feature_value']);?>
"<?php if ($_smarty_tpl->tpl_vars['feature']->value['current_item']==$_smarty_tpl->tpl_vars['option']->value['id_feature_value']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="form-group col-sm-12" style="display:none;">
                                                        <label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</label>
                                                        <?php if (isset($_smarty_tpl->tpl_vars['feature']->value['val'])&&$_smarty_tpl->tpl_vars['feature']->value['val']) {?>
                                                            <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['val']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
?>
                                                                <?php if ($_smarty_tpl->tpl_vars['feature']->value['current_item']==$_smarty_tpl->tpl_vars['val']->value['id_feature_value']) {?>
                                                                    <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['val']->value['id_lang']) {?> style="display:none;"<?php }?> class="form-control features_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
 input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" name="feature_value_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
_<?php echo intval($_smarty_tpl->tpl_vars['val']->value['id_lang']);?>
" id="feature_value_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
_<?php echo intval($_smarty_tpl->tpl_vars['val']->value['id_lang']);?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['val']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
" />
                                                                <?php }?>
                                                            <?php } ?>
                                                        <?php }?>
                                                    </div>
                                                <?php }?>
                                            <?php } ?>
                                        </div>
                                    <?php }?>      
                                </div>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['show_virtual']->value==1) {?>    
                                <div class="tab-pane panel" id="virtualproduct">   
                                    <div id="virtual_file" class="form-group">
                                        <label for="fileVirtual"><?php echo smartyTranslate(array('s'=>'Virtual file','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" type="file" name="virtual_file" />
                                        <?php if ($_smarty_tpl->tpl_vars['is_virtual']->value==1&&$_smarty_tpl->tpl_vars['display_filename']->value!='') {?>
                                            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_edit']->value, ENT_QUOTES, 'UTF-8', true);?>
&key=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filename']->value, ENT_QUOTES, 'UTF-8', true);?>
&download" title="<?php echo smartyTranslate(array('s'=>'Download this product','mod'=>'jmarketplace'),$_smarty_tpl);?>
"> 
                                                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/download_product.gif" class="icon" alt="<?php echo smartyTranslate(array('s'=>'Download product','mod'=>'jmarketplace'),$_smarty_tpl);?>
" />
                                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['display_filename']->value, ENT_QUOTES, 'UTF-8', true);?>

                                            </a>
                                        <?php }?>
                                        
                                        <p class="help-block">
                                            <?php if ($_smarty_tpl->tpl_vars['is_virtual']->value==1&&$_smarty_tpl->tpl_vars['display_filename']->value=='') {?>
                                                <?php echo smartyTranslate(array('s'=>'You have not uploaded a virtual file for this product. ','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                            <?php }?>
                                            <?php echo smartyTranslate(array('s'=>'Upload a file from your computer','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['attachment_maximun_size']->value);?>
 <?php echo smartyTranslate(array('s'=>'MB maximum.','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                        </p>
                                    </div>
                                    <div class="form-group hidden">
                                        <label for="filename"><?php echo smartyTranslate(array('s'=>'Filename','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" type="text" name="virtual_product_name" id="virtual_product_name" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_name']->value)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['virtual_product_name']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" maxlength="255" />
                                        <p class="help-block"><?php echo smartyTranslate(array('s'=>'The full filename with its extension (e.g. Book.pdf)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="virtual_product_nb_downloable"><?php echo smartyTranslate(array('s'=>'Number of allowed downloads','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" type="text" name="virtual_product_nb_downloable" id="virtual_product_nb_downloable" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_nb_downloable']->value)) {?><?php echo intval($_smarty_tpl->tpl_vars['virtual_product_nb_downloable']->value);?>
<?php }?>" maxlength="10" />
                                        <p class="help-block"><?php echo smartyTranslate(array('s'=>'Number of downloads allowed per customer. Set to 0 for unlimited downloads.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="virtual_product_expiration_date"><?php echo smartyTranslate(array('s'=>'Expiration date','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" type="text" name="virtual_product_expiration_date" id="virtual_product_expiration_date" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_expiration_date']->value)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['virtual_product_expiration_date']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" maxlength="10" />
                                        <p class="help-block"><?php echo smartyTranslate(array('s'=>'If set, the file will not be downloadable after this date. Leave blank if you do not wish to attach an expiration date.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="virtual_product_nb_days"><?php echo smartyTranslate(array('s'=>'Number of days','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                        <input class="form-control" type="text" name="virtual_product_nb_days" id="virtual_product_nb_days" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_nb_days']->value)) {?><?php echo intval($_smarty_tpl->tpl_vars['virtual_product_nb_days']->value);?>
<?php } else { ?>0<?php }?>" maxlength="10" />
                                        <p class="help-block"><?php echo smartyTranslate(array('s'=>'Number of days this file can be accessed by customers. Set to zero for unlimited access.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                        <p><?php echo smartyTranslate(array('s'=>'Important: If you edit this product, you must upload the virtual file again.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                    </div>  
                                </div>  
                            <?php }?>
                            <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceFormAddProductTabContent'),$_smarty_tpl);?>

                            <div class="form-group">
                                <button type="submit" name="submitAddProduct" class="btn btn-default button button-medium">
                                    <span><?php echo smartyTranslate(array('s'=>'Save','mod'=>'jmarketplace'),$_smarty_tpl);?>
<i class="icon-chevron-right right"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="form-group">
                        <?php if ($_smarty_tpl->tpl_vars['show_virtual']->value==1) {?>
                            <div class="form-group">
                                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Type','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <div>
                                    <div class="radio">
                                        <label for="simple_product">
                                            <input type="radio" checked="checked" value="0" id="simple_product" name="type_product">
                                            <?php echo smartyTranslate(array('s'=>'Standard product','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                        </label>
                                    </div>

                                    <div class="radio">
                                        <label for="virtual_product">
                                            <input type="radio" value="2" id="virtual_product" name="type_product"<?php if ($_smarty_tpl->tpl_vars['product']->value->is_virtual==1) {?> checked="checked"<?php }?>>
                                            <?php echo smartyTranslate(array('s'=>'Virtual product (services, booking, downloadable products, etc.)','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                        </label>
                                    </div>
                                </div>    
                            </div> 
                        <?php }?>
                        <div class="required form-group">
                            <label for="product_name" class="required"><?php echo smartyTranslate(array('s'=>'Product name','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                            <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control product_name input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="name_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="name_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp16=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name[$_tmp16], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                            <?php } ?> 
                        </div>

                        <?php if ($_smarty_tpl->tpl_vars['show_reference']->value==1) {?>
                            <div class="form-group">
                                <label for="reference">
                                    <?php echo smartyTranslate(array('s'=>'Reference','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                </label>
                                <input class="form-control" data-validate="isName" type="text" name="reference" id="reference" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->reference, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="32" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_isbn']->value==1) {?>
                            <div class="form-group">
                                <label for="isbn">
                                    <?php echo smartyTranslate(array('s'=>'ISBN','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                </label>
                                <input class="form-control" type="text" name="isbn" id="isbn" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->isbn, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="32" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_ean13']->value==1) {?>
                            <div class="form-group">
                                <label for="ean13">
                                    <?php echo smartyTranslate(array('s'=>'Ean13','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                </label>
                                <input class="form-control" data-validate="isName" type="text" name="ean13" id="ean13" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->ean13, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="13" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_upc']->value==1) {?>
                            <div class="form-group">
                                <label for="upc">
                                    <?php echo smartyTranslate(array('s'=>'UPC','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                </label>
                                <input class="form-control" data-validate="isName" type="text" name="upc" id="upc" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->upc, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="12" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_available_order']->value==1||$_smarty_tpl->tpl_vars['show_show_price']->value==1||$_smarty_tpl->tpl_vars['show_online_only']->value==1) {?>
                            <label for="options"><?php echo smartyTranslate(array('s'=>'Options','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                            <?php if ($_smarty_tpl->tpl_vars['show_available_order']->value==1) {?>
                                <div class="form-group">
                                    <p class="checkbox">
                                        <input type="checkbox" value="1" id="available_for_order" name="available_for_order"<?php if ($_smarty_tpl->tpl_vars['product']->value->available_for_order==1) {?> checked="checked"<?php }?>>
                                        <label for="available_for_order"><?php echo smartyTranslate(array('s'=>'Available for order','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    </p>
                                </div>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['show_show_price']->value==1) {?>
                                <div class="form-group">
                                    <p class="checkbox">
                                        <input type="checkbox" value="1" id="show_price" name="show_price"<?php if ($_smarty_tpl->tpl_vars['product']->value->show_price==1) {?> checked="checked"<?php }?>>
                                        <label for="show_price"><?php echo smartyTranslate(array('s'=>'Show price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    </p>
                                </div>
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['show_online_only']->value==1) {?>
                                <div class="form-group">
                                    <p class="checkbox">
                                        <input type="checkbox" value="1" id="online_only" name="online_only"<?php if ($_smarty_tpl->tpl_vars['product']->value->online_only==1) {?> checked="checked"<?php }?>>
                                        <label for="online_only"><?php echo smartyTranslate(array('s'=>'Online only (not sold in your retail store)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    </p>
                                </div>
                            <?php }?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_condition']->value==1) {?>
                            <div class="form-group">
                                <label><?php echo smartyTranslate(array('s'=>'Condition','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <select id="condition" name="condition">
                                    <option<?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='new') {?> selected="selected"<?php }?> value="new"><?php echo smartyTranslate(array('s'=>'New','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                    <option<?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='used') {?> selected="selected"<?php }?> value="used"><?php echo smartyTranslate(array('s'=>'Used','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                    <option<?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='refurbished') {?> selected="selected"<?php }?> value="refurbished"><?php echo smartyTranslate(array('s'=>'Refurbished','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                </select>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_desc_short']->value==1) {?>
                            <div class="form-group">
                                <label for="short_description"><?php echo smartyTranslate(array('s'=>'Short description','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <div id="short_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" class="short_description input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?>>
                                        <textarea name="short_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" cols="40" rows="7"><?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp17=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['product']->value->description_short[$_tmp17];?>
</textarea> 
                                    </div>
                                <?php } ?> 
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_desc']->value==1) {?>
                            <div class="form-group">
                                <label for="description"><?php echo smartyTranslate(array('s'=>'Description','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <div id="description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" class="description input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?>>
                                        <textarea name="description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" cols="40" rows="7"><?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp18=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['product']->value->description[$_tmp18];?>
</textarea> 
                                    </div>
                                <?php } ?> 
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_price']->value==1) {?>
                            <input type="hidden" name="seller_commission" id="seller_commission" value="<?php echo floatval($_smarty_tpl->tpl_vars['seller_commission']->value);?>
" />
                            <input type="hidden" name="fixed_commission" id="fixed_commission" value="<?php echo floatval($_smarty_tpl->tpl_vars['fixed_commission']->value);?>
" />
                            
                            <?php if ($_smarty_tpl->tpl_vars['show_wholesale_price']->value==1) {?>    
                                <div class="required form-group">
                                    <label for="wholesale_price"><?php echo smartyTranslate(array('s'=>'Wholesale price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <div class="input-group">
                                        <input class="form-control" data-validate="isNumber" type="text" name="wholesale_price" id="wholesale_price" value="<?php echo floatval($_smarty_tpl->tpl_vars['product']->value->wholesale_price);?>
" maxlength="10" />
                                        <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    </div>
                                </div>
                            <?php }?>
                            
                            <div class="form-group">
                                <label for="price"><?php echo smartyTranslate(array('s'=>'Price (tax excl.)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <div class="input-group">
                                    <input class="form-control" data-validate="isNumber" type="text" name="price" id="price" value="<?php echo floatval($_smarty_tpl->tpl_vars['product']->value->price);?>
" maxlength="10" />
                                    <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                </div>
                            </div>
                            <?php if ($_smarty_tpl->tpl_vars['show_offer_price']->value==1) {?>    
                                <div class="required form-group">
                                    <label for="offer_price"><?php echo smartyTranslate(array('s'=>'Offer price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <div class="input-group">
                                        <input class="form-control" data-validate="isNumber" type="text" name="specific_price" id="specific_price" value="<?php if (isset($_smarty_tpl->tpl_vars['specific_price']->value)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['final_price_tax_excl']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>0<?php }?>" maxlength="10" />
                                        <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    </div>
                                        <p class="help-block"><small><?php echo smartyTranslate(array('s'=>'Leave 0 if no offer. The offer price must be lower than the price.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</small></p>
                                </div>  
                            <?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['show_tax']->value==1) {?>
                                <div class="form-group">
                                    <label><?php echo smartyTranslate(array('s'=>'Tax','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <select id="id_tax" name="id_tax">
                                        <option value="0"><?php echo smartyTranslate(array('s'=>'no tax','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                        <?php  $_smarty_tpl->tpl_vars['tax'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['taxes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax']->key => $_smarty_tpl->tpl_vars['tax']->value) {
$_smarty_tpl->tpl_vars['tax']->_loop = true;
?>
                                            <option value="<?php echo intval($_smarty_tpl->tpl_vars['tax']->value['id_tax_rules_group']);?>
" data="<?php echo floatval($_smarty_tpl->tpl_vars['tax']->value['rate']);?>
"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->id_tax_rules_group)&&$_smarty_tpl->tpl_vars['product']->value->id_tax_rules_group==$_smarty_tpl->tpl_vars['tax']->value['id_tax_rules_group']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php }?>
                            <div class="form-group"<?php if ($_smarty_tpl->tpl_vars['show_tax']->value==0) {?>  style="display:none;"<?php }?>>
                                <label for="price"><?php echo smartyTranslate(array('s'=>'Price (tax incl.)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <div class="input-group">
                                    <input class="form-control" data-validate="isNumber" type="text" name="price_tax_incl" id="price_tax_incl" value="<?php echo floatval($_smarty_tpl->tpl_vars['final_price_tax_incl']->value);?>
" readonly="readonly" />
                                    <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                </div>
                            </div>
                            <?php if ($_smarty_tpl->tpl_vars['show_commission']->value==1) {?>
                                <div class="form-group">
                                    <label for="commission"><?php echo smartyTranslate(array('s'=>'Commission for you','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <div class="input-group">
                                        <input class="form-control" data-validate="isNumber" type="text" name="commission" id="commission" value="<?php if ($_smarty_tpl->tpl_vars['tax_commission']->value==1) {?><?php echo (($_smarty_tpl->tpl_vars['final_price_tax_incl']->value*$_smarty_tpl->tpl_vars['seller_commission']->value)/100)-floatval($_smarty_tpl->tpl_vars['fixed_commission']->value);?>
<?php } else { ?><?php echo (($_smarty_tpl->tpl_vars['final_price_tax_excl']->value*$_smarty_tpl->tpl_vars['seller_commission']->value)/100)-floatval($_smarty_tpl->tpl_vars['fixed_commission']->value);?>
<?php }?>" readonly="readonly" />
                                        <span class="input-group-addon"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sign']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    </div>
                                </div>
                            <?php }?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_on_sale']->value==1) {?>
                            <div class="form-group">
                                <p class="checkbox">
                                    <input type="checkbox" value="1" id="on_sale" name="on_sale"<?php if ($_smarty_tpl->tpl_vars['product']->value->on_sale==1) {?> checked="checked"<?php }?>>
                                    <label for="on_sale"><?php echo smartyTranslate(array('s'=>'Display the "on sale" icon on the product page, and in the text found within the product listing.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                </p>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_meta_keywords']->value==1||$_smarty_tpl->tpl_vars['show_meta_title']->value==1||$_smarty_tpl->tpl_vars['show_meta_desc']->value==1||$_smarty_tpl->tpl_vars['show_link_rewrite']->value==1) {?>
                            <h4><?php echo smartyTranslate(array('s'=>'Search Engine Optimization','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_meta_keywords']->value==1) {?>
                            <div class="form-group">
                                <label for="meta_keywords"><?php echo smartyTranslate(array('s'=>'Meta keywords (Every keyword separate by coma, ex. key1, key2, key3...)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control meta_keywords input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="meta_keywords_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="meta_keywords_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp19=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->meta_keywords[$_tmp19], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="255" />
                                <?php } ?> 
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_meta_title']->value==1) {?>
                            <div class="form-group">
                                <label for="meta_title"><?php echo smartyTranslate(array('s'=>'Meta title','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control meta_title input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="meta_title_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="meta_title_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp20=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->meta_title[$_tmp20], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                                <?php } ?> 
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_meta_desc']->value==1) {?>
                            <div class="form-group">
                                <label for="meta_description"><?php echo smartyTranslate(array('s'=>'Meta description','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control meta_description input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="meta_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="meta_description_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp21=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->meta_description[$_tmp21], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="255" />
                                <?php } ?> 
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_link_rewrite']->value==1) {?>
                            <div class="form-group">
                                <label for="link_rewrite"><?php echo smartyTranslate(array('s'=>'Friendly URL','mod'=>'jmarketplace'),$_smarty_tpl);?>
 </label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control link_rewrite input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" id="link_rewrite_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="link_rewrite_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp22=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->link_rewrite[$_tmp22], ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                                <?php } ?> 
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_categories']->value==1) {?>
                            <div class="form-group">
                                <div class="category_search_block">
                                    <label><?php echo smartyTranslate(array('s'=>'Categories','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <input name="search_tree_category" id="search_tree_category" type="text" class="search_category" placeholder="<?php echo smartyTranslate(array('s'=>'Search category','mod'=>'jmarketplace'),$_smarty_tpl);?>
" autocomplete="off">
                                    <div id="category_suggestions"></div>    
                                    <div class="checkok"></div>    
                                </div>
                                <?php echo $_smarty_tpl->tpl_vars['categoryTree']->value;?>
 
                            </div>
                            <p><?php echo smartyTranslate(array('s'=>'This product is associated with','mod'=>'jmarketplace'),$_smarty_tpl);?>
:</strong> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['categories_string']->value, ENT_QUOTES, 'UTF-8', true);?>
</p>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_categories']->value==1) {?>
                            <div id="category_default" class="form-group">
                                <label><?php echo smartyTranslate(array('s'=>'Category default','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <select id="id_category_default" name="id_category_default">
                                    <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories_selected']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                                        <option value="<?php echo intval($_smarty_tpl->tpl_vars['category']->value['id_category']);?>
"<?php if (($_smarty_tpl->tpl_vars['category']->value['id_category']==$_smarty_tpl->tpl_vars['product']->value->id_category_default)) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_suppliers']->value==1) {?>
                            <div class="form-group">
                                <label><?php echo smartyTranslate(array('s'=>'Supplier','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <select name="id_supplier">
                                    <option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                    <?php  $_smarty_tpl->tpl_vars['supplier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['supplier']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['suppliers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['supplier']->key => $_smarty_tpl->tpl_vars['supplier']->value) {
$_smarty_tpl->tpl_vars['supplier']->_loop = true;
?>
                                        <option value="<?php echo intval($_smarty_tpl->tpl_vars['supplier']->value['id_supplier']);?>
"<?php if ($_smarty_tpl->tpl_vars['product']->value->id_supplier==$_smarty_tpl->tpl_vars['supplier']->value['id_supplier']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['supplier']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_new_suppliers']->value==1) {?>
                            <div class="form-group"><a id="open_new_supplier" href="#"><?php echo smartyTranslate(array('s'=>'Add new supplier','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></div>
                            <div id="content_new_supplier" class="form-group" style="display:none;">
                                <label><?php echo smartyTranslate(array('s'=>'New supplier','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" data-validate="isName" type="text" name="new_supplier" id="new_supplier" maxlength="64" />
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_manufacturers']->value==1) {?>
                            <div class="form-group">
                                <label><?php echo smartyTranslate(array('s'=>'Manufacturer','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <select name="id_manufacturer">
                                    <option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                    <?php  $_smarty_tpl->tpl_vars['manufacturer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['manufacturer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['manufacturers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['manufacturer']->key => $_smarty_tpl->tpl_vars['manufacturer']->value) {
$_smarty_tpl->tpl_vars['manufacturer']->_loop = true;
?>
                                        <option value="<?php echo intval($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer']);?>
"<?php if ($_smarty_tpl->tpl_vars['product']->value->id_manufacturer==$_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_new_manufacturers']->value==1) {?>
                            <div class="form-group"><a id="open_new_manufacturer" href="#"><?php echo smartyTranslate(array('s'=>'Add new manufacturer','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a></div>
                            <div id="content_new_manufacturer" class="form-group" style="display:none;">
                                <label><?php echo smartyTranslate(array('s'=>'New manufacturer','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" data-validate="isName" type="text" name="new_manufacturer" id="new_manufacturer" maxlength="64" />
                            </div>
                        <?php }?>
                        <?php if (!$_smarty_tpl->tpl_vars['product']->value->is_virtual) {?>
                            <div id="shipping">
                                <?php if ($_smarty_tpl->tpl_vars['show_width']->value==1) {?>
                                    <div class="form-group">
                                        <label for="width">
                                            <?php echo smartyTranslate(array('s'=>'Width (cm)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" data-validate="isNumber" type="text" name="width" id="width" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->width, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_height']->value==1) {?>
                                    <div class="form-group">
                                        <label for="height">
                                            <?php echo smartyTranslate(array('s'=>'Height (cm)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" data-validate="isNumber" type="text" name="height" id="height" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->height, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_depth']->value==1) {?>
                                    <div class="form-group">
                                        <label for="depth">
                                            <?php echo smartyTranslate(array('s'=>'Depth (cm)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" data-validate="isNumber" type="text" name="depth" id="depth" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->depth, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                    </div>
                                <?php }?>
                                <?php if ($_smarty_tpl->tpl_vars['show_weight']->value==1) {?>
                                    <div class="form-group">
                                        <label for=weight">
                                            <?php echo smartyTranslate(array('s'=>'Weight (kg)','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                        </label>
                                        <input class="form-control" data-validate="isNumber" type="text" name="weight" id="weight" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->weight, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                    </div>
                                <?php }?>

                                <?php if ($_smarty_tpl->tpl_vars['show_shipping_product']->value==1) {?>                   
                                    <h4><?php echo smartyTranslate(array('s'=>'Select delivery method','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                    <?php if (isset($_smarty_tpl->tpl_vars['carriers']->value)&&$_smarty_tpl->tpl_vars['carriers']->value) {?>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo smartyTranslate(array('s'=>'Delivery service name','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                                        <th><?php echo smartyTranslate(array('s'=>'Delivery speed','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                                        <th><?php echo smartyTranslate(array('s'=>'Tick to enable for this product','mod'=>'jmarketplace'),$_smarty_tpl);?>
</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php  $_smarty_tpl->tpl_vars['carrier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carrier']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['carriers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carrier']->key => $_smarty_tpl->tpl_vars['carrier']->value) {
$_smarty_tpl->tpl_vars['carrier']->_loop = true;
?>
                                                        <tr>
                                                            <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</td>
                                                            <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['delay'], ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['carrier']->value['is_free']==1) {?> - <?php echo smartyTranslate(array('s'=>'Shipping free!','mod'=>'jmarketplace'),$_smarty_tpl);?>
<?php }?></td>
                                                            <td>
                                                                <input type="checkbox" name="carriers[]" value="<?php echo intval($_smarty_tpl->tpl_vars['carrier']->value['id_reference']);?>
"<?php if ($_smarty_tpl->tpl_vars['carrier']->value['checked']==1) {?> checked="checked"<?php }?> />
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            <label for="additional_shipping_cost">
                                                <?php echo smartyTranslate(array('s'=>'Additional shipping cost','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                            </label>
                                            <input class="form-control" type="text" name="additional_shipping_cost" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->additional_shipping_cost, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="10" />
                                        </div>
                                    <?php } else { ?>
                                         <?php if ($_smarty_tpl->tpl_vars['show_manage_carriers']->value==1) {?>
                                             <p>
                                                 <?php echo smartyTranslate(array('s'=>'First you must create at least one carrier.','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                                 <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','addcarrier',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Create your first carrier now','mod'=>'jmarketplace'),$_smarty_tpl);?>
</a>
                                             </p>
                                         <?php }?>
                                    <?php }?>  
                                <?php }?>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_features']->value==1) {?>     
                            <div id="features">          
                                <h4><?php echo smartyTranslate(array('s'=>'Features','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                <?php if (isset($_smarty_tpl->tpl_vars['features']->value)&&$_smarty_tpl->tpl_vars['features']->value) {?>
                                    <?php  $_smarty_tpl->tpl_vars['feature'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feature']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feature']->key => $_smarty_tpl->tpl_vars['feature']->value) {
$_smarty_tpl->tpl_vars['feature']->_loop = true;
?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['feature']->value['featureValues'])&&$_smarty_tpl->tpl_vars['feature']->value['featureValues']) {?>
                                            <div class="form-group">
                                                <label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</label>
                                                <select name="feature_value_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
">
                                                    <option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                                    <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['featureValues']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                                                        <option value="<?php echo intval($_smarty_tpl->tpl_vars['option']->value['id_feature_value']);?>
"<?php if ($_smarty_tpl->tpl_vars['feature']->value['current_item']==$_smarty_tpl->tpl_vars['option']->value['id_feature_value']) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        <?php } else { ?>
                                            <div class="form-group" style="display:none;">
                                                <label><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</label>
                                                <?php if (isset($_smarty_tpl->tpl_vars['feature']->value['val'])&&$_smarty_tpl->tpl_vars['feature']->value['val']) {?>
                                                    <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['val']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['feature']->value['current_item']==$_smarty_tpl->tpl_vars['val']->value['id_feature_value']) {?>
                                                            <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['val']->value['id_lang']) {?> style="display:none;"<?php }?> class="form-control features_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
 input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" data-validate="isName" type="text" name="feature_value_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
_<?php echo intval($_smarty_tpl->tpl_vars['val']->value['id_lang']);?>
" id="feature_value_<?php echo intval($_smarty_tpl->tpl_vars['feature']->value['id_feature']);?>
_<?php echo intval($_smarty_tpl->tpl_vars['val']->value['id_lang']);?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['val']->value['value'], ENT_QUOTES, 'UTF-8', true);?>
" />
                                                        <?php }?>
                                                    <?php } ?>
                                                <?php }?>
                                            </div>
                                        <?php }?>
                                    <?php } ?>
                                <?php }?>      
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_attributes']->value==1&&!$_smarty_tpl->tpl_vars['product']->value->is_virtual) {?>  
                            <div id="combinations">
                                <h4><?php echo smartyTranslate(array('s'=>'Attributes','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                <?php if (isset($_smarty_tpl->tpl_vars['attribute_groups']->value)&&$_smarty_tpl->tpl_vars['attribute_groups']->value) {?>
                                    <div class="row" style="margin-bottom:15px;">
                                        <div class="form-group col-md-5">
                                            <label><?php echo smartyTranslate(array('s'=>'Attribute','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            <select id="attribute_group" name="attribute_group">
                                                <option value="0" selected="selected"><?php echo smartyTranslate(array('s'=>'-- Choose --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                                <?php  $_smarty_tpl->tpl_vars['ag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['attribute_groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ag']->key => $_smarty_tpl->tpl_vars['ag']->value) {
$_smarty_tpl->tpl_vars['ag']->_loop = true;
?>
                                                    <option value="<?php echo intval($_smarty_tpl->tpl_vars['ag']->value['id_attribute_group']);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ag']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label><?php echo smartyTranslate(array('s'=>'Value','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                            <select id="attribute" name="attribute">
                                                <option value="0" selected="selected"><?php echo smartyTranslate(array('s'=>'-- Choose attribute --','mod'=>'jmarketplace'),$_smarty_tpl);?>
</option>
                                                <?php  $_smarty_tpl->tpl_vars['option'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['option']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['first_options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['option']->key => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
?>
                                                    <option value="<?php echo intval($_smarty_tpl->tpl_vars['option']->value['id_attribute']);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <button id="button_add_combination" onclick="add_attr();" class="btn btn-default btn-block" type="button"><i class="icon-plus-sign-alt"></i> <?php echo smartyTranslate(array('s'=>'Add','mod'=>'jmarketplace'),$_smarty_tpl);?>
</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-12">
                                            <select class="form-control col-lg-12" multiple="multiple" name="attribute_combination_list[]" id="product_att_list"></select>
                                        </div>

                                        <div class="form-group col-lg-12">
                                            <button  onclick="add_combination()" class="btn btn-default btn-block" type="button"><i class="icon-plus-sign-alt"></i> <?php echo smartyTranslate(array('s'=>'Save combination','mod'=>'jmarketplace'),$_smarty_tpl);?>
</button>
                                        </div>

                                        <div class="form-group col-lg-12">
                                            <h4><?php echo smartyTranslate(array('s'=>'Combinations','mod'=>'jmarketplace'),$_smarty_tpl);?>
</h4>
                                            <div class="table-responsive">
                                                <table class="table" id="table-combinations-list">
                                                    <thead>
                                                        <tr class="nodrag nodrop">
                                                            <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Attribute - value','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                            <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Combination reference','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                            <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Impact price','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                            <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Impact weight','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                            <th class=" left"><span class="title_box"><?php echo smartyTranslate(array('s'=>'Quantity','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if (isset($_smarty_tpl->tpl_vars['attributes']->value)&&$_smarty_tpl->tpl_vars['attributes']->value) {?>
                                                            <?php  $_smarty_tpl->tpl_vars['attribute'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attribute']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['attributes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attribute']->key => $_smarty_tpl->tpl_vars['attribute']->value) {
$_smarty_tpl->tpl_vars['attribute']->_loop = true;
?>
                                                                <tr id="combination_<?php echo intval($_smarty_tpl->tpl_vars['attribute']->value['id_product_attribute']);?>
" class="highlighted odd selected-line">
                                                                    <td class="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attribute']->value['attribute_designation'], ENT_QUOTES, 'UTF-8', true);?>
</td>
                                                                    <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attribute']->value['reference'], ENT_QUOTES, 'UTF-8', true);?>
" name="combination_reference[]"  maxlength="32"></td>
                                                                    <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo floatval($_smarty_tpl->tpl_vars['attribute']->value['price']);?>
" name="combination_price[]"></td>
                                                                    <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo floatval($_smarty_tpl->tpl_vars['attribute']->value['weight']);?>
" name="combination_weight[]"></td>
                                                                    <td class="left"><input type="text" class="form-control col-md-12" value="<?php echo floatval($_smarty_tpl->tpl_vars['attribute']->value['quantity']);?>
" name="combination_qty[]"></td>
                                                                    <td>
                                                                        <input type="hidden" class="form-control col-md-2" value="<?php echo intval($_smarty_tpl->tpl_vars['attribute']->value['id_product_attribute']);?>
" name="id_product_attributes[]">
                                                                        <input type="hidden" name="attributes[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attribute']->value['attribute_designation'], ENT_QUOTES, 'UTF-8', true);?>
" />
                                                                        <a class="edit btn btn-default" data="<?php echo intval($_smarty_tpl->tpl_vars['attribute']->value['id_product_attribute']);?>
" onclick="delete_combination(this)">
                                                                            <i class="icon-minus-sign-alt"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php }?>  
                                                    </tbody>
                                                </table> 
                                            </div>
                                        </div> 
                                    </div>
                                <?php }?> 
                                <div class="clear"></div>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_quantity']->value==1) {?>
                            <div class="form-group">
                                <label for="quantity">
                                    <?php echo smartyTranslate(array('s'=>'Quantity','mod'=>'jmarketplace'),$_smarty_tpl);?>
 
                                </label>
                                <input class="form-control" data-validate="isNumber" type="text" name="quantity" id="quantity" value="<?php echo intval($_smarty_tpl->tpl_vars['real_quantity']->value);?>
" maxlength="10" />
                            </div>
                        <?php }?> 
                        <?php if ($_smarty_tpl->tpl_vars['show_minimal_quantity']->value==1) {?>
                            <div class="form-group">
                                <label for="minimal_quantity"><?php echo smartyTranslate(array('s'=>'Minimal quantity','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" data-validate="isNumber" type="text" name="minimal_quantity" id="quantity"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->minimal_quantity)) {?> value="<?php echo intval($_smarty_tpl->tpl_vars['product']->value->minimal_quantity);?>
"<?php } else { ?> value="1"<?php }?> maxlength="10" />
                            </div>
                        <?php }?> 
                        <?php if ($_smarty_tpl->tpl_vars['show_availability']->value==1) {?>
                            <div class="form-group">
                                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Availability preferences (Behavior when out of stock)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <div>
                                    <div class="radio">
                                        <label for="deny_orders">
                                            <input type="radio" value="0" id="deny_orders" name="out_of_stock"<?php if ((isset($_smarty_tpl->tpl_vars['out_of_stock']->value)&&$_smarty_tpl->tpl_vars['out_of_stock']->value==0)) {?> checked="checked"<?php }?>>
                                            <?php echo smartyTranslate(array('s'=>'Deny orders','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label for="allow_orders">
                                            <input type="radio" value="1" id="allow_orders" name="out_of_stock"<?php if ((isset($_smarty_tpl->tpl_vars['out_of_stock']->value)&&$_smarty_tpl->tpl_vars['out_of_stock']->value==1)) {?> checked="checked"<?php }?>>
                                            <?php echo smartyTranslate(array('s'=>'Allow orders','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                        </label>
                                    </div>     
                                    <div class="radio">
                                        <label for="default_behavior">
                                            <input type="radio" value="2" id="default_behavior" name="out_of_stock"<?php if ((isset($_smarty_tpl->tpl_vars['out_of_stock']->value)&&$_smarty_tpl->tpl_vars['out_of_stock']->value==2)) {?> checked="checked"<?php }?>>
                                            <?php echo smartyTranslate(array('s'=>'Use default behavior (Deny orders)','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                        </label>
                                    </div>  
                                </div>    
                            </div> 
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_available_now']->value==1) {?>
                            <div class="form-group">
                                <label for="available_now"><?php echo smartyTranslate(array('s'=>'Available now','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control product_name input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" type="text" id="available_now_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="available_now_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->available_now[intval($_smarty_tpl->tpl_vars['language']->value['id_lang'])])) {?> value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp23=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->available_now[$_tmp23], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?> maxlength="255" />
                                <?php } ?> 
                            </div>
                        <?php }?> 
                        <?php if ($_smarty_tpl->tpl_vars['show_available_later']->value==1) {?>
                            <div class="form-group">
                                <label for="available_later"><?php echo smartyTranslate(array('s'=>'Available later','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
                                    <input<?php if ($_smarty_tpl->tpl_vars['id_lang']->value!=$_smarty_tpl->tpl_vars['language']->value['id_lang']) {?> style="display:none;"<?php }?> class="is_required validate form-control product_name input_with_language lang_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" type="text" id="available_later_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
" name="available_later_<?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
"<?php if (isset($_smarty_tpl->tpl_vars['product']->value->available_later[intval($_smarty_tpl->tpl_vars['language']->value['id_lang'])])) {?> value="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['language']->value['id_lang']);?>
<?php $_tmp24=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->available_later[$_tmp24], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?> maxlength="255" />
                                <?php } ?> 
                            </div>
                        <?php }?> 
                        <?php if ($_smarty_tpl->tpl_vars['show_available_date']->value==1) {?>
                            <div class="form-group">
                                <label for="available_date"><?php echo smartyTranslate(array('s'=>'Available date','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <input class="form-control" type="text" name="available_date" id="available_date" value="<?php if (isset($_smarty_tpl->tpl_vars['product']->value->available_date)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->available_date, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>0000-00-00<?php }?>" maxlength="10" />
                            </div>
                        <?php }?> 
                        <?php if ($_smarty_tpl->tpl_vars['show_images']->value==1) {?>
                            <div class="form-group">
                                <label for="fileUpload"><?php echo smartyTranslate(array('s'=>'Images','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                <p><?php echo smartyTranslate(array('s'=>'You can upload up to','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['max_images']->value);?>
 <?php echo smartyTranslate(array('s'=>'images.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                <p><?php echo smartyTranslate(array('s'=>'The optimal size of the images is','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max_dimensions']->value, ENT_QUOTES, 'UTF-8', true);?>
</p><br/>
                                <?php if (isset($_smarty_tpl->tpl_vars['images']->value)) {?>
                                    <div class="">
                                        <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['thumbnails']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['thumbnails']['iteration']++;
?>
                                            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']<=$_smarty_tpl->tpl_vars['max_images']->value) {?>
                                                <?php $_smarty_tpl->tpl_vars['imageIds'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value->id)."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
                                                <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
                                                    <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['legend'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
                                                <?php } else { ?>
                                                    <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name[$_smarty_tpl->tpl_vars['id_lang']->value], ENT_QUOTES, 'UTF-8', true), null, 0);?>
                                                <?php }?> 
                                                <?php if ($_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
                                                    <?php $_smarty_tpl->tpl_vars['imageType'] = new Smarty_variable('large_default', null, 0);?>
                                                <?php } else { ?>
                                                    <?php $_smarty_tpl->tpl_vars['imageType'] = new Smarty_variable('thickbox_default', null, 0);?>
                                                <?php }?>
                                                <div class="row upload_image">
                                                    <div id="contentUploadPreview<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
" class="col-xs-12 col-md-3" data="<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
">
                                                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite[$_smarty_tpl->tpl_vars['id_lang']->value],$_smarty_tpl->tpl_vars['imageIds']->value,$_smarty_tpl->tpl_vars['imageType']->value), ENT_QUOTES, 'UTF-8', true);?>
" class="fancybox">
                                                            <img class="img-responsive fancybox" id="uploadPreview<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite[$_smarty_tpl->tpl_vars['id_lang']->value],$_smarty_tpl->tpl_vars['imageIds']->value,'medium_default'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" height="150" width="150" />
                                                        </a>
                                                        <a class="delete_product_image btn btn-default" href="#" data="<?php echo intval($_smarty_tpl->tpl_vars['image']->value['id_image']);?>
">
                                                            <i class="icon-trash fa fa-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                        </a>
                                                    </div>

                                                    <div class="col-xs-12 col-md-9">
                                                        <div class="form-group">
                                                            <label>
                                                                <?php echo smartyTranslate(array('s'=>'Image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>

                                                                <?php if ($_smarty_tpl->tpl_vars['image']->value['cover']==1) {?>
                                                                    <i class="icon-check-sign icon-2x"></i> <?php echo smartyTranslate(array('s'=>'Cover image','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                                <?php }?>
                                                            </label>
                                                            <input class="form-control not_uniform" id="uploadImage<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
" type="file" name="images[<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
]" onchange="previewImage(<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
);" />
                                                        </div>
                                                        <div class="form-group">             
                                                            <label for="legend"><?php echo smartyTranslate(array('s'=>'Legend image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
</label>
                                                            <input class="form-control" type="text" name="legends[<?php echo intval($_smarty_tpl->getVariable('smarty')->value['foreach']['thumbnails']['iteration']);?>
]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" maxlength="128" />
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        <?php } ?>

                                        <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int) ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? $_smarty_tpl->tpl_vars['max_images']->value+1 - (count($_smarty_tpl->tpl_vars['images']->value)+1) : count($_smarty_tpl->tpl_vars['images']->value)+1-($_smarty_tpl->tpl_vars['max_images']->value)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0) {
for ($_smarty_tpl->tpl_vars['foo']->value = count($_smarty_tpl->tpl_vars['images']->value)+1, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++) {
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
                                            <div class="row upload_image">
                                                <div class="col-xs-12 col-md-3">
                                                    <div class="preview">
                                                        <img class="img-responsive" id="uploadPreview<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
" width="150" height="150" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_not_available']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-md-9">
                                                    <div class="form-group">
                                                        <label>
                                                            <?php echo smartyTranslate(array('s'=>'Image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>

                                                            <?php if ($_smarty_tpl->tpl_vars['foo']->value==1) {?>
                                                                <i class="icon-check-sign icon-2x"></i> <?php echo smartyTranslate(array('s'=>'Cover image','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                                            <?php }?>
                                                        </label>
                                                        <input class="form-control not_uniform" id="uploadImage<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
" type="file" name="images[<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
]" onchange="previewImage(<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
);" />
                                                    </div>
                                                    <div class="form-group">             
                                                        <label for="legend"><?php echo smartyTranslate(array('s'=>'Legend image','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
</label>
                                                        <input class="form-control" type="text" name="legends[<?php echo intval($_smarty_tpl->tpl_vars['foo']->value);?>
]" value="" maxlength="128" />
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }} ?> 
                                    </div>
                                <?php }?>

                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['show_virtual']->value==1) {?>   
                            <div class="form-group" id="virtualproduct">          
                                <div id="virtual_file" class="form-group">
                                    <label for="fileVirtual"><?php echo smartyTranslate(array('s'=>'Virtual file','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <input class="form-control" type="file" name="virtual_file" />
                                    <?php if ($_smarty_tpl->tpl_vars['is_virtual']->value==1&&$_smarty_tpl->tpl_vars['display_filename']->value!='') {?>
                                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_edit']->value, ENT_QUOTES, 'UTF-8', true);?>
&key=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filename']->value, ENT_QUOTES, 'UTF-8', true);?>
&download" title="<?php echo smartyTranslate(array('s'=>'Download this product','mod'=>'jmarketplace'),$_smarty_tpl);?>
"> 
                                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
jmarketplace/views/img/download_product.gif" class="icon" alt="<?php echo smartyTranslate(array('s'=>'Download product','mod'=>'jmarketplace'),$_smarty_tpl);?>
" />
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['display_filename']->value, ENT_QUOTES, 'UTF-8', true);?>

                                        </a>
                                    <?php }?>

                                    <p class="help-block">
                                        <?php if ($_smarty_tpl->tpl_vars['is_virtual']->value==1&&$_smarty_tpl->tpl_vars['display_filename']->value=='') {?>
                                            <?php echo smartyTranslate(array('s'=>'You have not uploaded a virtual file for this product. ','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                        <?php }?>
                                        <?php echo smartyTranslate(array('s'=>'Upload a file from your computer','mod'=>'jmarketplace'),$_smarty_tpl);?>
 <?php echo intval($_smarty_tpl->tpl_vars['attachment_maximun_size']->value);?>
 <?php echo smartyTranslate(array('s'=>'MB maximum.','mod'=>'jmarketplace'),$_smarty_tpl);?>

                                    </p>
                                </div>
                                <div class="form-group hidden">
                                    <label for="filename"><?php echo smartyTranslate(array('s'=>'Filename','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <input class="form-control" type="text" name="virtual_product_name" id="virtual_product_name" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_name']->value)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['virtual_product_name']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" maxlength="255" />
                                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'The full filename with its extension (e.g. Book.pdf)','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                </div>
                                <div class="form-group">
                                    <label for="virtual_product_nb_downloable"><?php echo smartyTranslate(array('s'=>'Number of allowed downloads','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <input class="form-control" type="text" name="virtual_product_nb_downloable" id="virtual_product_nb_downloable" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_nb_downloable']->value)) {?><?php echo intval($_smarty_tpl->tpl_vars['virtual_product_nb_downloable']->value);?>
<?php }?>" maxlength="10" />
                                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Number of downloads allowed per customer. Set to 0 for unlimited downloads.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                </div>
                                <div class="form-group">
                                    <label for="virtual_product_expiration_date"><?php echo smartyTranslate(array('s'=>'Expiration date','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <input class="form-control" type="text" name="virtual_product_expiration_date" id="virtual_product_expiration_date" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_expiration_date']->value)) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['virtual_product_expiration_date']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" maxlength="10" />
                                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'If set, the file will not be downloadable after this date. Leave blank if you do not wish to attach an expiration date.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                </div>
                                <div class="form-group">
                                    <label for="virtual_product_nb_days"><?php echo smartyTranslate(array('s'=>'Number of days','mod'=>'jmarketplace'),$_smarty_tpl);?>
</label>
                                    <input class="form-control" type="text" name="virtual_product_nb_days" id="virtual_product_nb_days" value="<?php if (isset($_smarty_tpl->tpl_vars['virtual_product_nb_days']->value)) {?><?php echo intval($_smarty_tpl->tpl_vars['virtual_product_nb_days']->value);?>
<?php } else { ?>0<?php }?>" maxlength="10" />
                                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Number of days this file can be accessed by customers. Set to zero for unlimited access.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                    <p><?php echo smartyTranslate(array('s'=>'Important: If you edit this product, you must upload the virtual file again.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</p>
                                </div>  
                            </div>
                        <?php }?>
                        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayMarketplaceFormAddProduct'),$_smarty_tpl);?>

                        <button type="submit" name="submitAddProduct" class="btn btn-default button button-medium">
                            <span><?php echo smartyTranslate(array('s'=>'Save','mod'=>'jmarketplace'),$_smarty_tpl);?>
<i class="icon-chevron-right right"></i></span>
                        </button>
                    </div>
                <?php }?>
            </form>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ("./footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
</div>    
<?php echo $_smarty_tpl->getSubTemplate ("./varstoscript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
