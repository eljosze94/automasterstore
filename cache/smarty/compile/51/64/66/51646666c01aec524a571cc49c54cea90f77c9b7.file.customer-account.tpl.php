<?php /* Smarty version Smarty-3.1.19, created on 2018-03-31 00:48:55
         compiled from "/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/hook/customer-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1530514215abf05271e26e6-75549330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '51646666c01aec524a571cc49c54cea90f77c9b7' => 
    array (
      0 => '/home/h3hued5u4248/public_html/modules/jmarketplace/views/templates/hook/customer-account.tpl',
      1 => 1522348209,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1530514215abf05271e26e6-75549330',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ps_version' => 0,
    'is_seller' => 0,
    'customer_can_be_seller' => 0,
    'link' => 0,
    'is_active_seller' => 0,
    'show_contact' => 0,
    'show_seller_favorite' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5abf052723c1e9_57766857',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5abf052723c1e9_57766857')) {function content_5abf052723c1e9_57766857($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['ps_version']->value)&&$_smarty_tpl->tpl_vars['ps_version']->value=='1.7') {?>
    <?php if (($_smarty_tpl->tpl_vars['is_seller']->value==0&&$_smarty_tpl->tpl_vars['customer_can_be_seller']->value)) {?>
        <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo smartyTranslate(array('s'=>'Create seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','addseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <span class="link-item">
                <i class="material-icons">&#xE8A6;</i>
                <?php echo smartyTranslate(array('s'=>'Create seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </a>
    <?php } elseif ($_smarty_tpl->tpl_vars['is_seller']->value==1&&$_smarty_tpl->tpl_vars['is_active_seller']->value==0) {?>
        <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" href="#">
            <span class="link-item">
                <i class="material-icons">&#xE8A6;</i>
                <?php echo smartyTranslate(array('s'=>'Your seller account is pending approval.','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </a>
    <?php } elseif ($_smarty_tpl->tpl_vars['is_seller']->value==1&&$_smarty_tpl->tpl_vars['is_active_seller']->value==1) {?>  
        <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <span class="link-item">
                <i class="material-icons">&#xE8A6;</i>
                <?php echo smartyTranslate(array('s'=>'Seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </a> 
    <?php }?>
    <?php if (($_smarty_tpl->tpl_vars['show_contact']->value)) {?>
        <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo smartyTranslate(array('s'=>'Seller messages','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','contactseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <span class="link-item">
                <i class="material-icons">&#xE0E1;</i>
                <?php echo smartyTranslate(array('s'=>'Seller messages','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </a>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['show_seller_favorite']->value) {?>
        <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo smartyTranslate(array('s'=>'Favorite sellers','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','favoriteseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
            <span class="link-item">
                <i class="material-icons">&#xE87D;</i>
                <?php echo smartyTranslate(array('s'=>'Favorite sellers','mod'=>'jmarketplace'),$_smarty_tpl);?>

            </span>
        </a>
    <?php }?>
<?php } else { ?>
    <?php if (($_smarty_tpl->tpl_vars['is_seller']->value==0&&$_smarty_tpl->tpl_vars['customer_can_be_seller']->value)) {?>
        <li class="col-sm-6  menu-tienda-2">
            <a title="<?php echo smartyTranslate(array('s'=>'Create seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','addseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-user fa fa-user"></i>
                <span><?php echo smartyTranslate(array('s'=>'Create seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li>  
    <?php } elseif ($_smarty_tpl->tpl_vars['is_seller']->value==1&&$_smarty_tpl->tpl_vars['is_active_seller']->value==0) {?>
        <li class="col-sm-6  menu-tienda-2">
            <a href="#">
                <i class="icon-user fa fa-user"></i>
                <span><?php echo smartyTranslate(array('s'=>'Your seller account is pending approval.','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li>  
    <?php } elseif ($_smarty_tpl->tpl_vars['is_seller']->value==1&&$_smarty_tpl->tpl_vars['is_active_seller']->value==1) {?>  
        <li class="col-sm-6  menu-tienda-2">
            <a title="<?php echo smartyTranslate(array('s'=>'Your seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','selleraccount',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-user fa fa-user"></i>
                <span><?php echo smartyTranslate(array('s'=>'Seller account','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li> 
    <?php }?>
    <?php if (($_smarty_tpl->tpl_vars['show_contact']->value)) {?>
        <li class="col-sm-6  menu-tienda" > 
            <a title="<?php echo smartyTranslate(array('s'=>'Seller messages','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','contactseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-envelope fa fa-envelope-o"></i>
                <span><?php echo smartyTranslate(array('s'=>'Seller messages','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['show_seller_favorite']->value) {?>
        <li class="col-sm-6 menu-tienda-2">
            <a title="<?php echo smartyTranslate(array('s'=>'Favorite sellers','mod'=>'jmarketplace'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmarketplace','favoriteseller',array(),true), ENT_QUOTES, 'UTF-8', true);?>
">
                <i class="icon-heart fa fa-heart"></i>
                <span><?php echo smartyTranslate(array('s'=>'Favorite sellers','mod'=>'jmarketplace'),$_smarty_tpl);?>
</span>
            </a>
        </li>
    <?php }?>
<?php }?>

<?php }} ?>
