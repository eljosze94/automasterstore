<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 11:36:31
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/404.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9633339895ac23fef28f3a3-30973389%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca1db04329706e398de6f28267d2bd50f7706305' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/404.tpl',
      1 => 1519076592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9633339895ac23fef28f3a3-30973389',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'force_ssl' => 0,
    'base_dir_ssl' => 0,
    'base_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac23fef297672_16989959',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac23fef297672_16989959')) {function content_5ac23fef297672_16989959($_smarty_tpl) {?>
<div class="pagenotfound-box">
	<h4><?php echo smartyTranslate(array('s'=>'4'),$_smarty_tpl);?>
<span><?php echo smartyTranslate(array('s'=>'0'),$_smarty_tpl);?>
</span><?php echo smartyTranslate(array('s'=>'4'),$_smarty_tpl);?>
</h4>
	<p class="large-text"><?php echo smartyTranslate(array('s'=>'page not found'),$_smarty_tpl);?>
</p>
	<p class="small-text"><?php echo smartyTranslate(array('s'=>'Search again what you are looking for'),$_smarty_tpl);?>
</p>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std">
		<fieldset>
			<div class="input_group">
				<label for="search_query"><?php echo smartyTranslate(array('s'=>'Search our product catalog:'),$_smarty_tpl);?>
</label>
				<div class="flex-box">
					<input id="search_query" name="search_query" type="text" class="form-control grey" />
               	 	<button type="submit" name="Submit" value="OK" class="btn btn-default button button-small"><span><?php echo smartyTranslate(array('s'=>'Ok'),$_smarty_tpl);?>
</span></button>
				</div>
			</div>
		</fieldset>
	</form>

	<div class="buttons"><a class="btn btn-default button button-medium" href="<?php if (isset($_smarty_tpl->tpl_vars['force_ssl']->value)&&$_smarty_tpl->tpl_vars['force_ssl']->value) {?><?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
<?php }?>" title="<?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
"><span><i class="icon-chevron-left left"></i><?php echo smartyTranslate(array('s'=>'Home page'),$_smarty_tpl);?>
</span></a></div>
</div>
<?php }} ?>
