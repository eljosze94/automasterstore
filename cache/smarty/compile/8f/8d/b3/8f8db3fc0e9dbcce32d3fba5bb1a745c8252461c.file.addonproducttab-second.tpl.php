<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 10:58:36
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addonproducttab-second.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17288216185ac2370cc42880-94632651%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8f8db3fc0e9dbcce32d3fba5bb1a745c8252461c' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmspagebuilder/views/templates/hook/addonproducttab-second.tpl',
      1 => 1520223242,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17288216185ac2370cc42880-94632651',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cols' => 0,
    'cols_md' => 0,
    'cols_sm' => 0,
    'cols_xs' => 0,
    'navigation' => 0,
    'pagination' => 0,
    'autoplay' => 0,
    'rewind' => 0,
    'slidebypage' => 0,
    'addon_title' => 0,
    'addon_desc' => 0,
    'addon_tpl_dir' => 0,
    'config' => 0,
    'cf' => 0,
    'featured_products' => 0,
    'products_slide' => 0,
    'box_template' => 0,
    'product' => 0,
    'new_products' => 0,
    'topseller_products' => 0,
    'special_products' => 0,
    'onsale_products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac2370cc89a38_37430533',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac2370cc89a38_37430533')) {function content_5ac2370cc89a38_37430533($_smarty_tpl) {?>
<script type="text/javascript">
jQuery(function ($) {
    "use strict";
	var producttabCarousel = $(".producttab-carousel1");			
	var items = <?php if ($_smarty_tpl->tpl_vars['cols']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['cols']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>4<?php }?>,
	itemsDesktop = <?php if ($_smarty_tpl->tpl_vars['cols']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['cols']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>4<?php }?>,
	itemsDesktopSmall = <?php if ($_smarty_tpl->tpl_vars['cols_md']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['cols_md']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>3<?php }?>,
	itemsTablet = <?php if ($_smarty_tpl->tpl_vars['cols_sm']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['cols_sm']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>2<?php }?>,
	itemsMobile = <?php if ($_smarty_tpl->tpl_vars['cols_xs']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['cols_xs']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>1<?php }?>;
	var rtl = false;
	if ($("body").hasClass("rtl")) rtl = true;				
	producttabCarousel.owlCarousel({
		responsiveClass:true,
		responsive:{			
			1280:{
				items:itemsDesktop
			},
			991:{
				items:itemsDesktopSmall
			},
			768:{
				items:itemsTablet
			},
			480:{
				items:itemsMobile
			},
			0:{
				items:1
			}
		},
		rtl: rtl,		
		margin:0,
		nav: <?php if ($_smarty_tpl->tpl_vars['navigation']->value=='1') {?>true<?php } else { ?>false<?php }?>,
		dots: <?php if ($_smarty_tpl->tpl_vars['pagination']->value=='1') {?>true<?php } else { ?>false<?php }?>,
		autoplay:<?php if ($_smarty_tpl->tpl_vars['autoplay']->value=='1') {?>true<?php } else { ?>false<?php }?>,
		rewindNav: <?php if ($_smarty_tpl->tpl_vars['rewind']->value=='1') {?>true<?php } else { ?>false<?php }?>,
		navigationText: ["", ""],
		slideBy: <?php if ($_smarty_tpl->tpl_vars['slidebypage']->value=='1') {?>'page'<?php } else { ?>1<?php }?>,
		slideSpeed: 200	
	});
});
</script>
<?php if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>
<div class="addon-title">
	<h3><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['addon_title']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h3>
</div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['addon_desc']->value) {?>
<p class="addon-desc"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['addon_desc']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
<?php }?>	
<?php $_smarty_tpl->tpl_vars["box_template"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['addon_tpl_dir']->value)."productbox.tpl", null, 0);?>
<div class="jms-tab1">
	<ul class="nav nav-tabs" role="tablist">
	<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable(0, null, 0);?>
		<?php if ($_smarty_tpl->tpl_vars['config']->value['show_featured']=='1') {?>
			<li class="active"><a class="button" data-toggle="tab" href="#featured1"><?php echo smartyTranslate(array('s'=>'DESTACADOS ','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</a></li>
		<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
		<?php }?>	
		<?php if ($_smarty_tpl->tpl_vars['config']->value['show_new']=='1') {?>
			<li <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>class="active"<?php }?>><a class="button" data-toggle="tab" href="#latest1"><?php echo smartyTranslate(array('s'=>'NOVEDADES','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</a></li>
			<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
		<?php }?>		
		<?php if ($_smarty_tpl->tpl_vars['config']->value['show_topseller']=='1') {?>
			<li <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>class="active"<?php }?>><a class="button" data-toggle="tab" href="#topseller1"><?php echo smartyTranslate(array('s'=>'MÁS VENDIDOS','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</a></li>
			<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
		<?php }?>		
		<?php if ($_smarty_tpl->tpl_vars['config']->value['show_special']=='1') {?>
			<li <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>class="active"<?php }?>><a class="button" data-toggle="tab" href="#special1"><?php echo smartyTranslate(array('s'=>'ESPECIALES','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</a></li>
			<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
		<?php }?>			
		<?php if ($_smarty_tpl->tpl_vars['config']->value['show_onsale']=='1') {?>
			<li <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>class="active"<?php }?>><a class="button" data-toggle="tab" href="#onsale1"><?php echo smartyTranslate(array('s'=>'EN OFERTA','mod'=>'jmspagebuilder'),$_smarty_tpl);?>
</a></li>
			<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
		<?php }?>			
	</ul>
	<a class="show-cat btn-default" id="show-cat1">
		<i class="fa fa-arrow-down" aria-hidden="true"></i>
	</a>
</div>
<div class="tab-content">
	<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable(0, null, 0);?>
	<?php if ($_smarty_tpl->tpl_vars['config']->value['show_featured']=='1') {?>
		 <div role="tabpanel" class="tab-pane active" id="featured1">
			<div class="producttab-carousel1">	
				<?php  $_smarty_tpl->tpl_vars['products_slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['products_slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featured_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['products_slide']->key => $_smarty_tpl->tpl_vars['products_slide']->value) {
$_smarty_tpl->tpl_vars['products_slide']->_loop = true;
?>
					<div class="item">
						<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products_slide']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
							<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['box_template']->value;?>
<?php $_tmp4=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ($_tmp4, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>

						<?php } ?>
					</div>
				<?php } ?>
			</div>
		 </div>
		<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['config']->value['show_new']=='1') {?>
		 <div role="tabpanel" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>active<?php }?>" id="latest1">
			<div class="producttab-carousel1">	
				<?php  $_smarty_tpl->tpl_vars['products_slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['products_slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['new_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['products_slide']->key => $_smarty_tpl->tpl_vars['products_slide']->value) {
$_smarty_tpl->tpl_vars['products_slide']->_loop = true;
?>
					<div class="item">
						<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products_slide']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
							<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['box_template']->value;?>
<?php $_tmp5=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ($_tmp5, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>
	
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		 </div>
		<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['config']->value['show_topseller']=='1') {?>
		 <div role="tabpanel" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>active<?php }?>" id="topseller1">
			<div class="producttab-carousel1">	
				<?php  $_smarty_tpl->tpl_vars['products_slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['products_slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['topseller_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['products_slide']->key => $_smarty_tpl->tpl_vars['products_slide']->value) {
$_smarty_tpl->tpl_vars['products_slide']->_loop = true;
?>
					<div class="item">
						<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products_slide']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
							<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['box_template']->value;?>
<?php $_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ($_tmp6, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>

						<?php } ?>
					</div>
				<?php } ?>
			</div>
		 </div>
		<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['config']->value['show_special']=='1') {?>
		 <div role="tabpanel" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>active<?php }?>" id="special1">
			<div class="producttab-carousel1">	
				<?php  $_smarty_tpl->tpl_vars['products_slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['products_slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['special_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['products_slide']->key => $_smarty_tpl->tpl_vars['products_slide']->value) {
$_smarty_tpl->tpl_vars['products_slide']->_loop = true;
?>
					<div class="item">
						<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products_slide']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
							<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['box_template']->value;?>
<?php $_tmp7=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ($_tmp7, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>

						<?php } ?>
					</div>
				<?php } ?>
			</div>
		 </div>
		<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['config']->value['show_onsale']=='1') {?>
		 <div role="tabpanel" class="tab-pane <?php if ($_smarty_tpl->tpl_vars['cf']->value==0) {?>active<?php }?>" id="onsale1">
			<div class="producttab-carousel1">	
				<?php  $_smarty_tpl->tpl_vars['products_slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['products_slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['onsale_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['products_slide']->key => $_smarty_tpl->tpl_vars['products_slide']->value) {
$_smarty_tpl->tpl_vars['products_slide']->_loop = true;
?>
					<div class="item">
						<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products_slide']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
							<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['box_template']->value;?>
<?php $_tmp8=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ($_tmp8, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>

						<?php } ?>
					</div>
				<?php } ?>
			</div>
		 </div>
		<?php $_smarty_tpl->tpl_vars['cf'] = new Smarty_variable($_smarty_tpl->tpl_vars['cf']->value+1, null, 0);?>
	<?php }?>
</div>
<?php }} ?>
