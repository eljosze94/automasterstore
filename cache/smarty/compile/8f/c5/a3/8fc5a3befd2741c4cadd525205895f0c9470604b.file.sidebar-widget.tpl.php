<?php /* Smarty version Smarty-3.1.19, created on 2018-04-02 11:36:24
         compiled from "/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmsblogwidget/views/templates/hook/sidebar-widget.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16410630605ac23fe811b607-51057503%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8fc5a3befd2741c4cadd525205895f0c9470604b' => 
    array (
      0 => '/home/h3hued5u4248/public_html/themes/jms_deermarket/modules/jmsblogwidget/views/templates/hook/sidebar-widget.tpl',
      1 => 1519321010,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16410630605ac23fe811b607-51057503',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'widget_setting' => 0,
    'category_menu' => 0,
    'latestpost' => 0,
    'post' => 0,
    'params' => 0,
    'image_baseurl' => 0,
    'show_view' => 0,
    'popularpost' => 0,
    'latestcomment' => 0,
    'comment' => 0,
    'archives' => 0,
    'archive' => 0,
    'aparams' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ac23fe8176e32_11859447',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ac23fe8176e32_11859447')) {function content_5ac23fe8176e32_11859447($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/h3hued5u4248/public_html/tools/smarty/plugins/modifier.date_format.php';
?>
<?php if ($_smarty_tpl->tpl_vars['widget_setting']->value['JBW_SB_SHOW_CATEGORYMENU']) {?>
<aside class="blog-widget widget-categories">
	<h3 class="widget-title"><span><?php echo smartyTranslate(array('s'=>'Categorias','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span></h3>
	<ul>
	<?php echo $_smarty_tpl->tpl_vars['category_menu']->value;?>

	</ul>
</aside>
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['widget_setting']->value['JBW_SB_SHOW_RECENT']) {?>
<aside class="blog-widget">
	<h3 class="widget-title"><span><?php echo smartyTranslate(array('s'=>'Publicaciones recientes','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span></h3>
	<ul class="post-list">
		<?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['latestpost']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['post']->key;
?>
			<?php $_smarty_tpl->tpl_vars["show_view"] = new Smarty_variable($_smarty_tpl->tpl_vars['post']->value['views']+1, null, 0);?>
			<?php $_smarty_tpl->tpl_vars['params'] = new Smarty_variable(array('post_id'=>$_smarty_tpl->tpl_vars['post']->value['post_id'],'category_slug'=>$_smarty_tpl->tpl_vars['post']->value['category_alias'],'slug'=>$_smarty_tpl->tpl_vars['post']->value['alias']), null, 0);?>
			<li>
				<div class="entry-thumb">
					<a href="<?php echo jmsblog::getPageLink('jmsblog-post',$_smarty_tpl->tpl_vars['params']->value);?>
" class="post-img">
					<?php if ($_smarty_tpl->tpl_vars['post']->value['image']) {?>
						<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_baseurl']->value, ENT_QUOTES, 'UTF-8', true);?>
thumb_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['image'], ENT_QUOTES, 'UTF-8', true);?>
" class="img-responsive" />
					<?php } else { ?>	
						<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_baseurl']->value, ENT_QUOTES, 'UTF-8', true);?>
no-img.jpg" class="img-responsive" />
					<?php }?>
					</a>
				</div>
				<div class="entry-title">
					<a href="<?php echo jmsblog::getPageLink('jmsblog-post',$_smarty_tpl->tpl_vars['params']->value);?>
">
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

					</a>
				</div>
				<div class="date">
					<ul>
						<li>
							<span class="label" style="color: #000;" ><?php echo smartyTranslate(array('s'=>'Publicado: ','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span><?php echo smarty_modifier_date_format(htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['created'], ENT_QUOTES, 'UTF-8', true),"%b %d, %Y");?>

						</li>
						<li>
							<span class="label view" style="color: #000;"><?php echo smartyTranslate(array('s'=>'Visitas: ','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_view']->value, ENT_QUOTES, 'UTF-8', true);?>

						</li>
<br>
					</ul>	
				</div>
			</li>
		<?php } ?>
	</ul>
	
</aside>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['widget_setting']->value['JBW_SB_SHOW_POPULAR']) {?>
<aside class="blog-widget">
	<h3 class="widget-title"><span><?php echo smartyTranslate(array('s'=>'Publicaciones populares','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span></h3>
	<ul class="post-list">
		<?php  $_smarty_tpl->tpl_vars['post'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['post']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['popularpost']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['post']->key => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['post']->key;
?>	
		<?php $_smarty_tpl->tpl_vars["show_view"] = new Smarty_variable($_smarty_tpl->tpl_vars['post']->value['views']+1, null, 0);?>	
		<?php $_smarty_tpl->tpl_vars['params'] = new Smarty_variable(array('post_id'=>$_smarty_tpl->tpl_vars['post']->value['post_id'],'category_slug'=>$_smarty_tpl->tpl_vars['post']->value['category_alias'],'slug'=>$_smarty_tpl->tpl_vars['post']->value['alias']), null, 0);?>
			<li>
				<div class="entry-thumb">
					<a href="<?php echo jmsblog::getPageLink('jmsblog-post',$_smarty_tpl->tpl_vars['params']->value);?>
" class="post-img">
						<?php if ($_smarty_tpl->tpl_vars['post']->value['image']) {?>
							<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_baseurl']->value, ENT_QUOTES, 'UTF-8', true);?>
thumb_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['image'], ENT_QUOTES, 'UTF-8', true);?>
" class="img-responsive" />
						<?php } else { ?>	
							<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_baseurl']->value, ENT_QUOTES, 'UTF-8', true);?>
no-img.jpg" class="img-responsive" />
						<?php }?>
					</a>
				</div>
				<div class="entry-title">
					<a href="<?php echo jmsblog::getPageLink('jmsblog-post',$_smarty_tpl->tpl_vars['params']->value);?>
">
						<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['title'], ENT_QUOTES, 'UTF-8', true);?>

					</a>
				</div>
				
<div class="date">
					<ul>
						<li>
							<span class="label" style="color: #000;" ><?php echo smartyTranslate(array('s'=>'Publicado: ','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span><?php echo smarty_modifier_date_format(htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['created'], ENT_QUOTES, 'UTF-8', true),"%b %d, %Y");?>

						</li>
						<li>
							<span class="label view" style="color: #000;"><?php echo smartyTranslate(array('s'=>'Visitas: ','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['show_view']->value, ENT_QUOTES, 'UTF-8', true);?>

						</li>
<br>
					</ul>	
				</div>
			</li>
		<?php } ?>
	</ul>
</aside>
<?php }?>
	
<?php if ($_smarty_tpl->tpl_vars['widget_setting']->value['JBW_SB_SHOW_LATESTCOMMENT']) {?>
<aside class="blog-widget">
	<h3 class="widget-title"><span><?php echo smartyTranslate(array('s'=>'Últimos comentarios','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span></h3>
	<?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['latestcomment']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['comment']->key;
?>
		<article class="comment-item clearfix">
			<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_baseurl']->value, ENT_QUOTES, 'UTF-8', true);?>
user.png" class="img-responsive">
			<div class="comment-info">
				<h6><?php echo $_smarty_tpl->tpl_vars['comment']->value['customer_name'];?>
</h6>
				<p><?php echo $_smarty_tpl->tpl_vars['comment']->value['comment'];?>
</p>
			</div>
		</article>
	<?php } ?>
</aside>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['widget_setting']->value['JBW_SB_SHOW_ARCHIVES']) {?>
<aside class="blog-widget widget-categories">
	<h3 class="widget-title">
		<span><?php echo smartyTranslate(array('s'=>'Archivos','mod'=>'jmsblogwidget'),$_smarty_tpl);?>
</span>
	</h3>
	<ul>
	<?php  $_smarty_tpl->tpl_vars['archive'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['archive']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['archives']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['archive']->key => $_smarty_tpl->tpl_vars['archive']->value) {
$_smarty_tpl->tpl_vars['archive']->_loop = true;
?>
		<?php $_smarty_tpl->tpl_vars['aparams'] = new Smarty_variable(array('archive'=>$_smarty_tpl->tpl_vars['archive']->value['postmonth']), null, 0);?>
		<li><a href="<?php echo jmsblog::getPageLink('jmsblog-archive',$_smarty_tpl->tpl_vars['aparams']->value);?>
"><?php echo $_smarty_tpl->tpl_vars['archive']->value['postmonth'];?>
</a></li>
	<?php } ?>
	</ul>
</aside>
<?php }?>
<?php }} ?>
