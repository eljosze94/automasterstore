<?php

class BridgeConnector
{
    public $rootPath = '';
    public $bridgePath = '';
    public $currentFolder = '';
    public $errorMessage = '';
    public $configFilePath = '';
    public $bridgeFilePath = '';
    public $connectorBridgePath = 'http://api.api2cart.com/v1.0/bridge.download.file';

    public function __construct()
    {
        $this->rootPath = dirname(_PS_MODULE_DIR_) . '/';
        $this->currentFolder = dirname(__FILE__) . '/';
        $this->bridgePath = $this->rootPath . 'bridge2cart/';
        $this->configFilePath = $this->bridgePath . 'config.php';
        $this->bridgeFilePath = $this->bridgePath . 'bridge.php';
    }

    public function getStoreKey()
    {
        if (file_exists($this->configFilePath)) {
            require_once($this->configFilePath);
            return M1_TOKEN;
        }

        return false;
    }

    public function isBridgeExist()
    {
        if (is_dir($this->bridgePath)
          && file_exists($this->bridgeFilePath)
          && file_exists($this->configFilePath)
        ) {
            return true;
        }

        return false;
    }

    public function installBridge()
    {
        if ($this->isBridgeExist()) {
            return true;
        }

        file_put_contents("bridge.zip", Tools::file_get_contents($this->connectorBridgePath));
        $zip = new ZipArchive;

        $res = $zip->open("bridge.zip");

        if ($res === true) {
            $zip->extractTo($this->currentFolder);
            $zip->close();
        }

        $res =  $this->xcopy($this->currentFolder . '/bridge2cart/', $this->rootPath . '/bridge2cart/');
        $this->deleteDir($this->currentFolder . '/bridge2cart/');
        unlink($this->currentFolder . '/readme.txt');

        return $res;
    }

    public function unInstallBridge()
    {
        if (!$this->isBridgeExist()) {
            return true;
        }

        return $this->deleteDir($this->bridgePath);
    }

    public function updateToken($token)
    {
        $config = @fopen($this->configFilePath, 'w');
        $write = fwrite($config, "<?php define('M1_TOKEN', '" . $token . "');");

        if (($config === false) || ($write === false) || (fclose($config) === false)) {
            return false;
        }

        return true;
    }

    private function deleteDir($dirPath)
    {
        if (is_dir($dirPath)) {
            $objects = scandir($dirPath);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                        $this->deleteDir($dirPath . DIRECTORY_SEPARATOR . $object);
                    } else {
                        if (!unlink($dirPath . DIRECTORY_SEPARATOR . $object)) {
                            return false;
                        }
                    }
                }
            }

            reset($objects);

            if (!rmdir($dirPath)) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    private function xcopy($src, $dst)
    {
        $dir = opendir($src);

        if (!$dir || !mkdir($dst)) {
            return false;
        }

        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    $this->xcopy($src . '/' . $file, $dst . '/' . $file);
                } elseif (!copy($src . '/' . $file, $dst . '/' . $file)) {
                    $this->deleteDir($dst);
                    return false;
                }

                chmod($dst . $file, 0755);
                chmod($dst, 0755);
            }
        }

        closedir($dir);

        return true;
    }
}
