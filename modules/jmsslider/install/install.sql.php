<?php
/**
* 2007-2017 PrestaShop
*
* Slider Layer module for prestashop
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

$query = "DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `class_suffix` varchar(100) NOT NULL,
  `bg_type` int(10) NOT NULL DEFAULT '1',
  `bg_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bg_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFF',
  `slide_link` varchar(100) NOT NULL,
  `order` int(10) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_slide`)
) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

INSERT INTO `_DB_PREFIX_jms_slides` (`id_slide`, `title`, `class_suffix`, `bg_type`, `bg_image`, `bg_color`, `slide_link`, `order`, `status`) VALUES
(17, 'Slider Home 1', '', 1, '6ff8686102cb8f306fb420bf4ed5d987.jpg', '#ffffff', '#', 0, 1),
(26, 'Slider Home 3', '', 1, '31bd08862b7e476750a55e9d1db3b89e.jpg', '#ffffff', '#', 2, 1),
(27, 'Slider Home 4', '', 1, '3ae16c33d44982446eea79fe3b483639.jpg', '#ffffff', '#', 3, 1),
(28, 'Slider Home 5', '', 1, '5b5b3bb310d6d6aae60d5da92032e6fd.jpg', '', '', 4, 1),
(29, 'Slider2  Home 1', '', 1, '31bd08862b7e476750a55e9d1db3b89e.jpg', '#ffffff', '#', 1, 1);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_lang`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_lang` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) NOT NULL,
  PRIMARY KEY (`id_slide`,`id_lang`)
) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

INSERT INTO `_DB_PREFIX_jms_slides_lang` (`id_slide`, `id_lang`) VALUES
(17, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_layers`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_layers` (
  `id_layer` int(10) NOT NULL AUTO_INCREMENT,
  `id_slide` int(10) NOT NULL,
  `data_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data_class_suffix` varchar(50) NOT NULL,
  `data_fixed` int(10) NOT NULL DEFAULT '0',
  `data_delay` int(10) NOT NULL DEFAULT '1000',
  `data_time` int(10) NOT NULL DEFAULT '1000',
  `data_x` int(10) NOT NULL DEFAULT '0',
  `data_y` int(10) NOT NULL DEFAULT '0',
  `data_in` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'left',
  `data_out` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'right',
  `data_ease_in` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'linear',
  `data_ease_out` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'linear',
  `data_step` int(10) NOT NULL DEFAULT '0',
  `data_special` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cycle',
  `data_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_html` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_video` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_video_controls` int(10) NOT NULL DEFAULT '1',
  `data_video_muted` int(10) NOT NULL DEFAULT '0',
  `data_video_autoplay` int(10) NOT NULL DEFAULT '1',
  `data_video_loop` int(10) NOT NULL DEFAULT '1',
  `data_video_bg` int(10) NOT NULL DEFAULT '0',
  `data_font_size` int(10) NOT NULL DEFAULT '14',
  `data_line_height` int(10) NOT NULL,
  `data_style` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'normal',
  `data_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '#FFFFFF',
  `data_width` int(10) NOT NULL,
  `data_height` int(10) NOT NULL,
  `data_order` int(10) NOT NULL,
  `data_status` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_layer`,`id_slide`)
) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

INSERT INTO `_DB_PREFIX_jms_slides_layers` (`id_layer`, `id_slide`, `data_title`, `data_class_suffix`, `data_fixed`, `data_delay`, `data_time`, `data_x`, `data_y`, `data_in`, `data_out`, `data_ease_in`, `data_ease_out`, `data_step`, `data_special`, `data_type`, `data_image`, `data_html`, `data_video`, `data_video_controls`, `data_video_muted`, `data_video_autoplay`, `data_video_loop`, `data_video_bg`, `data_font_size`, `data_line_height`, `data_style`, `data_color`, `data_width`, `data_height`, `data_order`, `data_status`) VALUES
(1, 17, 'Large Text', 'home1-large-text home1-slide1', 0, 1000, 2000, 100, 145, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<div class=\"large-text\">Apple iPhone 7 Plus</div>', '', 0, 0, 0, 0, 0, 36, 35, 'normal', '#232530', 200, 50, 0, 1),
(2, 17, 'Medium Text', 'home1 home1-medium-text', 0, 2000, 3000, 100, 200, 'bottom', 'top', 'easeInBounce', 'easeInOutExpo', 0, '', 'text', '', '<div class=\"medium-text\">iPhone 7 dramatically improves the most important aspects of the iPhone experience.</div>', '', 0, 0, 0, 0, 0, 14, 24, 'normal', '#646a7c', 200, 50, 1, 1),
(3, 17, 'Button Shop Now', 'home1-button home1-slide1', 0, 3000, 3500, 100, 410, 'bottom', 'top', 'easeInQuint', 'linear', 0, '', 'text', '', '<a class=\"btn-shop-now btn-effect\">BUY NOW</a>', '', 0, 0, 0, 0, 0, 12, 40, 'normal', '#ffffff', 200, 50, 4, 1),
(28, 17, 'Price', 'home1 home1-price', 0, 1000, 0, 100, 330, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', '<div class=\"price\">\r\n<span>$</span>980\r\n</div>', '', 0, 0, 0, 0, 0, 60, 30, 'normal', '#67bc0f', 200, 50, 3, 1),
(29, 17, 'Small Text', 'home1 home1-small-text', 0, 1000, 0, 100, 280, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', '<div class=\"small-text\">\r\nSAVE UP TO\r\n</div>', '', 0, 0, 0, 0, 0, 16, 30, 'normal', '#232530', 200, 50, 2, 1),
(30, 26, 'Large Text', 'home3-large-text home3', 0, 1000, 2000, 375, 155, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<div class=\"large-text\">Bomber Jackets \r\nCollection 2017</div>', '', 0, 0, 0, 0, 0, 48, 55, 'normal', '#232530', 200, 50, 1, 1),
(31, 26, 'Medium Text', 'home3-medium-text home3', 0, 2000, 3000, 375, 116, 'bottom', 'top', 'easeInBounce', 'easeInOutExpo', 0, '', 'text', '', '<div class=\"medium-text\">Spring | Summer</div>', '', 0, 0, 0, 0, 0, 18, 24, 'normal', '#67bc0f', 200, 50, 0, 1),
(32, 26, 'Small Text', 'home3-small-text home3', 0, 1000, 0, 375, 285, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', '<div class=\"small-text\">\r\nGet 30% off to our products\r\n</div>', '', 0, 0, 0, 0, 0, 16, 30, 'normal', '#646a7c', 200, 50, 2, 1),
(34, 26, 'Button Shop Now', 'home3-button home3', 0, 3000, 3500, 375, 360, 'bottom', 'top', 'easeInQuint', 'linear', 0, '', 'text', '', '<a class=\"btn-shop-now btn-effect\">BUY NOW</a>', '', 0, 0, 0, 0, 0, 12, 40, 'normal', '#ffffff', 200, 50, 4, 1),
(35, 27, 'Large Text', 'home4 home4-large-text', 0, 1000, 2000, 505, 205, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<div class=\"large-text\">Clothing 30% OFF</div>', '', 0, 0, 0, 0, 0, 48, 55, 'normal', '#232530', 200, 50, 1, 1),
(37, 27, 'Small Text', 'home4 home4-small-text', 0, 1000, 0, 670, 168, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', '<div class=\"small-text\">\r\nseson sale!\r\n</div>', '', 0, 0, 0, 0, 0, 14, 35, 'normal', '#67bc0f', 200, 50, 0, 1),
(39, 27, 'Button 1', 'home4 home4-button1', 0, 3000, 3500, 535, 303, 'bottom', 'top', 'easeInQuint', 'linear', 0, '', 'text', '', '<a class=\"button1 btn-hover\">FOR WOMEN’S</a>', '', 0, 0, 0, 0, 0, 12, 40, 'normal', '#ffffff', 200, 50, 2, 1),
(40, 27, 'Button 2', 'home4-button2', 0, 3600, 3800, 722, 303, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<a class=\"button2 btn-effect\">FOR MEN’S</a>', '', 0, 0, 0, 0, 0, 12, 40, 'normal', '#ffffff', 200, 50, 3, 1),
(41, 28, 'Small Text', 'home4-small-text home5', 0, 500, 1500, 375, 175, 'right', 'left', 'easeInCubic', 'easeInOutCubic', 0, '', 'text', '', '<div class=\"small-text\">Spring Collection 2017</div>', '', 0, 0, 0, 0, 0, 14, 35, 'normal', '#67bc0f', 200, 50, 0, 1),
(42, 28, 'Large-text', 'home4-large-text home5', 0, 1500, 2500, 375, 210, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<div class=\"large-text\">New Arrivals</div>', '', 0, 0, 0, 0, 0, 48, 55, 'normal', '#232530', 200, 50, 0, 1),
(43, 28, 'Button 1', 'home5-button1 home5', 0, 2500, 3000, 375, 310, 'right', 'left', 'easeInOutQuint', 'easeInOutQuint', 0, '', 'text', '', '<a class=\"button1 btn-hover\">FOR WOMEN’S</a>', '', 0, 0, 0, 0, 0, 14, 40, 'normal', '#ffffff', 200, 50, 0, 1),
(44, 28, 'Button 2', 'home5-button2', 0, 3000, 3500, 580, 310, 'bottom', 'top', 'easeInCirc', 'easeOutCirc', 0, '', 'text', '', '<a class=\"button2 btn-effect\">FOR MEN’S</a>', '', 0, 0, 0, 0, 0, 14, 40, 'normal', '#ffffff', 200, 50, 0, 1),
(45, 29, 'Large Text', 'home1-large-text home1-slide1', 0, 1000, 2000, 100, 145, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<div class=\"large-text\">Lorem ipsum dolor </div>', '', 0, 0, 0, 0, 0, 36, 35, 'normal', '#232530', 200, 50, 0, 1),
(46, 29, 'Medium Text', 'home1', 0, 2000, 3000, 100, 200, 'bottom', 'top', 'easeInBounce', 'easeInOutExpo', 0, '', 'text', '', '<div class=\"medium-text\">Lorem ipsum dolor sit amet, consectetur adipiscing elit pulvinar dictum purus.</div>', '', 0, 0, 0, 0, 0, 14, 24, 'normal', '#646a7c', 200, 50, 1, 1),
(47, 29, 'Small Text', '', 0, 1000, 0, 100, 280, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', '<div class=\"small-text\">\r\nSAVE UP TO\r\n</div>', '', 0, 0, 0, 0, 0, 16, 30, 'normal', '#232530', 200, 50, 2, 1),
(48, 29, 'Price', '', 0, 1000, 0, 100, 330, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', '<div class=\"price\">\r\n<span>$</span>780\r\n</div>', '', 0, 0, 0, 0, 0, 60, 30, 'normal', '#67bc0f', 200, 50, 3, 1),
(49, 29, 'Button Shop Now', 'home1-button home1-slide1', 0, 3000, 3500, 100, 410, 'bottom', 'top', 'easeInQuint', 'linear', 0, '', 'text', '', '<a class=\"btn-shop-now btn-effect\">BUY NOW</a>', '', 0, 0, 0, 0, 0, 12, 40, 'normal', '#ffffff', 200, 50, 4, 1);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_shop`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_shop` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) NOT NULL,
  PRIMARY KEY (`id_slide`,`id_shop`)
) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

INSERT INTO `_DB_PREFIX_jms_slides_shop` (`id_slide`, `id_shop`) VALUES
(17, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1);
";
