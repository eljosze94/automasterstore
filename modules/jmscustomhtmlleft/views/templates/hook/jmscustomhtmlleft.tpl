{*
 * @package Jms Custom Html
 * @version 1.0
 * @Copyright (C) 2009 - 2015 Joommasters.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @Website: http://www.joommasters.com
*}
{if $page_name=='category' || $page_name=='product'}
{$html_left|escape:'htmlall':'UTF-8'}
{/if}