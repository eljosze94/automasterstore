<?php
/**
* 2007-2014 PrestaShop
*
* Jms Adv Search
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

/**
 * @since 1.5.0
 */

class JMSAdvSearchSearchModuleFrontController extends ModuleFrontController
{
    public $instant_search;
    public $ajax_search;

    /**
     * Initialize search controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();

        $this->instant_search = Tools::getValue('instantSearch');

        $this->ajax_search = Tools::getValue('ajaxSearch');

        if ($this->instant_search || $this->ajax_search) {
            $this->display_header = false;
            $this->display_footer = false;
        }
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        $query = Tools::replaceAccentedChars(urldecode(Tools::getValue('q')));
        $cat_id = (int)Tools::getValue('cat_id');
        $original_query = Tools::getValue('q');

        if ($this->ajax_search) {
            $searchResults = Search::find((int)(Tools::getValue('id_lang')), $query, 1, 10, 'position', 'desc', true);
            $searchResults = $this->checkCategories($searchResults['result'], $cat_id);
            foreach ($searchResults as &$product) {
                $product['product_link'] = $this->context->link->getProductLink($product['id_product'], $product['prewrite'], $product['crewrite']);
            }
            die(Tools::jsonEncode($searchResults));
        }

        if ($this->instant_search && !is_array($query)) {
            $this->productSort();
            $this->n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
            $this->p = abs((int)(Tools::getValue('p', 1)));
            $search_total = Search::find($this->context->language->id, $query, 1, 10, 'position', 'desc');
            $search = $this->checkCategories($search_total['result'], $cat_id);
            $total = count($search);
            Hook::exec('actionSearch', array('expr' => $query, 'total' => $total));
            $nbProducts = $total;
            $this->pagination($nbProducts);
            //$this->addColorsToProductList($search['result']);
            $this->context->smarty->assign(array(
                'products' => $search, // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
                'search_products' => $search,
                'nbProducts' => $total,
                'search_query' => $original_query,
                'instant_search' => $this->instant_search,
                'homeSize' => Image::getSize(ImageType::getFormatedName('home'))));
        } elseif (($query = Tools::getValue('search_query', Tools::getValue('ref'))) && !is_array($query)) {
            $this->productSort();
            $this->n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
            $this->p = abs((int)(Tools::getValue('p', 1)));
            $original_query = $query;
            //echo $query; exit;
            $search_total = Search::find($this->context->language->id, $query, 1, 10, 'position', 'desc');
            //print_r($this->context->language->id); exit;
            $search = $this->checkCategories($search_total['result'], $cat_id);
            $total = count($search);
            Hook::exec('actionSearch', array('expr' => $query, 'total' => $total));
            $nbProducts = $total;
            $this->pagination($nbProducts);

            //$this->addColorsToProductList($search['result']);
            $this->context->smarty->assign(array(
                'products' => $search, // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
                'search_products' => $search,
                'nbProducts' => $total,
                'search_query' => $original_query,
                'instant_search' => $this->instant_search,
                'homeSize' => Image::getSize(ImageType::getFormatedName('home'))));
        } elseif (($tag = urldecode(Tools::getValue('tag'))) && !is_array($tag)) {
            $nbProducts = (int)(Search::searchTag($this->context->language->id, $tag, true));
            $this->pagination($nbProducts);
            $result = Search::searchTag($this->context->language->id, $tag, false, $this->p, $this->n, $this->orderBy, $this->orderWay);
            Hook::exec('actionSearch', array('expr' => $tag, 'total' => count($result)));
            //$this->addColorsToProductList($result);
            $this->context->smarty->assign(array(
                'search_tag' => $tag,
                'products' => $result,
                'search_products' => $result,
                'nbProducts' => $nbProducts,
                'homeSize' => Image::getSize(ImageType::getFormatedName('home'))));
        } else {
            $this->context->smarty->assign(array(
                'products' => array(),
                'search_products' => array(),
                'pages_nb' => 1,
                'nbProducts' => 0));
        }
        $this->context->smarty->assign(array('add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'), 'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM')));

        $this->setTemplate('search.tpl');
    }

    public function displayHeader()
    {
        if (!$this->instant_search && !$this->ajax_search) {
            parent::displayHeader();
        } else {
            $this->context->smarty->assign('static_token', Tools::getToken(false));
        }
    }

    public function displayFooter()
    {
        if (!$this->instant_search && !$this->ajax_search) {
            parent::displayFooter();
        }
    }

    public function checkCategories($searchs, $id_category)
    {
        $getids = array();
        $getid = array();
        $checkId = array();

        foreach ($searchs as $key => $search) {
            $query = 'SELECT DISTINCT p.id_product
                FROM '._DB_PREFIX_.'product p
                LEFT JOIN '._DB_PREFIX_.'category_product catp ON (catp.id_product = p.id_product)
                WHERE p.id_product = '.$search['id_product'];
            if ((int)$id_category > 0) {
                $query .= ' AND catp.id_category = '.$id_category;
            }
            //echo $query; exit;
            $getid[$key] = Db::getInstance()->executeS($query);
            if ($getid[$key] != null) {
                $getids[$key] = $getid[$key][0];
            }
        }
        foreach ($searchs as $key => $search) {
            foreach ($getids as $number => $get_id) {
                if ($search['id_product'] == $get_id['id_product']) {
                    $checkId[$number] = $search;
                }
            }
        }
        return $checkId;
    }
}
