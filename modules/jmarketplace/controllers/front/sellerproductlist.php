<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class JmarketplaceSellerproductlistModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $nbProducts;
    public $cat_products;
    public $seller;

    public function setMedia() {   
        parent::setMedia(); 
        
        if (version_compare(_PS_VERSION_, '1.6.0', '<')) {
            $this->addCSS(_PS_BASE_URL_.__PS_BASE_URI__.'category.css', 'all');
            $this->addCSS(_PS_BASE_URL_.__PS_BASE_URI__.'product_list.css', 'all');
        }
        else if (version_compare(_PS_VERSION_, '1.6.0', '>') && version_compare(_PS_VERSION_, '1.7.0', '<')) {
            $this->addCSS(_THEME_CSS_DIR_.'category.css', 'all');
            $this->addCSS(_THEME_CSS_DIR_.'product_list.css', 'all');
        } 
        else {
            $this->addCSS(_MODULE_DIR_.$this->module->name.'/views/css/product_list.css', 'all');
        }
    }

    public function initContent() {
        
        parent::initContent();
        
        if(!Tools::getValue('id_seller'))
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $id_seller = (int)Tools::getValue('id_seller');
        $this->seller = new Seller($id_seller);
        
        if (version_compare(_PS_VERSION_, '1.7', '<')) 
            $this->productSort();
        
        $this->assignProductList();

        $param = array('id_seller' => $this->seller->id, 'link_rewrite' => $this->seller->link_rewrite);
        
	if (version_compare(_PS_VERSION_, '1.6.0.12', '>'))
            $url_seller_profile = $this->module->getJmarketplaceLink('jmarketplace_seller_rule', $param);
        else
            $url_seller_profile = $this->context->link->getModuleLink('jmarketplace', 'sellerprofile', $param);
        
        if ($this->products) {
            $i = 0;
            foreach ($this->products as $p) {
                $product = new Product($p['id_product']);
                $this->products[$i]['regular_price'] = Tools::displayPrice($product->getPrice(true, null, 6, null, false, false, 1), $this->context->currency->id);
                $this->products[$i]['final_price'] = Tools::displayPrice($product->getPrice(true, null, 6, null, false, true, 1), $this->context->currency->id);
                $i++;
            }  
        }

        $this->context->smarty->assign(array(
            'seller' => $this->seller,
            'products' => (isset($this->products) && $this->products) ? $this->products : null,
            'seller_link' => $url_seller_profile,
            'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            //'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
            'allow_oosp' => (int)Configuration::get('PS_ORDER_OUT_OF_STOCK'),
            'comparator_max_item' => (int)Configuration::get('PS_COMPARATOR_MAX_ITEM'),
        ));
        
        $ps_version = $this->module->getPrestaShopVersion();
        $this->context->smarty->assign('ps_version', $ps_version);
        
        if ($ps_version == '1.7') {
            $this->context->smarty->assign(array(
                'navigationPipe' => '/',
                'page_name' => $this->context->controller->page_name,
                'PS_CATALOG_MODE' => Configuration::get('PS_CATALOG_MODE'),
                'PS_STOCK_MANAGEMENT' => Configuration::get('PS_STOCK_MANAGEMENT'),
                'priceDisplay' => 1,
                'tpl_name' => 'sellerproductlist'
            ));
            
             $this->setTemplate('module:jmarketplace/views/templates/front/page.tpl');
        }
        else {
            $this->setTemplate('sellerproductlist.tpl');
        }
    }    
    
    protected function assignProductList()
    {    
        $this->nbProducts = $this->seller->getNumActiveProducts();

        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            $this->pagination((int)$this->nbProducts); // Pagination must be call after "getProducts"
        }
        else {
            $this->orderBy = 'p.date_add';
            $this->orderWay = 'ASC';
        }
        
        if (!Tools::getValue('p'))
            $this->p = 0;
        else 
            $this->p = (int)(Tools::getValue('p')-1) * Configuration::get('PS_PRODUCTS_PER_PAGE');

        $this->n = Configuration::get('PS_PRODUCTS_PER_PAGE');
        
        if (version_compare(_PS_VERSION_, '1.7', '>'))
            $this->n = 9999;

        $this->products = $this->seller->getProducts($this->context->language->id, $this->p, $this->n, $this->orderBy, $this->orderWay, false, true);
        
        if (version_compare(_PS_VERSION_, '1.7', '>')) {
            if ($this->products) {
                $i = 0;
                foreach ($this->products as $p) {
                    //PrestaShop 1.7
                    $product = new Product($p['id_product']);
                    $this->products[$i]['regular_price'] = Tools::displayPrice($product->getPrice(true, null, 6, null, false, false, 1), $this->context->currency->id);
                    $this->products[$i]['price'] = Tools::displayPrice($product->getPrice(true, null, 6, null, false, true, 1), $this->context->currency->id);

                    if (is_array($this->products[$i]['specific_prices'])) 
                        $this->products[$i]['price_without_reduction'] = Tools::displayPrice($this->products[$i]['price_without_reduction'], $this->context->currency->id);
                    $i++;
                }  
            }
        }

        $this->context->smarty->assign(array(
            'pages_nb' => ceil($this->nbProducts / (int)$this->n),
            'nb_products' => $this->nbProducts,
        ));
    }
}