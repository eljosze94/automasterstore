<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class JmarketplaceDashboardModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    
    public function setMedia() {
        parent::setMedia();
        
        $this->addJqueryUI('ui.datepicker');
        //$this->context->controller->addJS(_MODULE_DIR_.$this->module->name.'/views/js/Chart.bundle.min.js', 'all');
        $this->context->controller->addJS(_MODULE_DIR_.$this->module->name.'/views/js/dashboard.js', 'all');
    }

    public function initContent() {
        
        parent::initContent();
        
        if(!$this->context->cookie->id_customer)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        if (Configuration::get('JMARKETPLACE_SHOW_DASHBOARD') == 0)
            Tools::redirect($this->context->link->getModuleLink('jmarketplace', 'selleraccount', array(), true));
        
        $is_seller = Seller::isSeller($this->context->cookie->id_customer, $this->context->shop->id);
        if (!$is_seller)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $id_seller = Seller::getSellerByCustomer($this->context->cookie->id_customer);
        
        $seller = new Seller($id_seller);
        
        if ($seller->active == 0)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $param = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);
        $url_seller_profile = $this->module->getJmarketplaceLink('jmarketplace_seller_rule', $param);
        
        $chart_label = '';
        $chart_data = '';
        $from = explode(' ', $seller->date_add);
        $from = $from[0].' 00:00:00';        
        $to = date('Y-m-d').' 23:59:59';
        
        if (Tools::isSubmit('submitFilterDate')) {
            $from = Tools::getValue('from').' 00:00:00';
            $to = Tools::getValue('to').' 23:59:59';
        }
        
        $commissions_chart = array();
        //$current_time = 0;
        $current_date_from_time = strtotime('+0 month', strtotime($from));
        $current_date_from = date('Y-m' , $current_date_from_time);
        $commissions_chart[$current_date_from] = 0;
        
        $current_date_to_time = strtotime('+0 month', strtotime($to));
        $current_date_to = date('Y-m' , $current_date_to_time);
        
        $i = 1;
        if ($current_date_from != $current_date_to) {
            while ($current_date_from != $current_date_to) {
                $current_date_from_time = strtotime('+'.$i.' month', strtotime($from));
                $current_date_from = date('Y-m' , $current_date_from_time);
                $commissions_chart[$current_date_from] = 0;
                $i++;
            }
        }
        
        $ids_not_in_order_states = '';
        
        $states = OrderState::getOrderStates($this->context->language->id);
        foreach ($states as $state) {
            if (Configuration::get('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']) == 1)
                $ids_not_in_order_states .= $state['id_order_state'].',';
        }

        $ids_not_in_order_states = Tools::substr($ids_not_in_order_states, 0, -1);
        
        //$commissions = Dashboard::getCommissionHistoryBySeller($id_seller, $this->context->language->id, $this->context->shop->id, $from, $to);
        $seller_orders = SellerOrder::getSellerOrdersByDate($id_seller, $this->context->language->id, $from, $to, $ids_not_in_order_states);
        
        if ($seller_orders) {
            $i = 0;
            foreach ($seller_orders as $o) {
                $seller_orders[$i]['total_paid_tax_incl'] = $o['total_paid_tax_incl'];
                $seller_orders[$i]['total_paid_tax_excl'] = $o['total_paid_tax_excl'];
                $i++;
            }  
        }
        
        //echo '<pre>'; print_r($seller_orders);
        
        if (Configuration::get('JMARKETPLACE_TAX_COMMISSION') == 1)
            $field_commission = 'total_paid_tax_incl';
        else
            $field_commission = 'total_paid_tax_excl';
        
        if (is_array($seller_orders) && count($seller_orders) > 0) {
            foreach ($seller_orders as $so) {
                $date_add_parts = explode(' ', $so['date_add']);
                $date_add_parts = explode('-', $date_add_parts[0]);
                
                $index = $date_add_parts[0].'-'.$date_add_parts[1];
                
                if (array_key_exists($index, $commissions_chart)) 
                    $commissions_chart[$index] = Tools::ps_round($commissions_chart[$index] + $so[$field_commission], 2);
                else 
                    $commissions_chart[$index] = Tools::ps_round($so[$field_commission], 2);
            }
            
            ksort($commissions_chart);
            
            //echo '<pre>'; print_r($commissions_chart);
            
            foreach ($commissions_chart as $key => $value) {
                $chart_label .= "'".$key."',";
                $chart_data .= (float)$value.',';
            }
            
            $sales = SellerOrderDetail::getTotalPriceBySeller($id_seller, $from, $to, $ids_not_in_order_states);
            $num_orders = SellerOrder::getNumOrdersBySeller($id_seller, $from, $to, $ids_not_in_order_states);
            $cart_value = $sales / $num_orders;
            $num_products = SellerOrderDetail::getProductQuantityBySeller($id_seller, $from, $to, $ids_not_in_order_states);
            $commission = SellerCommission::getCommissionBySeller($id_seller);
            $benefits = SellerOrder::getTotalPaidBySeller($id_seller, $from, $to, $ids_not_in_order_states);
        }
        else {
            $sales = 0;
            $num_orders = 0;
            $cart_value = 0;
            $num_products = 0;
            $commission = SellerCommission::getCommissionBySeller($id_seller);
            $benefits = 0;
        }      
        
        $from = explode(' ', $from);
        $from = $from[0];
        $to = explode(' ', $to);
        $to = $to[0];

        $orders = SellerOrder::getLastSellerOrders($id_seller, $this->context->language->id);

        if ($orders) {
            $i = 0;
            foreach ($orders as $o) {
                $orders[$i]['total_paid_tax_incl'] = Tools::displayPrice($o['total_paid_tax_incl']);
                $orders[$i]['total_paid_tax_excl'] = Tools::displayPrice($o['total_paid_tax_excl']);
                $orders[$i]['total_discounts_tax_incl'] = Tools::displayPrice($o['total_discounts_tax_incl']);
                $orders[$i]['total_discounts_tax_excl'] = Tools::displayPrice($o['total_discounts_tax_excl']);
                $params = array('id_order' => $o['id_order']);
                $orders[$i]['link'] = $this->context->link->getModuleLink('jmarketplace', 'orders', $params, true);
                $i++;
            }  
        }
        
        //echo '$chart_label: '.$chart_label.'<br/>';
        //echo '$chart_data: '.$chart_data.'<br/>';

        $this->context->smarty->assign(array(
            'seller_link' => $url_seller_profile,
            'show_import_product' => Configuration::get('JMARKETPLACE_SELLER_IMPORT_PROD'),
            'show_orders' => Configuration::get('JMARKETPLACE_SHOW_ORDERS'),
            'show_contact' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
            'show_edit_seller_account' => Configuration::get('JMARKETPLACE_SHOW_EDIT_ACCOUNT'),
            'show_manage_orders' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS'),
            'show_manage_carriers' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER'),
            'show_dashboard' => Configuration::get('JMARKETPLACE_SHOW_DASHBOARD'),
            'show_seller_invoice' => Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE'),
            'show_menu_top' => Configuration::get('JMARKETPLACE_MENU_TOP'),
            'show_menu_options' => Configuration::get('JMARKETPLACE_MENU_OPTIONS'),
            'sales' => Tools::displayPrice($sales, $this->context->currency->id),
            'num_orders' => $num_orders,
            'cart_value' => Tools::displayPrice($cart_value, $this->context->currency->id),
            'num_products' => $num_products,
            'commission' => $commission,
            'benefits' => Tools::displayPrice($benefits, $this->context->currency->id),
            'chart_label' => $chart_label,
            'chart_data' => $chart_data,
            'currency_iso_code' => $this->context->currency->iso_code,
            'from' => $from,
            'to' => $to,
            'tax_commission' => Configuration::get('JMARKETPLACE_TAX_COMMISSION'),
            'orders' => $orders,
            'mesages_not_readed' => SellerIncidenceMessage::getNumMessagesNotReadedBySeller($id_seller),
            'chart_js' => _MODULE_DIR_.$this->module->name.'/views/js/Chart.bundle.min.js',
        ));
        
        if (Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE') == 1) {
            $total_funds = 0;
            $orders = SellerTransferCommission::getCommissionHistoryBySeller($id_seller, (int)$this->context->language->id, (int)$this->context->shop->id);

            if (is_array($orders) && count($orders) > 0) {
                foreach ($orders as $o) {
                    if (SellerTransferCommission::isSellerTransferCommission($o['id_seller_commission_history']) == 0) {
                        if (Configuration::get('JMARKETPLACE_TAX_COMMISSION') == 1)
                            $total_funds = $total_funds + $o['total_commission_tax_incl'];
                        else
                            $total_funds = $total_funds + $o['total_commission_tax_excl'];
                    }
                }          
            }

            $this->context->smarty->assign('total_funds', Tools::displayPrice($total_funds, $this->context->currency->id));
        }
        
        $ps_version = $this->module->getPrestaShopVersion();
        $this->context->smarty->assign('ps_version', $ps_version);
        
        if ($ps_version == '1.7') {
            $this->context->smarty->assign(array(
                'navigationPipe' => '/',
                'tpl_name' => 'dashboard'
            ));
            
            $this->setTemplate('module:jmarketplace/views/templates/front/page.tpl');
        }
        else {
            $this->setTemplate('dashboard.tpl');
        } 
    }
}