<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class JmarketplaceOrdersModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    
    public function postProcess() {
        if (Tools::isSubmit('submitState')) {
            $id_seller = Seller::getSellerByCustomer($this->context->cookie->id_customer);
            $seller = new Seller($id_seller);
            $id_order = (int)Tools::getValue('id_order');
            $id_seller_order = SellerOrder::getIdSellerOrderByOrderAndSeller($id_order, $id_seller);
            $id_order_state = (int)Tools::getValue('id_order_state');
            $order = new SellerOrder($id_seller_order);
            $order_state = new OrderState($id_order_state);

            if (!Validate::isLoadedObject($order_state)) {
                $this->errors[] = Tools::displayError('The new order status is invalid.');
            } 
            else {
                //$current_order_state = $order->getCurrentOrderState();
                if ($order->current_state != $order_state->id) {
                    // Create new OrderHistory
                    /*$history = new OrderHistory();
                    $history->id_order = $order->id;
                    $history->id_employee = 1;

                    $use_existings_payment = false;
                    if (!$order->hasInvoice()) {
                        $use_existings_payment = true;
                    }
                    $history->changeIdOrderState((int)$order_state->id, $order, $use_existings_payment);*/
                    
                    //update history commissions
                    $states = OrderState::getOrderStates($this->context->language->id);
                    $cancel_commissions = false;
                    foreach ($states as $state) {
                        if (Configuration::get('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']) == 1 && $id_order_state == $state['id_order_state'])
                            $cancel_commissions = true;
                    }
                    
                    //si toca cancelar comisiones
                    if ($cancel_commissions) 
                        SellerCommissionHistory::changeStateCommissionsByOrder($id_order, 'cancel');
                    
                    // Create new SellerOrderHistory
                    $seller_order_history = new SellerOrderHistory();
                    $seller_order_history->id_seller_order = SellerOrder::getIdSellerOrderByOrderAndSeller($id_order, $id_seller);
                    $seller_order_history->id_seller = $id_seller;
                    $seller_order_history->id_order_state = $id_order_state;
                    $seller_order_history->add();
                    
                    $seller_order = new SellerOrder($seller_order_history->id_seller_order);
                    $seller_order->current_state = $id_order_state;
                    $seller_order->update();
                    
                    //send email to administrator when seller change order status
                    if (Configuration::get('JMARKETPLACE_SEND_ORDER_CHANGED') == 1) {
                        $id_seller_email = false;
                        $to = Configuration::get('JMARKETPLACE_SEND_ADMIN');
                        $to_name = Configuration::get('PS_SHOP_NAME');
                        $from = Configuration::get('PS_SHOP_EMAIL');
                        $from_name = Configuration::get('PS_SHOP_NAME');

                        $template = 'base';
                        $reference = 'seller-order-changed';
                        $id_seller_email = SellerEmail::getIdByReference($reference);

                        if ($id_seller_email) {
                            $seller_email = new SellerEmail($id_seller_email, Configuration::get('PS_LANG_DEFAULT'));
                            $vars = array("{shop_name}", "{seller_name}", "{seller_shop}", "{order_reference}");
                            $values = array(Configuration::get('PS_SHOP_NAME'), $seller->name, $seller->shop, $seller_order->reference);
                            $subject_var = $seller_email->subject; 
                            $subject_value = str_replace($vars, $values, $subject_var);
                            $content_var = $seller_email->content;
                            $content_value = str_replace($vars, $values, $content_var);

                            $template_vars = array(
                                '{content}' => $content_value,
                                '{shop_name}' => Configuration::get('PS_SHOP_NAME')
                            );

                            $iso = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));

                            if (file_exists(dirname(__FILE__).'/../../mails/'.$iso.'/'.$template.'.txt') && file_exists(dirname(__FILE__).'/../../mails/'.$iso.'/'.$template.'.html')) {
                                Mail::Send(
                                    Configuration::get('PS_LANG_DEFAULT'),
                                    $template,
                                    $subject_value,
                                    $template_vars,
                                    $to,
                                    $to_name,
                                    $from,
                                    $from_name,
                                    null,
                                    null,
                                    dirname(__FILE__).'/../../mails/'
                                );
                            }
                        }
                    }

                    /*$carrier = new Carrier($order->id_carrier, $order->id_lang);
                    $templateVars = array();
                    if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number) {
                        $templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
                    }

                    // Save all changes
                    if ($history->addWithemail(true, $templateVars)) {
                        // synchronizes quantities if needed..
                        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                            foreach ($order->getProducts() as $product) {
                                if (StockAvailable::dependsOnStock($product['product_id'])) {
                                    StockAvailable::synchronize($product['product_id'], $product['id_shop']);
                                }
                            }
                        }

                        $params = array('id_order' => $id_order);
                        Tools::redirect($this->context->link->getModuleLink('jmarketplace', 'orders', $params, true));
                    }*/
                }
            }
        }
        
        if (Tools::getValue('action') == 'generateInvoicePDF') {
            $id_order = (int)Tools::getValue('id_order');
            $this->generateInvoicePDFByIdOrder($id_order);
        }
        
        if (Tools::isSubmit('submitShippingNumber')) {
            
            $tracking_number = Tools::getValue('tracking_number');
            $id_order_carrier = Tools::getValue('id_order_carrier');
            $id_order = (int)Tools::getValue('id_order');
            $order = new Order($id_order);
            $order_carrier = new OrderCarrier($id_order_carrier);

            if (!Validate::isLoadedObject($order_carrier)) 
                $this->errors[] = $this->module->l('The order carrier ID is invalid.', 'orders');
            
            if (!Validate::isLoadedObject($order)) 
                $this->errors[] = $this->module->l('The order ID is invalid.', 'orders');
            
            if (!Validate::isTrackingNumber($tracking_number)) 
                $this->errors[] = $this->module->l('The tracking number is incorrect.', 'orders');

            if (count($this->errors) > 0) { 
                $this->context->smarty->assign(array(
                    'errors' => $this->errors,
                )); 
            }
            else {
                // update shipping number
                $order->shipping_number = $tracking_number;
                $order->update();
                
                // Update order_carrier
                $order_carrier->tracking_number = pSQL($tracking_number);
                if ($order_carrier->update()) {
                    $customer = new Customer((int)$order->id_customer);
                    $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);
                    
                    $templateVars = array(
                        '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname,
                        '{id_order}' => $order->id,
                        '{shipping_number}' => $order->shipping_number,
                        '{order_name}' => $order->getUniqReference()
                    );
                    
                    if (@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Package in transit', (int)$order->id_lang), $templateVars,
                        $customer->email, $customer->firstname.' '.$customer->lastname, null, null, null, null,
                        _PS_MAIL_DIR_, true, (int)$order->id_shop)) {
                        $params = array('id_order' => $id_order);
                        Tools::redirect($this->context->link->getModuleLink('jmarketplace', 'orders', $params, true));
                    } 
                }   
            }
        }
    }

    public function initContent() {
        
        parent::initContent();
        
        if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS') == 0)
            Tools::redirect($this->context->link->getModuleLink('jmarketplace', 'selleraccount', array(), true));
        
        if(!$this->context->cookie->id_customer)
            Tools::redirect($this->context->link->getPageLink('my-account', true));

        $id_seller = Seller::getSellerByCustomer($this->context->cookie->id_customer);
        $is_seller = Seller::isSeller($this->context->cookie->id_customer, $this->context->shop->id);
        
        if (!$is_seller)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $seller = new Seller($id_seller);
        
        if ($seller->active == 0)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $param = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);
        $url_seller_profile = $this->module->getJmarketplaceLink('jmarketplace_seller_rule', $param);
       
        if (!Tools::getValue('id_order')) {
            /*if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 1) {
                $orders = SellerOrder::getOrdersBySeller($id_seller, $this->context->language->id);
            }   
            else {*/
                $orders = SellerOrder::getSellerOrders($id_seller, $this->context->language->id);
            //}
            
            //echo '<pre>'; print_r($orders);
            
            if ($orders) {
                $i = 0;
                foreach ($orders as $o) {
                    $orders[$i]['total_paid_tax_incl'] = Tools::displayPrice($o['total_paid_tax_incl']);
                    $orders[$i]['total_paid_tax_excl'] = Tools::displayPrice($o['total_paid_tax_excl']);
                    $orders[$i]['total_discounts_tax_incl'] = Tools::displayPrice($o['total_discounts_tax_incl']);
                    $orders[$i]['total_discounts_tax_excl'] = Tools::displayPrice($o['total_discounts_tax_excl']);
                    $params = array('id_order' => $o['id_order']);
                    $orders[$i]['link'] = $this->context->link->getModuleLink('jmarketplace', 'orders', $params, true);
                    $i++;
                }  
            }

            $this->context->smarty->assign(array(
                'seller_link' => $url_seller_profile,
                'show_import_product' => Configuration::get('JMARKETPLACE_SELLER_IMPORT_PROD'),
                'show_orders' => Configuration::get('JMARKETPLACE_SHOW_ORDERS'),
                'show_manage_orders' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS'),
                'show_manage_carriers' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER'),
                'show_edit_seller_account' => Configuration::get('JMARKETPLACE_SHOW_EDIT_ACCOUNT'),
                'show_contact' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
                'show_dashboard' => Configuration::get('JMARKETPLACE_SHOW_DASHBOARD'),
                'show_seller_invoice' => Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE'),
                'show_menu_top' => Configuration::get('JMARKETPLACE_MENU_TOP'),
                'show_menu_options' => Configuration::get('JMARKETPLACE_MENU_OPTIONS'),
                'tax_commission' => Configuration::get('JMARKETPLACE_TAX_COMMISSION'),
                'orders' => $orders,
                'mesages_not_readed' => SellerIncidenceMessage::getNumMessagesNotReadedBySeller($id_seller),
            ));
            
            if (Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE') == 1) {
                $total_funds = 0;
                $orders = SellerTransferCommission::getCommissionHistoryBySeller($id_seller, (int)$this->context->language->id, (int)$this->context->shop->id);

                if (is_array($orders) && count($orders) > 0) {
                    foreach ($orders as $o) {
                        if (SellerTransferCommission::isSellerTransferCommission($o['id_seller_commission_history']) == 0) {
                            if (Configuration::get('JMARKETPLACE_TAX_COMMISSION') == 1)
                                $total_funds = $total_funds + $o['total_commission_tax_incl'];
                            else
                                $total_funds = $total_funds + $o['total_commission_tax_excl'];
                        }
                    }          
                }

                $this->context->smarty->assign('total_funds', Tools::displayPrice($total_funds, $this->context->currency->id));
            }
        
            $ps_version = $this->module->getPrestaShopVersion();
            $this->context->smarty->assign('ps_version', $ps_version);

            if ($ps_version == '1.7') {
                $this->context->smarty->assign(array(
                    'navigationPipe' => '/',
                    'tpl_name' => 'orders'
                ));
                
                $this->setTemplate('module:jmarketplace/views/templates/front/page.tpl');
            }
            else {
                $this->setTemplate('orders.tpl');
            }  
        }
        else {
            $id_order = (int)Tools::getValue('id_order');
            
            if (!SellerOrder::existSellerOrder($id_order, $id_seller))
                Tools::redirect($this->context->link->getModuleLink('jmarketplace', 'orders', array(), true));
            
            $commission_history_by_order = SellerCommissionHistory::getCommissionHistoryByOrder($id_order, $this->context->language->id, $this->context->shop->id);
            
            if (!$commission_history_by_order)
                Tools::redirect($this->context->link->getModuleLink('jmarketplace', 'orders', array(), true));
            
            /*if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 1) {
                $order = new Order($id_order);
            }
            else {*/
                $id_seller_order = SellerOrder::getIdSellerOrderByOrderAndSeller($id_order, $id_seller);
                $order = new SellerOrder($id_seller_order);
            //}
            
            $customer = new Customer($order->id_customer);
            $address_delivery = new Address($order->id_address_delivery);
            //$address_invoice = new Address($order->id_address_invoice);
            
            //$inv_adr_fields = AddressFormat::getOrderedAddressFields($address_invoice->id_country);
            $dlv_adr_fields = AddressFormat::getOrderedAddressFields($address_delivery->id_country);
            
            //$invoiceAddressFormatedValues = AddressFormat::getFormattedAddressFieldsValues($address_invoice, $inv_adr_fields);
            $deliveryAddressFormatedValues = AddressFormat::getFormattedAddressFieldsValues($address_delivery, $dlv_adr_fields);
            
            $params_order = array('id_order' => $id_order);
            //$params_order_invoice = array('id_order' => $id_order, 'action' => 'generateInvoicePDF');
            
            $products = $order->getProductsDetail();
            $i = 0;
            foreach ($products as $product) { 
                $products[$i]['current_stock'] = StockAvailable::getQuantityAvailableByProduct($product['product_id'], $product['product_attribute_id'], $product['id_shop']);
                //$products[$i]['unit_price_tax_incl'] = Tools::displayPrice($product['unit_price_tax_incl']);
                //$products[$i]['total_price_tax_incl'] = Tools::displayPrice($product['total_price_tax_incl']);
                $products[$i]['unit_price_tax_incl'] = Tools::displayPrice($product['unit_price_tax_incl']);
                $products[$i]['unit_price_tax_excl'] = Tools::displayPrice($product['unit_price_tax_excl']);
                $products[$i]['total_price_tax_incl'] = Tools::displayPrice($product['total_price_tax_incl']);
                $products[$i]['total_price_tax_excl'] = Tools::displayPrice($product['total_price_tax_excl']);
                $products[$i]['unit_commission_tax_incl'] = Tools::displayPrice($product['unit_commission_tax_incl']);
                $products[$i]['unit_commission_tax_excl'] = Tools::displayPrice($product['unit_commission_tax_excl']);
                $products[$i]['total_commission_tax_incl'] = Tools::displayPrice($product['total_commission_tax_incl']);
                $products[$i]['total_commission_tax_excl'] = Tools::displayPrice($product['total_commission_tax_excl']);
                $products[$i]['reduction_amount_display'] = Tools::displayPrice($product['reduction_amount']);
                $i++;
            }
            
            $order_states = OrderState::getOrderStates($this->context->language->id);
            if (is_array($order_states) && count($order_states)) {
                foreach ($order_states as $key => $state) {
                    if (Configuration::get('JMARKETPLACE_SELL_ORDER_STATE_'.$state['id_order_state']) == 0)
                        unset($order_states[$key]);
                }
            }
            
            $this->context->smarty->assign(array(
                'seller_link' => $url_seller_profile,
                'show_import_product' => Configuration::get('JMARKETPLACE_SELLER_IMPORT_PROD'),
                'show_orders' => Configuration::get('JMARKETPLACE_SHOW_ORDERS'),
                'show_manage_orders' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS'),
                'show_manage_carriers' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER'),
                'show_contact' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
                'show_edit_seller_account' => Configuration::get('JMARKETPLACE_SHOW_EDIT_ACCOUNT'),
                'show_dashboard' => Configuration::get('JMARKETPLACE_SHOW_DASHBOARD'),
                'show_seller_invoice' => Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE'),
                'show_menu_top' => Configuration::get('JMARKETPLACE_MENU_TOP'),
                'show_menu_options' => Configuration::get('JMARKETPLACE_MENU_OPTIONS'),
                'tax_commission' => Configuration::get('JMARKETPLACE_TAX_COMMISSION'),
                'shipping_commission' => Configuration::get('JMARKETPLACE_SHIPPING_COMMISSION'),
                'fixed_commission' => Tools::displayPrice($order->total_fixed_commission),
                'order_link' => $this->context->link->getModuleLink('jmarketplace', 'orders', $params_order, true),
                //'order_invoice_link' => $this->context->link->getModuleLink('jmarketplace', 'orders', $params_order_invoice, true),
                'order' => $order,
                'order_state_history' => $order->getHistory($this->context->language->id),
                'order_shipping' => $order->getShipping(),
                'ps_weight_unit' => Configuration::get('PS_WEIGHT_UNIT'),
                'order_states' => $order_states,
                'customer_name' => $customer->firstname.' '.$customer->lastname,
                'address_delivery' => $deliveryAddressFormatedValues,
                //'address_invoice' => $invoiceAddressFormatedValues,
                'products' => $products,
                //'total_products' => Tools::displayPrice($order->getTotalProductsWithTaxes()),
                //'total_products' => Tools::displayPrice($order->total_paid),
                'total_weight' => $order->getTotalWeight(),
                //'total_paid' => Tools::displayPrice($order->total_paid),
                'total_products_tax_incl' => Tools::displayPrice($order->total_products_tax_incl),
                'total_products_tax_excl' => Tools::displayPrice($order->total_products_tax_excl),
                'total_paid_tax_incl' => Tools::displayPrice($order->total_paid_tax_incl),
                'total_paid_tax_excl' => Tools::displayPrice($order->total_paid_tax_excl),
                'total_discounts' => Tools::displayPrice($order->total_discounts),
                'total_discounts_tax_excl' => Tools::displayPrice($order->total_discounts_tax_excl),
                'total_discounts_tax_incl' => Tools::displayPrice($order->total_discounts_tax_incl),
                'total_shipping' => $order->total_shipping,
                'total_shipping_tax_incl' => Tools::displayPrice($order->total_shipping_tax_incl),
                'total_shipping_tax_excl' => Tools::displayPrice($order->total_shipping_tax_excl),
                'weight_unit' => Configuration::get('PS_WEIGHT_UNIT'),
                //'commision' => SellerCommision::getCommisionBySeller($id_seller, $id_seller),
                //'total_commision' => Tools::displayPrice($this->getCommisionByOrder($id_order, $id_seller)),
                'mesages_not_readed' => SellerIncidenceMessage::getNumMessagesNotReadedBySeller($id_seller),
                'sign' => $this->context->currency->sign,
            ));
            
            if (Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE') == 1) {
                $total_funds = 0;
                $orders = SellerTransferCommission::getCommissionHistoryBySeller($id_seller, (int)$this->context->language->id, (int)$this->context->shop->id);

                if (is_array($orders) && count($orders) > 0) {
                    foreach ($orders as $o) {
                        if (SellerTransferCommission::isSellerTransferCommission($o['id_seller_commission_history']) == 0) {
                            if (Configuration::get('JMARKETPLACE_TAX_COMMISSION') == 1)
                                $total_funds = $total_funds + $o['total_commission_tax_incl'];
                            else
                                $total_funds = $total_funds + $o['total_commission_tax_excl'];
                        }
                    }          
                }

                $this->context->smarty->assign('total_funds', Tools::displayPrice($total_funds, $this->context->currency->id));
            }
            
            $ps_version = $this->module->getPrestaShopVersion();
            $this->context->smarty->assign('ps_version', $ps_version);

            if ($ps_version == '1.7') {
                $this->context->smarty->assign(array(
                    'navigationPipe' => '/',
                    'tpl_name' => 'order'
                ));
                
                $this->setTemplate('module:jmarketplace/views/templates/front/page.tpl');
            }
            else {
                $this->setTemplate('order.tpl');
            }  
        }
    }
    
    public function getCommisionByOrder($id_order, $id_seller) {
        return Db::getInstance()->getValue('SELECT SUM(commision) FROM '._DB_PREFIX_.'seller_commision_history WHERE id_order = '.(int)$id_order.' AND id_seller = '.(int)$id_seller);
    }
    
    /*public function generateInvoicePDFByIdOrder($id_order)
    {
        $order = new Order((int)$id_order);

        $order_invoice_list = $order->getInvoicesCollection();
        
        $pdf = new PDF($order_invoice_list, PDF::TEMPLATE_INVOICE, Context::getContext()->smarty);
        $pdf->render();
    }*/
}