{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}
{block name="override_tpl"}
<div class="panele">
    {if isset($confirmation) && $confirmation}
        <p class="alert alert-success">{l s='Your response has been successfully sent.' mod='jmarketplace'}</p>
    {/if}
    <table id="incidence-list" class="table table-bordered footab">
        <tr>
            <td class="first_item">{l s='Reference' mod='jmarketplace'} </td>
            <td class="item">{$incidence->reference|escape:'html':'UTF-8'}</td>
        </tr>
        <tr>
            <td class="first_item">{l s='Date add' mod='jmarketplace'} </td>
            <td class="item">{$incidence->date_add|escape:'html':'UTF-8'}</td>
        </tr>
        <tr>
            <td class="first_item">{l s='Date updated' mod='jmarketplace'} </td>
            <td class="item">{$incidence->date_upd|escape:'html':'UTF-8'}</td>
        </tr>
    </table><br/>
</div>
<div class="panel">
    <h3>{l s='Messages' mod='jmarketplace'} <span class="badge">{count($messages)|intval}</span></h3>
    
        {if $messages && count($messages)}
            {foreach from=$messages item=message name=messages}
                <div class="panel">
                    <div class="message{if $message.id_seller == 0} customer{else} employee{/if}">
                        <h3 class="author">
                            {if $message.id_seller == 0}
                                {l s='Customer' mod='jmarketplace'} {$message.customer_firstname|escape:'html':'UTF-8'} {$message.customer_lastname|escape:'html':'UTF-8'} - <i class="icon-calendar"></i> - {dateFormat date=$message.date_add full=0} - <i class="icon-time"></i> {$message.date_add|escape:'htmlall':'UTF-8'|substr:11:5}
                            {else}
                                {l s='Seller' mod='jmarketplace'} {$message.seller_name|escape:'html':'UTF-8'} - <i class="icon-calendar"></i> - {dateFormat date=$message.date_add full=0} - <i class="icon-time"></i> {$message.date_add|escape:'htmlall':'UTF-8'|substr:11:5}
                            {/if}
                        </h3>
                        <div class="description">{$message.description|escape:'html':'UTF-8'|nl2br}</div>
                    </div>
                </div>
            {/foreach}
            <div class="panel" style="display:none;">
                <form action="{$url_post|escape:'html':'UTF-8'}" method="post" class="std">                    
                    <fieldset>                 
                        <div class="form-group">
                            <label for="description">
                                {l s='Add response' mod='jmarketplace'} 
                            </label>
                            <textarea class="form-control" name="description" cols="40" rows="7"></textarea>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default pull-right" name="submitResponse" id="incidence_state_form_submit_btn" value="1" type="submit">
				<i class="process-icon-save"></i>{l s='Send' mod='jmarketplace'}
                            </button>
                            <a onclick="window.history.back();" class="btn btn-default" href="index.php?controller=AdminSellerIncidences&amp;token={$token|escape:'html':'UTF-8'}">
				<i class="process-icon-cancel"></i> {l s='Cancel' mod='jmarketplace'}
                            </a>
                        </div>
                    </fieldset>
                </form>
            </div>
        {else}
            <p class="alert alert-info">{l s='There is not messages.' mod='jmarketplace'} </p>
        {/if}
    </div>   
</div>
{/block}
