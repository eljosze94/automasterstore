{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $show_menu_top == 1}
    {include file="./selleraccount.tpl"}
{/if}

{if $ps_version == '1.7'}
    <div class="row">
        <div class="col-md-12 jmarketplace_breadcrumb">
            <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
                {l s='Your account' mod='jmarketplace'}
            </a>
            <span class="navigation-pipe">
                {$navigationPipe|escape:'html':'UTF-8'}
            </span>
            <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
                {l s='Your seller account' mod='jmarketplace'}
            </a>
            <span class="navigation-pipe">
                {$navigationPipe|escape:'html':'UTF-8'}
            </span>
            <span class="navigation_page">
                {l s='Your products' mod='jmarketplace'}
            </span> 
        </div>
    </div>
{else}
    {capture name=path}
        <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
            {l s='Your account' mod='jmarketplace'}
        </a>
        <span class="navigation-pipe">
            {$navigationPipe|escape:'html':'UTF-8'}
        </span>
        <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
            {l s='Your seller account' mod='jmarketplace'}
        </a>
        <span class="navigation-pipe">
            {$navigationPipe|escape:'html':'UTF-8'}
        </span>
        <span class="navigation_page">
            {l s='Your products' mod='jmarketplace'}
        </span>
    {/capture} 
{/if}

{if isset($confirmation) && $confirmation}
    {if $moderate}
        <div class="alert alert-success" style="float:left; width: 100%;">
            {l s='Your product has been successfully saved. It is pending approval.' mod='jmarketplace'} 
        </div>
    {else}
        <div class="alert alert-success" style="float:left; width: 100%;">
            {l s='Your product has been successfully saved.' mod='jmarketplace'} 
        </div>
    {/if}
{/if}        

<div class="row">
    <div class="column col-xs-12 col-sm-12 col-md-3"{if $show_menu_options == 0} style="display:none;"{/if}>
        {include file="./sellermenu.tpl"}
    </div>
    
    <div class="col-xs-12 {if $show_menu_options == 1}col-sm-12 col-md-9{/if}">
        {if $products && count($products) > 0 || $search_query != ''}
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-lg-8">
                        <h4 class="box-title">{l s='Order and Search' mod='jmarketplace'}</h4>
                        <form action="{$link->getModuleLink('jmarketplace', 'sellerproducts', array(), true)|escape:'html':'UTF-8'}" method="post" class="std">
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                    <label>{l s='Order by' mod='jmarketplace'}</label>
                                    <select name="orderby" class="form-control">
                                        <option value="name">{l s='Product name' mod='jmarketplace'}</option>
                                        <option value="date_add"{if $order_by == 'date_add'} selected="selected"{/if}>{l s='Date add' mod='jmarketplace'}</option>
                                        <option value="date_upd"{if $order_by == 'date_upd'} selected="selected"{/if}>{l s='Date update' mod='jmarketplace'}</option>
                                        <option value="active"{if $order_by == 'active'} selected="selected"{/if}>{l s='Status' mod='jmarketplace'}</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-5">
                                    <label>{l s='Order way' mod='jmarketplace'}</label>
                                    <select name="orderway" class="form-control">
                                        <option value="desc"{if $order_way == 'desc'} selected="selected"{/if}>{l s='Descending' mod='jmarketplace'}</option>
                                        <option value="asc"{if $order_way == 'asc'} selected="selected"{/if}>{l s='Ascending' mod='jmarketplace'}</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-3" style="margin-top: 31px;">
                                    <button type="submit" name="submitOrder" class="btn btn-primary">
                                        <span>{l s='Order' mod='jmarketplace'}</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-12 col-md-12 col-lg-4">
                        <form action="{$link->getModuleLink('jmarketplace', 'sellerproducts', array(), true)|escape:'html':'UTF-8'}" method="post" class="std">
                            <div class="input-group input-group-sm search-seller-products">
                                <input type="text" value="{$search_query|escape:'html':'UTF-8'}" name="search_query" id="search_seller_product_query" class="form-control">
                                <span class="input-group-btn">
                                  <button type="submit" class="btn btn-primary btn-flat">{l s='Search' mod='jmarketplace'}</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        {/if}
        
        <div class="box">
            <h1 class="page-subheading">
                {l s='Your products' mod='jmarketplace'} ({$num_products|intval})
                <div class="form-group pull-right">
                    <a href="{$link->getModuleLink('jmarketplace', 'addproduct', array(), true)|escape:'html':'UTF-8'}" class="btn btn-default button button-small">
                        <span><i class="icon-plus fa fa-plus"></i>{l s='Add new product' mod='jmarketplace'}</span>
                    </a>
                </div> 
            </h1>
            {if $ps_version != '1.7'}
                {if isset($errors) && $errors}
                    {include file="./errors.tpl"}
                {/if}
            {/if}
            {if $products && count($products) > 0}
                <form id="form-products" method="post" action="{$link->getModuleLink('jmarketplace', 'sellerproducts', array(), true)|escape:'html':'UTF-8'}">
                    <div class="table-responsive">
                        <table id="order-list" class="table table-bordered footab">
                            <thead>
                                <tr>
                                    {if $show_active_product == 1 || $show_delete_product == 1}<th class="first_item text-center hidden-xs"></th>{/if}
                                    <th class="item text-center">{l s='Image' mod='jmarketplace'}</th>
                                    <th class="item">{l s='Product name' mod='jmarketplace'}</th>
                                    <th class="item">{l s='Date add' mod='jmarketplace'}</th>
                                    <th class="item">{l s='Date update' mod='jmarketplace'}</th>
                                    <th class="item text-center">{l s='Status' mod='jmarketplace'}</th>
                                    <th class="item text-center">{l s='Actions' mod='jmarketplace'}</th>
                                </tr>
                            </thead>
                            <tbody>
                            {foreach from=$products item=product name=sellerproducts}
                                <tr>
                                    {if $show_active_product == 1 || $show_delete_product == 1}
                                        <td class="first_item text-center">
                                            <input name="productBox[]" value="{$product.id_product|intval}" class="ck not_uniform" type="checkbox">
                                        </td>
                                    {/if}
                                    <td class="item text-center">
                                        {if $product.id_image}
                                            <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" />
                                        {else}
                                            <img src="{$img_prod_dir|escape:'html':'UTF-8'}{$lang_iso|escape:'html':'UTF-8'}-default-small_default.jpg" />
                                        {/if}
                                    </td>
                                    <td class="item">
                                        <strong>{$product.name|escape:'html':'UTF-8'}</strong><br/>
                                        <small>{l s='Price' mod='jmarketplace'}: {if $product.price_without_reduction > $product.price}<span class="price_without_reduction">{$product.regular_price|escape:'htmlall':'UTF-8'}</span>{/if} {$product.final_price|escape:'htmlall':'UTF-8'}</small><br/>
                                        <small>{l s='Quantity' mod='jmarketplace'}: {$product.real_quantity|intval} {if $product.real_quantity == 1}{l s='unit' mod='jmarketplace'}{else}{l s='units' mod='jmarketplace'}{/if}</small>
                                    </td>
                                    <td class="item"><i class="icon-calendar fa fa-calendar"></i> - {dateFormat date=$product.date_add full=0} - <i class="icon-time fa fa-clock-o"></i> {$product.date_add|escape:'htmlall':'UTF-8'|substr:11:5}</td>
                                    <td class="item"><i class="icon-calendar fa fa-calendar"></i> - {dateFormat date=$product.date_upd full=0} - <i class="icon-time fa fa-clock-o"></i> {$product.date_upd|escape:'htmlall':'UTF-8'|substr:11:5}</td>
                                    <td class="item text-center">
                                        {if $show_active_product == 1}
                                            {if $product.active == 1}
                                                <a href="{$product.active_product_link|escape:'html':'UTF-8'}">
                                                    {if $ps_version == '1.6' OR $ps_version == '1.7'}
                                                        <i class="icon-check fa fa-check" title="{l s='Disable' mod='jmarketplace'} {$product.name|escape:'html':'UTF-8'}"></i>
                                                    {else}
                                                        <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/status_green.png" title="{l s='Disable' mod='jmarketplace'} {$product.name|escape:'html':'UTF-8'}" />
                                                    {/if}
                                                </a>
                                            {else} 
                                                <a href="{$product.active_product_link|escape:'html':'UTF-8'}">
                                                    {if $ps_version == '1.6' OR $ps_version == '1.7'}
                                                        <i class="icon-times fa fa-times" title="{l s='Enable' mod='jmarketplace'} {$product.name|escape:'html':'UTF-8'}"></i>
                                                    {else}
                                                        <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/status_red.png" title="{l s='Enable' mod='jmarketplace'} {$product.name|escape:'html':'UTF-8'}" />
                                                    {/if}

                                                </a>
                                            {/if}
                                        {else}
                                            {if $product.active == 1}
                                                {if $ps_version == '1.6' OR $ps_version == '1.7'}
                                                    <i class="icon-check fa fa-check" title="{l s='Active' mod='jmarketplace'}"></i>
                                                {else}
                                                    <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/status_green.png" title="{l s='Active' mod='jmarketplace'}" />
                                                {/if}

                                            {else} 
                                                {if $ps_version == '1.6' OR $ps_version == '1.7'}
                                                    <i class="icon-times fa fa-times" title="{l s='Pending approval' mod='jmarketplace'}"></i>
                                                {else}
                                                    <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/status_red.png" title="{l s='Pending approval' mod='jmarketplace'}" />
                                                {/if}
                                            {/if}
                                        {/if}
                                    </td>
                                    <td class="item text-center">
                                        <a class="btn btn-primary btn-xs btn-view" href="{$link->getProductLink($product.id_product)|escape:'html':'UTF-8'}" title="{l s='View' mod='jmarketplace'} {$product.name|escape:'html':'UTF-8'}" target="_blank">
                                            {if $ps_version == '1.6' OR $ps_version == '1.7'}
                                                <i class="icon-eye fa fa-eye"></i> {l s='View' mod='jmarketplace'}
                                            {else}
                                                <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/view.gif" />
                                            {/if}
                                        </a><br/>
                                        {if $show_edit_product == 1}
                                            <a class="btn btn-primary btn-xs btn-edit" href="{$product.edit_product_link|escape:'html':'UTF-8'}" title="{l s='Edit' mod='jmarketplace'} {$product.name|escape:'html':'UTF-8'}">
                                                {if $ps_version == '1.6' OR $ps_version == '1.7'}
                                                    <i class="icon-pencil fa fa-pencil"></i> {l s='Edit' mod='jmarketplace'}
                                                {else}
                                                    <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/edit.gif" />
                                                {/if}
                                            </a><br/>
                                        {/if}
                                        {if $show_delete_product == 1}
                                            <a class="btn btn-primary btn-xs delete_product" href="{$product.delete_product_link|escape:'html':'UTF-8'}" title="{l s='Delete' mod='jmarketplace'} {$product.name|escape:'html':'UTF-8'}" onclick="return confirm('¿Estás seguro de borrar este producto?');">
                                                {if $ps_version == '1.6' OR $ps_version == '1.7'}
                                                    <i class="icon-trash-o fa fa-trash-o"></i> {l s='Delete' mod='jmarketplace'}
                                                {else}
                                                    <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/remove.gif" />
                                                {/if} 
                                            </a>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
                <div class="row" style="margin-top:15px;">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                        {if $show_active_product == 1 || $show_delete_product == 1}
                            <div id="bulk_actions" class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    {l s='Bulk actions' mod='jmarketplace'} <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a id="check_all" href="#"><i class="icon-check-sign fa fa-check-sign"></i> {l s='Select all' mod='jmarketplace'}</a></li>
                                    <li><a id="uncheck_all" href="#"><i class="icon-check-empty fa fa-check-empty"></i> {l s='Unselect all' mod='jmarketplace'}</a></li>
                                    {if $show_active_product == 1}
                                        <li role="separator" class="divider"></li>
                                        <li><a class="bulk_all" id="submitBulkenableSelectionproduct" href="#"><i class="icon-power-off fa fa-power-off text-success"></i> {l s='Enable selection' mod='jmarketplace'}</a></li>
                                        <li><a class="bulk_all" id="submitBulkdisableSelectionproduct" href="#"><i class="icon-power-off fa fa-power-off text-danger"></i> {l s='Disable selection' mod='jmarketplace'}</a></li>
                                    {/if}
                                    {if $show_delete_product == 1}
                                        <li role="separator" class="divider"></li>
                                        <li><a class="bulk_all" id="submitBulkdeleteSelectionproduct" href="#"><i class="icon-trash-o fa fa-trash-o"></i> {l s='Delete selected' mod='jmarketplace'}</a></li>
                                    {/if}
                                </ul>
                            </div>
                         {/if}
                    </div>
                    {if $num_pages > 1}
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <div class="dataTables_paginate paging_simple_numbers pull-right">
                            <ul class="pagination">
                                {if $current_page <= $num_pages}
                                    <li class="paginate_button next">
                                        <a tabindex="0" data-dt-idx="{$current_page - 1|intval}" href="{$link->getModuleLink('jmarketplace', 'sellerproducts', array(), true)|escape:'html':'UTF-8'}?orderby={$order_by|escape:'html':'UTF-8'}&orderway={$order_way|escape:'html':'UTF-8'}&page={$current_page - 1|intval}">{l s='Previus' mod='jmarketplace'}</a>
                                    </li>
                                {/if}
                                {for $foo=1 to $num_pages}
                                    <li class="paginate_button{if $current_page == $foo} active{/if}">
                                        <a tabindex="0" data-dt-idx="{$foo|intval}" href="{$link->getModuleLink('jmarketplace', 'sellerproducts', array(), true)|escape:'html':'UTF-8'}?orderby={$order_by|escape:'html':'UTF-8'}&orderway={$order_way|escape:'html':'UTF-8'}&page={$foo|intval}">{$foo|intval}</a>
                                    </li>
                                {/for}  
                                {if $current_page < $num_pages}
                                    <li class="paginate_button next">
                                        <a tabindex="0" data-dt-idx="{$current_page + 1|intval}" href="{$link->getModuleLink('jmarketplace', 'sellerproducts', array(), true)|escape:'html':'UTF-8'}?orderby={$order_by|escape:'html':'UTF-8'}&orderway={$order_way|escape:'html':'UTF-8'}&page={$current_page + 1|intval}">{l s='Next' mod='jmarketplace'}</a>
                                    </li>
                                {/if}
                            </ul>
                        </div>
                    </div>
                    {/if}
                </div>
                </form>
            {else}
                <p class="alert alert-info">{l s='There are not products.' mod='jmarketplace'} </p>
            {/if}
        </div>
        {include file="./footer.tpl"}
    </div>
</div>

{include file="./varstoscript.tpl"}