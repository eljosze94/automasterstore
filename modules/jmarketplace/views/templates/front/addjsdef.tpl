{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $page_name == 'module-jmarketplace-sellerproducts'}
    {addJsDefL name=confirmProductDelete}{l s='Are you sure you want to delete your product?' mod='jmarketplace' js=1}{/addJsDefL} 
{else}
    {addJsDef editproduct_controller_url = $link->getModuleLink('jmarketplace', 'editproduct', array(), true)|escape:'html':'UTF-8'}
    {addJsDef image_not_available = $image_not_available|escape:'html':'UTF-8'}
    {addJsDef has_attributes = $has_attributes|intval}

    {if $show_attributes == 1}  
        {addJsDef modules_dir=$modules_dir|escape:'quotes':'UTF-8'} 
        {addJsDef token=$token|escape:'quotes':'UTF-8'} 
        {addJsDef id_product=$id_product|intval}
        {addJsDefL name=errorSaveCombination}{l s='You must select an option.' mod='jmarketplace' js=1}{/addJsDefL} 
        {addJsDefL name=errorAttributeGroupEmpty}{l s='You have not selected attribute group.' mod='jmarketplace' js=1}{/addJsDefL} 
        {addJsDefL name=errorAttributeGroupAlreadySelected}{l s='has already been selected.' mod='jmarketplace' js=1}{/addJsDefL} 
        {addJsDefL name=errorAttributeEmpty}{l s='You have not selected attribute.' mod='jmarketplace' js=1}{/addJsDefL} 
        {addJsDefL name=buttonDelete}{l s='Delete' mod='jmarketplace' js=1}{/addJsDefL} 
    {/if}
{/if}