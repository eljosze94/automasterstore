{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$seller_link|escape:'html':'UTF-8'}">
        {$seller->name|escape:'html':'UTF-8'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Products' mod='jmarketplace'}
    </span>
{/capture}
<h1 class="page-heading">{l s='Products of' mod='jmarketplace'} {$seller->name|escape:'html':'UTF-8'}</h1>

{if isset($products) && $products}
    <div class="row">       
        {block name='jmarketplace-sellerproductlist'}
            {foreach from=$products item="product"}
                <article class="product-miniature js-product-miniature col-xs-12 col-md-6 col-lg-3" data-id-product="{$product.id_product|intval}" data-id-product-attribute="{$product.id_product_attribute|intval}" itemscope itemtype="http://schema.org/Product">
                    <div class="thumbnail-container">
                        {block name='product_thumbnail'}
                            <a href="{$product.link|escape:'html':'UTF-8'}" class="thumbnail product-thumbnail">
                                <img
                                    src = "{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"
                                    alt = "{if isset($product.legend) AND $product.legend != ''}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"
                                    data-full-size-image-url = "{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}"
                                >
                            </a>
                        {/block}

                        <div class="product-description">
                            {block name='product_name'}
                                <h1 class="h3 product-title" itemprop="name">
                                    <a href="{$product.link|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'|truncate:30:'...'}</a>
                                </h1>
                            {/block}

                            {block name='product_price_and_shipping'}
                                {if $product.show_price}
                                    <div class="product-price-and-shipping">                                            
                                        {if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                                            {hook h="displayProductPriceBlock" product=$product type="old_price"}
                                            <span class="regular-price">{$product.price_without_reduction|escape:'html':'UTF-8'}</span>
                                            {if $product.specific_prices.reduction_type == 'percentage'}
                                                <span class="discount-percentage">-{$product.specific_prices.reduction * 100|intval}%</span>
                                            {/if}
                                        {/if}

                                        {hook h='displayProductPriceBlock' product=$product type="before_price"}

                                        <span itemprop="price" class="price">{$product.price|escape:'html':'UTF-8'}</span>

                                        {hook h='displayProductPriceBlock' product=$product type='unit_price'}

                                        {hook h='displayProductPriceBlock' product=$product type='weight'}
                                    </div>
                                {/if}
                            {/block}

                            {block name='product_reviews'}
                                {hook h='displayProductListReviews' product=$product}
                            {/block}
                        </div>

                        <div class="highlighted-informations no-variants hidden-sm-down">
                            {block name='quick_view'}
                                <a class="quick-view" href="#" data-link-action="quickview">
                                    <i class="material-icons search">&#xE8B6;</i> {l s='Quick view' mod='jmarketplace'}
                                </a>
                            {/block}
                        </div>
                    </div>
                </article>
            {/foreach}
        {/block}   
    </div>
{else}
    <p class="alert alert-info">{l s='This seller have not products.' mod='jmarketplace'}</p>
{/if}