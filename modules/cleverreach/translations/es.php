<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cleverreach}prestashop>cleverreach_a84a884610b6a67840935aef22c43d7c'] = 'CleverReach';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_50985615900a80617eb1632efb3bea83'] = 'ID del cliente de CleverReach®: %s';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_964548574436bd636dcca659b318eb4d'] = 'Correos personalizados para más ventas en su tienda %s.';
$_MODULE['<{cleverreach}prestashop>admincleverreachinitialsynccontroller_a5f7f2a65325a4d2520ac4bbdad9efaf'] = 'Importe de destinatarios de %s a CleverReach®';
$_MODULE['<{cleverreach}prestashop>dashboard_2938c7f7e560ed972f8a4f68e80ff834'] = 'Panel de control';
$_MODULE['<{cleverreach}prestashop>dashboard_e7fbee2c9c3ca587f80a4da87916cab5'] = 'Ayuda y soporte';
$_MODULE['<{cleverreach}prestashop>dashboard_e1d47ee19f550574c20e1d4c54546ea3'] = 'Correos personalizados para más ventas!';
$_MODULE['<{cleverreach}prestashop>dashboard_79067f5df00ed0c912a857115e6a01f3'] = 'Cree newsletters elegantes con sus productos a los clientes.';
$_MODULE['<{cleverreach}prestashop>dashboard_d519a5bf07ada673864ad2318bb4018b'] = 'Cree el próximo newsletter ahora →';
$_MODULE['<{cleverreach}prestashop>dashboard_3fa8258d372aa789274ceac68cef569b'] = 'Cree su primer newsletter →';
$_MODULE['<{cleverreach}prestashop>task_list_c6759854dce56c1926deb0e107325cb1'] = 'Cree una lista de destinatarios en CleverReach®';
$_MODULE['<{cleverreach}prestashop>task_list_f2a5f768b13dc10f72f5c042f8234aa3'] = 'Añada campos de datos, segmentos y etiquetas a la lista de destinatarios';
$_MODULE['<{cleverreach}prestashop>task_list_60e2016ddc5fc624739607e22b531cda'] = '¡Bien hecho!';
$_MODULE['<{cleverreach}prestashop>task_list_1873a81cfd84f5cdf929b0a434f01d5d'] = '%s los destinatarios se han añadido con éxito a la lista %s en su cuenta de CleverReach®.';
$_MODULE['<{cleverreach}prestashop>task_list_cc992378d058c6a50720a747f10f827f'] = 'Usted será dirigido a nuestro plugin dashboard en... %d sec.';
$_MODULE['<{cleverreach}prestashop>welcome_3cc3bd74387dbede9e3d4e20353a701d'] = 'Conectando...';
$_MODULE['<{cleverreach}prestashop>welcome_6b53bddb57a264b30645c321d015ea73'] = 'Bienvenido a CleverReach email marketing';
$_MODULE['<{cleverreach}prestashop>welcome_634c3ad08487ceb37e100ea884867f53'] = 'Para iniciar el trabajo con Prestashop y empezar sin gastos con CleverReach®';
$_MODULE['<{cleverreach}prestashop>welcome_a4bd19a96f9aac47deac1846a752ed25'] = 'Cree y conecte con una cuenta de CleverReach®';
$_MODULE['<{cleverreach}prestashop>welcome_3ea0ec9cbbe451e3dea6a5d793c2af3f'] = 'Inicie sesión y conecte con una cuenta existente de CleverReach®';
