<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cleverreach}prestashop>cleverreach_a84a884610b6a67840935aef22c43d7c'] = 'CleverReach';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_50985615900a80617eb1632efb3bea83'] = 'ID cliente CleverReach®: %s';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_964548574436bd636dcca659b318eb4d'] = 'E-mail marketing mirato per incrementare i profitti nel tuo %s shop.';
$_MODULE['<{cleverreach}prestashop>admincleverreachinitialsynccontroller_a5f7f2a65325a4d2520ac4bbdad9efaf'] = 'Importa i destinatari da %s in CleverReach®';
$_MODULE['<{cleverreach}prestashop>dashboard_2938c7f7e560ed972f8a4f68e80ff834'] = 'Dashboard';
$_MODULE['<{cleverreach}prestashop>dashboard_e7fbee2c9c3ca587f80a4da87916cab5'] = 'Aiuto & Supporto';
$_MODULE['<{cleverreach}prestashop>dashboard_e1d47ee19f550574c20e1d4c54546ea3'] = 'E-mail marketing mirato per incrementare i profitti!';
$_MODULE['<{cleverreach}prestashop>dashboard_79067f5df00ed0c912a857115e6a01f3'] = 'Crea e-mail accattivanti per presentare i tuoi prodotti ai tuoi clienti.';
$_MODULE['<{cleverreach}prestashop>dashboard_3fa8258d372aa789274ceac68cef569b'] = 'Crea la tua prima newsletter →';
$_MODULE['<{cleverreach}prestashop>dashboard_d519a5bf07ada673864ad2318bb4018b'] = 'Crea ora la tua prossima newsletter! →';
$_MODULE['<{cleverreach}prestashop>task_list_c6759854dce56c1926deb0e107325cb1'] = 'Crea un elenco di destinatari in CleverReach®';
$_MODULE['<{cleverreach}prestashop>task_list_f2a5f768b13dc10f72f5c042f8234aa3'] = 'Aggiungi campi, segmenti e tag alle tue liste di destinatari';
$_MODULE['<{cleverreach}prestashop>task_list_60e2016ddc5fc624739607e22b531cda'] = 'Ben fatto!';
$_MODULE['<{cleverreach}prestashop>task_list_1873a81cfd84f5cdf929b0a434f01d5d'] = '%s destinatari sono stati aggiunti con successo alla lista %s nel tuo account CleverReach®.';
$_MODULE['<{cleverreach}prestashop>task_list_cc992378d058c6a50720a747f10f827f'] = 'Sarai reindirizzato all tua dashboard di plugin in... %d sec.';
$_MODULE['<{cleverreach}prestashop>welcome_3cc3bd74387dbede9e3d4e20353a701d'] = 'Connessione...';
$_MODULE['<{cleverreach}prestashop>welcome_6b53bddb57a264b30645c321d015ea73'] = 'Benvenuti nel mondo dell\'e-mail marketing intelligente';
$_MODULE['<{cleverreach}prestashop>welcome_634c3ad08487ceb37e100ea884867f53'] = 'Trasmetti i dati contenuti nella tua piattaforma PrestaShop e comincia gratuitamente ad usare CleverReach®!';
$_MODULE['<{cleverreach}prestashop>welcome_a4bd19a96f9aac47deac1846a752ed25'] = 'Crea e connetti adesso il tuo nuovo account CleverReach®!';
$_MODULE['<{cleverreach}prestashop>welcome_3ea0ec9cbbe451e3dea6a5d793c2af3f'] = 'Accedi e connettiti ad un account CleverReach® esistente';
