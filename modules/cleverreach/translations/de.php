<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cleverreach}prestashop>cleverreach_a84a884610b6a67840935aef22c43d7c'] = 'CleverReach';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_50985615900a80617eb1632efb3bea83'] = 'CleverReach® Kundennummer: %s';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_964548574436bd636dcca659b318eb4d'] = 'Gezieltes E-Mail Marketing für mehr Umsatz in Ihrem %s Shop.';
$_MODULE['<{cleverreach}prestashop>admincleverreachinitialsynccontroller_a5f7f2a65325a4d2520ac4bbdad9efaf'] = 'Importieren Sie Empfänger von %s zu CleverReach®';
$_MODULE['<{cleverreach}prestashop>dashboard_2938c7f7e560ed972f8a4f68e80ff834'] = 'Dashboard';
$_MODULE['<{cleverreach}prestashop>dashboard_e7fbee2c9c3ca587f80a4da87916cab5'] = 'Hilfe & Support';
$_MODULE['<{cleverreach}prestashop>dashboard_e1d47ee19f550574c20e1d4c54546ea3'] = 'Gezieltes E-Mail Marketing für mehr Umsatz!';
$_MODULE['<{cleverreach}prestashop>dashboard_79067f5df00ed0c912a857115e6a01f3'] = 'Erstellen Sie ansprechende Mailings und präsentieren Sie Ihren Kunden Ihre Produkte.';
$_MODULE['<{cleverreach}prestashop>dashboard_3fa8258d372aa789274ceac68cef569b'] = 'Erstellen Sie Ihren ersten Newsletter →';
$_MODULE['<{cleverreach}prestashop>dashboard_d519a5bf07ada673864ad2318bb4018b'] = 'Erstellen Sie jetzt Ihren nächsten Newsletter! →';
$_MODULE['<{cleverreach}prestashop>task_list_c6759854dce56c1926deb0e107325cb1'] = 'Empfängerliste in CleverReach® erstellen';
$_MODULE['<{cleverreach}prestashop>task_list_f2a5f768b13dc10f72f5c042f8234aa3'] = 'Datenfelder, Segmente und Tags zur Empfängerliste hinzufügen';
$_MODULE['<{cleverreach}prestashop>task_list_60e2016ddc5fc624739607e22b531cda'] = 'Gut gemacht!';
$_MODULE['<{cleverreach}prestashop>task_list_1873a81cfd84f5cdf929b0a434f01d5d'] = '%s Empfänger wurden erfolgreich zur Liste %s in Ihrem CleverReach® Account hinzugefügt.';
$_MODULE['<{cleverreach}prestashop>task_list_cc992378d058c6a50720a747f10f827f'] = 'Sie werden in %d Sek. zu Ihrem Plugin Dashboard weitergeleitet...';
$_MODULE['<{cleverreach}prestashop>welcome_3cc3bd74387dbede9e3d4e20353a701d'] = 'Verbindung wird hergestellt...';
$_MODULE['<{cleverreach}prestashop>welcome_6b53bddb57a264b30645c321d015ea73'] = 'Willkommen - los geht\'s mit cleverem E-Mail Marketing';
$_MODULE['<{cleverreach}prestashop>welcome_634c3ad08487ceb37e100ea884867f53'] = 'Einfach PrestaShop-Daten übertragen und kostenfrei mit CleverReach® starten!';
$_MODULE['<{cleverreach}prestashop>welcome_a4bd19a96f9aac47deac1846a752ed25'] = 'Jetzt neuen CleverReach® Account erstellen & verbinden!';
$_MODULE['<{cleverreach}prestashop>welcome_3ea0ec9cbbe451e3dea6a5d793c2af3f'] = 'Mit meinem bestehenden CleverReach® Account einloggen & verbinden';
