<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cleverreach}prestashop>cleverreach_a84a884610b6a67840935aef22c43d7c'] = 'CleverReach';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_50985615900a80617eb1632efb3bea83'] = 'CleverReach® Numéro Client: %s';
$_MODULE['<{cleverreach}prestashop>admincleverreachdashboardcontroller_964548574436bd636dcca659b318eb4d'] = 'Marketing par Email Ciblé pour réaliser plus de ventes sur votre %s boutique en ligne.';
$_MODULE['<{cleverreach}prestashop>admincleverreachinitialsynccontroller_a5f7f2a65325a4d2520ac4bbdad9efaf'] = 'Importer vos destinataires de %s à CleverReach®';
$_MODULE['<{cleverreach}prestashop>dashboard_2938c7f7e560ed972f8a4f68e80ff834'] = 'Tableaux de bord';
$_MODULE['<{cleverreach}prestashop>dashboard_e7fbee2c9c3ca587f80a4da87916cab5'] = 'Aide & Support';
$_MODULE['<{cleverreach}prestashop>dashboard_e1d47ee19f550574c20e1d4c54546ea3'] = 'Marketing par Email Ciblées pour réaliser plus de ventes!';
$_MODULE['<{cleverreach}prestashop>dashboard_79067f5df00ed0c912a857115e6a01f3'] = 'Créer des mailings attractifs afin de présentez vos produits à vos clients.';
$_MODULE['<{cleverreach}prestashop>dashboard_3fa8258d372aa789274ceac68cef569b'] = 'Créer votre première newsletter →';
$_MODULE['<{cleverreach}prestashop>dashboard_d519a5bf07ada673864ad2318bb4018b'] = 'Créez maintenant votre prochaine Newsletters! →';
$_MODULE['<{cleverreach}prestashop>task_list_c6759854dce56c1926deb0e107325cb1'] = 'Créer une liste d\'abonnés dans CleverReach®';
$_MODULE['<{cleverreach}prestashop>task_list_f2a5f768b13dc10f72f5c042f8234aa3'] = 'Ajouter des champs de données, des segments et des Balises à liste d\'abonnés';
$_MODULE['<{cleverreach}prestashop>task_list_60e2016ddc5fc624739607e22b531cda'] = 'Bien fait!';
$_MODULE['<{cleverreach}prestashop>task_list_1873a81cfd84f5cdf929b0a434f01d5d'] = '%s destinataires ont été ajoutés avec succès à la liste %s de votre compte CleverReach®.';
$_MODULE['<{cleverreach}prestashop>task_list_cc992378d058c6a50720a747f10f827f'] = 'Vous serez redirigé vers votre tableau de bord plugin dans ...%d sec.';
$_MODULE['<{cleverreach}prestashop>welcome_3cc3bd74387dbede9e3d4e20353a701d'] = 'La connexion est établie...';
$_MODULE['<{cleverreach}prestashop>welcome_6b53bddb57a264b30645c321d015ea73'] = 'Bienvenue à CleverReach® - Intelligent Email Marketing';
$_MODULE['<{cleverreach}prestashop>welcome_634c3ad08487ceb37e100ea884867f53'] = 'Transférer tout simplement vos données PrestaShop et démarrez gratuitement avec CleverReach®!';
$_MODULE['<{cleverreach}prestashop>welcome_a4bd19a96f9aac47deac1846a752ed25'] = 'Créer et connecter dès maintenant votre nouveau compte CleverReach®!';
$_MODULE['<{cleverreach}prestashop>welcome_3ea0ec9cbbe451e3dea6a5d793c2af3f'] = 'Identifiez et connectez-vous à votre compte CleverReach®';
