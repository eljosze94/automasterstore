<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\InfrastructureServices;

use CleverReach\Infrastructure\Interfaces\Required\HttpClient;
use CleverReach\Infrastructure\Utility\Exceptions\HttpCommunicationException;
use CleverReach\Infrastructure\Utility\HttpResponse;

class HttpClientService extends HttpClient
{
    private $curlSession;

    public function sendHttpRequest($method, $url, $headers = array(), $body = '')
    {
        $this->setCurlSessionAndCommonRequestParts($method, $url, $headers, $body);
        $this->setCurlSessionOptionsForSynchronousRequest();

        return $this->executeAndReturnResponseForSynchronousRequest($url);
    }

    public function sendHttpRequestAsync($method, $url, $headers = array(), $body = '')
    {
        $this->setCurlSessionAndCommonRequestParts($method, $url, $headers, $body);
        $this->setCurlSessionOptionsForAsynchronousRequest();

        return curl_exec($this->curlSession);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param string $body
     */
    private function setCurlSessionAndCommonRequestParts($method, $url, array $headers, $body)
    {
        $this->initializeCurlSession();
        $this->setCurlSessionOptionsBasedOnMethod($method);
        $this->setCurlSessionUrlHeadersAndBody($url, $headers, $body);
        $this->setCommonOptionsForCurlSession();
    }

    private function initializeCurlSession()
    {
        $this->curlSession = curl_init();
    }

    /**
     * @param string $method
     */
    private function setCurlSessionOptionsBasedOnMethod($method)
    {
        if ($method === 'DELETE') {
            curl_setopt($this->curlSession, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        if ($method == 'POST') {
            curl_setopt($this->curlSession, CURLOPT_POST, true);
        }

        if ($method == 'PUT') {
            curl_setopt($this->curlSession, CURLOPT_CUSTOMREQUEST, 'PUT');
        }
    }

    /**
     * @param string $url
     * @param array $headers
     * @param string $body
     */
    private function setCurlSessionUrlHeadersAndBody($url, array $headers, $body)
    {
        curl_setopt($this->curlSession, CURLOPT_URL, $url);
        curl_setopt($this->curlSession, CURLOPT_HTTPHEADER, $headers);
        if (!empty($body)) {
            curl_setopt($this->curlSession, CURLOPT_POSTFIELDS, $body);
        }
    }

    private function setCommonOptionsForCurlSession()
    {
        curl_setopt($this->curlSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlSession, CURLOPT_FOLLOWLOCATION, true);
    }

    private function setCurlSessionOptionsForSynchronousRequest()
    {
        curl_setopt($this->curlSession, CURLOPT_HEADER, true);
        curl_setopt($this->curlSession, CURLOPT_SSL_VERIFYPEER, false);
    }

    private function setCurlSessionOptionsForAsynchronousRequest()
    {
        // Follow the redirects (needed for mod_rewrite)
        curl_setopt($this->curlSession, CURLOPT_FOLLOWLOCATION, true);
        // Don't retrieve headers
        curl_setopt($this->curlSession, CURLOPT_HEADER, false);
        // Don't retrieve the body
        curl_setopt($this->curlSession, CURLOPT_NOBODY, true);
        // Always ensure the connection is fresh
        curl_setopt($this->curlSession, CURLOPT_FRESH_CONNECT, true);
        // Timeout super fast once connected, so it goes into async
        curl_setopt($this->curlSession, CURLOPT_TIMEOUT, 1);
    }

    /**
     * @param string $url
     * @return HttpResponse
     * @throws HttpCommunicationException
     */
    private function executeAndReturnResponseForSynchronousRequest($url)
    {
        $apiResponse = curl_exec($this->curlSession);
        $statusCode = curl_getinfo($this->curlSession, CURLINFO_HTTP_CODE);
        curl_close($this->curlSession);

        if ($apiResponse === false) {
            throw new HttpCommunicationException('Request ' . $url . ' failed.');
        }

        return new HttpResponse(
            $statusCode,
            $this->getHeadersFromCurlResponse($apiResponse),
            $this->getBodyFromCurlResponse($apiResponse)
        );
    }

    /**
     * @param string $response
     * @return array
     */
    private function getHeadersFromCurlResponse($response)
    {
        $headers = array();
        $headersBodyDelimiter = "\r\n\r\n";
        $headerText = \Tools::substr($response, 0, \Tools::strpos($response, $headersBodyDelimiter));
        $headersDelimiter = "\r\n";

        foreach (explode($headersDelimiter, $headerText) as $i => $line) {
            if ($i === 0) {
                $headers[] = $line;
            } else {
                list($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        }

        return $headers;
    }

    /**
     * @param string $response
     * @return string
     */
    private function getBodyFromCurlResponse($response)
    {
        $headersBodyDelimiter = "\r\n\r\n";
        $bodyStartingPositionOffset = 4; // number of special signs in delimiter;
        return \Tools::substr(
            $response,
            \Tools::strpos($response, $headersBodyDelimiter) + $bodyStartingPositionOffset,
            \Tools::strlen($response)
        );
    }
}
