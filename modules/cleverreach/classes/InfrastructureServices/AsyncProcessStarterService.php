<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\InfrastructureServices;

use CleverReach\Infrastructure\Interfaces\Required\AsyncProcessStarter;
use CleverReach\Infrastructure\Interfaces\Exposed\Runnable;
use CleverReach\Infrastructure\Interfaces\Required\HttpClient;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Exceptions\ProcessStarterSaveException;
use CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException;
use CleverReach\Infrastructure\Utility\GuidProvider;
use CleverReach\PrestaShop\Classes\Repositories\ProcessRepository;

class AsyncProcessStarterService implements AsyncProcessStarter
{
    /**
     * @var string
     */
    private $processGuid;

    /**
     * @var Runnable
     */
    private $runner;

    /**
     * @var HttpClientService
     */
    private $httpClientService;

    public function __construct()
    {
        $guidProvider = new GuidProvider();
        $this->processGuid = trim($guidProvider->generateGuid());
        $this->httpClientService = ServiceRegister::getService(HttpClient::CLASS_NAME);
    }

    /**
     * @param Runnable $runner
     * @throws HttpRequestException
     * @throws ProcessStarterSaveException
     */
    public function start(Runnable $runner)
    {
        $this->setTaskRunner($runner);
        $this->saveGuidAndTaskRunner();
        $this->startTaskRunnerAsynchronously();
    }

    private function setTaskRunner($runner)
    {
        $this->runner = $runner;
    }

    private function saveGuidAndTaskRunner()
    {
        try {
            $processRepository = new ProcessRepository();
            $processRepository->saveGuidAndRunner($this->processGuid, $this->runner);
        } catch (\Exception $e) {
            Logger::logError($e->getMessage(), 'Integration');
            throw new ProcessStarterSaveException($e->getMessage(), 0, $e);
        }
    }

    private function startTaskRunnerAsynchronously()
    {
        try {
            $this->httpClientService->requestAsync('POST', $this->formatAsyncProcessStartUrl());
        } catch (\Exception $e) {
            Logger::logError($e->getMessage(), 'Integration');
            throw new HttpRequestException($e->getMessage(), 0, $e);
        }
    }

    private function formatAsyncProcessStartUrl()
    {
        $params = array(
            'guid' => $this->processGuid,
        );

        return \Context::getContext()->link->getModuleLink(
            'cleverreach',
            'asyncprocess',
            $params,
            null,
            null,
            \Configuration::get('PS_SHOP_DEFAULT')
        );
    }
}
