<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\InfrastructureServices;

use CleverReach\Infrastructure\Interfaces\Required\TaskQueueStorage;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\TaskExecution\Exceptions\QueueItemDeserializationException;
use CleverReach\Infrastructure\TaskExecution\Exceptions\QueueItemSaveException;
use CleverReach\Infrastructure\TaskExecution\QueueItem;

class TaskQueueStorageService implements TaskQueueStorage
{

    const TABLE_NAME = 'cleverreach_queue';

    /**
     * Creates or updates given queue item. If queue item id is not set,
     * new queue item will be created otherwise update will be performed.
     *
     * @param \CleverReach\Infrastructure\TaskExecution\QueueItem $queueItem Item to save
     * @param array $additionalWhere List of key/value pairs that must be satisfied upon saving queue item.
     * Key is queue item property and value is condition value for that property.
     * Example for MySql storage:
     *      $storage->save($queueItem, array('status' => 'queued')) should produce query
     *      UPDATE queue_storage_table SET .... WHERE .... AND status => 'queued'
     *
     * @return int Id of saved queue item
     * @throws QueueItemSaveException if queue item could not be saved
     */
    public function save(QueueItem $queueItem, array $additionalWhere = array())
    {
        $savedItemId = null;
        try {
            $itemId = $queueItem->getId();
            if (is_null($itemId) || $itemId <= 0) {
                $savedItemId = $this->insert($queueItem);
            } else {
                $savedItemId = $this->update($queueItem, $additionalWhere);
            }
        } catch (\PrestaShopDatabaseException $exception) {
            throw new QueueItemSaveException(
                'Failed to save queue item. SQL error: ' . \Db::getInstance()->getMsgError(),
                0,
                $exception
            );
        }

        return (int)$savedItemId;
    }

    /**
     * Finds queue item by id
     *
     * @param int $id Id of a queue item to find
     *
     * @return QueueItem|null Found queue item or null when queue item does not exist
     */
    public function find($id)
    {
        $item = null;
        try {
            $data = \Db::getInstance()->getRow('SELECT * FROM `'.pSQL($this->getTableName()).'` WHERE id = '.(int)$id);
            if (!empty($data)) {
                $item = $this->createQueueItem($data);
            }
        } catch (\PrestaShopDatabaseException $exception) {
            // In case of exception return null result
        }

        return $item;
    }

    /**
     * Finds queue item by id
     *
     * @param string $type Type of a queue item to find
     *
     * @return QueueItem|null Found queue item or null when queue item does not exist
     */
    public function findLatestByType($type)
    {
        $item = null;
        try {
            $where = sprintf(
                ' WHERE %s',
                $this->buildWhereString(array(
                    'type' => $type
                ))
            );
            $orderBy = sprintf(
                ' ORDER BY %s',
                $this->buildOrderByString(array('queueTimestamp' => TaskQueueStorage::SORT_DESC))
            );
            $data = \Db::getInstance()->getRow('SELECT * FROM `'.pSQL($this->getTableName()).'`'. $where . $orderBy) ;
            if (!empty($data)) {
                $item = $this->createQueueItem($data);
            }
        } catch (\PrestaShopDatabaseException $exception) {
            // In case of exception return null result
        }

        return $item;
    }

    /**
     * Finds list of earliest queued queue items per queue. Following list of criteria for searching must be satisfied:
     *      - Queue must be without already running queue items
     *      - For one queue only one (oldest queued) item should be returned
     *
     * @param int $limit Result set limit. By default max 10 earliest queue items will be returned
     *
     * @return \CleverReach\Infrastructure\TaskExecution\QueueItem[] Found queue item list
     */
    public function findOldestQueuedItems($limit = 10)
    {
        $items = array();

        try {
            $runningQueues = $this->findRunningQueues();

            $limit = (int)$limit;
            $where = sprintf(
                ' WHERE %s',
                $this->buildWhereString(array(
                    'status' => QueueItem::QUEUED
                ))
            );
            if (!empty($runningQueues)) {
                $where .= sprintf(' AND `queueName` NOT IN ("%s")', join('", "', array_map('pSQL', $runningQueues)));
            }

            $tableName = pSQL($this->getTableName());
            $sql = "SELECT * FROM {$tableName} {$where} 
              GROUP BY `queueName` ORDER BY `queueTimestamp` ASC LIMIT {$limit}";
            $statement = \Db::getInstance()->query($sql);
            while ($row = \Db::getInstance()->nextRow($statement)) {
                $items[] = $this->createQueueItem($row);
            }
        } catch (\PrestaShopDatabaseException $exception) {
            // In case of exception return empty result set
        }

        return $items;
    }

    /**
     * Finds all queue items from all queues
     *
     * @param array $filterBy List of simple search filters, where key is queue item property and value is condition
     *      value for that property. Leave empty for unfiltered result.
     * @param array $sortBy List of sorting options where key is queue item property and value sort direction
     * ("ASC" or "DESC"). Leave empty for default sorting.
     * @param int $start From which record index result set should start
     * @param int $limit Max number of records that should be returned (default is 10)
     *
     * @return \CleverReach\Infrastructure\TaskExecution\QueueItem[] Found queue item list
     */
    public function findAll(array $filterBy = array(), array $sortBy = array(), $start = 0, $limit = 10)
    {
        $items = array();
        $where = !empty($filterBy) ? sprintf(' WHERE %s', $this->buildWhereString($filterBy)) : '';
        $orderBy = !empty($sortBy) ? sprintf(' ORDER BY %s', $this->buildOrderByString($sortBy)) : '';
        $start = (int)$start;
        $limit = (int)$limit;
        try {
            $tableName = pSQL($this->getTableName());
            $sql = "SELECT * FROM {$tableName}  {$where}  {$orderBy}  LIMIT {$start}, {$limit}";
            $statement = \Db::getInstance()->query($sql);
            while ($row = \Db::getInstance()->nextRow($statement)) {
                $items[] = $this->createQueueItem($row);
            }
        } catch (\PrestaShopDatabaseException $exception) {
            // In case of exception return empty result set
        }

        return $items;
    }

    /**
     * Inserts new queue item in DB
     *
     * @param QueueItem $queueItem
     *
     * @return int Saved item id
     * @throws QueueItemSaveException
     * @throws \PrestaShopDatabaseException
     * @throws QueueItemDeserializationException
     */
    private function insert(QueueItem $queueItem)
    {
        $result = \Db::getInstance()->insert(self::TABLE_NAME, $this->getDBRepresentation($queueItem), true);

        if (empty($result)) {
            throw new QueueItemSaveException(
                'Failed to save queue item. SQL error: ' . \Db::getInstance()->getMsgError()
            );
        }

        $itemId = \Db::getInstance()->Insert_ID();

        return (int)$itemId;
    }

    /**
     * Updates queue item in DB
     *
     * @param QueueItem $queueItem
     * @param array $additionalWhere
     *
     * @return int Updated item id
     * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\QueueItemSaveException
     * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\QueueItemDeserializationException
     */
    private function update(QueueItem $queueItem, array $additionalWhere = array())
    {
        $storedItemData = \Db::getInstance()->nextRow(\Db::getInstance()->query(
            'SELECT * FROM `'.pSQL($this->getTableName()).'`'.
            ' WHERE id = '.(int)$queueItem->getId() . ' LIMIT 1 FOR UPDATE'
        ));

        if (empty($storedItemData)) {
            throw new QueueItemSaveException('Failed to save queue item, there is no queue item with give id.');
        }

        if (!empty($additionalWhere)) {
            foreach ($additionalWhere as $field => $value) {
                if (!array_key_exists($field, $storedItemData) || $storedItemData[$field] != $value) {
                    Logger::logDebug(\json_encode(array(
                        'Message' => 'Failed to save queue item, update condition not met.',
                        'WhereCondition' => $additionalWhere,
                        'FailedFiled' => $field,
                        'DBValue' => !array_key_exists($field, $storedItemData) ? 'not set' : $storedItemData[$field]
                    )));
                    throw new QueueItemSaveException('Failed to save queue item, update condition not met.');
                }
            }
        }

        $additionalWhere = array_merge($additionalWhere, array('id' => (int)$queueItem->getId()));
        $result = \Db::getInstance()->update(
            self::TABLE_NAME,
            $this->getDBRepresentation($queueItem),
            $this->buildWhereString($additionalWhere),
            0,
            true
        );

        if (empty($result)) {
            throw new QueueItemSaveException(
                'Failed to save queue item. SQL error: ' . \Db::getInstance()->getMsgError()
            );
        }

        return $queueItem->getId();
    }

    private function findRunningQueues()
    {
        $runningQueueItems = $this->findAll(array('status' => QueueItem::IN_PROGRESS), array(), 0, 10000);
        return array_map(function (QueueItem $runningQueueItem) {
            return pSQL($runningQueueItem->getQueueName());
        }, $runningQueueItems);
    }

    private function createQueueItem($data)
    {
        $item = new QueueItem();
        $item->setId((int)$data['id']);
        $item->setStatus($data['status']);
        $item->setQueueName($data['queueName']);
        $item->setProgress((int)$data['progress']);
        $item->setRetries((int)$data['retries']);
        $item->setFailureDescription($data['failureDescription']);
        $item->setSerializedTask($data['serializedTask']);
        $item->setCreateTimestamp(0 < (int)$data['createTimestamp'] ? (int)$data['createTimestamp'] : null);
        $item->setQueueTimestamp(0 < (int)$data['queueTimestamp'] ? (int)$data['queueTimestamp'] : null);
        $item->setLastUpdateTimestamp(0 < (int)$data['lastUpdateTimestamp'] ? (int)$data['lastUpdateTimestamp'] : null);
        $item->setStartTimestamp(0 < (int)$data['startTimestamp'] ? (int)$data['startTimestamp'] : null);
        $item->setFinishTimestamp(0 < (int)$data['finishTimestamp'] ? (int)$data['finishTimestamp'] : null);
        $item->setFailTimestamp(0 < (int)$data['failTimestamp'] ? (int)$data['failTimestamp'] : null);

        return $item;
    }

    /**
     * Converts queue item in array of sanitized DB fields
     *
     * @param QueueItem $queueItem
     *
     * @return array Array of sanitized DB queue item fields
     * @throws QueueItemDeserializationException
     */
    private function getDBRepresentation(QueueItem $queueItem)
    {
        $createTimestamp = $queueItem->getCreateTimestamp();
        $queueTimestamp = $queueItem->getQueueTimestamp();
        $lastUpdateTimestamp = $queueItem->getLastUpdateTimestamp();
        $startTimestamp = $queueItem->getStartTimestamp();
        $finishTimestamp = $queueItem->getFinishTimestamp();
        $failTimestamp = $queueItem->getFailTimestamp();

        return array(
            'status' => pSQL($queueItem->getStatus()),
            'type' => pSQL($queueItem->getTaskType()),
            'queueName' => pSQL($queueItem->getQueueName()),
            'progress' => (int)$queueItem->getProgress(),
            'retries' => (int)$queueItem->getRetries(),
            'failureDescription' => pSQL($queueItem->getFailureDescription()),
            'serializedTask' => pSQL($queueItem->getSerializedTask()),
            'createTimestamp' => !is_null($createTimestamp) ? (int)$createTimestamp : null,
            'queueTimestamp' => !is_null($queueTimestamp) ? (int)$queueTimestamp : null,
            'lastUpdateTimestamp' => !is_null($lastUpdateTimestamp) ? (int)$lastUpdateTimestamp : null,
            'startTimestamp' => !is_null($startTimestamp) ? (int)$startTimestamp : null,
            'finishTimestamp' => !is_null($finishTimestamp) ? (int)$finishTimestamp : null,
            'failTimestamp' => !is_null($failTimestamp) ? (int)$failTimestamp : null,
        );
    }

    /**
     * Build properly escaped where condition string based on given key/value parameters.
     * String parameters will be sanitized with pSQL method call and other fields will be cast to integer values
     *
     * @param array $whereFields Key value pairs of where condition
     *
     * @return string Properly sanitized where condition string
     */
    private function buildWhereString(array $whereFields = array())
    {
        $stringFields = array('status', 'type', 'queueName', 'failureDescription', 'serializedTask');
        $where = array();
        foreach ($whereFields as $field => $value) {
            if (in_array($field, $stringFields)) {
                $sanitizedValue = pSQL($value);
                $where[] = '`'.pSQL($field).'`' . " = '{$sanitizedValue}'";
            } else {
                $sanitizedValue = is_null($value) ? " IS NULL" : " = " . (int)$value;
                $where[] = '`'.pSQL($field).'`' . $sanitizedValue;
            }
        }

        return join(' AND ', $where);
    }

    /**
     * Builds properly sanitized order by query string base on key/value parameters
     *
     * @param array $orderBy Key value order by parameters
     *
     * @return string Properly sanitized order by string
     */
    private function buildOrderByString(array $orderBy = array())
    {
        $order = array();
        foreach ($orderBy as $field => $value) {
            $sortDirection = \Tools::strtoupper($value) === 'DESC' ? 'DESC' : 'ASC';
            $order[] = pSQL($field) . ' ' . $sortDirection;
        }

        return join(', ', $order);
    }

    private function getTableName()
    {
        return  _DB_PREFIX_ . self::TABLE_NAME;
    }
}
