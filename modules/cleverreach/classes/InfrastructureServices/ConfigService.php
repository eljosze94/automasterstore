<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\InfrastructureServices;

use CleverReach\Infrastructure\Interfaces\Required\Configuration as ConfigInterface;
use CleverReach\Infrastructure\TaskExecution\Exceptions\TaskRunnerStatusStorageUnavailableException;

class ConfigService implements ConfigInterface
{

    const MODULE_NAME = 'cleverreach';

    const INTEGRATION_NAME = 'PrestaShop';

    const INSTANCE_TAG_PREFIX = 'PS-';

    const INITIAL_BATCH_SIZE = 250;

    const DEFAULT_MAX_STARTED_TASK_LIMIT = 8;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var array
     */
    private $userInfo;

    /**
     * Saves min log level in integration database
     *
     * @param int $minLogLevel
     */
    public function saveMinLogLevel($minLogLevel)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_MIN_LOG_LEVEL', $minLogLevel);
    }

    /**
     * Retrieves min log level from integration database
     *
     * @return int
     */
    public function getMinLogLevel()
    {
        return \Configuration::get('CLEVERREACH_MIN_LOG_LEVEL');
    }

    /**
     * Retrieves access token from integration database
     *
     * @return int
     */
    public function getAccessToken()
    {
        if (empty($this->accessToken)) {
            $this->accessToken = \Configuration::get('CLEVERREACH_ACCESS_TOKEN');
        }

        return $this->accessToken;
    }

    /**
     * Return whether product search is enabled or not
     *
     * @return bool
     */
    public function isProductSearchEnabled()
    {
        return true;
    }

    /**
     * Retrieves parameters needed for product search registrations
     *
     * @return array, with array keys name, url, password
     */
    public function getProductSearchParameters()
    {
        return array(
            'name' => self::INTEGRATION_NAME . '(' . \Context::getContext()->shop->name . ') - Product search',
            'url' => \Context::getContext()->link->getModuleLink(
                self::MODULE_NAME,
                'search',
                array(),
                null,
                null,
                \Configuration::get('PS_SHOP_DEFAULT')
            ),
            'password' => $this->getProductSearchEndpointPassword()
        );
    }

    /**
     * Retrieves integration name
     *
     * @return string
     */
    public function getIntegrationName()
    {
        return self::INTEGRATION_NAME;
    }

    /**
     * Retrieves integration id
     *
     * @return int
     */
    public function getIntegrationId()
    {
        return \Configuration::get('CLEVERREACH_INTEGRATION_ID');
    }

    /**
     * Retrieves user account id
     *
     * @return string
     */
    public function getUserAccountId()
    {
        $userInfo = $this->getUserInfo();

        return !empty($userInfo['id']) ? $userInfo['id'] : '';
    }

    /**
     * Set default logger status (enabled/disabled)
     *
     * @param bool $status
     */
    public function setDefaultLoggerEnabled($status)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_DEFAULT_LOGGER_STATUS', $status);
    }

    /**
     * Return whether default logger is enabled or not
     *
     * @return bool
     */
    public function isDefaultLoggerEnabled()
    {
        return (\Configuration::get('CLEVERREACH_DEFAULT_LOGGER_STATUS') == 1);
    }

    /**
     * Gets the number of maximum allowed started task at the point in time.
     * This number will determine how many tasks can be in "in_progress" status at the same time.
     *
     * @return int
     */
    public function getMaxStartedTasksLimit()
    {
        return \Configuration::get('CLEVERREACH_MAX_STARTED_TASK_LIMIT') ?: self::DEFAULT_MAX_STARTED_TASK_LIMIT;
    }

    /**
     * @param int $maxStartedTaskLimit
     */
    public function setMaxStartedTaskLimit($maxStartedTaskLimit)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_MAX_STARTED_TASK_LIMIT', $maxStartedTaskLimit);
    }

    /**
     * Get user information
     *
     * @return array
     */
    public function getUserInfo()
    {
        if (empty($this->userInfo)) {
            $this->userInfo = json_decode(\Configuration::get('CLEVERREACH_USER_INFO'), true);
        }

        return $this->userInfo;
    }

    /**
     * Return if first email is already build
     *
     * @return bool
     */
    public function isFirstEmailBuild()
    {
        return \Configuration::get('CLEVERREACH_FIRST_EMAIL_BUILD') === '1';
    }

    /**
     * Set if first email is build
     *
     * @param $value
     */
    public function setIsFirstEmailBuild($value)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_FIRST_EMAIL_BUILD', $value);
    }

    /**
     * Saves created groupId in CR to integration
     *
     * @param int $id
     */
    public function setIntegrationId($id)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_INTEGRATION_ID', $id);
    }

    /**
     * Automatic task runner wakeup delay in seconds.
     * Task runner will sleep at the end of its lifecycle for this value seconds
     * before it sends wakeup signal for a new lifecycle. Return null to use default system value (10)
     *
     * @return int|null
     */
    public function getTaskRunnerWakeupDelay()
    {
        return \Configuration::get('CLEVERREACH_TASK_RUNNER_WAKEUP_DELAY') ?: null;
    }

    /**
     * @param int $taskRunnerWakeUpDelay
     */
    public function setTaskRunnerWakeUpDelay($taskRunnerWakeUpDelay)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_TASK_RUNNER_WAKEUP_DELAY', $taskRunnerWakeUpDelay);
    }

    /**
     * Gets maximal time in seconds allowed for runner instance to stay in alive (running) status.
     * After this period system will automatically start new runner instance and shutdown old one.
     * Return null to use default system value (60).
     *
     * @return int|null
     */
    public function getTaskRunnerMaxAliveTime()
    {
        return \Configuration::get('CLEVERREACH_MAX_ALIVE_TIME') ?: null;
    }

    /**
     * @param int $taskRunnerMaxAliveTime
     */
    public function setTaskRunnerMaxAliveTime($taskRunnerMaxAliveTime)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_MAX_ALIVE_TIME', $taskRunnerMaxAliveTime);
    }



    /**
     * Gets maximum number of failed task execution retries.
     * System will retry task execution in case of error until this number is reached.
     * Return null to use default system value (5).
     *
     * @return int|null
     */
    public function getMaxTaskExecutionRetries()
    {
        return \Configuration::get('CLEVERREACH_MAX_TASK_EXECUTION_RETRIES') ?: null;
    }

    /**
     * @param int $maxTaskExecutionRetries
     */
    public function setMaxTaskExecutionRetries($maxTaskExecutionRetries)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_MAX_TASK_EXECUTION_RETRIES', $maxTaskExecutionRetries);
    }

    /**
     * Gets max inactivity period for a task in seconds.
     * After inactivity period is passed, system will fail such tasks as expired.
     * Return null to use default system value (30).
     *
     * @return int|null
     */
    public function getMaxTaskInactivityPeriod()
    {
        return \Configuration::get('CLEVERREACH_MAX_TASK_INACTIVITY_PERIOD') ?: 300;
    }

    /**
     * @param int $maxTaskInactivityPeriod
     */
    public function setMaxTaskInactivityPeriod($maxTaskInactivityPeriod)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_MAX_TASK_INACTIVITY_PERIOD', $maxTaskInactivityPeriod);
    }

    public function getTaskRunnerStatus()
    {
        return json_decode(\Configuration::get('CLEVERREACH_TASK_RUNNER_STATUS'), true);
    }

    /**
     * Sets task runner status information as JSON encoded string.
     *
     * @param string $guid
     * @param int $timestamp
     * @throws TaskRunnerStatusStorageUnavailableException
     */
    public function setTaskRunnerStatus($guid, $timestamp)
    {
        $taskRunnerStatus = json_encode(array('guid'=> $guid, 'timestamp' => $timestamp));
        $response = \Configuration::updateGlobalValue('CLEVERREACH_TASK_RUNNER_STATUS', $taskRunnerStatus);

        if (empty($response)) {
            throw new TaskRunnerStatusStorageUnavailableException('Task runner status storage is not available.');
        }
    }

    /**
     * Save access token in integration database
     *
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_ACCESS_TOKEN', $accessToken);
        $this->accessToken = $accessToken;
    }

    /**
     * @param array $userInfo
     */
    public function setUserInfo($userInfo)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_USER_INFO', json_encode($userInfo));
        $this->userInfo = $userInfo;
    }

    public function getProductSearchEndpointPassword()
    {
        return \Configuration::get('CLEVERREACH_PRODUCT_SEARCH_PASSWORD');
    }

    public function setProductSearchEndpointPassword($password)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_PRODUCT_SEARCH_PASSWORD', $password);
    }

    public function getQueueName()
    {
        return 'PrestaShopDefault';
    }

    /**
     * Gets batch size for synchronization set in configuration.
     *
     * @return int
     */
    public function getRecipientsSynchronizationBatchSize()
    {
        return \Configuration::get('CLEVERREACH_RECIPIENT_SYNC_BATCH_SIZE') ?: self::INITIAL_BATCH_SIZE;
    }

    /**
     * Sets synchronization batch size.
     *
     * @param int $batchSize
     */
    public function setRecipientsSynchronizationBatchSize($batchSize)
    {
        \Configuration::updateGlobalValue('CLEVERREACH_RECIPIENT_SYNC_BATCH_SIZE', $batchSize);
    }

    /**
     * Gets instance specific prefix that will be used for forming tag names.
     *
     * @return string
     */
    public function getInstanceTagPrefix()
    {
        return self::INSTANCE_TAG_PREFIX;
    }
}
