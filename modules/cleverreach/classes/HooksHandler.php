<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes;

use CleverReach\BusinessLogic\Sync\CampaignOrderSync;
use CleverReach\BusinessLogic\Sync\FilterSyncTask;
use CleverReach\BusinessLogic\Sync\RecipientSyncTask;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Exceptions\QueueStorageUnavailableException;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\Infrastructure\TaskExecution\Task;
use CleverReach\PrestaShop\Classes\BusinessLogicServices\RecipientService;

class HooksHandler
{
    /** @var \CleverReach */
    private $cleverReachModule;

    /** @var Configuration */
    private $configService;

    /** @var Queue */
    private $queue;

    public function __construct(\CleverReach $cleverReachModule)
    {
        $this->cleverReachModule = $cleverReachModule;

        $initializer = new Initializer();
        $initializer->registerServices();
    }

    public function handleDisplayNewsletterBottom()
    {
        // if not connected to CleverReach skip execution or not submitNewsletter event
        if (!\Tools::isSubmit('submitNewsletter')) {
            return;
        }

        $subscriberEmail = \Tools::getValue('email');
        if (empty($subscriberEmail) || !\Validate::isEmail($subscriberEmail)) {
            return;
        }

        if (\Tools::getValue('action') == '0') {
            Logger::logInfo('New newsletter subscribe event detected for email: '. $subscriberEmail, 'Integration');
        } else {
            Logger::logInfo('New newsletter unsubscribe even detected for email: '. $subscriberEmail, 'Integration');
        }

        $recipientService = new RecipientService();
        $subscriberId = $recipientService->getSubscriberIdByEmail($subscriberEmail);
        if (!empty($subscriberId)) {
            $this->enqueueTask(new RecipientSyncTask(array($subscriberId), array(), false));
        }
    }

    public function handleCustomerCreatedEvent(\Customer $customer)
    {
        Logger::logInfo('New shop customer created event detected. New customer id: ' . $customer->id, 'Integration');

        $recipientService = new RecipientService();

        $this->enqueueTask(
            new RecipientSyncTask(array($recipientService->getPrefixedCustomerId($customer->id)), array(), false)
        );
    }

    public function handleCustomerUpdateEvent(\Customer $customer)
    {
        Logger::logInfo(
            'New shop customer updated event detected. Updated customer id: ' . $customer->id,
            'Integration'
        );

        $recipientService = new RecipientService();

        $this->enqueueTask(
            new RecipientSyncTask(array($recipientService->getPrefixedCustomerId($customer->id)), array(), false)
        );
    }

    public function handleAddressCreatedEvent(\Address $address)
    {
        Logger::logInfo(
            'New shop customer address created event detected. Updated customer id: ' . $address->id_customer,
            'Integration'
        );

        $recipientService = new RecipientService();

        $this->enqueueTask(
            new RecipientSyncTask(
                array($recipientService->getPrefixedCustomerId($address->id_customer)),
                array(),
                false
            )
        );
    }

    public function handleAddressUpdateEvent(\Address $address)
    {
        Logger::logInfo(
            'New shop customer address updated event detected. Updated customer id: ' . $address->id_customer,
            'Integration'
        );

        $recipientService = new RecipientService();

        $this->enqueueTask(
            new RecipientSyncTask(
                array($recipientService->getPrefixedCustomerId($address->id_customer)),
                array(),
                false
            )
        );
    }

    public function handleCustomerGroupCreatedEvent(\Group $group)
    {
        Logger::logInfo(
            'New customer group created event detected. New customer group id: ' . $group->id,
            'Integration'
        );

        $this->enqueueTask(new FilterSyncTask());
    }

    public function handleCustomerGroupUpdatedEvent(\Group $group)
    {
        Logger::logInfo(
            'New customer group updated event detected. Updated customer group id: ' . $group->id,
            'Integration'
        );

        $this->enqueueTask(new FilterSyncTask());

        $customersIdsInGroup = $this->getPrefixedCustomerIdsInGroup($group);
        if (!empty($customersIdsInGroup)) {
            $this->enqueueTask(new RecipientSyncTask($customersIdsInGroup, array(), false));
        }
    }

    public function handleShopCreatedEvent(\Shop $shop)
    {
        Logger::logInfo('New shop created event detected. New shop id: ' . $shop->id, 'Integration');

        $this->enqueueTask(new FilterSyncTask());
    }

    public function handleShopUpdatedEvent(\Shop $shop)
    {
        Logger::logInfo('New shop updated event detected. New shop id: ' . $shop->id, 'Integration');

        $this->enqueueTask(new FilterSyncTask());

        $recipientService = new RecipientService();
        $recipientIdsInShop = array_merge(
            $this->getPrefixedCustomerIdsInShop($shop),
            $recipientService->getSubscriberIdsByShop($shop)
        );
        if (!empty($recipientIdsInShop)) {
            $this->enqueueTask(new RecipientSyncTask($recipientIdsInShop, array(), false));
        }
    }

    public function handleDisplayFooterProduct()
    {
        // acquire GET parameter crmailing
        $campaignParameterValue = \Tools::getValue('crmailing');

        // do nothing if the parameter doesn't exist
        if (empty($campaignParameterValue)) {
            return;
        }

        // extract product ID
        $productId = (int)\Tools::getValue('id_product');

        // read data from cookie if it already exists
        $data = json_decode($this->cleverReachModule->getContext()->cookie->campaignData, true);
        $data[$productId] = $campaignParameterValue;

        // finally, save json encoded data to cookie
        $this->cleverReachModule->getContext()->cookie->campaignData = json_encode($data);
    }

    public function handleDisplayOrderConfirmation(\Order $order)
    {
        Logger::logInfo('New order created event detected. Order id: ' . $order->id, 'Integration');

        $sendCampaignSyncTask = false;
        $orderItemToCampaignMap = array();
        $productsViewedFromCleverReach = json_decode(
            $this->cleverReachModule->getContext()->cookie->campaignData,
            true
        );

        if (!empty($productsViewedFromCleverReach)) {
            $products = $order->getProducts();
            foreach ($products as $orderItemId => $product) {
                $orderItemToCampaignMap[$orderItemId] = null;
                if (array_key_exists($product['product_id'], $productsViewedFromCleverReach)) {
                    $sendCampaignSyncTask = true;
                    $orderItemToCampaignMap[$orderItemId] = $productsViewedFromCleverReach[$product['product_id']];
                }
            }
        }

        if ($sendCampaignSyncTask) {
            $this->enqueueTask(new CampaignOrderSync($orderItemToCampaignMap));
        } else {
            $recipientService = new RecipientService();
            $this->enqueueTask(
                new RecipientSyncTask(
                    array($recipientService->getPrefixedCustomerId($order->id_customer)),
                    array(),
                    true
                )
            );
        }
    }

    private function enqueueTask(Task $task)
    {
        try {
            $this->getQueueService()->enqueue($this->getConfigService()->getIntegrationName(), $task);
        } catch (QueueStorageUnavailableException $ex) {
            Logger::logDebug(
                json_encode(array(
                    'Message' => 'Failed to enqueue task ' . $task->getType(),
                    'ExceptionMessage' => $ex->getMessage(),
                    'ExceptionTrace' => $ex->getTraceAsString(),
                    'ShopData' => serialize($task)
                )),
                'Integration'
            );
        }
    }

    private function getPrefixedCustomerIdsInGroup(\Group $group)
    {
        $customersInGroup = $group->getCustomers();
        if (empty($customersInGroup)) {
            return array();
        }

        $recipientService = new RecipientService();
        return array_map(function ($customer) use ($recipientService) {
            return $recipientService->getPrefixedCustomerId($customer['id_customer']);
        }, $customersInGroup);
    }

    private function getPrefixedCustomerIdsInShop(\Shop $shop)
    {
        $allShopIds = \Shop::getSharedShops($shop->id, \Shop::SHARE_CUSTOMER);
        $sql = 'SELECT `id_customer`
				FROM `'._DB_PREFIX_.'customer`
				WHERE `id_shop` IN ('.implode(', ', array_map('intval', $allShopIds)).') AND `active` = 1
				ORDER BY `id_customer` ASC';
        $customerIds = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (empty($customerIds)) {
            return array();
        }

        $recipientService = new RecipientService();
        return array_map(function ($customer) use ($recipientService) {
            return $recipientService->getPrefixedCustomerId($customer['id_customer']);
        }, $customerIds);
    }

    private function getConfigService()
    {
        if (empty($this->configService)) {
            $this->configService = ServiceRegister::getService(Configuration::CLASS_NAME);
        }

        return $this->configService;
    }

    private function getQueueService()
    {
        if (empty($this->queue)) {
            $this->queue = ServiceRegister::getService(Queue::CLASS_NAME);
        }

        return $this->queue;
    }
}
