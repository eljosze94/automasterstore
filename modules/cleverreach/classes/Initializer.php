<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes;

use CleverReach\PrestaShop\Classes\BusinessLogicServices\AttributesService;
use InvalidArgumentException;

class Initializer
{

    /**
     * Register all services
     */
    public function registerServices()
    {
        try {
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\Required\Configuration::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\InfrastructureServices\ConfigService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Utility\TimeProvider::CLASS_NAME,
                function () {
                    return new \CleverReach\Infrastructure\Utility\TimeProvider();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\Required\HttpClient::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\InfrastructureServices\HttpClientService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\BusinessLogic\Interfaces\Proxy::CLASS_NAME,
                function () {
                    return new \CleverReach\BusinessLogic\Proxy();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\BusinessLogic\Interfaces\Recipients::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\BusinessLogicServices\RecipientService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\BusinessLogic\Interfaces\OrderItems::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\BusinessLogicServices\OrdersItemsService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\Required\ShopLoggerAdapter::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\InfrastructureServices\LoggerService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\DefaultLoggerAdapter::CLASS_NAME,
                function () {
                    return new \CleverReach\Infrastructure\Logger\DefaultLogger();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\Required\TaskQueueStorage::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\InfrastructureServices\TaskQueueStorageService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\Required\TaskRunnerStatusStorage::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\InfrastructureServices\TaskRunnerStatusService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\Required\AsyncProcessStarter::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\InfrastructureServices\AsyncProcessStarterService();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Interfaces\Exposed\TaskRunnerWakeup::CLASS_NAME,
                function () {
                    return new \CleverReach\Infrastructure\TaskExecution\TaskRunnerWakeup();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\TaskExecution\TaskRunner::CLASS_NAME,
                function () {
                    return new \CleverReach\Infrastructure\TaskExecution\TaskRunner();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\TaskExecution\Queue::CLASS_NAME,
                function () {
                    return new \CleverReach\Infrastructure\TaskExecution\Queue();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\Infrastructure\Utility\GuidProvider::CLASS_NAME,
                function () {
                    return new \CleverReach\Infrastructure\Utility\GuidProvider();
                }
            );
            \CleverReach\Infrastructure\ServiceRegister::registerService(
                \CleverReach\BusinessLogic\Interfaces\Attributes::CLASS_NAME,
                function () {
                    return new \CleverReach\PrestaShop\Classes\BusinessLogicServices\AttributesService();
                }
            );
        } catch (InvalidArgumentException $exception) {
            // Don't do nothing if service is already register
        }
    }
}
