<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\BusinessLogicServices;

use CleverReach\BusinessLogic\Entity\OrderItem;
use CleverReach\BusinessLogic\Interfaces\OrderItems;
use CleverReach\Infrastructure\Logger\Logger;

class OrdersItemsService implements OrderItems
{
    public function getOrderItems($orderItemsIds)
    {
        $formattedOrderItems = array();

        foreach ($orderItemsIds as $orderItemId) {
            $sourceOrderItem = new \OrderDetail($orderItemId);
            $recipientEmail = $this->getReceiverMailByReceiverId($sourceOrderItem);
            
            if (empty($recipientEmail)) {
                continue;
            }
            
            $destinationFormattedOrder = new OrderItem(
                $sourceOrderItem->id_order_detail,
                $sourceOrderItem->product_name
            );
            $destinationFormattedOrder->setProductId((string)$sourceOrderItem->product_id);

            if (isset($sourceOrderItem->product_price)) {
                $destinationFormattedOrder->setPrice($sourceOrderItem->product_price);
            }

            if (isset($destinationFormattedOrder->date_add)) {
                $destinationFormattedOrder->setStamp(date_create_from_format('Y-m-d H:i:s', date('Y-m-d H:i:s')));
            }

            $currency = \Context::getContext()->currency->iso_code;

            if ($currency !== null) {
                $destinationFormattedOrder->setCurrency($currency);
            }

            if (isset($sourceOrderItem->product_quantity)) {
                $destinationFormattedOrder->setAmount($sourceOrderItem->product_quantity);
            }

            $destinationFormattedOrder->setProductSource(\Configuration::get('PS_SHOP_NAME'));
            $destinationFormattedOrder->setRecipientEmail($recipientEmail);

            $formattedOrderItems[] = $destinationFormattedOrder;
        }

        return $formattedOrderItems;
    }

    private function getReceiverMailByReceiverId($sourceOrderItem)
    {
        $order = new \Order($sourceOrderItem->id_order);

        $queryForCustomerMail = 'SELECT `email` 
                                FROM `' . _DB_PREFIX_ . 'customer`
                                WHERE `id_customer` = ' .  (int)$order->id_customer;

        $receiverMail = array();
        
        try {
            $receiverMail = \Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($queryForCustomerMail);
        } catch (\Exception $e) {
            Logger::logDebug(\json_encode(array(
                'Message' => 'Failed to fetch email for user with id: ' . $order->id_customer,
            )));
        }
        
        return !empty($receiverMail) ? $receiverMail : '';
    }
}
