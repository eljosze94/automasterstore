<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\BusinessLogicServices;

use CleverReach\BusinessLogic\Entity\OrderItem;
use CleverReach\BusinessLogic\Entity\Recipient;
use CleverReach\BusinessLogic\Interfaces\Recipients;
use CleverReach\Infrastructure\TaskExecution\Exceptions\RecipientsGetException;
use CleverReach\Infrastructure\Logger\Logger;

class RecipientService implements Recipients
{
    const GROUP_PREFIX = 'G-';
    
    const SHOP_PREFIX = 'S-';
    
    const CUSTOMERS_ID_PREFIX = 'C-';

    const SUBSCRIBERS_ID_PREFIX = 'S-';

    /**
     * @var string
     */
    private $subscribersTableName;

    public function __construct()
    {
        $this->subscribersTableName = _PS_VERSION_ >= '1.7.0.0' ? 'emailsubscription' : 'newsletter';
    }

    /**
     * Gets all tags.
     *
     * @return array of strings with all tags formatted in the instance specific way. If instance has groups G1 and G2
     * - returned array of strings should be: array(G-G1, G-G2,)
     */
    public function getAllTags()
    {
        /** @var int $langId */
        $langId = \Context::getContext()->language->id;

        $query = 'SELECT g.`id_group`, gl.`name`
                  FROM `' . _DB_PREFIX_ . 'group` AS g 
                  INNER JOIN `' . _DB_PREFIX_ . 'group_lang` as gl ON gl.`id_group` = g.`id_group`
                  WHERE gl.`id_lang`="'.pSQL($langId).'"
                  GROUP BY gl.`name`';

        /** @var array $groups */
        $groups = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        $query = 'SELECT s.`id_shop`, s.`name`
                  FROM `' . _DB_PREFIX_ . 'shop` AS s';

        /** @var array $allShops */
        $allShops = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

        return array_merge(
            $this->formatById($groups, self::GROUP_PREFIX),
            $this->formatById($allShops, self::SHOP_PREFIX)
        );
    }

    /**
     * Converts array[] to string[]
     *
     * @param array $dbRows
     * @param string $prefix
     * @return array
     */
    private function formatById($dbRows, $prefix)
    {
        if ($prefix === self::SHOP_PREFIX) {
            $id = 'id_shop';
        } else {
            $id = 'id_group';
        }

        $results = array();

        foreach ($dbRows as $row) {
            if (isset($row[$id])) {
                $results[$row[$id]] = $prefix . $row['name'];
            }
        }

        return $results;
    }

    /**
     * @param array $batchRecipientIds
     * @param bool $includeOrders
     * @return array
     * @throws RecipientsGetException
     */
    public function getRecipientsWithTags(array $batchRecipientIds, $includeOrders)
    {
        /** @var array $arrangedSubscribersAndCustomersIds */
        $arrangedSubscribersAndCustomersIds =
            $this->arrangeSubscribersAndCustomersIdsAndRemovePrefix($batchRecipientIds);

        /** @var array $subscribers */
        $subscribers = $this->getFormattedSubscribersForDestination(
            implode(',', array_map('intval', $arrangedSubscribersAndCustomersIds['subscribers']))
        );
        
        /** @var array $customers */
        $customers = $this->getFormattedCustomersForDestination(
            implode(',', array_map('intval', $arrangedSubscribersAndCustomersIds['customers'])),
            $includeOrders
        );

        return array_merge($subscribers, $customers);
    }

    /**
     * @param array $batchRecipientIds
     * @return array
     */
    private function arrangeSubscribersAndCustomersIdsAndRemovePrefix($batchRecipientIds)
    {
        $formattedIds = array(
            'customers' => array(),
            'subscribers' => array(),
        );

        foreach ($batchRecipientIds as $recipientId) {
            $firstTwoSigns = \Tools::substr($recipientId, 0, 2);
            $recipientIdWithoutPrefix = (int)\Tools::substr($recipientId, 2, \Tools::strlen($recipientId));

            if (\Tools::strtolower($firstTwoSigns) === \Tools::strtolower(self::CUSTOMERS_ID_PREFIX) &&
                !in_array($recipientIdWithoutPrefix, $formattedIds['customers'])
            ) {
                $formattedIds['customers'][] = (int)\Tools::substr($recipientId, 2, \Tools::strlen($recipientId));
            }

            if (\Tools::strtolower($firstTwoSigns) === \Tools::strtolower(self::SUBSCRIBERS_ID_PREFIX) &&
                !in_array($recipientIdWithoutPrefix, $formattedIds['subscribers'])
            ) {
                $formattedIds['subscribers'][] = (int)\Tools::substr($recipientId, 2, \Tools::strlen($recipientId));
            }
        }

        return $formattedIds;
    }

    /**
     * @param string $batchRecipientIds Properly escaped and sanitized comma separated list of recipient ids
     * @return array
     */
    private function getFormattedSubscribersForDestination($batchRecipientIds)
    {
        $formattedSubscribers = array();
        $shops = array();
        $sourceSubscribers = $this->getSourceSubscribers($batchRecipientIds);

        if (!empty($batchRecipientIds)) {
            $shops = $this->getShops('newsletter', $batchRecipientIds);
        }

        foreach ($sourceSubscribers as $subscriber) {
            $formattedSubscriber = new Recipient($subscriber['email']);

            $registered =  isset($subscriber['newsletter_date_add']) ?
                date_create_from_format('Y-m-d H:i:s', $subscriber['newsletter_date_add']) : null;
            $formattedSubscriber->setRegistered($registered);

            $dateAdded = date_create_from_format('Y-m-d H:i:s', $subscriber['newsletter_date_add']);
            if (empty($dateAdded)) {
                Logger::logWarning(\json_encode(array(
                    'Message' => 'Setting current time for subscriber activation date because of invalid shop data',
                    'SubscriberId' => $subscriber['id'],
                    'CreationDate' => $dateAdded
                )));
                $dateAdded = date_create_from_format('Y-m-d H:i:s', date('Y-m-d H:i:s'));
            }

            $formattedSubscriber->setActive($subscriber['active']);
            $formattedSubscriber->setActivated($dateAdded);

            $formattedSubscriber->setSource(\Configuration::get('PS_SHOP_NAME'));
            $formattedSubscriber->setCustomerNumber($subscriber['id']);
            $formattedSubscriber->setNewsletterSubscription((bool)$subscriber['active']);
            $formattedSubscriber->setTags($shops[$subscriber['id']]);

            $formattedSubscribers[] = $formattedSubscriber;
        }

        return $formattedSubscribers;
    }

    /**
     * @param string $batchRecipientIds Properly escaped and sanitized comma separated list of recipient ids
     * @return array
     * @throws RecipientsGetException
     */
    private function getSourceSubscribers($batchRecipientIds)
    {
        if (empty($batchRecipientIds)) {
            return array();
        }

        $query = 'SELECT sub.`id`, sub.`email`, sub.`newsletter_date_add`, sub.`active`
                  FROM `' . _DB_PREFIX_ . pSQL($this->subscribersTableName) . '` as sub 
                  LEFT JOIN `' . _DB_PREFIX_ . 'shop` as s ON s.`id_shop` = sub.`id_shop` 
                  WHERE sub.`id` IN (' . pSQL($batchRecipientIds) . ')';

        $errorMessage = 'Subscribers with ids' . $batchRecipientIds . 'can not be fetched from DB';
        $subscribers = array();
        
        try {
            $subscribers = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        } catch (\Exception $e) {
            $this->handleExceptionAndLogDebugMessage($errorMessage, $e);
        }

        if (!is_array($subscribers)) {
            $this->handleExceptionAndLogDebugMessage($errorMessage);
        }

        return $subscribers;
    }
    
    private function handleExceptionAndLogDebugMessage($errorMessage, $exception = null)
    {
        Logger::logDebug(\json_encode(array(
            'Message' => $errorMessage
        )));
        throw new RecipientsGetException($errorMessage, 0, $exception);
    }

    /**
     * @param string $batchRecipientIds Properly escaped and sanitized comma separated list of recipient ids
     * @param $includeOrders
     *
     * @return array
     * @throws \PrestaShopDatabaseException
     */
    private function getFormattedCustomersForDestination($batchRecipientIds, $includeOrders)
    {
        $formattedCustomers = array();
        $shops = array();
        $groups = array();

        $sourceCustomers = $this->fetchSourceCustomers($batchRecipientIds);
        if (!empty($batchRecipientIds)) {
            $shops = $this->getShops('customer', $batchRecipientIds);
            $groups = $this->getGroups($batchRecipientIds);
        }

        foreach ($sourceCustomers as $sourceCustomer) {
            $destinationCustomer = new Recipient($sourceCustomer['email']);
            
            $dateAdded = date_create_from_format('Y-m-d H:i:s', $sourceCustomer['date_add']);
            if (empty($dateAdded)) {
                Logger::logWarning(\json_encode(array(
                    'Message' => 'Setting current time for customer activation date because of invalid shop data',
                    'CustomerId' => $sourceCustomer['id_customer'],
                    'CreationDate' => $dateAdded
                )));
                $dateAdded = date_create_from_format('Y-m-d H:i:s', date('Y-m-d H:i:s'));
            }

            $destinationCustomer->setRegistered($dateAdded);
            $destinationCustomer->setSource(\Tools::getHttpHost(false));

            $destinationCustomer->setActivated($dateAdded);
            $destinationCustomer->setActive($sourceCustomer['active'] && $sourceCustomer['newsletter']);

            $destinationCustomer->setSalutation($sourceCustomer['gender']);
            $destinationCustomer->setFirstName($sourceCustomer['firstname']);
            $destinationCustomer->setLastName($sourceCustomer['lastname']);
            $destinationCustomer->setPhone($sourceCustomer['phone']);
            $destinationCustomer->setBirthday(\DateTime::createFromFormat('Y-m-d', $sourceCustomer['birthday']));
            $street = $sourceCustomer['address1'] .
                (!empty($sourceCustomer['address2']) ? ", {$sourceCustomer['address2']}" : '');
            $destinationCustomer->setStreet($street);
            $destinationCustomer->setZip($sourceCustomer['postcode']);
            $destinationCustomer->setCity($sourceCustomer['city']);
            $destinationCustomer->setCompany($sourceCustomer['company']);
            $destinationCustomer->setCountry($sourceCustomer['country']);
            $destinationCustomer->setShop(\Configuration::get('PS_SHOP_NAME'));
            $destinationCustomer->setCustomerNumber($sourceCustomer['id_customer']);
            $destinationCustomer->setLanguage($sourceCustomer['language_name']);
            $destinationCustomer->setNewsletterSubscription((bool)$sourceCustomer['newsletter']);
            $destinationCustomer->setTags($this->getTagsForRecipient($sourceCustomer['id_customer'], $shops, $groups));

            if ($includeOrders) {
                $sourceOrders = \Order::getCustomerOrders((int)$sourceCustomer['id_customer']);
                $destinationCustomer->setOrders($this->getFormattedOrdersForDestination($sourceOrders));
            }

            $formattedCustomers[] = $destinationCustomer;
        }
        
        return $formattedCustomers;
    }

    /**
     * @param string $batchRecipientIds Properly escaped and sanitized comma separated list of recipient ids
     *
     * @return array|false|\mysqli_result|null|\PDOStatement|resource
     * @throws \CleverReach\Infrastructure\TaskExecution\Exceptions\RecipientsGetException
     */
    private function fetchSourceCustomers($batchRecipientIds)
    {
        if (empty($batchRecipientIds)) {
            return array();
        }
        
        $query = $this->getQueryForFetchingCustomers($batchRecipientIds);
        $errorMessage  =  'Customers with ids' . $batchRecipientIds . 'can not be fetched from DB';
        $sourceCustomers = array();
        
        try {
            $sourceCustomers = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        } catch (\Exception $e) {
            $this->handleExceptionAndLogDebugMessage($errorMessage, $e);
        }

        if (!is_array($sourceCustomers)) {
            $this->handleExceptionAndLogDebugMessage($errorMessage);
        }
        
        return $sourceCustomers;
    }

    /**
     * @param string $customerIds Properly escaped and sanitized comma separated list of recipient ids
     * @return string
     */
    private function getQueryForFetchingCustomers($customerIds)
    {
        return 'SELECT 
                  customer.`id_customer`,
                  customer.`email`,
                  customer.`date_add`, 
                  customer.`id_shop_group` , 
                  customer.`id_shop`, 
                  customer.`id_gender`, 
                  customer.`firstname`, 
                  customer.`lastname`, 
                  customer.`birthday`, 
                  customer.`newsletter`, 
                  customer.`active`, 
                  customer_address.`address1`,
                  customer_address.`address2`,
                  customer_address.`postcode`,
                  customer_address.`city`,
                  customer_address.`country`,
                  customer_address.`phone`,
                  customer_address.`company`,
                  gender.`name` AS gender,
                  language.`name` AS language_name
                  FROM `' . _DB_PREFIX_ . 'customer` AS customer 
                  LEFT JOIN (SELECT a.`id_customer`, a.`address1`, a.`address2`, 
                                    a.`postcode`, a.`city`, a.`company`, a.`phone`, cl.`name` AS `country` 
                              FROM `' . _DB_PREFIX_ . 'address` AS a 
                              LEFT JOIN `' . _DB_PREFIX_ . 'country_lang` AS cl ON a.`id_country` = cl.`id_country`
                              WHERE a.`id_customer` IN (' . pSQL($customerIds) . ')
                              AND cl.`id_lang` = ' . (int)\Context::getContext()->language->id . '
                              AND a.`deleted` = 0
                              GROUP BY a.`id_customer`) 
                              AS customer_address ON customer.`id_customer` = customer_address.`id_customer`
                  LEFT JOIN `' . _DB_PREFIX_ . 'gender_lang` AS gender 
                            ON gender.`id_gender` = customer.`id_gender` AND gender.`id_lang` = customer.`id_lang`
                  LEFT JOIN `' . _DB_PREFIX_ . 'lang` AS language 
                            ON language.`id_lang` = customer.`id_lang`
                  WHERE customer.`id_customer` IN ('. pSQL($customerIds) . ')';
    }

    /**
     * @param array $sourceOrders
     * @return array
     */
    private function getFormattedOrdersForDestination($sourceOrders)
    {
        $destinationFormattedOrders = array();

        foreach ($sourceOrders as $sourceOrder) {
            $sourceOrderDecorated = new \Order((int)$sourceOrder['id_order']);

            foreach ($sourceOrderDecorated->getProducts() as $product) {
                $destinationFormattedOrder = new OrderItem($product['id_order_detail'], $product['product_name']);
                $destinationFormattedOrder->setProductId((string)$product['product_id']);

                if (isset($product['product_price'])) {
                    $destinationFormattedOrder->setPrice($product['product_price']);
                }
                
                if (isset($product['date_add'])) {
                    $destinationFormattedOrder->setStamp(date_create_from_format('Y-m-d H:i:s', $product['date_add']));
                }

                $currency = \Context::getContext()->currency->iso_code;

                if ($currency !== null) {
                    $destinationFormattedOrder->setCurrency($currency);
                }

                if (isset($product['product_quantity'])) {
                    $destinationFormattedOrder->setAmount($product['product_quantity']);
                }
                
                $destinationFormattedOrder->setProductSource(\Configuration::get('PS_SHOP_NAME'));

                $destinationFormattedOrders[] = $destinationFormattedOrder;
            }
        }

        return $destinationFormattedOrders;
    }

    /**
     * @return array
     * @throws RecipientsGetException
     */
    public function getAllRecipientsIds()
    {
        return array_merge(
            $this->getRecipientsIdsWithPrefix($this->getAllSubscribersIdsWithoutPrefix(), self::SUBSCRIBERS_ID_PREFIX),
            $this->getRecipientsIdsWithPrefix($this->getAllCustomersIdsWithoutPrefix(), self::CUSTOMERS_ID_PREFIX)
        );
    }

    /**
     * Gets prefixed subscriber id by subscriber email
     *
     * @param string $email
     *
     * @return null|string Prefixed subscriber id
     */
    public function getSubscriberIdByEmail($email)
    {
        $subscriberId = null;
        try {
            $query = 'SELECT `id` FROM `' . _DB_PREFIX_ . pSQL($this->subscribersTableName) .
                '` WHERE `email` = "'.pSQL($email).'"';
            $subscriberId = \Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
        } catch (\PrestaShopDatabaseException $exception) {
            // In case of exception return null result
        }

        return !empty($subscriberId) ? self::SUBSCRIBERS_ID_PREFIX . $subscriberId : null;
    }

    /**
     * Gets prefixed subscriber ids that belong to a given shop
     *
     * @param \Shop $shop
     *
     * @return array List of prefixed subscriber ids in shop
     */
    public function getSubscriberIdsByShop(\Shop $shop)
    {
        $subscriberIds = array();
        try {
            $query = 'SELECT `id` FROM `' . _DB_PREFIX_ . pSQL($this->subscribersTableName) .
                '` WHERE `id_shop` = '.(int)$shop->id;
            $subscriberIds = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        } catch (\PrestaShopDatabaseException $exception) {
            // In case of exception return empty result
        }

        if (!empty($subscriberIds)) {
            $subscriberIds = $this->getRecipientsIdsWithPrefix($subscriberIds, self::SUBSCRIBERS_ID_PREFIX);
        } else {
            $subscriberIds = array();
        }

        return $subscriberIds;
    }

    /**
     * Gets prefixed customer id
     *
     * @param string $id Customer id
     *
     * @return string Prefixed customer id
     */
    public function getPrefixedCustomerId($id)
    {
        return self::CUSTOMERS_ID_PREFIX.$id;
    }

    private function getAllSubscribersIdsWithoutPrefix()
    {
        $queryForSubscribers = 'SELECT `id` FROM `' . _DB_PREFIX_ . pSQL($this->subscribersTableName) . '`';
        $errorMessage = 'Subscribers IDs can not be fetched from DB.';
        $allSubscribersIdsWithoutPrefix = array();
        
        try {
            $allSubscribersIdsWithoutPrefix = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($queryForSubscribers);
        } catch (\Exception $e) {
            $this->handleExceptionAndLogDebugMessage($errorMessage, $e);
        }
        
        if (!is_array($allSubscribersIdsWithoutPrefix)) {
            $this->handleExceptionAndLogDebugMessage($errorMessage);
        }
        
        return $allSubscribersIdsWithoutPrefix;
    }

    private function getAllCustomersIdsWithoutPrefix()
    {
        $queryForCustomers = 'SELECT `id_customer` AS id FROM `' . _DB_PREFIX_ . 'customer`';
        $errorMessage = 'Customers IDs can not be fetched from DB';
        $allCustomersIdsWithoutPrefix = array();
            
        try {
            $allCustomersIdsWithoutPrefix = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($queryForCustomers);
        } catch (\Exception $e) {
            $this->handleExceptionAndLogDebugMessage($errorMessage, $e);
        }

        if (!is_array($allCustomersIdsWithoutPrefix)) {
            $this->handleExceptionAndLogDebugMessage($errorMessage);
        }

        return $allCustomersIdsWithoutPrefix;
    }

    /**
     * @param array $recipients
     * @param string $prefix
     * @return array
     */
    private function getRecipientsIdsWithPrefix($recipients, $prefix)
    {
        $formattedRecipientsIds = array();

        foreach ($recipients as $recipient) {
            $formattedRecipientsIds[] = $prefix . $recipient['id'];
        }

        return $formattedRecipientsIds;
    }

    /**
     * Get customer groups for customer ids.
     *
     * @param string $customerIds Properly escaped and sanitized comma separated list of recipient ids
     * @return array
     * @throws \PrestaShopDatabaseException
     */
    private function getGroups($customerIds)
    {
        $query = 'SELECT cg.`id_customer`, gl.`name` as `value` 
                  FROM `' . _DB_PREFIX_ . 'customer_group` AS cg 
                  LEFT JOIN `' . _DB_PREFIX_ . 'group_lang` as gl ON gl.`id_group` = cg.`id_group`
                  WHERE cg.`id_customer` IN (' . pSQL($customerIds) . ') 
                  AND gl.`id_lang` = ' . (int)\Context::getContext()->language->id;

        $groups = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        $result = array();

        foreach ($groups as $group) {
            $result[$group['id_customer']][] = self::GROUP_PREFIX . $group['value'];
        }

        return $result;
    }

    /**
     * Get shops for customer ids
     *
     * @param $tableAlias
     * @param string $ids Properly escaped and sanitized comma separated list of recipient ids
     * @return array
     */
    private function getShops($tableAlias, $ids)
    {
        if ($tableAlias === 'newsletter') {
            $columnId = 'id';
            $table = $this->subscribersTableName;
        } else {
            $columnId = 'id_customer';
            $table = 'customer';
        }

        $query = 'SELECT `' . pSQL($tableAlias) . '`.`'. pSQL($columnId) .'`, `' . pSQL($tableAlias) .
                    '`.`id_shop`, s.`name` as `value`, sg.`share_customer`, sg.`id_shop_group` 
                  FROM `' . _DB_PREFIX_ . pSQL($table). '` AS ' . pSQL($tableAlias) . ' 
                  LEFT JOIN `' . _DB_PREFIX_ . 'shop` as s ON s.`id_shop` = `' . pSQL($tableAlias) . '`.`id_shop` 
                  LEFT JOIN `' . _DB_PREFIX_ . 'shop_group` as sg ON sg.`id_shop_group` = s.`id_shop_group`
                  WHERE `' . pSQL($tableAlias) . '`.`'. pSQL($columnId) .'` IN (' . pSQL($ids) . ') AND s.`active` = 1';

        $shops = \Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        $shops = $this->addSharedShops($shops);
        $result = array();

        foreach ($shops as $shop) {
            $result[$shop[$columnId]][] = self::SHOP_PREFIX . $shop['value'];
        }

        return $result;
    }

    /**
     * Add Shared Shops
     *
     * @param array $shops
     * @return array $result
     */
    private function addSharedShops($shops = array())
    {
        $result = $shops;
        foreach ($shops as $shop) {
            if ($shop['share_customer'] === '1') {
                // If shop is in the shared group, get other shops from same group
                $shopObj = new \Shop();
                $sharedShops = $shopObj->getShopsCollection(true, $shop['id_shop_group'])->getAll();
                foreach ($sharedShops as $sharedShop) {
                    if ($sharedShop->id != $shop['id_shop']) {
                        $shop['value'] = $sharedShop->name;
                        $result[] = $shop;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get formatted tags for customer
     *
     * @param string $recipientId
     * @param array $shops
     * @param array $groups
     * @return array
     */
    private function getTagsForRecipient($recipientId, $shops, $groups)
    {
        if (isset($shops[$recipientId]) && isset($groups[$recipientId])) {
            return array_merge($shops[$recipientId], $groups[$recipientId]);
        }

        return array();
    }
}
