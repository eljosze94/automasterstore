<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\BusinessLogicServices;

use CleverReach\BusinessLogic\Entity\ShopAttribute;
use CleverReach\BusinessLogic\Interfaces\Attributes;
use PrestaShop\PrestaShop\Adapter\Entity\Module;

class AttributesService implements Attributes
{

    private $attributes = array(
        array(
            'name' => 'email',
            'type' => 'text',
            'description' => 'Email address',
            'preview_value' => 'example@email.com',
        ),
        array(
            'name' => 'salutation',
            'type' => 'text',
            'description' => 'Social title'
        ),
        array(
            'name' => 'title',
            'type' => 'text'
        ),
        array(
            'name' => 'firstname',
            'type' => 'text',
            'description' => 'First name',
            'preview_value' => 'John',
        ),
        array(
            'name' => 'lastname',
            'type' => 'text',
            'description' => 'Last name',
            'preview_value' => 'Doe',
        ),
        array(
            'name' => 'street',
            'type' => 'text',
            'description' => 'Address',
            'preview_value' => 'South Street 1a'
        ),
        array(
            'name' => 'zip',
            'type' => 'text',
            'description' => 'Zip/postal code'
        ),
        array(
            'name' => 'city',
            'type' => 'text',
            'description' => 'City'
        ),
        array(
            'name' => 'company',
            'type' => 'text',
            'description' => 'Company'
        ),
        array(
            'name' => 'state',
            'type' => 'text',
            'description' => 'State',
        ),
        array(
            'name' => 'country',
            'type' => 'text',
            'description' => 'Country',
            'default_value' => 'United States'
        ),
        array(
            'name' => 'birthday',
            'type' => 'date',
            'description' => 'Birthday',
            'preview_value' => 'yyyy-mm-dd'
        ),
        array(
            'name' => 'phone',
            'type' => 'text',
            'description' => 'Phone',
            'preview_value' => '+4917612345678'
        ),
        array(
            'name' => 'shop',
            'type' => 'text',
            'description' => 'Shop'
        ),
        array(
            'name' => 'customernumber',
            'type' => 'text',
            'description' => 'ID',
        ),
        array(
            'name' => 'language',
            'type' => 'text',
            'description' => 'Languages',
            'default_value' => 'English'
        ),
        array(
            'name' => 'newsletter',
            'type' => 'number',
            'description' => 'Newsletter'
        )
    );

    public function getAttributeByName($attributeName)
    {
        $attribute = new ShopAttribute();
        $attributeMapped = $this->getMappedAttribute($attributeName);

        if (array_key_exists('description', $attributeMapped)) {
            $attribute->setDescription($this->translate($attributeMapped['description']));
        }

        if (array_key_exists('preview_value', $attributeMapped)) {
            $attribute->setPreviewValue($this->translate($attributeMapped['preview_value']));
        }

        if (array_key_exists('default_value', $attributeMapped)) {
            $attribute->setDefaultValue($this->translate($attributeMapped['default_value']));
        }

        return $attribute;
    }

    private function getMappedAttribute($attributeName)
    {
        foreach ($this->attributes as $attribute) {
            if ($attribute['name'] == $attributeName) {
                return $attribute;
            }
        }
    }

    private function translate($string)
    {
        return \Translate::getModuleTranslation('cleverreach', $string, 'cleverreach');
    }
}
