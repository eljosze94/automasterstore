<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

namespace CleverReach\PrestaShop\Classes\Repositories;

use CleverReach\Infrastructure\Interfaces\Exposed\Runnable;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\TaskExecution\Exceptions\ProcessStorageGetException;

class ProcessRepository
{
    const TABLE_NAME = 'cleverreach_process';
    
    /**
     * @param string $processGuid
     * @param Runnable $runner
     * @throws \Exception
     */
    public function saveGuidAndRunner($processGuid, $runner)
    {
        $result = \Db::getInstance()->insert(
            self::TABLE_NAME,
            array(
                'id' => pSQL($processGuid),
                'runner' => pSQL(serialize($runner)),
            )
        );

        if (empty($result)) {
            Logger::logError('Process runner with guid ' . $processGuid . ' can not be saved.');
            throw new \Exception('Process runner with guid ' . $processGuid . ' can not be saved.');
        }
    }

    /**
     * @param string $processGuid
     *
     * @return Runnable
     * @throws ProcessStorageGetException
     */
    public function getRunnerForGuid($processGuid)
    {
        $query = 'SELECT `runner` FROM '. pSQL($this->getProcessTableName()) .
            ' WHERE `id`=\'' . pSQL($processGuid) . '\'';

        $result = unserialize(\Db::getInstance()->getValue($query));

        if (empty($result)) {
            Logger::logError('Process runner with guid ' . $processGuid . ' does not exist.');
            throw new ProcessStorageGetException(
                'Process runner with guid ' . $processGuid . ' does not exist.'
            );
        }
        
        return $result;
    }

    /**
     * @param string $processGuid
     */
    public function deleteProcessForGuid($processGuid)
    {
        $result = \Db::getInstance()->delete(self::TABLE_NAME, 'id = \'' . pSQL($processGuid) . '\'');

        if (empty($result)) {
            Logger::logError('Could not delete process with guid ' . $processGuid);
        }
    }

    private function getProcessTableName()
    {
        return  _DB_PREFIX_ . self::TABLE_NAME;
    }
}
