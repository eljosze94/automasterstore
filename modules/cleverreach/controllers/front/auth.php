<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

use CleverReach\BusinessLogic\Sync\RefreshUserInfoTask;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\BusinessLogic\Interfaces\Proxy;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\Interfaces\Required\Configuration as ConfigInterface;

class CleverReachAuthModuleFrontController extends ModuleFrontController
{

    public function __construct()
    {
        parent::__construct();
        $initializer = new \CleverReach\PrestaShop\Classes\Initializer();
        $initializer->registerServices();
    }

    public function initContent()
    {
        $code = Tools::getValue('code');
        if (empty($code)) {
            die(json_encode(array(
                'success' => false,
                'message' => $this->module->l('Wrong parameters. Code not set.')
            )));
        }

        $redirectUrl = $this->context->link->getModuleLink(
            'cleverreach',
            'auth',
            array(),
            null,
            null,
            Configuration::get('PS_SHOP_DEFAULT')
        );
        
        $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        $result = $proxy->getAccessToken($code, $redirectUrl);

        if (isset($result['error']) || empty($result['access_token'])) {
            die(json_encode(array(
                'status' => isset($result['error']) ? $result['error'] : false,
                'message' => isset($result['error_description']) ?
                    $result['error_description'] :
                    $this->module->l('Unsuccessful connection.'),
            )));
        }

        $queue = ServiceRegister::getService(Queue::CLASS_NAME);
        $configService = ServiceRegister::getService(ConfigInterface::CLASS_NAME);
        $queue->enqueue($configService->getQueueName(), new RefreshUserInfoTask($result['access_token']));

        if (_PS_VERSION_ >= '1.7.0.0') {
            $this->setTemplate('module:cleverreach/views/templates/front/windowClose.tpl');
        } else {
            $this->setTemplate('windowClose.tpl');
        }
    }
}
