<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

use CleverReach\PrestaShop\Classes\InfrastructureServices\ConfigService;
use CleverReach\PrestaShop\Classes\Utility\CleverReachUtility;

class CleverReachSearchModuleFrontController extends ModuleFrontController
{

    /**
     * Status for nonexistent product
     */
    const NO_PRODUCT = 8;

    /** @var  ConfigService */
    private $configService;

    /**
     * CleverReachProductSearchModuleFrontController constructor.
     */
    public function __construct()
    {
        $this->configService = new ConfigService();
        parent::__construct();
    }

    public function initContent()
    {
        $param = Tools::getValue('get');

        switch ($param) {
            case 'filter':
                $description = 'Products will be searched by name, reference, ean, upc, isbn or supplier reference';
                $filter = array(
                    'name' => 'Product name OR Product ID',
                    'description' => $description,
                    'required' => false,
                    'query_key' => 'sku',
                    'type' => 'input',
                );

                CleverReachUtility::dieJson(array($filter, $this->getFilterSelect()));
                break;
            case 'search':
                CleverReachUtility::dieJson($this->getSearchItems());
                break;
        }

        Controller::getController('PageNotFoundController')->run();
    }

    /**
     * Create select filter of all available shops
     *
     * @return array
     */
    private function getFilterSelect()
    {
        $filterSelect = array(
            'name' => 'Store',
            'description' => '',
            'required' => false,
            'query_key' => 'shop',
            'type' => 'dropdown',
        );

        $shops = $this->context->shop->getShops();

        $filterSelect['values'] = array(
            array(
                'text' => 'Please select shop',
                'value' => 0,
            )
        );

        foreach ($shops as $shop) {
            $filterSelect['values'][] = array(
                'text' => $shop['name'],
                'value' => $shop['id_shop'],
            );
        }

        return $filterSelect;
    }

    /**
     * Searches for the product by given POST parameters
     * @return stdClass|array
     */
    private function getSearchItems()
    {
        $items = new \stdClass();
        $items->settings = new \stdClass();
        $items->settings->type = 'product';
        $items->settings->link_editable = false;
        $items->settings->link_text_editable = false;
        $items->settings->image_size_editable = false;
        $items->items = array();

        // SEARCH
        $productClass = new \Product();
        $sku = Tools::getValue('sku');
        $shopId = (int)Tools::getValue('shop') ?: null;
        $products = array();

        $previousShopContext = Shop::getContext();
        if (empty($shopId)) {
            Shop::setContext(Shop::CONTEXT_ALL);
        } else {
            Shop::setContext(Shop::CONTEXT_SHOP, $shopId);
        }

        $references = $productClass->searchByName($this->context->language->id, $sku);
        if (!empty($references)) {
            foreach ($references as $reference) {
                if (isset($reference['id_product'])) {
                    $products[] = new \Product(
                        (int)$reference['id_product'],
                        true,
                        $this->context->language->id,
                        $shopId
                    );
                }
            }
        }

        foreach ($products as $product) {
            if (empty($product) || $product->id === null) {
                continue;
            }

            $items->items[] = $this->convertProductToSearchItem($product);
        }

        Shop::setContext($previousShopContext);

        if (empty($items->items)) {
            return array(
                'status' => self::NO_PRODUCT,
                'message' => $this->module->l('There is no product with given Reference, ID or title', 'en'),
            );
        }

        return $items;
    }

    private function convertProductToSearchItem(Product $product)
    {
        $shopId = (int)Tools::getValue('shop') ?: null;

        $item = new \stdClass();
        $item->title = $product->name;
        $item->description = $product->description;

        // get product image link
        $imageId = $product->getCover($product->id);
        if (_PS_VERSION_ >= '1.7.0.0') {
            $imageSize = ImageType::getFormattedName('large');
        } else {
            $imageSize = ImageType::getFormatedName('large');
        }

        $item->image = $this->context->link->getImageLink($product->link_rewrite, $imageId['id_image'], $imageSize);

        // get product price
        if (!empty($product->tax_rate)) {
            $item->price = $this->calculateProductPrice($product->price, $product->tax_rate);
        } else {
            $item->price = (float)$product->price;
        }

        $item->price .= ' ' . Context::getContext()->currency->sign;
        $item->link = Context::getContext()->link->getProductLink(
            (int)$product->id,
            null,
            null,
            null,
            null,
            $shopId
        );

        return $item;
    }

    /**
     * Calculate product price based on tax rate
     *
     * @param $price
     * @param $tax_rate
     * @return float
     */
    private function calculateProductPrice($price, $tax_rate)
    {
        return round($price + ((float)$price * (float)$tax_rate * 0.01), 2);
    }
}
