<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

require_once 'AdminCleverReachBaseController.php';

class AdminCleverReachWelcomeController extends AdminCleverReachBaseController
{
    
    public function initContent()
    {
        parent::initContent();
        $this->initTabModuleList();
        $this->initToolbar();
        $this->initPageHeaderToolbar();
        $this->addToolBarModulesListButton();
        unset($this->toolbar_btn['save']);

        $logoUrl = $this->getModuleFileUrl('views/img/logo_cleverreach.svg');
        $helloImgUrl = $this->getModuleFileUrl('views/img/icon_hello.png');
        $redirectUrl = $this->context->link->getModuleLink(
            'cleverreach',
            'auth',
            array(),
            null,
            null,
            Configuration::get('PS_SHOP_DEFAULT')
        );

        $registerData = $this->getRegisterData();

        $this->context->smarty->assign(
            array(
                'cleverreach_logoUrl' => $logoUrl,
                'cleverreach_helloImgUrl' => $helloImgUrl,
                'cleverreach_adminUrl' => $this->context->link->getAdminLink('AdminCleverReachWelcome'),
                'cleverreach_authUrl' => $this->getProxy()->getAuthUrl($redirectUrl, $registerData),
            )
        );

        $this->setTemplate('welcome.tpl');
    }

    /**
     * Check task status for refreshing user data based on access token
     */
    public function displayAjaxCheckConnectionStatus()
    {
        $status = 'finished';
        $queue = CleverReach\Infrastructure\ServiceRegister::getService(
            CleverReach\Infrastructure\TaskExecution\Queue::CLASS_NAME
        );
        /** @var CleverReach\Infrastructure\TaskExecution\QueueItem $queueItem */
        $queueItem = $queue->findLatestByType('RefreshUserInfoTask');

        if (isset($queueItem)) {
            $queueStatus = $queueItem->getStatus();
            if ($queueStatus !== CleverReach\Infrastructure\TaskExecution\QueueItem::FAILED
                && $queueStatus !== CleverReach\Infrastructure\TaskExecution\QueueItem::COMPLETED) {
                $status = 'in_progress';
            }
        }

        die(json_encode(array(
            'status' => $status,
        )));
    }

    /**
     * Get registration data for user
     *
     * @return string
     */
    private function getRegisterData()
    {
        $address = Configuration::get('PS_SHOP_ADDR1', null, null, $this->context->shop->id);
        if (empty($address)) {
            $address = Configuration::get('PS_SHOP_ADDR2', null, null, $this->context->shop->id);
        }

        $countryId = Configuration::get('PS_SHOP_COUNTRY_ID', null, null, $this->context->shop->id);
        $country = new Country();

        $registerData = array(
            'email' => $this->context->employee->email,
            'company' => $this->context->shop->name,
            'firstname' => $this->context->employee->firstname,
            'lastname' => $this->context->employee->lastname,
            'gender' => '',
            'street' => $address,
            'zip' => Configuration::get('PS_SHOP_CODE', null, null, $this->context->shop->id),
            'city' => Configuration::get('PS_SHOP_CITY', null, null, $this->context->shop->id),
            'country' => $country->getNameById($this->context->language->id, $countryId),
            'phone' => Configuration::get('PS_SHOP_PHONE', null, null, $this->context->shop->id)
        );

        return base64_encode(json_encode($registerData));
    }
}
