<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

use CleverReach\BusinessLogic\Sync\InitialSyncTask;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\Interfaces\Required\Configuration as ConfigInterface;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\PrestaShop\Classes\InfrastructureServices\ConfigService;
use CleverReach\PrestaShop\Classes\Utility\CleverReachUtility;

class AdminCleverReachConfigController extends ModuleAdminController
{

    /** @var ConfigService  */
    private $configService;

    public function __construct()
    {
        parent::__construct();
    }

    public function init()
    {
        parent::init();
        $this->configService = ServiceRegister::getService(ConfigInterface::CLASS_NAME);
    }

    public function initContent()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            CleverReachUtility::dieJson($this->post());
        } else {
            CleverReachUtility::dieJson($this->getConfigParameters());
        }
    }

    public function getConfigParameters()
    {
        return array(
            'integrationId' => $this->configService->getIntegrationId(),
            'integrationName' => $this->configService->getIntegrationName(),
            'minLogLevel' => $this->configService->getMinLogLevel(),
            'isProductSearchEnabled' => $this->configService->isProductSearchEnabled(),
            'productSearchParameters' => $this->configService->getProductSearchParameters(),
            'recipientsSynchronizationBatchSize' => $this->configService->getRecipientsSynchronizationBatchSize(),
            'maxStartedTasksLimit' => $this->configService->getMaxStartedTasksLimit(),
            'maxTaskExecutionRetries' => $this->configService->getMaxTaskExecutionRetries(),
            'maxTaskInactivityPeriod' => $this->configService->getMaxTaskInactivityPeriod(),
            'taskRunnerMaxAliveTime' => $this->configService->getTaskRunnerMaxAliveTime(),
            'taskRunnerStatus' => $this->configService->getTaskRunnerStatus(),
            'taskRunnerWakeupDelay' => $this->configService->getTaskRunnerWakeupDelay(),
            'queueName' => $this->configService->getQueueName()
        );
    }

    public function post()
    {
        $payload = json_decode(\Tools::file_get_contents('php://input'), true);
        $minLogLevel = $payload['minLogLevel'];
        if ($minLogLevel) {
            \Configuration::updateValue('CLEVERREACH_MIN_LOG_LEVEL', $minLogLevel);
        }

        $defaultLoggerStatus = $payload['defaultLoggerStatus'];
        if ($defaultLoggerStatus) {
            $this->configService->setDefaultLoggerEnabled($defaultLoggerStatus);
        }

        $maxStartedTasksLimit = $payload['maxStartedTasksLimit'];
        if ($maxStartedTasksLimit) {
            $this->configService->setMaxStartedTaskLimit($maxStartedTasksLimit);
        }

        $taskRunnerWakeUpDelay = $payload['taskRunnerWakeUpDelay'];
        if ($taskRunnerWakeUpDelay) {
            $this->configService->setTaskRunnerWakeUpDelay($taskRunnerWakeUpDelay);
        }

        $taskRunnerMaxAliveTime = $payload['taskRunnerMaxAliveTime'];
        if ($taskRunnerMaxAliveTime) {
            $this->configService->setTaskRunnerMaxAliveTime($taskRunnerMaxAliveTime);
        }

        $maxTaskExecutionRetries = $payload['maxTaskExecutionRetries'];
        if ($maxTaskExecutionRetries) {
            $this->configService->setMaxTaskExecutionRetries($maxTaskExecutionRetries);
        }

        $maxTaskInactivityPeriod = $payload['maxTaskInactivityPeriod'];
        if ($maxTaskInactivityPeriod) {
            $this->configService->setMaxTaskInactivityPeriod($maxTaskInactivityPeriod);
        }

        $productSearchEndpointPassword = $payload['productSearchEndpointPassword'];
        if ($productSearchEndpointPassword) {
            $this->configService->setProductSearchEndpointPassword($productSearchEndpointPassword);
        }

        $restartFullSynchronization = $payload['restartFullSynchronization'];
        if ($restartFullSynchronization) {
            /** @var Queue $queue */
            $queue = ServiceRegister::getService(Queue::CLASS_NAME);
            $queue->enqueue($this->configService->getIntegrationName(), new InitialSyncTask());
        }

        return array('message' => 'Successfully updated config values');
    }
}
