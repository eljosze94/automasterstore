<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

require_once 'AdminCleverReachBaseController.php';

class AdminCleverReachDashboardController extends AdminCleverReachBaseController
{

    const CLEVERREACH_HELP_URL = 'https://support.cleverreach.de/hc/en-us/requests/new';
    const CLEVERREACH_BUILD_EMAIL_URL = '/admin/login.php?ref=%2Fadmin%2Fmailing_create_new.php';

    public function initContent()
    {
        parent::initContent();
        $this->initTabModuleList();
        $this->initToolbar();
        $this->initPageHeaderToolbar();
        $this->addToolBarModulesListButton();
        unset($this->toolbar_btn['save']);

        $logoUrl = $this->getModuleFileUrl('views/img/logo_cleverreach.svg');
        $emailsImgUrl = $this->getModuleFileUrl('views/img/icon_quickstartmailing.svg');
        $userInfo = $this->getConfigService()->getUserInfo();

        $this->context->smarty->assign(
            array(
                'cleverreach_logoUrl' => $logoUrl,
                'cleverreach_emailsImgUrl' => $emailsImgUrl,
                'cleverreach_helpUrl' => self::CLEVERREACH_HELP_URL,
                'cleverreach_adminUrl' => $this->context->link->getAdminLink('AdminCleverReachDashboard'),
                'cleverreach_buildEmailUrl' => 'https://' . $userInfo['login_domain'] .
                    self::CLEVERREACH_BUILD_EMAIL_URL,
                'cleverreach_firstEmailBuild' => $this->getConfigService()->isFirstEmailBuild(),
                'cleverreach_customerId' => sprintf(
                    $this->l('CleverReach® customer ID: %s'),
                    $userInfo['id']
                ),
                'cleverreach_targetEmailMarketing' => sprintf(
                    $this->l('Targeted email marketing for more revenue in your %s shop.'),
                    $this->getConfigService()->getIntegrationName()
                ),
            )
        );

        $this->setTemplate('dashboard.tpl');
    }

    /**
     * Save configuration that client click on the button to build first email
     */
    public function displayAjaxFirstEmailBuild()
    {
        $this->getConfigService()->setIsFirstEmailBuild('1');
    }
}
