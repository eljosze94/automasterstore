<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

use CleverReach\BusinessLogic\Sync\InitialSyncTask;
use CleverReach\Infrastructure\TaskExecution\QueueItem;

require_once 'AdminCleverReachBaseController.php';

class AdminCleverReachInitialSyncController extends AdminCleverReachBaseController
{
    public function initContent()
    {
        parent::initContent();
        $this->initTabModuleList();
        $this->initToolbar();
        $this->initPageHeaderToolbar();
        $this->addToolBarModulesListButton();
        unset($this->toolbar_btn['save']);

        $logoUrl = $this->getModuleFileUrl('views/img/logo_cleverreach.svg');
        $statusCheckUrl = $this->context->link->getAdminLink('AdminCleverReachInitialSync') . '&' .
            http_build_query(array('ajax' => true,'action' => 'getInitialSyncStatus'));
        $dashboardUrl = $this->context->link->getAdminLink('AdminCleverReachDashboard');

        $this->context->smarty->assign(
            array(
                'cleverreach_logoUrl' => $logoUrl,
                'cleverreach_statusCheckUrl' => $statusCheckUrl,
                'cleverreach_dashboardUrl' => $dashboardUrl,
                'cleverreach_recipientSyncTaskTitle' => sprintf(
                    $this->l('Import recipients from %s to CleverReach®'),
                    $this->getConfigService()->getIntegrationName()
                ),
            )
        );

        $this->setTemplate('task_list.tpl');
    }

    public function displayAjaxGetInitialSyncStatus()
    {
        $initialSyncTaskQueueItem = $this->getQueueService()->findLatestByType('InitialSyncTask');
        if (empty($initialSyncTaskQueueItem)) {
            die(Tools::jsonEncode(array(
                'status' => QueueItem::FAILED,
            )));
        }

        /** @var InitialSyncTask $initialSyncTask */
        $initialSyncTask = $initialSyncTaskQueueItem->getTask();
        $initialSyncTaskProgress = $initialSyncTask->getProgressByTask();

        $subscriberListProgress = ($initialSyncTaskProgress['ProductSearchSyncTask'] / 2) +
            ($initialSyncTaskProgress['GroupSyncTask'] / 2);

        $addFieldsProgress = ($initialSyncTaskProgress['AttributesSyncTask'] / 2) +
            ($initialSyncTaskProgress['FilterSyncTask'] / 2);

        $recipientsProgress = $initialSyncTaskProgress['RecipientSyncTask'];

        $subscriberListStatus = QueueItem::QUEUED;
        if (0 < $subscriberListProgress && $subscriberListProgress < 100) {
            $subscriberListStatus = QueueItem::IN_PROGRESS;
        } elseif ($subscriberListProgress >= 100) {
            $subscriberListStatus = QueueItem::COMPLETED;
        }

        $addFieldsStatus = QueueItem::QUEUED;
        if (0 < $addFieldsProgress && $addFieldsProgress < 100) {
            $addFieldsStatus = QueueItem::IN_PROGRESS;
        } elseif ($addFieldsProgress >= 100) {
            $addFieldsStatus = QueueItem::COMPLETED;
        }

        $recipientsStatus = QueueItem::QUEUED;
        if (0 < $recipientsProgress && $recipientsProgress < 100) {
            $recipientsStatus = QueueItem::IN_PROGRESS;
        } elseif ($recipientsProgress >= 100) {
            $recipientsStatus = QueueItem::COMPLETED;
        }

        die(Tools::jsonEncode(array(
            'status' => $initialSyncTaskQueueItem->getStatus(),
            'statistics' => array(
                'recipients_count' => $initialSyncTask->getSyncedRecipientsCount(),
                'group_name' => $this->getConfigService()->getIntegrationName(),
            ),
            'taskStatuses' => array(
                'subscriberlist' => array(
                    'status' => $subscriberListStatus,
                    'progress' => $subscriberListProgress
                ),
                'add_fields' => array(
                    'status' => $addFieldsStatus,
                    'progress' => $addFieldsProgress
                ),
                'recipient_sync' => array(
                    'status' => $recipientsStatus,
                    'progress' => $recipientsProgress
                ),
            )
        )));
    }
}
