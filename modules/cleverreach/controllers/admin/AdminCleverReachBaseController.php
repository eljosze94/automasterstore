<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

use CleverReach\BusinessLogic\Proxy;
use CleverReach\BusinessLogic\Sync\InitialSyncTask;
use CleverReach\Infrastructure\Interfaces\Exposed\TaskRunnerWakeup;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\Interfaces\Required\Configuration as ConfigInterface;
use CleverReach\Infrastructure\TaskExecution\Exceptions\QueueStorageUnavailableException;
use CleverReach\Infrastructure\TaskExecution\Queue;
use CleverReach\Infrastructure\TaskExecution\QueueItem;
use CleverReach\PrestaShop\Classes\InfrastructureServices\ConfigService;

class AdminCleverReachBaseController extends ModuleAdminController
{

    /** @var ConfigService */
    private $configService;

    /** @var Proxy */
    private $proxy;

    /** @var Queue */
    private $queue;

    /** @var TaskRunnerWakeup */
    private $taskRunnerWakeupService;

    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();
    }

    public function init()
    {
        parent::init();

        if (!$this->isAuthTokenValid()) {
            $this->redirectToControllerIfNecessary('AdminCleverReachWelcome');
            return;
        }

        if ($this->isInitialSyncInProgress()) {
            $this->redirectToControllerIfNecessary('AdminCleverReachInitialSync');
            return;
        }

        $this->redirectToControllerIfNecessary('AdminCleverReachDashboard');

        $this->getTaskRunnerWakeupService()->wakeup();
    }

    /**
     * Returns static module file web URL
     *
     * @param string $file
     * @return string
     */
    protected function getModuleFileUrl($file)
    {
        return $this->context->shop->getBaseUrl() . 'modules/cleverreach/' . $file;
    }

    private function isAuthTokenValid()
    {
        $accessToken = $this->getConfigService()->getAccessToken();

        return !empty($accessToken);
    }

    private function isInitialSyncInProgress()
    {
        // Do not check anything for ajax calls
        if ($this->ajax) {
            return false;
        }

        $initialSyncTaskItem = $this->getQueueService()->findLatestByType('InitialSyncTask');
        if (!$initialSyncTaskItem || $initialSyncTaskItem->getStatus() === QueueItem::FAILED) {
            try {
                $this->getQueueService()->enqueue($this->getConfigService()->getQueueName(), new InitialSyncTask());
            } catch (QueueStorageUnavailableException $e) {
                // If task enqueue fails do nothing but report that initial sync is in progress
            }

            return true;
        }

        return $initialSyncTaskItem->getStatus() !== QueueItem::COMPLETED;
    }

    private function redirectToControllerIfNecessary($name)
    {
        if (!$this->ajax && $name !== $this->controller_name) {
            Tools::redirectAdmin($this->context->link->getAdminLink($name));
        }
    }

    /**
     * Get proxy
     *
     * @return Proxy
     */
    protected function getProxy()
    {
        if (empty($this->proxy)) {
            $this->proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        }

        return $this->proxy;
    }

    protected function getConfigService()
    {
        if (empty($this->configService)) {
            $this->configService = ServiceRegister::getService(ConfigInterface::CLASS_NAME);
        }

        return $this->configService;
    }

    protected function getQueueService()
    {
        if (empty($this->queue)) {
            $this->queue = ServiceRegister::getService(Queue::CLASS_NAME);
        }

        return $this->queue;
    }

    protected function getTaskRunnerWakeupService()
    {
        if (empty($this->taskRunnerWakeupService)) {
            $this->taskRunnerWakeupService = ServiceRegister::getService(TaskRunnerWakeup::CLASS_NAME);
        }

        return $this->taskRunnerWakeupService;
    }
}
