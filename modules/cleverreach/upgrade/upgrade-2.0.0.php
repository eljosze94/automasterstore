<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Updates module from previous versions to the version 2.0.0.
 * Modify database, register a new hooks ...
 */
function upgrade_module_2_0_0(CleverReach $module)
{
    $previousShopContext = Shop::getContext();
    Shop::setContext(Shop::CONTEXT_ALL);

    $query = 'DROP TABLE IF EXISTS ' . _DB_PREFIX_ . 'cleverreach';
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }

    $module->removeControllerBy('CleverReachAdmin');
    $module->removeControllerBy('CleverReachConfig');
    $module->removeControllerBy('CleverReachImport');
    $module->unregisterHook('actionObjectShopDeleteAfter');

    unlinkObsoleteFiles($module->getLocalPath());

    $module->installAndConfigurePluginBase();
    $success = $module->enable() && $module->registerControllersAndHooks();

    Shop::setContext($previousShopContext);

    return $success;
}

function unlinkObsoleteFiles($baseModulePath)
{
    $obsoleteFiles = array(
        $baseModulePath.'CleverReach-UserManual_de.pdf',
        $baseModulePath.'CleverReach-UserManual_en.pdf',
        $baseModulePath.'classes/BackgroundProcess.php',
        $baseModulePath.'classes/CleverReachApiClient.php',
        $baseModulePath.'classes/CleverReachCustomerFormatter.php',
        $baseModulePath.'classes/CleverReachUtility.php',
        $baseModulePath.'classes/ConfigModel.php',
        $baseModulePath.'classes/Data.php',
        $baseModulePath.'classes/DataModel.php',
        $baseModulePath.'classes/Abstract/ConfigAbstract.php',
        $baseModulePath.'classes/Abstract/index.php',
        $baseModulePath.'controllers/admin/CleverReachAdminController.php',
        $baseModulePath.'controllers/admin/CleverReachConfigController.php',
        $baseModulePath.'controllers/admin/CleverReachImportController.php',
        $baseModulePath.'controllers/front/import.php',
        $baseModulePath.'views/css/module.css',
        $baseModulePath.'views/img/cr.png',
        $baseModulePath.'views/img/envelope.svg',
        $baseModulePath.'views/js/cleverreach.js',
        $baseModulePath.'views/templates/admin/content.tpl',
    );


    foreach ($obsoleteFiles as $file) {
        @unlink($file);
    }

    $obsoleteDirs = array($baseModulePath.'classes/Abstract');
    foreach ($obsoleteDirs as $dir) {
        @rmdir($dir);
    }
}
