<?php
/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

$crProcessesTableCreationScript =
    'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'cleverreach_process 
    (
     `id` VARCHAR(50) NOT NULL,
     `runner` VARCHAR(500) NOT NULL,
      PRIMARY KEY(`id`)
    )
    ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

$crQueueTableCreationScript =
    'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'cleverreach_queue`
    (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `status` varchar(30) NOT NULL,
      `type` varchar(100) NOT NULL,
      `queueName` varchar(50) NOT NULL,
      `progress` tinyint(4) NOT NULL DEFAULT \'0\',
      `retries` tinyint(4) NOT NULL DEFAULT \'0\',
      `failureDescription` text,
      `serializedTask` mediumtext NOT NULL,
      `createTimestamp` int(11) DEFAULT NULL,
      `queueTimestamp` int(11) DEFAULT NULL,
      `lastUpdateTimestamp` int(11) DEFAULT NULL,
      `startTimestamp` int(11) DEFAULT NULL,
      `finishTimestamp` int(11) DEFAULT NULL,
      `failTimestamp` int(11) DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
';

$sql = array(
    $crProcessesTableCreationScript,
    $crQueueTableCreationScript
);

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
