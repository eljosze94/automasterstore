{**
* 2017 CleverReach
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    CleverReach <partner@cleverreach.com>
* @copyright 2017 CleverReach GmbH
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="panel clever-reach">
    <input type="hidden" id="cr_auth_url" value="{$cleverreach_authUrl|escape:'htmlall':'UTF-8'}">
    <input type="hidden" id="cr_admin_url" value="{$cleverreach_adminUrl|escape:'htmlall':'UTF-8'}">

    <img class="logo" src="{$cleverreach_logoUrl|escape:'htmlall':'UTF-8'}"/>

    <div class="loader-big">
        <span class="loader"></span>
    </div>

    <div class="connecting">
        <span>{l s='Connecting...' mod='cleverreach'}</span>
    </div>

    <div class="tab-content panel welcome">
        <div class="tab-pane active">
            <div class="form-wrapper">
                <img class="hello" src="{$cleverreach_helloImgUrl|escape:'htmlall':'UTF-8'}"/>
                <h1>{l s='Welcome to clever Email Marketing' mod='cleverreach'}</h1>
                <p>{l s='Simply transmit Prestashop data and get started with CleverReach® for free!' mod='cleverreach'}</p>
                <button type="button" class="btn btn-primary" id="cr-newAccount">
                    {l s='Create and connect your new CleverReach® account now!' mod='cleverreach'}
                </button>
                <button type="button" class="btn" id="cr-logAccount">
                    {l s='Log in and connect with an existing CleverReach® account' mod='cleverreach'}
                </button>
            </div>
        </div>
    </div>
</div>