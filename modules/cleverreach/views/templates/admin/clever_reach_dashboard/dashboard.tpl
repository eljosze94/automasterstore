{**
* 2017 CleverReach
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    CleverReach <partner@cleverreach.com>
* @copyright 2017 CleverReach GmbH
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="panel clever-reach">
    <input type="hidden" id="cr_admin_url" value="{$cleverreach_adminUrl|escape:'htmlall':'UTF-8'}">
    <input type="hidden" id="cr_build_email_url" value="{$cleverreach_buildEmailUrl|escape:'htmlall':'UTF-8'}">

    <img class="logo" src="{$cleverreach_logoUrl|escape:'htmlall':'UTF-8'}"/>

    <div class="form-horizontal">
        <ul class="nav nav-tabs">
            <li class="list-links active">
                <a href="#">
                    <div class="tab-links active">
                        {l s='Dashboard' mod='cleverreach'}
                    </div>
                </a>
            </li>
            <li class="list-links right">
                <div class="tab-links">
                    {$cleverreach_customerId|escape:'htmlall':'UTF-8'}
                </div>
            </li>
            <li class="list-links right">
                <div class="tab-links">
                    <a href="{$cleverreach_helpUrl|escape:'htmlall':'UTF-8'}" target="_blank">
                        {l s='Help & Support' mod='cleverreach'}
                    </a>
                </div>
            </li>
        </ul>

        <div class="tab-content panel">
            <div class="tab-pane active">
                <div class="dashboard">
                    <div class="form-wrapper">
                        <img class="emails" src="{$cleverreach_emailsImgUrl|escape:'htmlall':'UTF-8'}"/>
                        {if $cleverreach_firstEmailBuild === false}
                            <h1>{l s='Targeted email marketing for more revenue!' mod='cleverreach'}</h1>
                            <p>{$cleverreach_targetEmailMarketing|escape:'htmlall':'UTF-8'}</p>
                            <p>{l s='Create appealing emails to showcase your products to your customers.' mod='cleverreach'}</p>
                        {else}
                            <h1>{l s='Targeted email marketing for more revenue!' mod='cleverreach'}</h1>
                            <p>{$cleverreach_targetEmailMarketing|escape:'htmlall':'UTF-8'}</p>
                            <p>{l s='Create appealing emails to showcase your products to your customers.' mod='cleverreach'}</p>
                        {/if}

                        <button type="button" class="btn btn-primary" id="cr-buildEmail">
                            {if $cleverreach_firstEmailBuild === false}
                                {l s='Create your first newsletter →' mod='cleverreach'}
                            {else}
                                {l s='Create your next newsletter now! →' mod='cleverreach'}
                            {/if}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>