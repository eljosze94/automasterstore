{**
* 2017 CleverReach
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    CleverReach <partner@cleverreach.com>
* @copyright 2017 CleverReach GmbH
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="panel clever-reach">
    <input type="hidden" id="cr_admin_status_check_url" value="{$cleverreach_statusCheckUrl|escape:'htmlall':'UTF-8'}">
    <input type="hidden" id="cr_admin_dashboard_url" value="{$cleverreach_dashboardUrl|escape:'htmlall':'UTF-8'}">

    <img class="logo" src="{$cleverreach_logoUrl|escape:'htmlall':'UTF-8'}"/>

    <div class="form-horizontal">
        <div class="tab-content panel" data-task-list-panel>
            <div class="tab-pane active">
                <div class="task-list">
                    <div class="form-wrapper">
                        <ul class="list-group">
                            <li class="list-group-item disabled" data-task="subscriberlist">
                                <div class="pull-left">
                                    <span class="icon-circle" aria-hidden="true" data-status="subscriberlist"></span>
                                </div>
                                {l s='Create recipient list in CleverReach®' mod='cleverreach'}
                                <span class="badge" data-progress="subscriberlist">0%</span>
                            </li>
                            <li class="list-group-item disabled" data-task="add_fields">
                                <div class="pull-left">
                                    <span class="icon-circle" aria-hidden="true" data-status="add_fields"></span>
                                </div>
                                {l s='Add data fields, segments and tags to recipient list' mod='cleverreach'}
                                <span class="badge" data-progress="add_fields">0%</span>
                            </li>
                            <li class="list-group-item disabled" data-task="recipient_sync">
                                <div class="pull-left">
                                    <span class="icon-circle" aria-hidden="true" data-status="recipient_sync"></span>
                                </div>
                                {$cleverreach_recipientSyncTaskTitle|escape:'htmlall':'UTF-8'}
                                <span class="badge" data-progress="recipient_sync">0%</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content panel hidden" data-success-panel>
            <div class="tab-pane active">
                <div class="dashboard">
                    <div class="form-wrapper">
                        <h1>{l s='Well done!' mod='cleverreach'}</h1>
                        <p data-success-panel-message>
                            {l s='%s recipients have been successfully added to the list %s in your CleverReach® account.' mod='cleverreach'}
                        </p>
                        <button type="button" class="btn btn-primary" data-success-panel-go-to-dashboard-button data-success-panel-autoredirect-text>
                            {l s='You will be directed to your plugin dashboard in ... %d sec.' mod='cleverreach'}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>