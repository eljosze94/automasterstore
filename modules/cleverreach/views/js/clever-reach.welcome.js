/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(document).ready(function () {
    var authUrl = document.getElementById('cr_auth_url').value;
    var checkStatusUrl = document.getElementById('cr_admin_url').value;

    var loginButton = document.getElementById('cr-logAccount');
    var createAccountButton = document.getElementById('cr-newAccount');

    loginButton.addEventListener('click', function () {
        startAuthProcess(authUrl + '#login');
    });

    createAccountButton.addEventListener('click', function () {
        startAuthProcess(authUrl + '#register');
    });

    function startAuthProcess(authUrl)
    {
        showSpinner();

        var auth = new CleverReach.Authorization(authUrl, checkStatusUrl);
        auth.checkConnectionStatus(function () {
            location.reload();
        });
    }

    function showSpinner()
    {
        document.getElementsByClassName('loader-big')[0].style.display = 'flex';
        document.getElementsByClassName('connecting')[0].style.display = 'block';
        document.getElementsByClassName('tab-content panel welcome')[0].style.display = 'none';
    }
});
