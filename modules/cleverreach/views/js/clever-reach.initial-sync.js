/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2017 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

$(document).ready(function () {
    var redirectUrl = document.getElementById('cr_admin_dashboard_url').value;

    function initialSyncCompleteHandler(response) {
        if (response.status === 'completed') {
            attachRedirectButtonClickHandler();
            renderStatistics(response.statistics);
            CleverReach.AutoRedirect.start(redirectUrl, 5000);
            showSuccessPanel();
        } else {
            doRedirect();
        }
    }

    function attachRedirectButtonClickHandler() {
        $('[data-success-panel-go-to-dashboard-button]').click(doRedirect);
    }

    function renderStatistics(statistics) {
        var panelMessageEl, successMessage;

        panelMessageEl = $('[data-success-panel-message]');
        if (panelMessageEl) {
            successMessage = panelMessageEl.html()
                .replace('%s', statistics.recipients_count)
                .replace('%s', statistics.group_name);

            panelMessageEl.html(successMessage);
        }
    }

    function showSuccessPanel() {
        $('[data-task-list-panel]').addClass('hidden');
        $('[data-success-panel]').removeClass('hidden');
    }

    function doRedirect() {
        window.location.href = redirectUrl;
    }

    CleverReach.StatusChecker.init({
        statusCheckUrl: document.getElementById('cr_admin_status_check_url').value,
        finishedStatus: 'completed',
        onComplete: initialSyncCompleteHandler,
        pendingStatusClses: ['icon-circle'],
        inProgressStatusClses: ['loader'],
        doneStatusClses: ['icon-check', 'text-success']
    });
});