<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__) . '/vendor/autoload.php');

class CleverReach extends Module
{
    public function __construct()
    {
        $this->module_key = '70c54cb596a0f9c4428cce1ad60f7d19';
        $this->name = 'cleverreach';
        $this->tab = 'advertising_marketing';
        $this->version = '2.0.5';
        $this->author = 'CleverReach GmbH';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;
        $this->controllers = array('search');

        parent::__construct();

        $this->displayName = $this->l('CleverReach® – THE Email Marketing Solution');
        $this->description = $this->l('CleverReach® is THE Email Marketing Solution for beginners and experienced users
alike that lets you easily create newsletters online and send them to target customers.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        $previousShopContext = Shop::getContext();
        Shop::setContext(Shop::CONTEXT_ALL);

        $this->installAndConfigurePluginBase();
        $success = parent::install() && $this->registerControllersAndHooks();

        Shop::setContext($previousShopContext);

        return $success;
    }

    public function installAndConfigurePluginBase()
    {
        // Install Tabs
        $parentTabId = _PS_VERSION_ >= '1.7.0.0' ? (int)Tab::getIdFromClassName('CONFIGURE') : 0;
        $tab = $this->createController('AdminCleverReachBase', $parentTabId);

        // Set icon image when menu is collapsed
        if (_PS_VERSION_ >= '1.7') {
            $db = Db::getInstance();
            $db->update('tab', array('icon' => 'sms'), 'id_tab = ' . (int)$tab->id);
        }

        include(dirname(__FILE__) . '/sql/install.php');

        $config = new \CleverReach\PrestaShop\Classes\InfrastructureServices\ConfigService();
        $config->setTaskRunnerStatus('', null);

        if (!$config->getProductSearchEndpointPassword()) {
            $password = md5(time());
            $config->setProductSearchEndpointPassword($password);
        }
    }

    public function registerControllersAndHooks()
    {
        return $this->createController('AdminCleverReachWelcome') &&
            $this->createController('AdminCleverReachInitialSync') &&
            $this->createController('AdminCleverReachDashboard') &&
            $this->createController('AdminCleverReachConfigController') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayBlockNewsletterBottom') &&
            $this->registerHook('displayFooterBefore') &&
            $this->registerHook('actionObjectCustomerAddAfter') &&
            $this->registerHook('actionObjectCustomerUpdateAfter') &&
            $this->registerHook('actionObjectAddressAddAfter') &&
            $this->registerHook('actionObjectAddressUpdateAfter') &&
            $this->registerHook('actionObjectGroupAddAfter') &&
            $this->registerHook('actionObjectGroupUpdateAfter') &&
            $this->registerHook('actionObjectShopAddAfter') &&
            $this->registerHook('actionObjectShopUpdateAfter') &&
            $this->registerHook('displayOrderConfirmation') &&
            $this->registerHook('displayFooterProduct');
    }

    public function uninstall()
    {
        $this->removeControllerBy('AdminCleverReachBase');
        $this->removeControllerBy('AdminCleverReachWelcome');
        $this->removeControllerBy('AdminCleverReachInitialSync');
        $this->removeControllerBy('AdminCleverReachDashboard');
        $this->removeControllerBy('AdminCleverReachConfigController');

        $this->unregisterHook('backOfficeHeader');
        $this->unregisterHook('displayBlockNewsletterBottom');
        $this->unregisterHook('displayFooterBefore');
        $this->unregisterHook('actionObjectCustomerAddAfter');
        $this->unregisterHook('actionObjectCustomerUpdateAfter');
        $this->unregisterHook('actionObjectAddressAddAfter');
        $this->unregisterHook('actionObjectAddressUpdateAfter');
        $this->unregisterHook('actionObjectGroupAddAfter');
        $this->unregisterHook('actionObjectGroupUpdateAfter');
        $this->unregisterHook('actionObjectShopAddAfter');
        $this->unregisterHook('actionObjectShopUpdateAfter');
        $this->unregisterHook('displayOrderConfirmation');
        $this->unregisterHook('displayFooterProduct');

        include(dirname(__FILE__) . '/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Register additional admin controller
     *
     * @param string $className New controller class name
     * @param int $parentId Id of a parent tab
     *
     * @return Tab|null New controller tab
     */
    private function createController($className, $parentId = -1)
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->name[(int)Configuration::get('PS_LANG_DEFAULT')] = $this->l('CleverReach');
        $tab->class_name = $className;
        $tab->module = $this->name;
        $tab->id_parent = $parentId;
        $tab->add();

        return $tab;
    }

    /**
     * Removes additional admin controller by its class name
     *
     * @param string $className Controller class name
     */
    public function removeControllerBy($className)
    {
        $tab = new Tab((int)Tab::getIdFromClassName($className));
        $tab->delete();
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/icon.css');
        if (false !== strpos(Tools::getValue('controller'), 'AdminCleverReach')) {
            $this->initControllerAssets();
        }
    }

    private function initControllerAssets()
    {
        $initializer = new \CleverReach\PrestaShop\Classes\Initializer();
        $initializer->registerServices();

        $this->context->controller->addJquery();
        if (Tools::getValue('controller') === 'AdminCleverReachDashboard') {
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.dashboard.js');
        }

        if (Tools::getValue('controller') === 'AdminCleverReachWelcome') {
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.ajax.js');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.authorization.js');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.welcome.js');
        }

        if (Tools::getValue('controller') === 'AdminCleverReachInitialSync') {
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.ajax.js');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.autoredirect.js');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.status-checker.js');
            $this->context->controller->addJS($this->_path.'views/js/clever-reach.initial-sync.js');
        }
    }

    public function hookDisplayBlockNewsletterBottom()
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleDisplayNewsletterBottom();
    }

    public function hookDisplayFooterBefore()
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleDisplayNewsletterBottom();
    }

    public function hookActionObjectCustomerAddAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleCustomerCreatedEvent($params['object']);
    }

    public function hookActionObjectCustomerUpdateAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleCustomerUpdateEvent($params['object']);
    }

    public function hookActionObjectAddressAddAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleAddressCreatedEvent($params['object']);
    }

    public function hookActionObjectAddressUpdateAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleAddressUpdateEvent($params['object']);
    }

    public function hookActionObjectGroupAddAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleCustomerGroupCreatedEvent($params['object']);
    }

    public function hookActionObjectGroupUpdateAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleCustomerGroupUpdatedEvent($params['object']);
    }

    public function hookActionObjectShopAddAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleShopCreatedEvent($params['object']);
    }

    public function hookActionObjectShopUpdateAfter($params)
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleShopUpdatedEvent($params['object']);
    }

    public function hookDisplayOrderConfirmation($params)
    {
        $paramName = _PS_VERSION_ >= '1.7.0.0' ? 'order' : 'objOrder';
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleDisplayOrderConfirmation($params[$paramName]);
    }

    public function hookDisplayFooterProduct()
    {
        $hooksHandler = new \CleverReach\PrestaShop\Classes\HooksHandler($this);
        $hooksHandler->handleDisplayFooterProduct();
    }

    public function getContext()
    {
        return $this->context;
    }

    /**
     * Registers rewrite urls for frontend controller
     *
     * @return bool
     */
    public function registerUrls()
    {
        try {
            foreach (Language::getLanguages() as $language) {
                $data = Meta::getMetaByPage('module-cleverreach-AsyncProcess', $language['id_lang']);
                $meta = new Meta($data['id_meta']);
                if ($meta && $meta->id) {
                    $meta->url_rewrite = 'asyncprocess';
                    $meta->save();
                }
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}
