<?php

namespace CleverReach\BusinessLogic\Interfaces;

use CleverReach\Infrastructure\Utility\Exceptions\HttpCommunicationException;
use CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException;

interface Proxy
{
    const CLASS_NAME = __CLASS__;

    /**
     * Call http client
     *
     * @param $method
     * @param $endpoint
     * @param string $body
     * @return array
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function call($method, $endpoint, $body = '');
}