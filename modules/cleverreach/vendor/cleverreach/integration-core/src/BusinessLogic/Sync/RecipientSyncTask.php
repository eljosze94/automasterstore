<?php

namespace CleverReach\BusinessLogic\Sync;

use CleverReach\BusinessLogic\DTO\RecipientDTO;
use CleverReach\BusinessLogic\Entity\Recipient;
use CleverReach\BusinessLogic\Proxy;
use CleverReach\BusinessLogic\Interfaces\Recipients;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;
use CleverReach\Infrastructure\Utility\Exceptions\HttpBatchSizeTooBigException;
use CleverReach\Infrastructure\Utility\Exceptions\HttpUnhandledException;

class RecipientSyncTask extends Task
{
    const CLASS_NAME = __CLASS__;

    const INITIAL_PROGRESS_PERCENT = 5;

    /**
     * @var array
     */
    public $stateData;

    /**
     * @var
     */
    private $recipientsSyncService;

    /**
     * @var Configuration
     */
    private $configService;

    /**
     * @var Proxy
     */
    private $proxy;

    /**
     * RecipientSyncTask constructor.
     * 
     * @param array $recipientIds
     * @param array $additionalTagsToDelete
     * @param bool $includeOrders
     */
    public function __construct(array $recipientIds, array $additionalTagsToDelete, $includeOrders)
    {
        $this->configService = $this->getConfigService();
        $this->recipientsSyncService = $this->getRecipientSyncService();
        $this->proxy = $this->getProxy();

        $this->stateData = array(
            'batchSize' => $this->configService->getRecipientsSynchronizationBatchSize(),
            'allRecipientsIdsForSync' => $recipientIds,
            'additionalTagsToDelete' => $additionalTagsToDelete,
            'includeOrders' => $includeOrders,
            'numberOfRecipientsForSync' => count($recipientIds),
            'currentSyncProgress' => self::INITIAL_PROGRESS_PERCENT,
        );
    }

    /**
     * Returns state of the task
     * 
     * @return array
     */
    public function getStateData()
    {
        return $this->stateData;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this->stateData);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
       $this->stateData = unserialize($serialized);
    }

    /**
     * Runs task logic
     * 
     * @throws HttpUnhandledException
     * @throws \CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException
     */
    public function execute()
    {
        /** @var array $allTags */
        $allTags = $this->getRecipientSyncService()->getAllTags();
        $this->reportProgress($this->stateData['currentSyncProgress']);
        $this->reportProgressWhenNoRecipientIds();

        while (count($this->stateData['allRecipientsIdsForSync']) > 0) {
            /** @var array $recipientsWithTagsPerBatch*/
            $recipientsWithTagsPerBatch = $this->getBatchRecipientsWithTagsFromSourceSystem();
            $this->reportAlive();
            $recipientDTOs = $this->makeRecipientDTOs($recipientsWithTagsPerBatch, $allTags);
            try {
                $this->getProxy()->recipientsMassUpdate($recipientDTOs);

                $this->removeFinishedBatchRecipientsFromRecipientsForSync(); // if mass update is successful contacts
                // in batch should be removed from the contacts for sending. State of task is updated

                $this->reportProgressForBatch(); // If upload is successful progress should be reported for that batch.
            } catch (HttpBatchSizeTooBigException $e) {
                // If HttpBatchSizeTooBigException happens process should be continued with smaller calculated batch.
                $this->reconfigure();
            }
        }
    }

    private function reportProgressWhenNoRecipientIds()
    {
        if (count($this->stateData['allRecipientsIdsForSync']) === 0) {
            $this->reportProgressForBatch();
        }
    }
    
    private function makeRecipientDTOs(array $recipientsWithTags, array $allTags)
    {
        $recipientDTOs = array();
        
        /** @var Recipient $recipient */
        foreach ($recipientsWithTags as &$recipient) {
            $recipient->setTags(array_diff($recipient->getTags(), $this->stateData['additionalTagsToDelete']));
            $recipientDTOs[] = new RecipientDTO(
                $recipient,
                $this->getTagsForDelete($recipient->getTags(), $allTags),
                $this->stateData['includeOrders'],
                true, // Send activated time always
                !$recipient->isActive() // Send deactivated only if recipient is not active
            );
        }
        
        return $recipientDTOs;
    }

    /**
     * @param array$recipientTags
     * @param array $allTags
     *
     * @return array
     */
    private function getTagsForDelete($recipientTags, $allTags)
    {
        $tagsToDelete = array_diff($allTags, $recipientTags);

        return array_merge($tagsToDelete, $this->stateData['additionalTagsToDelete']);
    }

    /**
     * @param Recipient $recipient
     * @return bool
     */
    private function checkIfDeactivatedShouldBeSent($recipient)
    {
        return ($recipient->getDeactivated() instanceof \DateTime);
    }

    /**
     * @return array
     */
    private function getBatchRecipientsWithTagsFromSourceSystem()
    {
        $recipientIdsForBatch = array_slice(
            $this->stateData['allRecipientsIdsForSync'],
            0,
            $this->stateData['batchSize']
        );
        
        return $this->getRecipientSyncService()->getRecipientsWithTags(
            $recipientIdsForBatch,
            $this->stateData['includeOrders']
        );
    }

    private function removeFinishedBatchRecipientsFromRecipientsForSync()
    {
        $newBatchStartPosition = $this->stateData['batchSize'];
        
        $this->stateData['allRecipientsIdsForSync'] = array_slice(
            $this->stateData['allRecipientsIdsForSync'],
            $newBatchStartPosition,
            count($this->stateData['allRecipientsIdsForSync'])
        );
    }

    /**
     * Reduces batch size
     * 
     * @throws HttpUnhandledException
     */
    public function reconfigure()
    {
        if ($this->stateData['batchSize'] >= 100) {
            $this->stateData['batchSize'] -= 50;
        } else if($this->stateData['batchSize'] > 10 && $this->stateData['batchSize'] < 100) {
            $this->stateData['batchSize'] -= 10;
        } else if ($this->stateData['batchSize'] > 1 && $this->stateData['batchSize'] <= 10) {
            $this->stateData['batchSize'] -= 1;
        } else {
            throw new HttpUnhandledException('Batch size can not be smaller than 1');
        }

        $this->getConfigService()->setRecipientsSynchronizationBatchSize($this->stateData['batchSize']);
    }

    private function reportProgressForBatch()
    {
        if (count($this->stateData['allRecipientsIdsForSync']) < $this->stateData['batchSize']) {
            $this->stateData['currentSyncProgress'] = 100;
        } else {
            $numberSynchronizedRecipients =
                $this->stateData['numberOfRecipientsForSync'] - count($this->stateData['allRecipientsIdsForSync']);

            $progressStep = $numberSynchronizedRecipients  * (100 - self::INITIAL_PROGRESS_PERCENT) /
                $this->stateData['numberOfRecipientsForSync'];

            $this->stateData['currentSyncProgress'] = self::INITIAL_PROGRESS_PERCENT + (int)$progressStep;
        }
        
        $this->reportProgress($this->stateData['currentSyncProgress']);
    }

    /**
     * @return Configuration
     */
    private function getConfigService()
    {
        $configService = $this->configService;

        if (empty($configService)) {
            $configService = ServiceRegister::getService(Configuration::CLASS_NAME);
        }

        return $configService;
    }

    /**
     * @return Recipients
     */
    private function getRecipientSyncService()
    {
        $recipientSyncService = $this->recipientsSyncService;

        if (empty($recipientSyncService)) {
            $recipientSyncService = ServiceRegister::getService(Recipients::CLASS_NAME);
        }

        return $recipientSyncService;
    }

    /**
     * @return Proxy
     */
    private function getProxy()
    {
        $proxy = $this->proxy;

        if (empty($proxy)) {
            $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        }

        return $proxy;
    }
    
    public function canBeReconfigured()
    {
        return $this->stateData['batchSize'] > 1;
    }
}