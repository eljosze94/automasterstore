<?php

namespace CleverReach\BusinessLogic\Sync;

use CleverReach\BusinessLogic\Entity\OrderItem;
use CleverReach\BusinessLogic\Interfaces\OrderItems;
use CleverReach\BusinessLogic\Proxy;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Exceptions\SyncTaskFailedException;
use CleverReach\Infrastructure\TaskExecution\Task;

class CampaignOrderSync extends Task
{
    const INITIAL_PROGRESS_PERCENT = 10;

    /** @var OrderItems  */
    private $orderItemsService;

    /** @var Proxy */
    private $proxy;

    /** @var array */
    private $stateData;

    /**
     * CampaignOrderSync constructor.
     *
     * @param array $orderItemsIdMailingIdMap
     */
    public function __construct(array $orderItemsIdMailingIdMap)
    {
        $this->orderItemsService = $this->getOrderItemsService();
        $this->proxy = $this->getProxy();
        $this->stateData = array(
            'orderItemsIdMailingIdMap' => $orderItemsIdMailingIdMap,
            'failedOrderItemsIdMailingIdMap' => array(),
            'currentProgress' => self::INITIAL_PROGRESS_PERCENT,
            'numberOfOrderItemsForSync' => count($orderItemsIdMailingIdMap),
        );
    }

    private function getOrderItemsService()
    {
        $orderItemsService = $this->orderItemsService;

        if (empty($orderItemsService)) {
            $orderItemsService = ServiceRegister::getService(OrderItems::CLASS_NAME);
        }

        return $orderItemsService;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize($this->stateData);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $this->stateData = unserialize($serialized);
    }

    /**
     * @throws SyncTaskFailedException
     */
    public function execute()
    {
        $orderItemsForSync = $this->stateData['orderItemsIdMailingIdMap'] + $this->stateData['failedOrderItemsIdMailingIdMap'];
        $this->stateData['failedOrderItemsIdMailingIdMap'] = array();
        $orderItemsWithoutMailingIds = $this->getOrderItemsService()->getOrderItems(array_keys($orderItemsForSync));
        $this->reportAlive();

        /** @var OrderItem $orderItem */
        foreach ($orderItemsWithoutMailingIds as $orderItem) {
            $this->formatOrderItems($orderItem);
            $this->uploadOrderItemToDestinationSystem($orderItem);
            unset($this->stateData['orderItemsIdMailingIdMap'][$orderItem->getOrderId()]);
            $this->calculateProgress();
            $this->reportProgress($this->stateData['currentProgress']);
        }

        $this->checkIfAnyOrderItemFailed();
        $this->reportProgress(100);
    }

    /**
     * @param OrderItem $orderItem
     */
    private function formatOrderItems($orderItem)
    {
        $mailingId = $this->stateData['orderItemsIdMailingIdMap'][$orderItem->getOrderId()];

        if ($mailingId !== null) {
            $orderItem->setMailingId($mailingId);
        }
    }

    /**
     * @param OrderItem $orderItem
     */
    private function uploadOrderItemToDestinationSystem($orderItem)
    {
        try {
            $this->getProxy()->uploadOrderItem($orderItem);
        } catch (\Exception $e) {
            $this->stateData['failedOrderItemsIdMailingIdMap'][$orderItem->getOrderId()] =
                $this->stateData['orderItemsIdMailingIdMap'][$orderItem->getOrderId()];
        }
    }

    /**
     * @return Proxy
     */
    private function getProxy()
    {
        $proxy = $this->proxy;

        if (empty($proxy)) {
            $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        }

        return $proxy;
    }

    private function calculateProgress()
    {
        $numberOfSyncedOrdersItems =
            $this->stateData['numberOfOrderItemsForSync'] - count($this->stateData['orderItemsIdMailingIdMap']) +
            count($this->stateData['failedOrderItemsIdMailingIdMap']);

        $synchronizedOrdersItemsPercentage = $numberOfSyncedOrdersItems * (100 - self::INITIAL_PROGRESS_PERCENT) /
            $this->stateData['numberOfOrderItemsForSync'];

        $this->stateData['currentProgress'] =
            self::INITIAL_PROGRESS_PERCENT + (int)$synchronizedOrdersItemsPercentage;
    }

    /**
     * @throws SyncTaskFailedException
     */
    private function checkIfAnyOrderItemFailed()
    {
        if (count($this->stateData['failedOrderItemsIdMailingIdMap']) > 0) {
            $errorMessage = 'Campaign order sync task failed for order items: ' .
                implode(', ', $this->stateData['failedOrderItemsIdMailingIdMap']) . '. Current task progress is: ' .
                $this->stateData['currentProgress'];

            Logger::logDebug($errorMessage);

            throw new SyncTaskFailedException($errorMessage);
        }
    }

}