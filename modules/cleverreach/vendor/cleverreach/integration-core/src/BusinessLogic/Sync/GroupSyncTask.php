<?php

namespace CleverReach\BusinessLogic\Sync;

use CleverReach\BusinessLogic\Proxy;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;
use CleverReach\Infrastructure\Exceptions\InvalidConfigurationException;

class GroupSyncTask extends Task
{
    const CLASS_NAME = __CLASS__;
    public function serialize()
    {
        return serialize($this);
    }

    public function unserialize($serialized)
    {
        // This method was intentionally left blank because this task doesn't have any properties to encapsulate
    }

    /**
     * @var Proxy
     */
    private $proxy;

    /**
     * @var Configuration
     */
    private $config;

    /**
     * Runs task logic
     */
    public function execute()
    {
        /** @var string $serviceName */
        $serviceName = $this->getConfig()->getIntegrationName();

        $this->reportAlive();
        $this->validateServiceName($serviceName);
        $this->reportProgress(50);

        $groupId = $this->getProxy()->getGroupId($serviceName);

        if ($groupId == null) {
            $this->reportProgress(75);
            $newGroupId = $this->getProxy()->createGroup($serviceName);
            $this->getConfig()->setIntegrationId($newGroupId);
        } else {
            $this->getConfig()->setIntegrationId($groupId);
        }

        $this->reportProgress(100);
    }

    /**
     * Get proxy
     *
     * @return Proxy
     */
    private function getProxy()
    {
        if (empty($this->proxy)) {
            $this->proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        }

        return $this->proxy;
    }


    /**
     * Get configuration
     *
     * @return Configuration
     */
    private function getConfig()
    {
        if (empty($this->config)) {
            $this->config = ServiceRegister::getService(Configuration::CLASS_NAME);
        }

        return $this->config;
    }


    /**
     * Validates if $serviceName parameter is set
     * @param $serviceName
     * @throws InvalidConfigurationException
     */
    private function validateServiceName($serviceName)
    {
        if (empty($serviceName)) {
            throw new InvalidConfigurationException('Integration name not set in Configuration Service');
        }
    }
}