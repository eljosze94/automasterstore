<?php

namespace CleverReach\BusinessLogic\Sync;

use CleverReach\BusinessLogic\Proxy;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;
use CleverReach\Infrastructure\Utility\Exceptions\HttpAuthenticationException;
use CleverReach\Infrastructure\Utility\Exceptions\HttpCommunicationException;
use CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException;

class ProductSearchSyncTask extends Task
{

    const CLASS_NAME = __CLASS__;

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        // This method was intentionally left blank because this task doesn't have any properties to encapsulate
    }

    /**
     * Runs task logic
     *
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     * @throws HttpAuthenticationException
     */
    public function execute()
    {
        /** @var Proxy $proxy */
        $proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        /** @var Configuration $config */
        $config = ServiceRegister::getService(Configuration::CLASS_NAME);
        $productSearchParameters = $config->getProductSearchParameters();
        
        $this->validateProductSearchParameters($productSearchParameters);

        $proxy->addOrUpdateProductSearch($productSearchParameters);
        $this->reportProgress(100);
    }

    /**
     * Validate if all product search parameters are set
     * 
     * @param array $productSearchParameters
     */
    private function validateProductSearchParameters($productSearchParameters)
    {
        $errorMessage = '';
        
        if (empty($productSearchParameters['name'])) {
            $errorMessage .= 'Parameter "name" for product search is not set in Configuration service.';
        }

        if (empty($productSearchParameters['url'])) {
            $errorMessage .= 'Parameter "url" for product search is not set in Configuration service.';
        }

        if (empty($productSearchParameters['password'])) {
            $errorMessage .= 'Parameter "password" for product search is not set in Configuration service.';
        }

        if (!empty($errorMessage)) {
            Logger::logError($errorMessage);
            throw new \InvalidArgumentException($errorMessage);
        }
    }
    
}