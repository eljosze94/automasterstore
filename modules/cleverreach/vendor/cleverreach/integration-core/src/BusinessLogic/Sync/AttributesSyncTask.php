<?php

namespace CleverReach\BusinessLogic\Sync;

use CleverReach\BusinessLogic\Entity\ShopAttribute;
use CleverReach\BusinessLogic\Interfaces\Attributes;
use CleverReach\BusinessLogic\Proxy;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;

class AttributesSyncTask extends Task
{

    const CLASS_NAME = __CLASS__;
    
    const INITIAL_PROGRESS_PERCENT = 5;

    /**
     * @var array
     */
    private $globalAttributesIdsFromCR;

    /**
     * @var Proxy
     */
    private $proxy;

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this->globalAttributesIdsFromCR);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $this->globalAttributesIdsFromCR = unserialize($serialized);
    }

    /**
     * Runs task logic
     */
    public function execute()
    {
        $globalAttributes = $this->getGlobalAttributesIdsFromCR();
        // After retrieving all global attributes set initial progress
        $progressPercent = self::INITIAL_PROGRESS_PERCENT;
        $this->reportProgress($progressPercent);

        $attributesToSend = $this->getAllAttributes();

        // Calculate progress step after setting initially progress
        $progressStep = (100 - self::INITIAL_PROGRESS_PERCENT)/count($attributesToSend);
        $lastAttribute = end($attributesToSend);
        foreach ($attributesToSend as $attribute) {
            if (isset($globalAttributes[$attribute['name']])) {
                $attributeIdOnCR = $globalAttributes[$attribute['name']];
                $this->getProxy()->updateGlobalAttribute($attributeIdOnCR, $attribute);
            } else {
                $this->getProxy()->createGlobalAttribute($attribute);
            }

            $progressPercent += $progressStep;
            if ($lastAttribute == $attribute) {
                $this->reportProgress(100);
            } else {
                $this->reportProgress((int)$progressPercent);
            }
        }
    }

    /**
     * Get global attributes ids from CleverReach
     *
     * @return array
     */
    private function getGlobalAttributesIdsFromCR()
    {
        if (empty($this->globalAttributesIdsFromCR)) {
            $this->globalAttributesIdsFromCR = $this->getProxy()->getAllGlobalAttributes();
        }

        return $this->globalAttributesIdsFromCR;
    }

    /**
     * Get proxy
     *
     * @return Proxy
     */
    private function getProxy()
    {
        if (empty($this->proxy)) {
            $this->proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        }

        return $this->proxy;
    }

    /**
     * Return all global attributes formatted for sending to CR
     * 
     * @return array
     */
    private function getAllAttributes()
    {
        /** @var Attributes $attributesService */
        $attributesService = ServiceRegister::getService(Attributes::CLASS_NAME);
        $attributes = array(
            array(
                'name' => 'email',
                'type' => 'text',
            ),
            array(
                'name' => 'salutation',
                'type' => 'text',
            ),
            array(
                'name' => 'title',
                'type' => 'text',
            ),
            array(
                'name' => 'firstname',
                'type' => 'text',
            ),
            array(
                'name' => 'lastname',
                'type' => 'text',
            ),
            array(
                'name' => 'street',
                'type' => 'text',
            ),
            array(
                'name' => 'zip',
                'type' => 'text',
            ),
            array(
                'name' => 'city',
                'type' => 'text',
            ),
            array(
                'name' => 'company',
                'type' => 'text',
            ),
            array(
                'name' => 'state',
                'type' => 'text',
            ),
            array(
                'name' => 'country',
                'type' => 'text',
            ),
            array(
                'name' => 'birthday',
                'type' => 'date',
            ),
            array(
                'name' => 'phone',
                'type' => 'text',
            ),
            array(
                'name' => 'shop',
                'type' => 'text',
            ),
            array(
                'name' => 'customernumber',
                'type' => 'text',
            ),
            array(
                'name' => 'language',
                'type' => 'text',
            ),
            array(
                'name' => 'newsletter',
                'type' => 'text',
            ),
        );

        /** @var ShopAttribute $attribute */
        foreach ($attributes as &$attribute) {
            $shopAttribute = $attributesService->getAttributeByName($attribute['name']);
            $attribute['description'] = $shopAttribute->getDescription();
            $attribute['preview_value'] = $shopAttribute->getPreviewValue();
            $attribute['default_value'] = $shopAttribute->getDefaultValue();
        }
        
        return $attributes;
    }
}