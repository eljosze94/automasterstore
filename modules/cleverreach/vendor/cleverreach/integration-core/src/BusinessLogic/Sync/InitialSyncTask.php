<?php

namespace CleverReach\BusinessLogic\Sync;


use CleverReach\BusinessLogic\Interfaces\Recipients;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;
use CleverReach\Infrastructure\TaskExecution\TaskEvents\AliveAnnouncedTaskEvent;
use CleverReach\Infrastructure\TaskExecution\TaskEvents\ProgressedTaskEvent;

class InitialSyncTask extends Task
{
    const INITIAL_PROGRESS = 10;

    /**
     * @var array
     */
    protected $taskProgressMap;

    /**
     * @var Task AttributesSyncTask
     */
    protected $attributesSyncTask;

    /**
     * @var Task FilterSyncTask
     */
    protected $filterSyncTask;

    /**
     * @var Task GroupSyncTask
     */
    protected $groupSyncTask;

    /**
     * @var Task ProductSearchSyncTask
     */
    protected $productSearchSyncTask;

    /**
     * @var Task RecipientSyncTask
     */
    protected $recipientSyncTask;

    /**
     * @var Configuration
     */
    protected $configurationService;

    /**
     * @var array
     */
    private $subTasksProgressPartInInitialTask;

    public function __construct()
    {
        $this->taskProgressMap = array(
            'initialTaskProgress' => 0,
            'attributesSyncProgress' => 0,
            'filterSyncProgress' => 0,
            'groupSyncProgress' => 0,
            'productSearchSyncProgress' => $this->getConfigService()->isProductSearchEnabled() ? 0 : 100,
            'recipientSyncProgress' => 0,
        );

        $this->subTasksProgressPartInInitialTask = array(
            'attributesSyncProgress' => 10,
            'filterSyncProgress' => 10,
            'groupSyncProgress' => 10,
            'productSearchSyncProgress' => 10,
            'recipientSyncProgress' => 50,
        );

        $this->configurationService = ServiceRegister::getService(Configuration::CLASS_NAME);

        $this->attributesSyncTask = $this->makeAttributesSyncTask();
        $this->filterSyncTask = $this->makeFilterSyncTask();
        $this->groupSyncTask = $this->makeGroupSyncTask();
        $this->productSearchSyncTask = $this->makeProductSearchSyncTask();
        $this->recipientSyncTask = $this->makeRecipientSyncTask();
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(
            array(
                'taskProgress' => $this->taskProgressMap,
                'attributesSyncTask' => serialize($this->attributesSyncTask),
                'filterSyncTask' => serialize($this->filterSyncTask),
                'groupSyncTask' => serialize($this->groupSyncTask),
                'productSearchSyncTask' => serialize($this->productSearchSyncTask),
                'recipientSyncTask' => serialize($this->recipientSyncTask),
                'subTasksProgressPartInInitialTask' => $this->subTasksProgressPartInInitialTask,
            )
        );
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $unserializedStateData = unserialize($serialized);

        $this->taskProgressMap = $unserializedStateData['taskProgress'];
        $this->attributesSyncTask = unserialize($unserializedStateData['attributesSyncTask']);
        $this->filterSyncTask = unserialize($unserializedStateData['filterSyncTask']);
        $this->groupSyncTask = unserialize($unserializedStateData['groupSyncTask']);
        $this->productSearchSyncTask = unserialize($unserializedStateData['productSearchSyncTask']);
        $this->recipientSyncTask = unserialize($unserializedStateData['recipientSyncTask']);
        $this->subTasksProgressPartInInitialTask = $unserializedStateData['subTasksProgressPartInInitialTask'];
    }


    public function execute()
    {
        $this->hookOnSubTasksEvents();

        $numberOfMainTasks = 1;
        $numberOfSubTasks = count($this->taskProgressMap) - $numberOfMainTasks;

        for ($i = 0; $i < $numberOfSubTasks; $i++) {
            $activeTask = $this->getActiveTask();

            if ($activeTask !== null) {
                $activeTask->execute();
            }
        }
    }

    /**
     * Gets count of synchronized recipients
     *
     * @return int number of synchronized recipients
     */
    public function getSyncedRecipientsCount()
    {
        $recipientsStateData = $this->recipientSyncTask->getStateData();
        return $recipientsStateData['numberOfRecipientsForSync'] - count($recipientsStateData['allRecipientsIdsForSync']);
    }

    public function getProgressByTask()
    {
        return array(
            $this->productSearchSyncTask->getType() => $this->taskProgressMap['productSearchSyncProgress'],
            $this->groupSyncTask->getType() => $this->taskProgressMap['groupSyncProgress'],
            $this->attributesSyncTask->getType() => $this->taskProgressMap['attributesSyncProgress'],
            $this->filterSyncTask->getType() => $this->taskProgressMap['filterSyncProgress'],
            $this->recipientSyncTask->getType() => $this->taskProgressMap['recipientSyncProgress'],
        );
    }


    public function makeAttributesSyncTask()
    {
        return new AttributesSyncTask();
    }

    public function makeFilterSyncTask()
    {
        return new FilterSyncTask();
    }

    public function makeGroupSyncTask()
    {
        return new GroupSyncTask();
    }

    public function makeProductSearchSyncTask()
    {
        return new ProductSearchSyncTask();
    }

    public function makeRecipientSyncTask()
    {
        $recipientSyncService = ServiceRegister::getService(Recipients::CLASS_NAME);
        $allRecipientsIds = $recipientSyncService->getAllRecipientsIds();
        
        return new RecipientSyncTask($allRecipientsIds, array(), true);
    }

    protected function hookOnSubTasksEvents()
    {
        $this->hookOnReportAliveForEachSubTask();
        $this->hookOnReportProgressForEachSubTaskAndCalculateOverallProgress();
    }

    private function hookOnReportAliveForEachSubTask()
    {
        $self = $this;

        $this->attributesSyncTask->when(AliveAnnouncedTaskEvent::CLASS_NAME, function() use ($self) {
            $self->reportAlive();
        });

        $this->groupSyncTask->when(AliveAnnouncedTaskEvent::CLASS_NAME, function() use ($self) {
            $self->reportAlive();
        });

        $this->filterSyncTask->when(AliveAnnouncedTaskEvent::CLASS_NAME, function() use ($self) {
            $self->reportAlive();
        });

        $this->productSearchSyncTask->when(AliveAnnouncedTaskEvent::CLASS_NAME, function() use ($self) {
            $self->reportAlive();
        });

        $this->recipientSyncTask->when(AliveAnnouncedTaskEvent::CLASS_NAME, function() use ($self) {
            $self->reportAlive();
        });
    }

    private function hookOnReportProgressForEachSubTaskAndCalculateOverallProgress()
    {
        $self = $this;

        $this->attributesSyncTask->when(
            ProgressedTaskEvent::CLASS_NAME,
            function(ProgressedTaskEvent $event) use ($self) {
                $self->calculateProgress($event->getProgress(), 'attributesSyncProgress');
                $self->reportProgress($self->taskProgressMap['initialTaskProgress']);
            }
        );

        $this->filterSyncTask->when(
            ProgressedTaskEvent::CLASS_NAME,
            function(ProgressedTaskEvent $event) use ($self) {
                $self->calculateProgress($event->getProgress(), 'filterSyncProgress');
                $self->reportProgress($self->taskProgressMap['initialTaskProgress']);
            }
        );

        $this->groupSyncTask->when(
            ProgressedTaskEvent::CLASS_NAME,
            function(ProgressedTaskEvent $event) use ($self) {
                $self->calculateProgress($event->getProgress(), 'groupSyncProgress');
                $self->reportProgress($self->taskProgressMap['initialTaskProgress']);
            }
        );

        $this->productSearchSyncTask->when(
            ProgressedTaskEvent::CLASS_NAME,
            function(ProgressedTaskEvent $event) use ($self) {
                $self->calculateProgress($event->getProgress(), 'productSearchSyncProgress');
                $self->reportProgress($self->taskProgressMap['initialTaskProgress']);
            }
        );

        $this->recipientSyncTask->when(
            ProgressedTaskEvent::CLASS_NAME,
            function(ProgressedTaskEvent $event) use ($self) {
                $self->calculateProgress($event->getProgress(), 'recipientSyncProgress');
                $self->reportProgress($self->taskProgressMap['initialTaskProgress']);
            }
        );
    }

    private function getConfigService()
    {
        $configurationService = $this->configurationService;

        if (empty($configurationService)) {
            $configurationService = ServiceRegister::getService(Configuration::CLASS_NAME);
        }

        return $configurationService;
    }

    private function calculateProgress($subTaskWholeProgress, $subTaskProgressMapKey) {
        $this->taskProgressMap[$subTaskProgressMapKey] = $subTaskWholeProgress;
        $wholeSubTasksProgress = 0;

        foreach ($this->subTasksProgressPartInInitialTask as $subTaskKey => $subTaskPercentageInCompleteSyncProcess ) {
            $wholeSubTasksProgress +=
                $this->taskProgressMap[$subTaskKey] * $subTaskPercentageInCompleteSyncProcess / 100;
        }

        $this->taskProgressMap['initialTaskProgress'] = self::INITIAL_PROGRESS + (int)$wholeSubTasksProgress;

        if ($this->isSyncProcessCompleted()) {
            $this->taskProgressMap['initialTaskProgress'] = 100;
        }
    }

    private function isSyncProcessCompleted()
    {
        $allTasksSuccessful = true;

        foreach (array_keys($this->subTasksProgressPartInInitialTask) as $subTaskKey) {
            if ($this->taskProgressMap[$subTaskKey] !== 100) {
                $allTasksSuccessful = false;
                break;
            }
        }

        return $allTasksSuccessful;
    }

    public function canBeReconfigured()
    {
        $activeTask = $this->getActiveTask();
        $canBeReconfigured = false;

        if ($activeTask !== null) {
            $canBeReconfigured = $activeTask->canBeReconfigured();
        }

        return $canBeReconfigured;
    }

    public function reconfigure()
    {
        $activeTask = $this->getActiveTask();

        if ($activeTask !== null) {
            $activeTask->reconfigure();
        }
    }

    private function getActiveTask() {
        if ($this->isTaskInProgress($this->taskProgressMap['productSearchSyncProgress'])) {
            return $this->productSearchSyncTask;

        } else if ($this->isTaskInProgress($this->taskProgressMap['groupSyncProgress'])) {
            return $this->groupSyncTask;

        } else if ($this->isTaskInProgress($this->taskProgressMap['attributesSyncProgress'])) {
            return $this->attributesSyncTask;

        } else if ($this->isTaskInProgress($this->taskProgressMap['filterSyncProgress'])) {
            return $this->filterSyncTask;

        } else if ($this->isTaskInProgress($this->taskProgressMap['recipientSyncProgress'])) {
            return $this->recipientSyncTask;
        }

        return null;
    }

    /**
     * @param int $taskProgress
     * @return bool
     */
    private function isTaskInProgress($taskProgress)
    {
        return $taskProgress < 100;
    }
}