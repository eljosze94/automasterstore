<?php

namespace CleverReach\BusinessLogic\Sync;

use CleverReach\BusinessLogic\Proxy;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;

class RefreshUserInfoTask extends Task
{
    const CLASS_NAME = __CLASS__;
    
    /** @var string */
    private $accessToken;

    /** @var Proxy */
    private $proxy;
    
    /** @var Configuration */
    private $configService;

    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this->accessToken);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $this->accessToken = unserialize($serialized);
    }

    /**
     * Runs task logic
     * @throws \Exception
     */
    public function execute()
    {
        $userInfo = $this->getUserInfo();
        if (empty($userInfo)) {
            $this->getConfigService()->setAccessToken(null);
            $this->getConfigService()->setUserInfo(null);
        } else {
            $this->getConfigService()->setAccessToken($this->accessToken);
            $this->getConfigService()->setUserInfo($userInfo);
        }

        $this->reportProgress(100);
    }

    /**
     * Get user info from CleverReach
     * 
     * @return array
     * @throws \Exception
     */
    public function getUserInfo() 
    {
        try {
            return $this->getProxy()->getUserInfo($this->accessToken);
        } catch(\Exception $ex) {
            // Catch any exception to clear access token and user info and rethrow exception
            $this->getConfigService()->setAccessToken(null);
            $this->getConfigService()->setUserInfo(null);
            throw $ex;
        }
    }

    /**
     * Get proxy
     *
     * @return Proxy
     */
    private function getProxy()
    {
        if (empty($this->proxy)) {
            $this->proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        }

        return $this->proxy;
    }

    /**
     * Get config service
     *
     * @return Configuration
     */
    private function getConfigService()
    {
        if (empty($this->configService)) {
            $this->configService = ServiceRegister::getService(Configuration::CLASS_NAME);
        }

        return $this->configService;
    }

}