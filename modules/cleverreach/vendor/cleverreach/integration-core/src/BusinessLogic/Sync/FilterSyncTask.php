<?php

namespace CleverReach\BusinessLogic\Sync;

use CleverReach\BusinessLogic\Proxy;
use CleverReach\BusinessLogic\Interfaces\Recipients;
use CleverReach\BusinessLogic\Utility\Filter;
use CleverReach\Infrastructure\Exceptions\InvalidConfigurationException;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\Task;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\BusinessLogic\Utility\Rule;

class FilterSyncTask extends Task
{
    const CLASS_NAME = __CLASS__;

    const INITIAL_PROGRESS_PERCENT = 10;


    /** @var  Proxy */
    private $proxy;

    /** @var  Configuration */
    private $configService;

    /** @var  Recipients */
    private $recipients;

    /** @var  int */
    private $integrationId;

    /** @var int  */
    private $progressPercent = self::INITIAL_PROGRESS_PERCENT;

    /** @var  int */
    private $progressStep;

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize($this);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        //
    }

    public function execute()
    {
        $this->integrationId = $this->getConfigService()->getIntegrationId();

        if (!isset($this->integrationId)) {
            throw new InvalidConfigurationException('Integration ID not set');
        }

        /** @var string[] $shopTags */
        $shopTags = $this->getRecipients()->getAllTags();

        $this->reportAlive();

        /** @var Filter[] $allCRFilters */
        $allCRFilters = $this->getProxy()->getAllFilters($this->integrationId);

        $this->reportProgress($this->progressPercent);

        $this->progressStep = (100 - $this->progressPercent) / (count($shopTags) + count($allCRFilters));

        $this->checkForUpdate($allCRFilters, $shopTags);
        $this->reportAlive();
        $this->checkForDelete($allCRFilters, $shopTags);

        $this->reportProgress(100);
    }

    /**
     * @param Filter[] $allCRFilters
     * @param string[] $shopTags
     */
    private function checkForUpdate($allCRFilters, $shopTags)
    {
        //convert array of filter to array of strings
        $allFilterNames = array();
        foreach ($allCRFilters as $filter) {
            array_push($allFilterNames, $filter->getName());
        }

        foreach ($shopTags as $tagName) {

            if (!in_array($tagName, $allFilterNames)) {
                $this->createFilter($tagName);
            }

            $this->incrementProgress();
        }
    }

    /**
     * @param Filter[] $allCRFilters
     * @param string[] $shopTags
     */
    private function checkForDelete($allCRFilters, $shopTags)
    {
        if (empty($allCRFilters)) {
            return;
        }

        foreach ($allCRFilters as $filter) {

            if (!$this->startsWithPrefix($filter->getFirstCondition())) {
                $this->incrementProgress();
                continue;
            }

            if (!in_array($filter->getName(), $shopTags)) {
                $this->getProxy()->deleteFilter($filter->getId(), $this->integrationId);
            }

            $this->incrementProgress();
        }

    }

    /**
     * @param string $tagName
     */
    private function createFilter($tagName)
    {
        $formattedTagName = str_replace(' ', '_', $tagName);

        $rule = new Rule('tags', 'contains',
            $this->getConfigService()->getInstanceTagPrefix() . $formattedTagName);

        $filter = new Filter($tagName, $rule);
        $this->getProxy()->createFilter($filter, $this->integrationId);
    }

    private function incrementProgress()
    {
        $this->progressPercent += $this->progressStep;
        $this->reportProgress((int) $this->progressPercent);
    }

    /**
     * @param $haystack
     * @return bool
     */
    private function startsWithPrefix($haystack)
    {
        $length = strlen($this->getConfigService()->getInstanceTagPrefix());
        return (substr($haystack, 0, $length) === $this->getConfigService()->getInstanceTagPrefix());
    }

    /**
     * @return Proxy
     */
    private function getProxy()
    {
        if (empty($this->proxy)) {
            $this->proxy = ServiceRegister::getService(Proxy::CLASS_NAME);
        }

        return $this->proxy;
    }

    /**
     * @return Configuration
     */
    private function getConfigService()
    {
        if (empty($this->configService)) {
            $this->configService = ServiceRegister::getService(Configuration::CLASS_NAME);
        }

        return $this->configService;
    }

    /**
     * @return Recipients
     */
    private function getRecipients()
    {
        if (empty($this->recipients)) {
            $this->recipients = ServiceRegister::getService(Recipients::CLASS_NAME);
        }

        return $this->recipients;
    }
}