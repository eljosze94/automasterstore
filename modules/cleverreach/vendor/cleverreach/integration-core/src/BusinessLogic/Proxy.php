<?php

namespace CleverReach\BusinessLogic;

use CleverReach\BusinessLogic\DTO\RecipientDTO;
use CleverReach\BusinessLogic\Entity\OrderItem;
use CleverReach\BusinessLogic\Entity\Recipient;
use CleverReach\BusinessLogic\Utility\Filter;
use CleverReach\BusinessLogic\Utility\Rule;
use CleverReach\Infrastructure\Utility\Exceptions\HttpAuthenticationException;
use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\Interfaces\Required\HttpClient;
use CleverReach\Infrastructure\Utility\Exceptions\HttpBatchSizeTooBigException;
use CleverReach\Infrastructure\Utility\HttpResponse;
use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\Utility\Exceptions\HttpCommunicationException;
use CleverReach\Infrastructure\Utility\Exceptions\HttpRequestException;
use \CleverReach\BusinessLogic\Interfaces\Proxy as ProxyInterface;
use InvalidArgumentException;


class Proxy implements ProxyInterface
{

    const BASE_URL = 'https://rest.cleverreach.com';

    // Client id and secret are currently from our CleverReach application for v3 plugin version
    const CLIENT_ID = 'zhYTmczOCA';
    const CLIENT_SECRET = 'p0ZlXjkyvdjd23f5I2qSiZahSwurl62K';

    const AUTH_URL = 'https://rest.cleverreach.com/oauth/authorize.php';
    const TOKEN_URL = 'https://rest.cleverreach.com/oauth/token.php';

    const HTTP_STATUS_CODE_DEFAULT = 400;
    const HTTP_STATUS_CODE_UNAUTHORIZED = 401;
    const HTTP_STATUS_CODE_FORBIDDEN = 403;
    const HTTP_STATUS_CODE_CONFLICT = 409;
    const HTTP_STATUS_CODE_NOT_SUCCESSFUL_FOR_DEFINED_BATCH_SIZE = 413;

    /**
     * @var HttpClient
     */
    protected $client;

    /**
     * @var Configuration
     */
    private $configService;

    public function __construct()
    {
        $this->configService = ServiceRegister::getService(Configuration::CLASS_NAME);
    }

    /**
     * Check if group with given name exists
     *
     * @param string $serviceName
     * @return int
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function getGroupId($serviceName)
    {
        $response = $this->call('GET', 'groups.json');
        $allGroups = json_decode($response->getBody(), true);

        if (isset($allGroups) && is_array($allGroups)) {
            foreach ($allGroups as $group) {
                if ($group['name'] === $serviceName) {
                    return $group['id'];
                }
            }
        }

        return null;
    }

    /**
     * Creates new group
     *
     * @param string $serviceName
     * @throws HttpRequestException
     * @return int
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function createGroup($serviceName)
    {
        if (!$serviceName || empty($serviceName) || is_array($serviceName)) {
            throw new InvalidArgumentException('Argument null not allowed');
        }
        $argument = array('name' => $serviceName);
        $response = $this->call('POST', 'groups.json', $argument);
        $result = json_decode($response->getBody(), true);
        if (!isset($result['id'])) {
            $message = 'Creation of new group failed. Invalid response body from CR.';
            Logger::logError($message);
            throw new HttpRequestException($message);
        }

        return $result['id'];
    }

    /**
     * Creates new filter on CR API
     *
     * @param Filter $filter
     * @param int $integrationID
     * @throws HttpRequestException
     * @return array|bool
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function createFilter(Filter $filter, $integrationID)
    {
        if (!is_numeric($integrationID)) {
            throw new InvalidArgumentException();
        }
        $response = $this->call('POST', 'groups.json/' . $integrationID . '/filters', $filter->toArray());
        $result = json_decode($response->getBody(), true);
        if (!isset($result['id'])) {
            throw new HttpRequestException('Creation of new filter failed. Invalid response body from CR');
        }

        return $result;
    }

    /**
     * Delete filter in CR
     *
     * @param int $filterID
     * @param int $integrationID
     * @return bool
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function deleteFilter($filterID, $integrationID)
    {
        if (!is_numeric($filterID) || !is_numeric($integrationID) ) {
            throw new InvalidArgumentException();
        }

        $response = $this->call('DELETE', 'groups.json/' . $integrationID . '/filters/' . $filterID);

        if ($response->getBody() === 'true') {
            return true;
        }

        //invalid API response
        $response = json_decode($response->getBody(), true);

        //default error message
        $errorMessage = 'Deleting filter failed. Invalid response body from CR.';

        if (!empty($response['error']['message'])) {
            $errorMessage .= ' Response message: ' . $response['error']['message'];
        }

        $errorCode = $response['error']['code'] ?: self::HTTP_STATUS_CODE_DEFAULT;

        throw new HttpRequestException($errorMessage, $errorCode);
    }

    /**
     * Return all segments from CR
     *
     * @param int $integrationId
     * @return array
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function getAllFilters($integrationId)
    {
        $response = $this->call('GET', 'groups.json/' . $integrationId . '/filters');
        $allSegments = json_decode($response->getBody(), true);

        return $this->formatAllFilters($allSegments);
    }

    /**
     * @param array $allSegments
     * @return Filter[]
     */
    private function formatAllFilters($allSegments)
    {
        $results = array();
        if (empty($allSegments)) {
            return $results;
        }

        foreach ($allSegments as $segment) {
            if (empty($segment['rules'])) {
                continue;
            }

            $rule = new Rule($segment['rules'][0]['field'], $segment['rules'][0]['logic'],
                $segment['rules'][0]['condition']);

            $filter = new Filter($segment['name'], $rule);

            for ($i = 1; $i < count($segment['rules']); $i++) {

                $rule = new Rule($segment['rules'][$i]['field'], $segment['rules'][$i]['logic'],
                    $segment['rules'][$i]['condition']);
                $filter->addRule($rule);
            }

            $filter->setId($segment['id']);
            array_push($results, $filter);
        }

        return $results;
    }

    /**
     * Get all global attributes ids from CR
     *
     * @return array
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function getAllGlobalAttributes()
    {
        $response = $this->call('GET', 'attributes.json');
        $globalAttributes = json_decode($response->getBody(), true);
        $globalAttributesIds = array();
        
        if (isset($globalAttributes) && is_array($globalAttributes)) {
            foreach($globalAttributes as $globalAttribute) {
                $attributeKey = strtolower($globalAttribute['name']);
                $globalAttributesIds[$attributeKey] = $globalAttribute['id'];
            }
        }

        return $globalAttributesIds;
    }

    /**
     * Create attribute
     * Example attribute:
     * array(
     *   "name" => "FirstName",
     *   "type" => "text",
     *   "description" => "Description",
     *   "preview_value" => "real name",
     *   "default_value" => "Bruce"
     * )
     *
     * @param array $attribute
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function createGlobalAttribute($attribute)
    {
        try {
            $response = $this->call('POST', 'attributes.json', $attribute);
            $result = json_decode($response->getBody(), true);
        } catch (HttpRequestException $ex) {
            // Conflict status code means product search endpoint is already created
            if ($ex->getCode() === self::HTTP_STATUS_CODE_CONFLICT) {
                Logger::logInfo('Global attribute: ' . $attribute['name'] . ' endpoint already exists on CR.');
                return;
            }

            throw $ex;
        }

        if (!isset($result['id'])) {
            $message = 'Creation of global attribute "' . $attribute['name'] . '" failed. Invalid response body from CR.';
            Logger::logError($message);
            throw new HttpRequestException($message);
        }
    }

    /**
     * Update attribute
     * Example attribute:
     * array(
     *   "type" => "text",
     *   "description" => "Description",
     *   "preview_value" => "real name"
     * )
     *
     * @param int $id
     * @param array $attribute
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function updateGlobalAttribute($id, $attribute)
    {
        $response = $this->call('PUT', 'attributes.json/' . $id, $attribute);
        $result = json_decode($response->getBody(), true);

        if (!isset($result['id'])) {
            $message = 'Update of global attribute "' . $attribute['name'] . '" failed. Invalid response body from CR.';
            Logger::logError($message);
            throw new HttpRequestException($message);
        }
    }

    /**
     * Register or update product search endpoint
     * Example data:
     * array(
     *   "name" => "My Shop name (http://myshop.com)",
     *   "url" => "http://myshop.com/myendpoint",
     *   "password" => "as243FF3"
     * )
     *
     * @param array $data
     *
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     * @throws HttpAuthenticationException
     */
    public function addOrUpdateProductSearch($data)
    {
        try {
            $response = $this->call('POST', 'mycontent.json', $data);
            $result = json_decode($response->getBody(), true);
            $result = !is_array($result) ? array() : $result;

            if (!array_key_exists('id', $result)) {
                $message = 'Registration/update of product search endpoint failed. Invalid response body from CR.';
                Logger::logError($message);
                throw new HttpRequestException($message);
            }

        } catch(HttpRequestException $ex) {
            // Conflict status code means product search endpoint is already created
            if ($ex->getCode() === self::HTTP_STATUS_CODE_CONFLICT) {
                Logger::logInfo('Product search endpoint already exists on CR. If you want to update you need to change name and endpoint url.');
                return;
            }

            throw $ex;
        }
    }

    /**
     * Does mass update by sending the whole batch to CleverReach.
     * 
     * @param $recipients array of objects CleverReach\BusinessLogic\DTO\RecipientDTO
     *
     * @throws HttpBatchSizeTooBigException
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function recipientsMassUpdate(array $recipients) {
        $formattedRecipients = $this->addTagsForDeleteAndFormatRecipientsForApiCall($recipients);

        try {
            $response = $this->call(
                'POST', 
                'groups.json/'. $this->configService->getIntegrationId() . '/receivers/upsertplus',
                $formattedRecipients
            );
            $this->checkMassUpdateRequestSuccess($response, $recipients);
        } catch(HttpRequestException $ex) {
            $batchSize = count($recipients);
            $this->checkMassUpdateBatchSizeValidity($ex, $batchSize);

            throw $ex;
        }
    }

    /**
     * @param $recipientDTOs array of objects CleverReach\BusinessLogic\DTO\RecipientDTO
     *
     * @return array
     */
    private function addTagsForDeleteAndFormatRecipientsForApiCall(array $recipientDTOs){
        $formattedRecipients = array();

        /** @var RecipientDTO $recipientDTO */
        foreach ($recipientDTOs as $recipientDTO) {
            /** @var Recipient $recipientEntity */
            $recipientEntity = $recipientDTO->getRecipientEntity();
            $registered = $recipientEntity->getRegistered();

            $formattedRecipient = array(
                'email' => $recipientEntity->getEmail(),
                'registered' => ($registered !== null) ? $registered->getTimestamp() : '',
                'source' => $recipientEntity->getSource(),
                'attributes' => $this->formatAttributesForSendingToCleverReach($recipientEntity->getAttributes()),
                'global_attributes' => $this->formatGlobalAttributesForSendingToCleverReach($recipientEntity),
                'tags' => $this->formatAndMergeExistingAndTagsForDelete(
                    $recipientEntity->getTags(),
                    $recipientDTO->getTagsForDelete()
                )
            );

            if ($recipientDTO->shouldActivatedFieldBeSent()) {
                $formattedRecipient['activated'] = $recipientEntity->getActivated()->getTimestamp();
            }

            if ($recipientDTO->shouldDeactivatedFieldBeSent()) {
                $formattedRecipient['deactivated'] = $recipientEntity->getDeactivated()->getTimestamp();
            }

            if ($recipientDTO->isIncludeOrdersActivated()) {
                $formattedRecipient['orders'] = $this->formatOrdersForSendingToCleverReach($recipientEntity->getOrders());
            }
            
            $formattedRecipients[] = $formattedRecipient;
        }
        
        return $formattedRecipients;
    }

    private function formatAttributesForSendingToCleverReach($rawAttributes) {
        $formattedAttributes = array();

        foreach ($rawAttributes as $key => $value) {
            $formattedAttributes[] = $key . ':' . $value;
        }

        return implode(',', $formattedAttributes);
    }


    /**
     * @param Recipient $recipient
     *
     * @return array
     */
    private function formatGlobalAttributesForSendingToCleverReach($recipient)
    {
        $newsletterSubscription = $recipient->getNewsletterSubscription() ? 'yes' : 'no';
        $birthday = $recipient->getBirthday();

        return array(
            'firstname' => $recipient->getFirstName(),
            'lastname' => $recipient->getLastName(),
            'salutation' => $recipient->getSalutation(),
            'title' => $recipient->getTitle(),
            'street' => $recipient->getStreet(),
            'zip' => $recipient->getZip(),
            'city' => $recipient->getCity(),
            'company' => $recipient->getCompany(),
            'state' => $recipient->getState(),
            'country' => $recipient->getCountry(),
            'birthday' => ($birthday !== null) ? date_format($birthday, 'Y-m-d') : '',
            'phone' => $recipient->getPhone(),
            'shop' => $recipient->getShop(),
            'customernumber' => (string)$recipient->getCustomerNumber(),
            'language' => $recipient->getLanguage(),
            'newsletter' => $newsletterSubscription,
        );
    }

    /**
     * @param array $orders
     * @return array
     */
    private function formatOrdersForSendingToCleverReach(array $orders)
    {
        $formattedOrders = array();

        /** @var OrderItem $order */
        foreach ($orders as $order) {
            $formattedOrders[] = $this->getOrderFormattedForRequest($order);
        }
        
        return $formattedOrders;
    }

    /**
     * @param OrderItem $orderItem
     * @return array
     */
    private function getOrderFormattedForRequest($orderItem)
    {
        $formattedOrder = array();
        $formattedOrder['order_id'] = $orderItem->getOrderId();
        $formattedOrder['product_id'] = (string)$orderItem->getProductId();
        $formattedOrder['product'] = $orderItem->getProduct();

        $dateOfOrder = $orderItem->getStamp();
        $formattedOrder['date_of_order'] = $dateOfOrder !== null ? $dateOfOrder->getTimestamp() : '';

        $formattedOrder['price'] = $orderItem->getPrice();
        $formattedOrder['currency'] = $orderItem->getCurrency();
        $formattedOrder['quantity'] = $orderItem->getAmount();
        $formattedOrder['product_source'] = $orderItem->getProductSource();
        $formattedOrder['brand'] = $orderItem->getBrand();
        $formattedOrder['product_category'] = implode(',', $orderItem->getProductCategory());
        $formattedOrder['attributes'] = $this->formatAttributesForSendingToCleverReach($orderItem->getAttributes());

        $mailingId = $orderItem->getMailingId();

        if ($mailingId !== null) {
            $formattedOrder['mailing_id'] = $mailingId;
        }

        return $formattedOrder;
    }

    /**
     * @param array $recipientTags
     * @param array $tagsForDelete
     *
     * @return array
     */
    private function formatAndMergeExistingAndTagsForDelete(array $recipientTags, array $tagsForDelete)
    {
        return array_merge(
            $this->getTagsWithInstanceTagPrefix($recipientTags),
            $this->getTagsForDeleteInApiFormat($tagsForDelete)
        );
    }

    /**
     * @param array $tagsForDelete
     *
     * @return array
     */
    private function getTagsForDeleteInApiFormat($tagsForDelete)
    {
        $tagsForDeleteInCleverReachFormat = array();

        foreach ($this->getTagsWithInstanceTagPrefix($tagsForDelete) as $tag) {
            $tagsForDeleteInCleverReachFormat[] = '-' . $tag;
        }

        return $tagsForDeleteInCleverReachFormat;
    }

    /**
     * @param array $tags
     * @return array
     */
    private function getTagsWithInstanceTagPrefix($tags)
    {
        $instanceTagPrefix = $this->configService->getInstanceTagPrefix();
        $formattedTags = array();

        foreach ($tags as $tag) {
            $formattedTags[] = $instanceTagPrefix . str_replace(' ', '_', $tag);
        }

        return $formattedTags;
    }

    /**
     * @param HttpResponse $response
     * @param array $recipientDTOs
     * @throws HttpRequestException
     */
    private function checkMassUpdateRequestSuccess($response, $recipientDTOs)
    {
        $responseBody = json_decode($response->getBody(), true);
        if ($responseBody === false) {
            $firstRecipient = !empty($recipientDTOs[0]) ? $recipientDTOs[0]->getRecipientEntity()->getEmail() : '';
            $message = 'Upsert of recipients not done for batch starting from recipient id ' . $firstRecipient  . '.' .
                'Batch size is ' . count($recipientDTOs) . '.';
            Logger::logError($message);

            throw new HttpRequestException($message);
        }
    }

    /**
     * @param HttpRequestException $ex
     * @param int $batchSize
     * @throws HttpBatchSizeTooBigException
     */
    private function checkMassUpdateBatchSizeValidity($ex, $batchSize)
    {
        if ($ex->getCode() === self::HTTP_STATUS_CODE_NOT_SUCCESSFUL_FOR_DEFINED_BATCH_SIZE) {
            Logger::logInfo('Upsert of recipients not done for batch size ' . $batchSize . '.');

            throw new HttpBatchSizeTooBigException('Batch size ' . $batchSize . ' too big for uspert');
        }
    }

    /**
     * Get user information from CleverReach
     *
     * @param $accessToken
     * @return array
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function getUserInfo($accessToken)
    {
        try {
            $response = $this->call('GET', 'debug/whoami.json', array(), $accessToken);
            $userInfo = json_decode($response->getBody(), true);
            if (!isset($userInfo['id'])) {
                $message = 'Get user information failed. Invalid response body from CR.';
                Logger::logError($message);
                throw new HttpRequestException($message);
            }
        } catch (HttpAuthenticationException $ex) {
            // Invalid access token
            return array();
        }
        
        return $userInfo;
    }

    /**
     * @param OrderItem $orderItem
     *
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     * @throws HttpAuthenticationException
     */
    public function uploadOrderItem($orderItem)
    {
        $updatedReceiver = $this->call(
            'POST',
            'groups.json/' . $this->configService->getIntegrationId() . '/receivers/upsert',
            array(
                'email' => $orderItem->getRecipientEmail(),
                'orders' => array($this->getOrderFormattedForRequest($orderItem)),
            )
        );

        $this->checkUploadOrderItemResponse($updatedReceiver, $orderItem->getOrderId());
    }

    /**
     * @param HttpResponse$updatedReceiver
     * @param $orderId
     * @param null $exception
     * @throws HttpRequestException
     */
    private function checkUploadOrderItemResponse($updatedReceiver, $orderId, $exception = null)
    {
        $updatedReceiverDecoded = json_decode($updatedReceiver->getBody(), true);
        if (!isset($updatedReceiverDecoded['id'])) {
            $message = 'Upload of order item with id: ' . $orderId . ' failed. Invalid response body from CR.';
            Logger::logError($message);
            throw new HttpRequestException($message, 0, $exception);
        }
    }

    /**
     * Call http client
     *
     * @param string $method
     * @param string $endpoint
     * @param array $body
     * @param string $accessToken
     * @return HttpResponse
     *
     * @throws HttpAuthenticationException
     * @throws HttpCommunicationException
     * @throws HttpRequestException
     */
    public function call($method, $endpoint, $body = array(), $accessToken = '')
    {
        if (empty($accessToken)) {
            $accessToken = $this->configService->getAccessToken();
        }

        if (empty($accessToken)) {
            $errorMessage = 'Missing token. Token is not set in Configuration service.';
            Logger::logError($errorMessage);
            throw new HttpCommunicationException($errorMessage);
        }

        $header = array(
            'accept' => 'Accept: application/json',
            'content' => 'Content-Type: application/json',
            'token' => 'Authorization: Bearer ' . $accessToken,
        );

        $bodyStringToSend = '';
        if (in_array(strtoupper($method), array('POST', 'PUT'))) {
            $bodyStringToSend = json_encode($body);
        }

        $response = $this->getClient()->request($method, self::BASE_URL . '/v3/' . $endpoint, $header, $bodyStringToSend);
        $this->validateResponse($response);

        return $response;
    }

    /**
     * Returns an associative array with fields: access_token, expires_in, token_type, scope.
     *
     * @param string $code
     * @param string $redirectUrl
     * @return array
     */
    public function getAccessToken($code, $redirectUrl)
    {
        $header = array(
            'accept' => 'Accept: application/json',
        );

        // Assemble POST parameters for the request.
        $postFields = '&grant_type=authorization_code&client_id=' . self::CLIENT_ID . '&client_secret='
            . self::CLIENT_SECRET . '&code=' . $code . '&redirect_uri=' . urlencode($redirectUrl);

        $response = $this->getClient()->request('POST', self::TOKEN_URL, $header, $postFields);
        $result = json_decode($response->getBody(), true);

        if (!is_array($result)) {
            $result = array();
        }

        return $result;
    }

    /**
     * Returns auth URL
     *
     * @param string $redirectUrl
     * @param string $registerData
     * @return string
     */
    public function getAuthUrl($redirectUrl, $registerData)
    {
        return self::AUTH_URL . '?response_type=code&grant=basic&client_id=' . self::CLIENT_ID . '&redirect_uri=' .
        urlencode($redirectUrl) . '&registerdata=' . $registerData;
    }

    /**
     * Validate response
     *
     * @param $response HttpResponse
     * @throws HttpAuthenticationException
     * @throws HttpRequestException
     */
    protected function validateResponse($response)
    {
        $httpCode = $response->getStatus();
        $body = $response->getBody();
        if (isset($httpCode) && ($httpCode < 200 || $httpCode >= 300)) {
            $message = var_export($body, true);
            
            $error = json_decode($body, true);
            if (is_array($error)) {
                if (isset($error['error']['message'])) {
                    $message = $error['error']['message'];
                }

                if (isset($error['error']['code'])) {
                    $httpCode = $error['error']['code'];
                }
            }

            Logger::logError($message);
            if ($httpCode === self::HTTP_STATUS_CODE_UNAUTHORIZED ||
                $httpCode === self::HTTP_STATUS_CODE_FORBIDDEN
            ) {
                throw new HttpAuthenticationException($message, $httpCode);
            }

            throw new HttpRequestException($message, $httpCode);
        }
    }

    /**
     * Get http client
     *
     * @return HttpClient
     */
    private function getClient()
    {
        if (empty($this->client)) {
            $this->client = ServiceRegister::getService(HttpClient::CLASS_NAME);
        }

        return $this->client;
    }
    
}