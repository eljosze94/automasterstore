<?php

namespace CleverReach\BusinessLogic\DTO;

use CleverReach\BusinessLogic\Entity\Recipient;

class RecipientDTO
{
    /**
     * @var Recipient
     */
    private $recipientEntity;

    /**
     * @var array of strings
     */
    private $tagsForDelete;

    /**
     * @var bool
     */
    private $includeOrdersActivated;

    /**
     * @var bool
     */
    private $activatedFieldForSending;

    /**
     * @var bool
     */
    private $deactivatedFieldForSending;

    /**
     * RecipientDTO constructor.
     *
     * @param Recipient $recipientEntity
     * @param array $tagsForDelete
     * @param bool $shouldIncludeOrders
     * @param bool $shouldSendActivated
     * @param bool $shouldSendDeactivated
     */
    public function __construct(
        $recipientEntity, 
        array $tagsForDelete, 
        $shouldIncludeOrders, 
        $shouldSendActivated,
        $shouldSendDeactivated
    ) {
        $this->recipientEntity = $recipientEntity;
        $this->tagsForDelete = $tagsForDelete;
        $this->includeOrdersActivated = $shouldIncludeOrders;
        $this->activatedFieldForSending = $shouldSendActivated;
        $this->deactivatedFieldForSending = $shouldSendDeactivated;
    }

    /**
     * @return Recipient
     */
    public function getRecipientEntity()
    {
        return $this->recipientEntity;
    }

    /**
     * @return array
     */
    public function getTagsForDelete()
    {
        return $this->tagsForDelete;
    }

    /**
     * @return boolean
     */
    public function isIncludeOrdersActivated()
    {
        return $this->includeOrdersActivated;
    }

    /**
     * @param boolean $shouldIncludeOrders
     */
    public function setIncludeOrdersActivated($shouldIncludeOrders)
    {
        $this->includeOrdersActivated = $shouldIncludeOrders;
    }

    /**
     * @return boolean
     */
    public function shouldActivatedFieldBeSent()
    {
        return $this->activatedFieldForSending;
    }

    /**
     * @param boolean $activatedFieldForSending
     */
    public function setActivatedFieldForSending($activatedFieldForSending)
    {
        $this->activatedFieldForSending = $activatedFieldForSending;
    }

    /**
     * @return boolean
     */
    public function shouldDeactivatedFieldBeSent()
    {
        return $this->deactivatedFieldForSending;
    }

    /**
     * @param boolean $deactivatedFieldForSending
     */
    public function setDeactivatedFieldForSending($deactivatedFieldForSending)
    {
        $this->deactivatedFieldForSending = $deactivatedFieldForSending;
    }

}