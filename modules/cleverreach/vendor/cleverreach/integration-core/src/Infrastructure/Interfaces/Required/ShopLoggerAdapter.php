<?php

namespace CleverReach\Infrastructure\Interfaces\Required;

use CleverReach\Infrastructure\Interfaces\LoggerAdapter;

interface ShopLoggerAdapter extends LoggerAdapter
{
    const CLASS_NAME = __CLASS__;
}