<?php

namespace CleverReach\Infrastructure\Interfaces;

interface DefaultLoggerAdapter extends LoggerAdapter
{
    const CLASS_NAME = __CLASS__;
}