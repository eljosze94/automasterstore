<?php

namespace CleverReach\Infrastructure\Interfaces;

use CleverReach\Infrastructure\Logger\LogData;

interface LoggerAdapter
{

    /**
     * Log message in system
     *
     * @param LogData $data
     */
    public function logMessage($data);

}