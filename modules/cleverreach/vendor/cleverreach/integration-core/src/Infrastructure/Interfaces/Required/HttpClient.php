<?php

namespace CleverReach\Infrastructure\Interfaces\Required;

use CleverReach\Infrastructure\Logger\Logger;
use CleverReach\Infrastructure\Utility\Exceptions\HttpCommunicationException;
use CleverReach\Infrastructure\Utility\HttpResponse;

abstract class HttpClient
{
    const CLASS_NAME = __CLASS__;

    /**
     * Create, log and send request
     *
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param string $body
     * @return HttpResponse
     */
    public function request($method, $url, $headers = array(), $body = '')
    {
        Logger::logDebug(json_encode(array(
            'Type' => $method,
            'Endpoint' => $url,
            'Headers' => json_encode($headers),
            'Content' => $body
        )));

        /** @var HttpResponse $response */
        $response = $this->sendHttpRequest($method, $url, $headers, $body);

        Logger::logDebug(json_encode(array(
            'Status' => $response->getStatus(),
            'Headers' => json_encode($response->getHeaders()),
            'Content' => $response->getBody()
        )));

        return $response;
    }

    /**
     * Create, log and send request asynchronously
     *
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param string $body
     */
    public function requestAsync($method, $url, $headers = array(), $body = '')
    {
        Logger::logDebug(json_encode(array(
            'Type' => $method,
            'Endpoint' => $url,
            'Headers' => $headers,
            'Content' => $body
        )));
        
        $this->sendHttpRequestAsync($method, $url, $headers, $body);
    }

    /**
     * Create and send request
     *
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param string $body In JSON format
     * @return HttpResponse
     * @throws HttpCommunicationException Only in situation when there is no connection, no response, throw this exception
     */
    abstract function sendHttpRequest($method, $url, $headers = array(), $body = '');

    /**
     * Create and send request asynchronously
     *
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param string $body In JSON format
     */
    abstract function sendHttpRequestAsync($method, $url, $headers = array(), $body = '');

}