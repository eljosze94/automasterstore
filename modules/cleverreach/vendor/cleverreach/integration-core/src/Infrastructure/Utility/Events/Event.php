<?php

namespace CleverReach\Infrastructure\Utility\Events;

abstract class Event
{
    const CLASS_NAME = __CLASS__;
}