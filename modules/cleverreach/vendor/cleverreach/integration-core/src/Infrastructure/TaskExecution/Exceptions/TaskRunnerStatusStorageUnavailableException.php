<?php

namespace CleverReach\Infrastructure\TaskExecution\Exceptions;

class TaskRunnerStatusStorageUnavailableException extends \Exception
{
}