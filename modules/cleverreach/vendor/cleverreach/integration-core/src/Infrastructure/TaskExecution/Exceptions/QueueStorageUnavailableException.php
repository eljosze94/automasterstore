<?php

namespace CleverReach\Infrastructure\TaskExecution\Exceptions;

class QueueStorageUnavailableException extends \Exception
{
}