<?php

namespace CleverReach\Infrastructure\TaskExecution\TaskEvents;

use CleverReach\Infrastructure\Utility\Events\Event;

class AliveAnnouncedTaskEvent extends Event
{
    const CLASS_NAME = __CLASS__;
}