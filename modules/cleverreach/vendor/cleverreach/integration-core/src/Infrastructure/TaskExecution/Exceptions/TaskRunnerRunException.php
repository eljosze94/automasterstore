<?php

namespace CleverReach\Infrastructure\TaskExecution\Exceptions;

class TaskRunnerRunException extends \Exception
{
}