<?php

namespace CleverReach\Infrastructure\TaskExecution\Exceptions;

class QueueItemDeserializationException extends \Exception
{
}