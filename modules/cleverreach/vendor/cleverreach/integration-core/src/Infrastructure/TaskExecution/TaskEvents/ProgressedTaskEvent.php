<?php

namespace CleverReach\Infrastructure\TaskExecution\TaskEvents;

use CleverReach\Infrastructure\Utility\Events\Event;

class ProgressedTaskEvent extends Event
{
    const CLASS_NAME = __CLASS__;

    /** @var int */
    private $progressPercent;

    /**
     * ProgressedTaskEvent constructor.
     *
     * @param int $progressPercent Integer representation of progress percentage, value between 0 and 100
     * @throws \InvalidArgumentException In case when progress percent is outside of 0 - 100 boundaries or not an integer
     */
    public function __construct($progressPercent)
    {
        if (!is_int($progressPercent) || $progressPercent < 0 || 100 < $progressPercent) {
            throw new \InvalidArgumentException('Progress percentage must be value between 0 and 100.');
        }

        $this->progressPercent = $progressPercent;
    }

    /**
     * Gets progress percentage in form of integer value between 0 and 100
     *
     * @return int Progress percentage
     */
    public function getProgress()
    {
        return $this->progressPercent;
    }

}