<?php

namespace CleverReach\Infrastructure\TaskExecution\Exceptions;

class QueueItemSaveException extends \Exception
{
}