<?php

namespace CleverReach\Infrastructure\TaskExecution\Exceptions;

class TaskRunnerStatusChangeException extends \Exception
{
}