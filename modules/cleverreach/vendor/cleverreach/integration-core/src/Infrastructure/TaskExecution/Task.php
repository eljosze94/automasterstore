<?php

namespace CleverReach\Infrastructure\TaskExecution;

use CleverReach\Infrastructure\Interfaces\Required\Configuration;
use CleverReach\Infrastructure\ServiceRegister;
use CleverReach\Infrastructure\TaskExecution\TaskEvents\AliveAnnouncedTaskEvent;
use CleverReach\Infrastructure\TaskExecution\TaskEvents\ProgressedTaskEvent;
use CleverReach\Infrastructure\Utility\Events\EventEmitter;
use CleverReach\Infrastructure\Utility\TimeProvider;

abstract class Task extends EventEmitter implements \Serializable
{

    /** Max inactivity period for a task in seconds */
    const MAX_INACTIVITY_PERIOD = 60;

    /** Minimal number of seconds that must pass between two alive signals */
    const ALIVE_SIGNAL_FREQUENCY = 2;

    /** @var \DateTime */
    private $lastAliveSignalTime;

    /** @var Configuration */
    private $configService;

    /**
     * Runs task logic
     */
    abstract public function execute();

    /**
     * Reports task progress by emitting ProgressedTaskEvent and defers next AliveAnnouncedTaskEvent
     *
     * @param int $progressPercent Integer representation of progress percentage, value between 0 and 100
     * @throws \InvalidArgumentException In case when progress percent is outside of 0 - 100 boundaries or not an integer
     */
    public function reportProgress($progressPercent)
    {
        /** @var TimeProvider $timeProvider */
        $timeProvider = ServiceRegister::getService(TimeProvider::CLASS_NAME);
        $this->lastAliveSignalTime = $timeProvider->getCurrentLocalTime();

        $this->fire(new ProgressedTaskEvent($progressPercent));
    }

    /**
     * Reports that task is alive by emitting AliveAnnouncedTaskEvent
     */
    public function reportAlive()
    {
        /** @var TimeProvider $timeProvider */
        $timeProvider = ServiceRegister::getService(TimeProvider::CLASS_NAME);
        $currentTime = $timeProvider->getCurrentLocalTime();
        if (
            empty($this->lastAliveSignalTime) ||
            ($this->lastAliveSignalTime->getTimestamp() + self::ALIVE_SIGNAL_FREQUENCY < $currentTime->getTimestamp())
        ) {
            $this->lastAliveSignalTime = $currentTime;
            $this->fire(new AliveAnnouncedTaskEvent());
        }
    }

    /**
     * Gets max inactivity period for a task
     *
     * @return int Max inactivity period for a task in seconds
     */
    public function getMaxInactivityPeriod()
    {
        $configurationValue = $this->getConfigService()->getMaxTaskInactivityPeriod();
        return !is_null($configurationValue) ? $configurationValue : static::MAX_INACTIVITY_PERIOD;
    }

    /**
     * Gets task type descriptor
     *
     * @return string Task type. One word task descriptor
     */
    public function getType()
    {
        $namespaceParts = explode('\\', get_class($this));
        return end($namespaceParts);
    }

    /**
     * Determines whether task can be reconfigured.
     *
     * @return bool
     */
    public function canBeReconfigured()
    {
        return false;
    }

    /**
     * Reconfigures the task.
     */
    public function reconfigure()
    {

    }

    private function getConfigService()
    {
        if (empty($this->configService)) {
            $this->configService = ServiceRegister::getService(Configuration::CLASS_NAME);
        }

        return $this->configService;
    }
}