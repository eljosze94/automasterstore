<?php
/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'rg_chilexpress_services` (
    `id_service` int(11) NOT NULL AUTO_INCREMENT,
    `code` int(11) NOT NULL,
    `desc` varchar(64) NOT NULL,
    `id_reference` int(11) DEFAULT NULL,
    `date_add` datetime NOT NULL,
    `date_upd` datetime NOT NULL,
    PRIMARY KEY  (`id_service`),
    KEY `code` (`code`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'rg_chilexpress_cache` (
    `id_cart` int(10) UNSIGNED NOT NULL,
    `hash_cart` varchar(32) COLLATE utf8_general_ci DEFAULT NULL,
    `package` text COLLATE utf8_general_ci DEFAULT NULL,
    `carriers` text COLLATE utf8_general_ci DEFAULT NULL,
    PRIMARY KEY  (`id_cart`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
