Chilexpress - Module for PrestaShop (1.5.x - 1.7.x)
==================================================
## Description
Calculate shipping costs in real time through the webservice of Chilexpress.

## Requirements
- [PrestaShop 1.5.x - 1.7.x] (https://www.prestashop.com/)
- [PHP 5.4+] (http://www.php.net/)
- [PHP SOAP extension] (http://php.net/manual/en/book.soap.php)
- [PHP JSON extension] (http://php.net/manual/en/book.json.php)
- [PHP cURL extension] (http://php.net/manual/en/book.curl.php)

## Installation and configuration manual
- English: `/docs/readme_en.pdf`
- Spanish: `/docs/readme_es.pdf`
