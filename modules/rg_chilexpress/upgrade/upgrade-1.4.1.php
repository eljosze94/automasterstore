<?php
/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * This function updates the module from previous versions to the version 1.4.1
 */
function upgrade_module_1_4_1($module)
{
    if (@file_exists($module->getLocalPath().'libraries/LAFFPack.php')) {
        @unlink($module->getLocalPath().'libraries/LAFFPack.php');
    }

    return $module->uninstall() && $module->install();
}
