<?php
/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * This function updates the module from previous versions to the version 1.4.0
 */
function upgrade_module_1_4_0($module)
{
    return $module->uninstall() && $module->install();
}
