<?php
/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
include_once(dirname(__FILE__).'/rg_chilexpress.php');

$module = new Rg_Chilexpress();
if (Tools::getValue('secure_key') == $module->secure_key) {
    Configuration::updateValue('CHILEXPRESS_ALERTED_ASM', 1);
    die('1');
}

die('0');
