<?php
/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

class ChilexpressAPI
{
    private $login;
    private $user;
    private $pass;
    private $test_mode;
    private $log;
    private $services = array();

    public function __construct($login = '', $user = 'ps', $pass = '', $test_mode = false, $log = true)
    {
        $this->login = $login;
        $this->user = $user;
        $this->pass = $pass;
        $this->test_mode = $test_mode;
        $this->log = $log;
        $this->services = array(
            'TarificarCourier' => array(
                'namespace' => 'http://www.chilexpress.cl/TarificaCourier/'
            ),
        );
    }

    private function createPWSSOAPClient($function)
    {
        $client = new SoapClient(dirname(__FILE__).'/wsdl/'.$function.'.wsdl', array());

        $header_params = array(
            'transaccion' => array(
                'fechaHora' => date(DateTime::ATOM),
                'idTransaccionNegocio' => '0',
                'usuario' => $this->user,
                'sistema' => 'PrestaShop',
            )
        );
        $header = new SoapHeader($this->services[$function]['namespace'], 'headerRequest', $header_params);

        $client->__setSoapHeaders($header);

        return $client;
    }

    /**
     * Calculate shipment cost
     *
     * @param array $params Array for services params
     * @param array $errors By reference var for errors
     * @return bool|array array of stdObject for correct response or FALSE if errors.
     */
    public function getEstimateCost($params, &$errors)
    {
        $client = $this->createPWSSOAPClient('TarificarCourier', $params);

        try {
            $response = $client->TarificarCourier($params);
        } catch (SoapFault $e) {
            $errors[] = 'TarificarCourier: SoapFault (Code #'.$e->faultcode.') '.$e->faultstring.(isset($e->detail->desc) ? ' - '.$e->detail->desc : '');

            return false;
        }

        if ($errors = $this->checkErrors('TarificarCourier', $response)) {
            return false;
        }

        if (count($response->respValorizarCourier->Servicios) == 1) {
            return array($response->respValorizarCourier->Servicios);
        } else {
            return $response->respValorizarCourier->Servicios;
        }
    }
    
    private function checkErrors($service, $response)
    {
        $errors = array();

        if ($this->log) {
            switch ($service) {
                case 'TarificarCourier':
                    if ($response->respValorizarCourier->CodEstado) {
                        $errors[] = 'TarificarCourier: (Code #'.$response->respValorizarCourier->CodEstado.') '.$response->respValorizarCourier->GlsEstado;
                    }

                    break;
            }
        }

        return $errors;
    }
}
