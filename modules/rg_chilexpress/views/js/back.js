/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

function jQueryLoaded() {
    $(document).ready(function() {
        $('#alert_asm').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: module_dir + 'rg_chilexpress/ajax-bo.php',
                type: 'POST',
                data: {secure_key: rg_chilexpress.secure_key},
            }).done(function(data) {
                if (data == 1) {
                    $('#alert_asm').parent('div').hide();
                } else {
                    alert('There was a problem trying to turn off the message');
                }
            }).fail(function() {
                alert('There was a problem trying to turn off the message');
            });
        });

        if ((typeof rg_chilexpress != 'undefined') && (typeof rg_chilexpress.id_country != 'undefined')) {
            $('select#id_country').on('change', function() {
                var id_country = $(this).val();

                if (id_country == rg_chilexpress.id_country) {
                    var city = $('input#city').val();
                    var exists = $.grep(rg_chilexpress.cities, function(v) {
                        return v.name === city;
                    });

                    if (exists.length < 1) {
                        $('input#city').val('');
                    }

                    $('input#city').flexdatalist({
                        data: rg_chilexpress.cities,
                        minLength: 1,
                        valueProperty: (exists.length ? 'name' : null),
                        searchIn: 'name',
                        selectionRequired: true,
                        searchByWord: true,
                        noResultsText: rg_chilexpress.texts.no_results + ' "{keyword}"',
                        debug: false
                    });
                } else if ($('input#city').hasClass('flexdatalist-set')) {
                    $('input#city').flexdatalist('destroy');
                }
            }).change();
        }

        /*
         * Module configuration form
         */
        var _ps_version = _PS_VERSION_.substr(0, 3);

        $('select#CHILEXPRESS_SET_DIMENSIONS').on('change', function() {
            var val = parseInt($(this).val());

            if (_ps_version >= 1.6) {
                $('div.CHILEXPRESS_SET_VALUE_WIDTH').toggle(Boolean(val));
                $('div.CHILEXPRESS_SET_VALUE_HEIGHT').toggle(Boolean(val));
                $('div.CHILEXPRESS_SET_VALUE_DEPTH').toggle(Boolean(val));
            } else {
                $('#CHILEXPRESS_SET_VALUE_WIDTH').parent('.margin-form').toggle(Boolean(val));
                $('#CHILEXPRESS_SET_VALUE_HEIGHT').parent('.margin-form').toggle(Boolean(val));
                $('#CHILEXPRESS_SET_VALUE_DEPTH').parent('.margin-form').toggle(Boolean(val));
            }
        }).change();

        $('select#CHILEXPRESS_SET_WEIGHT').on('change', function() {
            var val = parseInt($(this).val());

            if (_ps_version >= 1.6) {
                $('div.CHILEXPRESS_SET_VALUE_WEIGHT').toggle(Boolean(val));
            } else {
                $('#CHILEXPRESS_SET_VALUE_WEIGHT').parent('.margin-form').toggle(Boolean(val));
            }
        }).change();

        $('select#CHILEXPRESS_IMPACT_PRICE').on('change', function() {
            var val = parseInt($(this).val());

            if (_ps_version >= 1.6) {
                if (val == 1 || val == 3) {
                    $('div.CHILEXPRESS_IMPACT_PRICE_AMOUNT span.input-group-addon').text('%');
                } else {
                    $('div.CHILEXPRESS_IMPACT_PRICE_AMOUNT span.input-group-addon').text(rg_chilexpress.currency_sign);
                }

                $('div.CHILEXPRESS_IMPACT_PRICE_AMOUNT').toggle(Boolean(val));
            } else {
                if (val == 1 || val == 3) {
                    $('#CHILEXPRESS_IMPACT_PRICE_AMOUNT + span').text('%');
                } else {
                    $('#CHILEXPRESS_IMPACT_PRICE_AMOUNT + span').text(rg_chilexpress.currency_sign);
                }

                $('#CHILEXPRESS_IMPACT_PRICE_AMOUNT').parent('.margin-form').toggle(Boolean(val));
            }
        }).change();
    });
}

/**
 * Hack for some shops that does not load the jQuery before this script
 * @return function [Start the code]
 */
var checkForjQuery = {
    check: function() {
        if (typeof window.jQuery != 'undefined') {
            return jQueryLoaded();
        } else {
            ++this.count;

            // After 20 seconds
            if (this.count >= 400) {
                return alert('You need to have jQuery loaded, some functions may work properly.');
            } else {
                setTimeout(function() {
                    return checkForjQuery.check();
                }, 50);
            }
        }
    },
    count: 0
};

checkForjQuery.check();
