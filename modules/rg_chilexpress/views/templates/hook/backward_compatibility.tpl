{**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 *}

<script type="text/javascript">
    var rg_chilexpress = {
            secure_key: "{$rg_chilexpress.secure_key|escape:'htmlall':'UTF-8'}",
            page_name: "{$rg_chilexpress.page_name|escape:'htmlall':'UTF-8'}",
            ps_version: "{$rg_chilexpress.ps_version|escape:'htmlall':'UTF-8'}",
            cities: JSON.parse('{$rg_chilexpress.cities}'),
            id_country: "{$rg_chilexpress.id_country|escape:'htmlall':'UTF-8'}",
            currency_sign: "{$rg_chilexpress.currency_sign|escape:'htmlall':'UTF-8'}",
            texts: JSON.parse('{$rg_chilexpress.texts}')
        };
</script>
