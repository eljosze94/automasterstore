{**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 *}

<div class="panel">
    <img src="{$rg_chilexpress.dir|escape:'html':'UTF-8'}/logo.png" id="module_logo" class="pull-left" />
    <p>
        <strong>{$rg_chilexpress.name|escape:'html':'UTF-8'}</strong><br />
        {$rg_chilexpress.description|escape:'html':'UTF-8'}<br />
        {l s='Version' mod='rg_chilexpress'} {$rg_chilexpress.version|escape:'html':'UTF-8'}<br />
    </p>
    <br style="display: none;"/>
    <p style="display: none;">
        <a id="module_doc" href="#">{l s='Documentation' mod='rg_chilexpress'}</a>
    </p>
</div>

<div class="panel">
    <h3><i class="icon icon-tags"></i> {l s='Documentation' mod='rg_chilexpress'}</h3>
    <p>
        &raquo; {l s='You can get PDF documentation to configure this module' mod='rg_chilexpress'} :
        <ul>
            <li><a href="{$rg_chilexpress.dir|escape:'html':'UTF-8'}/docs/readme_en.pdf" target="_blank">{l s='English' mod='rg_chilexpress'}</a></li>
            <li><a href="{$rg_chilexpress.dir|escape:'html':'UTF-8'}/docs/readme_es.pdf" target="_blank">{l s='Spanish' mod='rg_chilexpress'}</a></li>
        </ul>
    </p>
</div>
