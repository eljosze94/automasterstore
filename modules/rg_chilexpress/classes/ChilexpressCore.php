<?php
/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

require(dirname(__FILE__).'/ChilexpressLists.php');
require(dirname(__FILE__).'/ChilexpressTools.php');
require(dirname(__FILE__).'/ChilexpressCache.php');
require(dirname(__FILE__).'/ChilexpressServices.php');
require_once(dirname(__FILE__).'/../libraries/ChilexpressLAFFPack.php');
require(dirname(__FILE__).'/../libraries/ChilexpressAPI.php');

class ChilexpressCore extends CarrierModule
{
    const EST_MODE_WEIGHT = 1;
    const EST_MODE_CUBIC = 2;
    const EST_MODE_3D = 3;

    public function getOrderShippingCost($params, $shipping_cost) {}
    public function getOrderShippingCostExternal($params) {}

    protected function updateCache(Cart $cart, $hash_cart)
    {
        $cache = new ChilexpressCache((int)$cart->id);

        // Initialize cache values
        $cache->hash_cart = $hash_cart;
        $cache->package = null;
        $cache->carriers = null;

        $products = $cart->getProducts();
        $normalized = $this->config['CHILEXPRESS_NORMALIZED'];
        $estimation_mode = (int)$this->config['CHILEXPRESS_ESTIMATION_MODE'];
        $services_list = ChilexpressServices::getAll(true);
        $carrier_cities = ChilexpressLists::cities('dest');
        $address = new Address((int)$cart->id_address_delivery);

        $weight = 0;

        if ($products) {
            switch ($estimation_mode) {
                case self::EST_MODE_WEIGHT:
                    $weight = ChilexpressTools::getProductsWeight($this->config, $cart);

                    $width = 0;
                    $height = 0;
                    $depth = 0;
                    $weight = ChilexpressTools::convertToKg($weight);

                    break;

                case self::EST_MODE_CUBIC:
                    $volume = ChilexpressTools::getProductsVolume($this->config, $cart);
                    $weight = ChilexpressTools::getProductsWeight($this->config, $cart);

                    $pow = pow($volume, 1/3);
                    $width = $pow;
                    $height = $pow;
                    $depth = $pow;

                    $width = ChilexpressTools::convertToCm($width);
                    $height = ChilexpressTools::convertToCm($height);
                    $depth = ChilexpressTools::convertToCm($depth);
                    $weight = ChilexpressTools::convertToKg($weight);

                    break;

                case self::EST_MODE_3D:
                    $boxes = array();

                    foreach ($products as $product) {
                        $product_boxes = $this->getProductBoxes($product);
                        $boxes = !$boxes
                            ? $product_boxes
                            : array_merge($boxes, $product_boxes);
                    }

                    $weight = ChilexpressTools::getProductsWeight($this->config, $cart);

                    // Initialize ChilexpressLAFFPack
                    $lap = new ChilexpressLAFFPack();

                    // Start packing our nice boxes
                    $lap->pack($boxes);
                    // Collect our container details
                    $c_size = $lap->get_container_dimensions();

                    $width = ChilexpressTools::convertToCm($c_size['width']);
                    $height = ChilexpressTools::convertToCm($c_size['height']);
                    $depth = ChilexpressTools::convertToCm($c_size['length']);
                    $weight = ChilexpressTools::convertToKg($weight);

                    break;
            }
        }

        // Check if there is a valid weight
        if (($weight > 0) &&
            // Check if there is a services list available
            $services_list &&
            // Check if currency of Chile exists
            $this->config['chile_id_currency'] &&
            // Check if the country of Chile exists
            $this->config['chile_id_country'] &&
            // Check if the addresses were normalized
            $normalized &&
            // Check if the id_address in the delivery address correspond to Chile
            ($this->config['chile_id_country'] == $address->id_country)
        ) {
            $cache->package = array(
                'width' => $width,
                'height' => $height,
                'depth' => $depth,
                'weight' => $weight,
            );
            $dest_code = '';

            // Check if the city name is valid and get the according destination code
            foreach ($carrier_cities as $city) {
                if ($city['name'] == $address->city) {
                    $dest_code = $city['id'];
                    
                    break;
                }
            }

            if ($dest_code) {
                // Get the shipping costs available
                $services_cost = $this->getServicesCost($dest_code, $weight, $height, $width, $depth);

                // If there is a services cost available
                if ($services_cost) {
                    foreach ($services_cost as $service_cost) {
                        $service = ChilexpressServices::getByCode((int)$service_cost->CodServicio);
                        if ($service) {
                            $carrier = Carrier::getCarrierByReference($service->id_reference);
                            if ($carrier) {
                                $cost = $service_cost->ValorServicio;

                                if ($impact_price = $this->config['CHILEXPRESS_IMPACT_PRICE']) {
                                    $impact_price_amount = $this->config['CHILEXPRESS_IMPACT_PRICE_AMOUNT'];

                                    switch ($impact_price) {
                                        case 1: // Increase percent
                                            $cost += ($impact_price_amount / 100) * $cost;
                                            break;
                                        case 2: // Increase amount
                                            $cost += $impact_price_amount;
                                            break;
                                        case 3: // Reduction percent
                                            $cost -= ($impact_price_amount / 100) * $cost;
                                            break;
                                        case 4: // Reduction amount
                                            $cost -= $impact_price_amount;
                                            break;
                                    }
                                }
                                
                                $cache->carriers[$carrier->id] = $cost;
                            }
                        }
                    }
                }
            }
        }

        return $cache->save();
    }

    public function getProductBoxes($product)
    {
        $boxes = array();

        for ($i = 0; $i < $product['cart_quantity']; $i++) {
            $s_width = (float)$this->config['CHILEXPRESS_SET_VALUE_WIDTH'];
            $s_height = (float)$this->config['CHILEXPRESS_SET_VALUE_HEIGHT'];
            $s_depth = (float)$this->config['CHILEXPRESS_SET_VALUE_DEPTH'];
            $product_width = (float)$product['width'];
            $product_height = (float)$product['height'];
            $product_depth = (float)$product['depth'];

            switch ($this->config['CHILEXPRESS_SET_DIMENSIONS']) {
                case 1: // Set the specified dimensions
                    $values = array(
                        ($s_width ? $s_width : $product_width),
                        ($s_height ? $s_height : $product_height),
                        ($s_depth ? $s_depth : $product_depth)
                    );

                    break;

                case 2: // When the product dimension is missing or not set
                    $values = array(
                        ($s_width && !$product_width ? $s_width : $product_width),
                        ($s_height && !$product_height ? $s_height : $product_height),
                        ($s_depth && !$product_depth ? $s_depth : $product_depth)
                    );

                    break;

                case 3: // When the product dimension is less than specified
                    $values = array(
                        ($s_width && ($product_width < $s_width) ? $s_width : $product_width),
                        ($s_height && ($product_height < $s_height) ? $s_height : $product_height),
                        ($s_depth && ($product_depth < $s_depth) ? $s_depth : $product_depth)
                    );

                    break;

                case 4: // When the product dimension is greater than specified
                    $values = array(
                        ($s_width && ($product_width > $s_width) ? $s_width : $product_width),
                        ($s_height && ($product_height > $s_height) ? $s_height : $product_height),
                        ($s_depth && ($product_depth > $s_depth) ? $s_depth : $product_depth)
                    );

                    break;
                
                default:
                    $values = array(
                        $product_width,
                        $product_height,
                        $product_depth
                    );

                    break;
            }

            sort($values);

            $boxes[] = array_combine(array('height', 'width', 'length'), $values);
        }

        return $boxes;
    }

    /**
     * Get the cost of the available services
     * @param  [string] $origin         The city code origin
     * @param  [string] $destination    The city code destination
     * @param  [float] $weight          The weight of the product in Kg
     * @param  [float] $height          The height of the product in cm
     * @param  [float] $width           The width of the product in cm
     * @param  [float] $depth           The depth of the product in cm
     * @return [array]                  Return empty if no services
     */
    protected function getServicesCost($destination, $weight, $height, $width, $depth, $city_origin = null)
    {
        if ($city_origin == null) {
            if (!$city_origin = $this->config['CHILEXPRESS_CITY_ORIGIN']) {
                return false;
            }
        }

        $variables = array(
            'origin' => $city_origin,
            'destination' => $destination,
            'weight' => $weight,
            'height' => $height,
            'width' => $width,
            'depth' => $depth
        );
        
        $services = $this->calculatePricing($variables);

        if (is_array($services)) {
            if ((isset($services[0]->CodServicio) && $services[0]->CodServicio == 1) &&
                (isset($services[0]->GlsServicio) && $services[0]->ValorServicio == 0)) {
                return false;
            }

            return $services;
        }

        return false;
    }

    /**
     * Calculate the pricing for the shipping.
     *
     * @param array $variables
     *  The information to calculate the pricing. The structure:
     *    - origin: The city code origin.
     *    - destination: The city code destination.
     *    - weight: The weight of the product in Kg.
     *    - height: The height of the product in cm.
     *    - width: The width of the product in cm.
     *    - depth: The depth of the product in cm.
     *
     * @return mixed
     *  If fails FALSE is return or an array of services available.
     */
    private function calculatePricing(array $variables)
    {
        $variables = array(
            'reqValorizarCourier' => array(
                'CodCoberturaOrigen' => $variables['origin'],
                'CodCoberturaDestino' => $variables['destination'],
                'PesoPza' => $variables['weight'],
                'DimAltoPza' => $variables['height'],
                'DimAnchoPza' => $variables['width'],
                'DimLargoPza' => $variables['depth'],
            ),
        );

        $errors = false;
        $api = new ChilexpressAPI();
        $results = $api->getEstimateCost($variables, $errors);

        if (!$results) {
            Tools::error_log($this->name.': calculatePricing: '.implode(',', $errors));

            return false;
        }

        return $results;
    }
}
