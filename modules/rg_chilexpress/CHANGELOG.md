# CHANGELOG

## 1.4.3 -- (2018, Jan 16)
[*] Improved cost in the estimation mode based on 3D packaging
[*] Improved performance and automated in the address normalization system
[-] Fixed the input city marked as wrong in the guest checkout and the one page checkout

## 1.4.1 -- (2017, Nov 06)
[+] Added compatibility with the modules Starken 1.0.1+ and Correos Chile 1.0.0+
[-] Fixed error in the BO that did not allow to quote and gave error 500
[-] Fixed error that did not showed any results when there was only one service available
[-] Minor fix on currency exchange rate

## 1.4.0 -- (2017, Oct 16)
[+] Added option to quote only on checkout page
[+] Added option to set product dimensions and base weight
[+] Added message in order with package details and type of service
[+] Added warning message when advanced stock is active
[*] Webservice connection times has been optimized, now is at least 3x faster than before
[*] Updated name of communes by cities, and now has been added more than 390 new cities to add compatibility with Starken module
[*] Updated the city autocomplete script
[*] City autocomplete now allows search by word and validation is improved
[*] Multiple speed improvements
[-] Minor correction in 3D estimation mode
[-] Minor correction in address normalization
[-] Visual corrections in module configurator for PS 1.5

## 1.0.0 -- (2017, Mar 15)
[+] Initial release
