<?php
/**
 * Calculate shipping costs in real time through the webservice of Chilexpress
 *
 *  @author    Rolige <www.rolige.com>
 *  @copyright 2011-2018 Rolige - All Rights Reserved
 *  @license   Proprietary and confidential
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require(dirname(__FILE__).'/classes/ChilexpressCore.php');

class Rg_Chilexpress extends ChilexpressCore
{
    public $id_carrier;
    public $config;
    protected $config_form = false;
    protected $carrier_name = 'Chilexpress';
    private $deleting_from_module = false;

    public function __construct()
    {
        $this->name = 'rg_chilexpress';
        $this->tab = 'shipping_logistics';
        $this->version = '1.4.3';
        $this->author = 'Rolige';
        $this->author_link = 'http://www.rolige.com/';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->secure_key = Tools::encrypt($this->name);
        $this->module_key = '72691790fb5ded5630703c5a09fc64bd';

        parent::__construct();

        $this->displayName = $this->l('Chilexpress');
        $this->description = $this->l('Calculate shipping costs in real time through the webservice of Chilexpress');

        $this->config = Configuration::getMultiple(array(
            'CHILEXPRESS_NORMALIZED',
            'CHILEXPRESS_CITY_ORIGIN',
            'CHILEXPRESS_ONLY_CHECKOUT',
            'CHILEXPRESS_ESTIMATION_MODE',
            'CHILEXPRESS_SET_DIMENSIONS',
            'CHILEXPRESS_SET_VALUE_WIDTH',
            'CHILEXPRESS_SET_VALUE_HEIGHT',
            'CHILEXPRESS_SET_VALUE_DEPTH',
            'CHILEXPRESS_SET_WEIGHT',
            'CHILEXPRESS_SET_VALUE_WEIGHT',
            'CHILEXPRESS_IMPACT_PRICE',
            'CHILEXPRESS_IMPACT_PRICE_AMOUNT',
        ));
        $this->config['chile_id_country'] = (int)Country::getByIso('CL');
        $this->config['chile_id_currency'] = (int)Currency::getIdByIsoCode('CLP');

        if (!$this->config['CHILEXPRESS_NORMALIZED']) {
            $this->warning = $this->l('Addresses should be normalized');
        }
    }

    public function install()
    {
        if (extension_loaded('soap') == false) {
            $this->_errors[] = $this->l('To install this module, you must enable the SOAP extension on the server');
            return false;
        } elseif (!$this->config['chile_id_country']) {
            $this->_errors[] = $this->l('To install this module, you must have the country of Chile in your countries list');
            return false;
        } elseif (!$this->config['chile_id_currency']) {
            $this->_errors[] = $this->l('To install this module, you must have the Chilean Peso in your currencies list');
            return false;
        }

        include(dirname(__FILE__).'/sql/install.php');
        Configuration::updateValue('CHILEXPRESS_ONLY_CHECKOUT', 1);

        foreach (ChilexpressLists::services() as $service) {
            if (!$carrier = $this->addCarrier($service)) {
                $this->_errors[] = $this->l('An error occurred when creating the carrier');
                return false;
            }

            $this->addZones($carrier);
            $this->addGroups($carrier);
            $this->addRanges($carrier);
        }

        return parent::install() &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('displayHeader') &&
            $this->registerHook('actionObjectCarrierUpdateAfter') &&
            $this->registerHook('actionValidateOrder');
    }

    public function uninstall()
    {
        if ($services = ChilexpressServices::getAll(true)) {
            foreach ($services as $service) {
                if ($carrier = Carrier::getCarrierByReference($service['id_reference'])) {
                    $this->deleting_from_module = true;
                    $carrier->deleted = true;
                    $carrier->update();
                }
            }
        }

        include(dirname(__FILE__).'/sql/uninstall.php');
        Configuration::deleteByName('CHILEXPRESS_NORMALIZE_ADDRESSES');
        Configuration::deleteByName('CHILEXPRESS_NORMALIZED');
        Configuration::deleteByName('CHILEXPRESS_ALERTED_ASM');
        $config_values = $this->getConfigFormValues();

        foreach (array_keys($config_values) as $key) {
            Configuration::deleteByName($key);
        }

        return parent::uninstall();
    }

    /**
     * Configuration form
     */
    public function getContent()
    {
        $output = '';

        // Warning message when some carriers are not associated with any zone
        if ($services = ChilexpressServices::getAll(true)) {
            $carriers = array();
            foreach ($services as $service) {
                if ($carrier = Carrier::getCarrierByReference($service['id_reference'])) {
                    if (!$carrier->getZones()) {
                        $carriers[] = $carrier->name.': '.$carrier->delay[$this->context->language->id];
                    }
                }
            }

            if ($carriers) {
                $msg = $this->l('The following carriers are not associated with any Zone');
                $msg .= '<ul>';
                foreach ($carriers as $carrier) {
                    $msg .= '<li>'.$carrier.'</li>';
                }

                $msg .= '</ul>';
                $output .= $this->adminDisplayWarning($msg);
            }
        }

        // Warning message when advanced stock is enabled
        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && !Configuration::get('CHILEXPRESS_ALERTED_ASM')) {
            $output .= $this->adminDisplayWarning(
                $this->l('Because you are using "Advanced stock", you must make sure to associate the shipping carriers to the warehouses.').' <a id="alert_asm" href="#" class="btn btn-default">'.$this->l('Confirm!').'</a>'
            );
        }

        /**
         * If values have been submitted in the form, process
         */
        if (((bool)Tools::isSubmit('submitRg_ChilexpressNormalize')) == true) {
            ChilexpressTools::normalizeAddresses();
            if (Configuration::get('CHILEXPRESS_NORMALIZED') == true) {
                $output .= $this->displayConfirmation($this->l('All addresses were normalized successfully'));
            }
        } elseif (((bool)Tools::isSubmit('submitRg_ChilexpressConfig')) == true) {
            $value_width = trim(Tools::getValue('CHILEXPRESS_SET_VALUE_WIDTH'));
            $value_height = trim(Tools::getValue('CHILEXPRESS_SET_VALUE_HEIGHT'));
            $value_depth = trim(Tools::getValue('CHILEXPRESS_SET_VALUE_DEPTH'));
            $value_weight = trim(Tools::getValue('CHILEXPRESS_SET_VALUE_WEIGHT'));
            $impact_price_amount = trim(Tools::getValue('CHILEXPRESS_IMPACT_PRICE_AMOUNT'));

            // Validate city of origin
            if ((!$city_origin = Tools::getValue('CHILEXPRESS_CITY_ORIGIN')) ||
                !Validate::isGenericName($city_origin)
            ) {
                $output .= $this->displayError($this->l('You must select a valid city of origin'));
            // Validate estimation mode
            } elseif ((($estimation_mode = Tools::getValue('CHILEXPRESS_ESTIMATION_MODE')) && !Validate::isInt($estimation_mode)) ||
                ($estimation_mode && $estimation_mode < 1)
            ) {
                $output .= $this->displayError($this->l('The estimation mode, must be a positive integer'));
            // Validate impact on price
            } elseif ((($impact_price = Tools::getValue('CHILEXPRESS_IMPACT_PRICE')) && !Validate::isInt($impact_price)) ||
                ($impact_price && $impact_price < 0)
            ) {
                $output .= $this->displayError($this->l('Impact on price, must be a positive integer'));
            } elseif (($impact_price && (!$impact_price_amount = trim(Tools::getValue('CHILEXPRESS_IMPACT_PRICE_AMOUNT')))) ||
                ($impact_price && !Validate::isFloat($impact_price_amount)) ||
                ($impact_price && $impact_price_amount < 0)
            ) {
                $output .= $this->displayError($this->l('The amount/percent that will have an impact on price, must be a positive integer/float number'));
            } elseif (!$impact_price && (
                $impact_price_amount &&
                (!Validate::isFloat($impact_price_amount) || $impact_price_amount < 0)
            )) {
                $_POST['CHILEXPRESS_IMPACT_PRICE_AMOUNT'] = '';
            // Validate product dimensions
            } elseif ((($product_dimensions = Tools::getValue('CHILEXPRESS_SET_DIMENSIONS')) && !Validate::isInt($product_dimensions)) ||
                ($product_dimensions && $product_dimensions < 0)
            ) {
                $output .= $this->displayError($this->l('The option "Set product dimensions", must be a positive integer number'));
            } elseif ($product_dimensions && (
                !$value_width &&
                !$value_height &&
                !$value_depth
            )) {
                $output .= $this->displayError($this->l('You must set at least one product dimension'));
            } elseif ($product_dimensions && (
                ($value_width && (!Validate::isFloat($value_width) || $value_width < 0)) ||
                ($value_height && (!Validate::isFloat($value_height) || $value_height < 0)) ||
                ($value_depth && (!Validate::isFloat($value_depth) || $value_depth < 0))
            )) {
                $output .= $this->displayError($this->l('The product dimensions specified, must be a positive integer/float number'));
            } elseif (!$product_dimensions && (
                ($value_width && (!Validate::isFloat($value_width) || $value_width < 0)) ||
                ($value_height && (!Validate::isFloat($value_height) || $value_height < 0)) ||
                ($value_depth && (!Validate::isFloat($value_depth) || $value_depth < 0))
            )) {
                $_POST['CHILEXPRESS_SET_VALUE_WIDTH'] = '';
                $_POST['CHILEXPRESS_SET_VALUE_HEIGHT'] = '';
                $_POST['CHILEXPRESS_SET_VALUE_DEPTH'] = '';
            // Validate product weight
            } elseif ((($product_weight = Tools::getValue('CHILEXPRESS_SET_WEIGHT')) && !Validate::isInt($product_weight)) ||
                ($product_weight && $product_weight < 0)
            ) {
                $output .= $this->displayError($this->l('The option "Set product weight", must be a positive integer number'));
            } elseif ($product_weight && !$value_weight) {
                $output .= $this->displayError($this->l('You must set the product weight'));
            } elseif ($product_weight &&
                ($value_weight && (!Validate::isFloat($value_weight) || $value_weight < 0))
            ) {
                $output .= $this->displayError($this->l('The product weight specified, must be a positive integer/float number'));
            } elseif (!$product_weight && (
                $value_weight &&
                (!Validate::isFloat($value_weight) || $value_weight < 0)
            )) {
                $_POST['CHILEXPRESS_SET_VALUE_WEIGHT'] = '';
            // In case all validations are ok
            } else {
                $this->postProcess('Config');
                $output .= $this->displayConfirmation($this->l('Configuration updated successfully'));
            }
        }

        $this->context->smarty->assign('rg_chilexpress', array(
            'dir' => $this->_path,
            'name' => $this->displayName,
            'description' => $this->description,
            'version' => $this->version,
            'author' => $this->author,
            'author_link' => $this->author_link,
            'ps_15' => version_compare(_PS_VERSION_, '1.6.0.0', '<')
        ));

        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        if (Configuration::get('CHILEXPRESS_NORMALIZED') == false) {
            $output .= $this->renderNormalizeForm();
        } else {
            $output .= $this->renderConfigForm();
        }

        return $output;
    }

    /**
     * Config form that will be displayed in the configuration
     */
    protected function renderConfigForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitRg_ChilexpressConfig';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for the inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $ps_15 = version_compare(_PS_VERSION_, '1.6.0.0', '<');

        // Structure of the form
        return $helper->generateForm(array(array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => ($ps_15 ? 'radio' : 'switch'),
                        'label' => $this->l('Quote only on checkout page'),
                        'name' => 'CHILEXPRESS_ONLY_CHECKOUT',
                        'class' => 't',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                        ($ps_15 ? 'desc' : 'hint') => $this->l('If enabled, shop navigation will be faster avoiding unnecessary quotations (recommended).')
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('City of origin'),
                        'name' => 'CHILEXPRESS_CITY_ORIGIN',
                        'class' => 'chosen fixed-width-xxl',
                        'options' => array(
                            'query' => array_merge(
                                array(array('id' => '', 'name' => '')),
                                ChilexpressLists::cities('orig')
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        ($ps_15 ? 'desc' : 'hint') => $this->l('City of origin from which the shipments will be made (only the origin cities available by the carrier are listed).'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Estimation mode'),
                        'name' => 'CHILEXPRESS_ESTIMATION_MODE',
                        'class' => 'fixed-width-xxl',
                        'options' => array(
                            'query' => array(
                                array('id' => 1, 'name' => $this->l('Based on weight')),
                                array('id' => 2, 'name' => $this->l('Based on weight and cubic volume')),
                                array('id' => 3, 'name' => $this->l('Based on weight and a volume generated using an efficient algorithm for 3D rectangular box packing'))
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        ($ps_15 ? 'desc' : 'hint') => $this->l('Method with which the shipping cost is estimated, highly recommended 3D method.'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Set product dimensions'),
                        'name' => 'CHILEXPRESS_SET_DIMENSIONS',
                        'class' => 'fixed-width-xxl',
                        'options' => array(
                            'query' => array(
                                array('id' => 0, 'name' => $this->l('No')),
                                array('id' => 1, 'name' => $this->l('Yes, set the specified dimensions')),
                                array('id' => 2, 'name' => $this->l('Yes, when the product dimension is missing or not set')),
                                array('id' => 3, 'name' => $this->l('Yes, when the product dimension is less than specified')),
                                array('id' => 4, 'name' => $this->l('Yes, when the product dimension is greater than specified')),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        ($ps_15 ? 'desc' : 'hint') => $this->l('Set a predefined dimensions to your products at the time of quotation. Leave blank or "0" to omit.'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => ($name = 'CHILEXPRESS_SET_VALUE_WIDTH'),
                        'class' => 'input fixed-width-md',
                        'form_group_class' => $name,
                        'prefix' => $this->l('width'),
                        'suffix' => Configuration::get('PS_DIMENSION_UNIT'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => ($name = 'CHILEXPRESS_SET_VALUE_HEIGHT'),
                        'class' => 'input fixed-width-md',
                        'form_group_class' => $name,
                        'prefix' => $this->l('height'),
                        'suffix' => Configuration::get('PS_DIMENSION_UNIT'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => ($name = 'CHILEXPRESS_SET_VALUE_DEPTH'),
                        'class' => 'input fixed-width-md',
                        'form_group_class' => $name,
                        'prefix' => $this->l('depth'),
                        'suffix' => Configuration::get('PS_DIMENSION_UNIT'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Set product weight'),
                        'name' => 'CHILEXPRESS_SET_WEIGHT',
                        'class' => 'fixed-width-xxl',
                        'options' => array(
                            'query' => array(
                                array('id' => 0, 'name' => $this->l('No')),
                                array('id' => 1, 'name' => $this->l('Yes, set the specified weight')),
                                array('id' => 2, 'name' => $this->l('Yes, when the product weight is missing or not set')),
                                array('id' => 3, 'name' => $this->l('Yes, when the product weight is less than specified')),
                                array('id' => 4, 'name' => $this->l('Yes, when the product weight is greater than specified')),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        ($ps_15 ? 'desc' : 'hint') => $this->l('Set a predefined weight to your products at the time of quotation.'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => ($name = 'CHILEXPRESS_SET_VALUE_WEIGHT'),
                        'class' => 'input fixed-width-md',
                        'form_group_class' => $name,
                        'suffix' => Configuration::get('PS_WEIGHT_UNIT'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Impact on price'),
                        'name' => 'CHILEXPRESS_IMPACT_PRICE',
                        'class' => 'fixed-width-xxl',
                        'options' => array(
                            'query' => array(
                                array('id' => 0, 'name' => $this->l('No')),
                                array('id' => 1, 'name' => $this->l('Increase (Percent)')),
                                array('id' => 2, 'name' => $this->l('Increase (Amount)')),
                                array('id' => 3, 'name' => $this->l('Decrease (Percent)')),
                                array('id' => 4, 'name' => $this->l('Decrease (Amount)'))
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        ($ps_15 ? 'desc' : 'hint') => $this->l('Use this in case you need a small adjustment in the shipping cost.'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => ($name = 'CHILEXPRESS_IMPACT_PRICE_AMOUNT'),
                        'class' => 'input fixed-width-md',
                        'form_group_class' => $name,
                        'suffix' => '%',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        )));
    }

    /**
     * Values for the inputs
     */
    protected function getConfigFormValues()
    {
        return array(
            'CHILEXPRESS_CITY_ORIGIN' => trim(Tools::getValue(
                'CHILEXPRESS_CITY_ORIGIN',
                Configuration::get('CHILEXPRESS_CITY_ORIGIN')
            )),
            'CHILEXPRESS_ESTIMATION_MODE' => trim(Tools::getValue(
                'CHILEXPRESS_ESTIMATION_MODE',
                Configuration::get('CHILEXPRESS_ESTIMATION_MODE')
            )),
            'CHILEXPRESS_SET_DIMENSIONS' => trim(Tools::getValue(
                'CHILEXPRESS_SET_DIMENSIONS',
                Configuration::get('CHILEXPRESS_SET_DIMENSIONS')
            )),
            'CHILEXPRESS_SET_VALUE_WIDTH' => trim(Tools::getValue(
                'CHILEXPRESS_SET_VALUE_WIDTH',
                Configuration::get('CHILEXPRESS_SET_VALUE_WIDTH')
            )),
            'CHILEXPRESS_SET_VALUE_HEIGHT' => trim(Tools::getValue(
                'CHILEXPRESS_SET_VALUE_HEIGHT',
                Configuration::get('CHILEXPRESS_SET_VALUE_HEIGHT')
            )),
            'CHILEXPRESS_SET_VALUE_DEPTH' => trim(Tools::getValue(
                'CHILEXPRESS_SET_VALUE_DEPTH',
                Configuration::get('CHILEXPRESS_SET_VALUE_DEPTH')
            )),
            'CHILEXPRESS_SET_WEIGHT' => trim(Tools::getValue(
                'CHILEXPRESS_SET_WEIGHT',
                Configuration::get('CHILEXPRESS_SET_WEIGHT')
            )),
            'CHILEXPRESS_SET_VALUE_WEIGHT' => trim(Tools::getValue(
                'CHILEXPRESS_SET_VALUE_WEIGHT',
                Configuration::get('CHILEXPRESS_SET_VALUE_WEIGHT')
            )),
            'CHILEXPRESS_IMPACT_PRICE' => trim(Tools::getValue(
                'CHILEXPRESS_IMPACT_PRICE',
                Configuration::get('CHILEXPRESS_IMPACT_PRICE')
            )),
            'CHILEXPRESS_IMPACT_PRICE_AMOUNT' => trim(Tools::getValue(
                'CHILEXPRESS_IMPACT_PRICE_AMOUNT',
                Configuration::get('CHILEXPRESS_IMPACT_PRICE_AMOUNT')
            )),
            'CHILEXPRESS_ONLY_CHECKOUT' => (int)Tools::getValue(
                'CHILEXPRESS_ONLY_CHECKOUT',
                Configuration::get('CHILEXPRESS_ONLY_CHECKOUT')
            )
        );
    }

    /**
     * Config form that will be displayed in the configuration
     */
    protected function renderNormalizeForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitRg_ChilexpressNormalize';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => array(), /* Values for the inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $ps_15 = version_compare(_PS_VERSION_, '1.6.0.0', '<');
        $normalize_addresses = Configuration::get('CHILEXPRESS_NORMALIZE_ADDRESSES');

        // Structure of the form
        if ($normalize_addresses) {
            $normalize_addresses = ChilexpressTools::getAllChileAddresses($normalize_addresses);
            $total = count($normalize_addresses);
            $normalize_addresses = array_slice($normalize_addresses, 0, 20);
            $admin_address_link = $this->context->link->getAdminLink('AdminAddresses', true).'&token='.Tools::getAdminTokenLite('AdminAddresses');
            $links = '';
            foreach ($normalize_addresses as $address) {
                $links .= '<a class="btn btn-default" href="'.$admin_address_link.'&id_address='.$address['id_address'].'&updateaddress" target="_blank">'.$this->l('ID Address').' '.$address['id_address'].', "<strong>'.$address['city'].'</strong>" <i class="icon-external-link"></i></a><br /><br />';
            }

            $form = array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Normalize Addresses').' ('.$total.')',
                        'icon' => 'icon-refresh',
                    ),
                    ($ps_15 ? 'description' : 'warning') => $this->l('Some addresses could not be normalized, therefore, you need to manually update the city of these addresses, you can edit one by one by clicking on the links below.').'<br /><br />'.$this->l('Once completing the changes, you must press again the button "Normalize Addresses".'),
                    'input' => array(
                        array(
                            'type' => 'html',
                            'name' => 'html_data',
                            ($ps_15 ? 'desc' : 'html_content') => $links
                        ),
                    ),
                    'submit' => array(
                        'title' => $this->l('Normalize Addresses'),
                        'icon' => 'process-icon-refresh'
                    ),
                ),
            );
        } else {
            $form = array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Normalize Addresses'),
                        'icon' => 'icon-refresh',
                    ),
                    ($ps_15 ? 'description' : 'warning') => $this->l('Before using this module, it is necessary to normalize all the names of the cities of each customer address that currently are already registered and belong to the country of Chile, to do this, just press the button "Normalize addresses". This process can take several minutes, depending on the number of addresses currently registered.').'<br /><br />'.$this->l('Before continuing, it is highly recommended to do a full backup of the site because changes are not reversible.'),
                    'submit' => array(
                        'title' => $this->l('Normalize Addresses'),
                        'icon' => 'process-icon-refresh'
                    )
                ),
            );
        }

        return $helper->generateForm(array($form));
    }

    /**
     * Save form data
     */
    protected function postProcess($form)
    {
        $form = 'get'.$form.'FormValues';
        $form_values = $this->$form();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, trim(Tools::getValue($key)));
        }
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        $page_name = false;

        if ($this->context->controller->controller_type == 'front') {
            if (version_compare(_PS_VERSION_, '1.7.0.0', '>=') === true) {
                $page_name = $this->context->controller->getPageName();
            } elseif (method_exists($this->context->smarty, 'getTemplateVars')) {
                $page_name = $this->context->smarty->getTemplateVars('page_name');
            }
        }

        // Check if module is active
        if (!$this->active ||
            // Check if city of origin is selected
            !$this->config['CHILEXPRESS_CITY_ORIGIN'] ||
            // Check if there is a valid id_carrier
            !$this->id_carrier ||
            // Check if running in the Back Office and the employee is logged
            ($this->context->controller->controller_type == 'admin' && $this->context->employee->id == false) ||
            // Check if running in the Front Office
            ($this->context->controller->controller_type == 'front' && (
                // If customer is logged
                $this->context->customer->logged == false || (
                    // If quote on checkout only is enabled
                    $this->config['CHILEXPRESS_ONLY_CHECKOUT'] &&
                    !in_array($page_name, array('order', 'order-opc', 'checkout')) &&
                    Tools::substr($page_name, 0, 7) != 'module-'
                )
            )) ||
            // Check if there is an active address delivery
            (!isset($params->id_address_delivery) || !$params->id_address_delivery)
        ) {
            return false;
        }

        $cache = new ChilexpressCache((int)$params->id);
        $hash_cart = ChilexpressTools::getHashCart((int)$params->id, $this->config);

        // Check if it has not yet been cached
        if ($cache->hash_cart != $hash_cart) {
            if (!$this->updateCache($params, $hash_cart)) {
                return false;
            }
        }

        if ($cache->carriers && isset($cache->carriers[$this->id_carrier]) && $cache->carriers[$this->id_carrier]) {
            $cost = Tools::convertPriceFull(
                $cache->carriers[$this->id_carrier],
                new Currency($this->config['chile_id_currency']),
                new Currency((int)$params->id_currency)
            );

            return $cost;
        }

        return false;
    }

    public function getOrderShippingCostExternal($params)
    {
        return $this->getOrderShippingCost($params, null);
    }

    protected function addCarrier(array $service)
    {
        $carrier = new Carrier();
        $carrier->name = $this->carrier_name;
        $carrier->url = 'https://www.chilexpress.cl/Views/ChilexpressCL/Resultado-busqueda.aspx?DATA=@';
        $carrier->is_module = true;
        $carrier->active = 1;
        $carrier->range_behavior = 0;
        $carrier->need_range = 1;
        $carrier->shipping_external = true;
        $carrier->external_module_name = $this->name;
        $carrier->shipping_method = 1;
        $carrier->grade = $service['grade'];

        foreach (Language::getLanguages() as $lang) {
            if (in_array($lang['iso_code'], array('es', 'ag', 'cl', 'cb', 'mx'))) {
                $carrier->delay[$lang['id_lang']] = Tools::ucfirst(Tools::strtolower($service['desc']['es']));
            } else {
                $carrier->delay[$lang['id_lang']] = Tools::ucfirst(Tools::strtolower($service['desc']['en']));
            }
        }

        if ($carrier->add() == true) {
            @copy($this->local_path.'views/img/chilexpress_logo.png', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
            
            $services = new ChilexpressServices();
            $services->code = $service['code'];
            $services->desc = $service['desc']['es'];
            $services->id_reference = (int)$carrier->id;
            $services->add();

            return $carrier;
        }

        return false;
    }

    protected function addGroups($carrier)
    {
        $groups_ids = array();
        $groups = Group::getGroups(Context::getContext()->language->id);
        foreach ($groups as $group) {
            $groups_ids[] = $group['id_group'];
        }

        if (version_compare(_PS_VERSION_, '1.5.5.0', '>=') === true) {
            $carrier->setGroups($groups_ids);
        } else {
            ChilexpressTools::setGroups($groups_ids, $carrier->id);
        }
    }

    protected function addRanges($carrier)
    {
        $range_weight = new RangeWeight();
        $range_weight->id_carrier = $carrier->id;
        $range_weight->delimiter1 = '0';
        $range_weight->delimiter2 = '999999';
        $range_weight->add();
    }

    protected function addZones($carrier)
    {
        if ($this->config['chile_id_country']) {
            if ($id_zone = Country::getIdZone($this->config['chile_id_country'])) {
                return $carrier->addZone($id_zone);
            }
        }
    }

    private function prepareCommonHook($page_name = false)
    {
        $chile_cities = ChilexpressLists::cities();
        $secure_key = false;

        if ($this->context->controller->controller_type == 'admin') {
            $secure_key = $this->secure_key;
        }

        $currency = Currency::getCurrencyInstance($this->config['chile_id_currency']);

        if (version_compare(_PS_VERSION_, '1.6.0.0', '>=') === true) {
            return Media::addJsDef(array($this->name => array(
                'secure_key' => $secure_key,
                'page_name' => $page_name,
                'ps_version' => Tools::substr(_PS_VERSION_, 0, 3),
                'cities' => $chile_cities,
                'id_country' => $this->config['chile_id_country'],
                'currency_sign' => $currency->sign.' ('.$currency->iso_code.')',
                'texts' => array(
                    'wrong_city' => $this->l('City name is wrong'),
                    'no_results' => $this->l('No results found for')
                )
            )));
        // Backward compatibility for PrestaShop 1.5
        } else {
            $this->context->smarty->assign($this->name, array(
                'secure_key' => $secure_key,
                'page_name' => $page_name,
                'ps_version' => Tools::substr(_PS_VERSION_, 0, 3),
                'cities' => Tools::jsonEncode($chile_cities),
                'id_country' => $this->config['chile_id_country'],
                'currency_sign' => $currency->sign.' ('.$currency->iso_code.')',
                'texts' => Tools::jsonEncode(array(
                    'wrong_city' => $this->l('City name is wrong'),
                    'no_results' => $this->l('No results found for')
                ))
            ));

            return $this->display(__FILE__, 'views/templates/hook/backward_compatibility.tpl');
        }
    }

    /**
     * CSS & JavaScript files loaded in the BO
     */
    public function hookDisplayBackOfficeHeader()
    {
        $controller_name = $this->context->controller->controller_name;

        if (($controller_name == 'AdminModules' && Tools::getValue('configure') == $this->name) ||
            ($controller_name == 'AdminAddresses')
        ) {
            $this->context->controller->addCSS($this->_path.'views/libs/jquery.flexdatalist.css');
            $this->context->controller->addJS($this->_path.'views/libs/jquery.flexdatalist.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
            $this->context->controller->addJS($this->_path.'views/js/back.js');

            return $this->prepareCommonHook();
        }
    }

    /**
     * CSS & JavaScript files loaded on the FO
     */
    public function hookDisplayHeader()
    {
        $page_name = false;

        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=') === true) {
            $page_name = $this->context->controller->getPageName();
        } elseif (method_exists($this->context->smarty, 'getTemplateVars')) {
            $page_name = $this->context->smarty->getTemplateVars('page_name');
        }

        if ($page_name && in_array($page_name, array('authentication', 'order', 'order-opc', 'checkout', 'address'))) {
            $this->context->controller->addCSS($this->_path.'views/libs/jquery.flexdatalist.css');
            $this->context->controller->addJS($this->_path.'views/libs/jquery.flexdatalist.js');
            $this->context->controller->addJS($this->_path.'views/libs/jquery.waituntilexists.js');
            $this->context->controller->addJS($this->_path.'views/js/front.js');

            return $this->prepareCommonHook($page_name);
        }
    }

    public function hookActionObjectCarrierUpdateAfter($params)
    {
        if (!$this->deleting_from_module) {
            $carrier = Carrier::getCarrierByReference($params['object']->id_reference);
            
            if (!$carrier) {
                $services = ChilexpressServices::getByReference($params['object']->id_reference);

                if ($services) {
                    $services->delete();
                }
            }
        }
    }

    public function hookActionValidateOrder($params)
    {
        if ((isset($params['cart']) && isset($params['cart']->id) && $params['cart']->id) &&
            (isset($params['order']) && isset($params['order']->id) && $params['order']->id)
        ) {
            $carrier = new Carrier($params['order']->id_carrier);
            $cache = new ChilexpressCache((int)$params['cart']->id);
            $services = ChilexpressServices::getByReference((int)$carrier->id_reference);

            if ($cache->package && (int)$services->id_service) {
                $msg = new Message();
                $msg->message = $this->l('Chilexpress Shipment Details').":\n\n"
                    .$this->l('Service').": ".$carrier->delay[(int)$params['order']->id_lang]."\n"
                    .$this->l('Package width').": ".$cache->package['width']." ".Configuration::get('PS_DIMENSION_UNIT')."\n"
                    .$this->l('Package height').": ".$cache->package['height']." ".Configuration::get('PS_DIMENSION_UNIT')."\n"
                    .$this->l('Package depth').": ".$cache->package['depth']." ".Configuration::get('PS_DIMENSION_UNIT')."\n"
                    .$this->l('Package weight').": ".$cache->package['weight']." ".Configuration::get('PS_WEIGHT_UNIT')."\n";
                $msg->id_cart = (int)$params['cart']->id;
                $msg->id_customer = (int)$params['cart']->id_customer;
                $msg->id_order = (int)$params['order']->id;
                $msg->private = 1;
                
                return $msg->add();
            }
        }
    }
}
