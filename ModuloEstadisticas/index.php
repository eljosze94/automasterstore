<?php 
	$conexion = new MySQLi('localhost','h3hued5u4248','#Amch2018','ps16_automaster');//conexion a la base de datos
	if ($conexion) {//verificar la conexion

		/* cargar tiendas */
		$consulta = "select * from ps_seller";//diseño de la consulta
		$resultado = $conexion->query($consulta);//ejecucion de la consulta
		$conexion->set_charset("utf8");
		if ($resultado) {
			$echoSelect = "<option value='0'>Elija una tienda</option>";//inicializacion variable para rellenar las opciones de tiendas.
			while ($fila = $resultado->fetch_array()) {
				$aux = "<option value='$fila[0]'>$fila[4]</option>";
				$echoSelect = $echoSelect.$aux;
			}
		}


		/*cargar combobox categorias */
		// $consulta = "select * from ps_category_lang";
		// $resultado = $conexion->query($consulta);
		// $conexion->set_charset("utf8");
		// if ($resultado) {
		// 	$echoCategorias = "";
		// 	while ($fila = $resultado->fetch_array()) {
		// 		$aux = "<option value='$fila[0]'>$fila[3]</option>";
		// 		$echoCategorias = $echoCategorias.$aux;
		// 	}
		// }

		/*cargar cantidad de productos de acuerdo a la tienda */
		$echoCantidadProductos = "0";
		if (isset($_POST['btnTienda'])) {
			$var = $_POST['cbxTienda'];
			$consulta = "SELECT SUM(stock.quantity) FROM ps_stock_available stock INNER JOIN ps_seller_product seller ON stock.id_product = seller.id_product 			WHERE seller.id_seller_product = '$var'";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				
				while ($fila = $resultado->fetch_array()) {
					$aux = "$fila[0]";
					$echoCantidadProductos = $aux;
				}
			}

		}


		/*cargar datos de la tienda */
		$echoCantidadProductos = "0";
		$echoProductosNoComprados = "0";
		$contador = 0;
		$echoCantidadCompras = "0";
		$echoPromedioPrecio = 0 ;
		$contadorCliente = 0;
		$echoDatoClientes = "";
		$echoProdMasComprados = "";
		$echoProdMenosComprados = "";
		$cantidadComprasPorMes = "";
		$cantidadComprasPorDia = "";
		$cantidadComprasPorHora = "";
		if (isset($_POST['btnTienda'])) {
			/* cargar cantidad de productos */
			$var = $_POST['cbxTienda'];
			$consulta = "SELECT SUM(stock.quantity) FROM ps_stock_available stock INNER JOIN ps_seller_product seller ON stock.id_product = seller.id_product 			WHERE seller.id_seller_product = '$var'";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				
				while ($fila = $resultado->fetch_array()) {
					$aux = "$fila[0]";
					$echoCantidadProductos = $aux;
				}
			}


			/* cargar productos no comprados */
			$var = $_POST['cbxTienda'];
			$aux = "";
			$consulta = "	SELECT vendedor.id_product, producto.name, venta.id_order_detail, stock.quantity 
							FROM ps_product_lang producto 
							INNER JOIN ps_seller_product vendedor 
							ON producto.id_product = vendedor.id_product
                            INNER JOIN ps_stock_available stock 
                            ON stock.id_product = producto.id_product
							LEFT JOIN ps_order_detail venta
							ON producto.id_product = venta.product_id
							WHERE venta.id_order_detail IS NULL AND vendedor.id_seller_product = '$var'";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				
				while ($fila = $resultado->fetch_array()) {
					$aux .= "<tr>";
					$aux .= 	"<td>$fila[0]</td>";
					$aux .= 	"<td>$fila[1]</td>";
					$aux .= 	"<td>$fila[3]</td>";
					$aux .= "</tr>";
					$echoProductosNoComprados = $aux;
					$contador++;
				}
			}

			/* cargar cantidad productos comprados */
			$var = $_POST['cbxTienda'];
			$aux = "";
			$consulta = "	SELECT SUM(venta.product_quantity)
							FROM ps_product_lang producto 
							INNER JOIN ps_seller_product vendedor 
							ON producto.id_product = vendedor.id_product
							INNER JOIN ps_order_detail venta
							ON producto.id_product = venta.product_id	
							WHERE vendedor.id_seller_product = '$var'";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				
				while ($fila = $resultado->fetch_array()) {
					$aux = 0;
					$aux = "$fila[0]";
					if (!$aux) {
						$aux = 0;
					}
					$echoCantidadCompras = $aux;
				}
			}


			/* cargar precio promedio */
			$var = $_POST['cbxTienda'];
			$aux = "";
			$consulta = "	SELECT AVG(productos.price)
							FROM ps_product productos
							INNER JOIN ps_seller_product vendedor
							ON productos.id_product = vendedor.id_product
							WHERE vendedor.id_seller_product ='$var'";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				
				while ($fila = $resultado->fetch_array()) {
					$aux = "$fila[0]";
					// if (isset($aux)) {
					// 	$aux = "0";
					// }
					$echoPromedioPrecio = round($aux);
				}
			}



			/* cargar tabla info datos clientes del marketplace */
			$var = $_POST['cbxTienda'];
			$aux = "";
			$auxDataTableClientes = "";
			$contadorHombres = 0;
			$contadorMujeres = 0;
			$consulta = "	SELECT 
								cliente.firstname,
							    cliente.lastname,
							    cliente.email,
							    cliente.birthday,
							    direccion.address1,
							    direccion.city,
							    pais.name,
							    cliente.id_gender
							FROM ps_customer cliente
							LEFT JOIN ps_address direccion
							on cliente.id_customer = direccion.id_customer
							LEFT JOIN ps_country_lang pais
							ON pais.id_country = direccion.id_country";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				
				while ($fila = $resultado->fetch_array()) {
					$aux .= "<tr>";
					$aux .= 	"<td>$fila[0]</td>";
					$aux .= 	"<td>$fila[1]</td>";
					$aux .= 	"<td>$fila[2]</td>";
					$aux .= 	"<td>$fila[3]</td>";
					$aux .= 	"<td>$fila[4]</td>";
					$aux .= 	"<td>$fila[5]</td>";
					$aux .= 	"<td>$fila[6]</td>";
					if ($fila[7] == 1) {
						$aux .= 	"<td>hombre</td>";
						$contadorHombres = $contadorHombres + 1;
					}
					else{
						$aux .= 	"<td>mujer</td>";
						$contadorMujeres = $contadorMujeres + 1;
					}					
					$aux .= "</tr>";
					$echoDatoClientes = $aux;
					$contadorCliente++;
				}
				$auxDataTableClientes .= "['Hombres',$contadorHombres]";
				$auxDataTableClientes .= ",['Mujeres',$contadorMujeres]";
			}


			/* cargar tabla 5 productos más vendidos */
			$var = $_POST['cbxTienda'];
			$aux = "";
			$consulta = "	SELECT 
								SUM(prodComprado.quantity),
							    prodComprado.id_product,
							    producto.name,
							    prodComprado.date_add 
							FROM ps_cart_product prodComprado 
							INNER JOIN ps_product_lang producto 
							ON producto.id_product = prodComprado.id_product 
							INNER JOIN ps_seller_product vendedor 
							ON vendedor.id_product = producto.id_product 
							WHERE vendedor.id_seller_product = $var
							GROUP BY prodComprado.id_product 
							ORDER BY 1 DESC
							LIMIT 5";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				while ($fila = $resultado->fetch_array()) {
					$aux .= "<tr>";
					$aux .= 	"<td>$fila[0]</td>";
					$aux .= 	"<td>$fila[1]</td>";
					$aux .= 	"<td>$fila[2]</td>";
					$aux .= 	"<td>$fila[3]</td>";					
					$aux .= "</tr>";
					$echoProdMasComprados = $aux;
				}
			}

			/* cargar cantidad empresas y clientes */
			$var = $_POST['cbxTienda'];
			$aux = "";
			$auxEmpresaCliente = "";
			$consulta = "SELECT COUNT(*) FROM ps_customer";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				while ($fila = $resultado->fetch_array()) {
					$aux .= "['Clientes',$fila[0], '#fbbc05']";
				}
			}
			$consulta = "SELECT COUNT(*) FROM ps_seller";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				while ($fila = $resultado->fetch_array()) {
					$aux .= ",['Empresas',$fila[0], '#34a853']";
				}
			}
			$auxEmpresaCliente = $aux;




			/* cargar tabla 5 productos menos vendidos */
			$var = $_POST['cbxTienda'];
			$aux = "";
			$consulta = "	SELECT 
								SUM(prodComprado.quantity),
							    prodComprado.id_product,
							    producto.name,
							    prodComprado.date_add 
							FROM ps_cart_product prodComprado 
							INNER JOIN ps_product_lang producto 
							ON producto.id_product = prodComprado.id_product 
							INNER JOIN ps_seller_product vendedor 
							ON vendedor.id_product = producto.id_product 
							WHERE vendedor.id_seller_product = $var
							GROUP BY prodComprado.id_product 
							ORDER BY 1 ASC
							LIMIT 5";
			$resultado = $conexion->query($consulta);
			$conexion->set_charset("utf8");
			if ($resultado) {
				while ($fila = $resultado->fetch_array()) {
					$aux .= "<tr>";
					$aux .= 	"<td>$fila[0]</td>";
					$aux .= 	"<td>$fila[1]</td>";
					$aux .= 	"<td>$fila[2]</td>";
					$aux .= 	"<td>$fila[3]</td>";					
					$aux .= "</tr>";
					$echoProdMenosComprados = $aux;
				}
			}


			/* cargar cantidad ventas por mes*/
			$var = $_POST['cbxTienda'];
			$aux = "";
			$auxDataTable = "";
			$fechaTermino = "0000-00-00";
			for ($meses = 1; $meses < 13; $meses++) { 
				$i = $meses;
				if ($meses == 1) {
					$fechaInicial = date('Y-m-j');
					$fechaTermino = strtotime("-$i month", strtotime($fechaInicial));
					$fechaTermino = date('Y-m-j', $fechaTermino);
					$consulta = "	SELECT COUNT(miVenta) 
									FROM ( 
									    SELECT COUNT(compra.id_order) AS miVenta 
									    FROM ps_seller tienda 
									    INNER JOIN ps_seller_product producto 
									    ON tienda.id_seller = producto.id_seller_product 
									    INNER JOIN ps_order_detail detalleCompra 
									    ON detalleCompra.product_id = producto.id_product
									    INNER JOIN ps_orders compra 
									    ON compra.id_order = detalleCompra.id_order 
									    WHERE (tienda.id_seller = '$var') 
									    AND (compra.date_upd BETWEEN '$fechaTermino' AND '$fechaInicial') 
									    GROUP BY compra.id_order 
									) AS miResultado";
					$resultado = $conexion->query($consulta);
					$conexion->set_charset("utf8");
					if ($resultado) {
						
						while ($fila = $resultado->fetch_array()) {
							if (!$fila[0]) {
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaInicial</td>";	
								$aux .= 	"<td>0</td>";				
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTable .= "['$fechaInicial',0]";
							}
							else
							{
								$miDato = 0;
								$miDato = (int)$fila[0];
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaInicial</td>";
								$aux .= 	"<td>$fila[0]</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTable .= "['$fechaInicial', $miDato]";
							}
							$cantidadComprasPorMes = $aux;
						}
					}
				}
				else
				{
					$fechaAux = $fechaTermino;
					$fechaTermino = strtotime("-$i month", strtotime($fechaInicial));
					$fechaTermino = date('Y-m-j', $fechaTermino);
					$consulta = "	SELECT COUNT(miVenta) 
									FROM ( 
									    SELECT COUNT(compra.id_order) AS miVenta 
									    FROM ps_seller tienda 
									    INNER JOIN ps_seller_product producto 
									    ON tienda.id_seller = producto.id_seller_product 
									    INNER JOIN ps_order_detail detalleCompra 
									    ON detalleCompra.product_id = producto.id_product
									    INNER JOIN ps_orders compra 
									    ON compra.id_order = detalleCompra.id_order 
									    WHERE (tienda.id_seller = '$var') 
									    AND (compra.date_upd BETWEEN '$fechaTermino' AND '$fechaAux') 
									    GROUP BY compra.id_order 
									) AS miResultado";
					$resultado = $conexion->query($consulta);
					$conexion->set_charset("utf8");
					if ($resultado) {
						
						while ($fila = $resultado->fetch_array()) {
							if (!$aux) {
								$aux .= "<tr>";
								$aux .= 	"<td>0</td>";
								$aux .= 	"<td>0</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTable .= ",['$fechaAux',0]";
							}
							else
							{
								$miDato = 0;
								$miDato = (int)$fila[0];
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaAux</td>";
								$aux .= 	"<td>$fila[0]</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTable .= ",['$fechaAux', $miDato]";
							}
							$cantidadComprasPorMes = $aux;
						}
					}
				}

			}
			
			/* cargar cantidad ventas por dia*/
			$var = $_POST['cbxTienda'];
			$aux = "";
			$auxDataTableDia = "";
			$fechaTermino = "";
			$fechaInicial = date('m-j');
			
			for ($dias = 1; $dias < 31; $dias++) { 
				$i = $dias;
				if ($dias == 1) {
					$fechaInicial = date('y-m-j');
					$fechaTermino = strtotime("-$i day", strtotime($fechaInicial));
					$fechaTermino = date('y-m-j', $fechaTermino);
					
					$consulta = "	SELECT COUNT(miVenta) 
									FROM ( 
									    SELECT COUNT(compra.id_order) AS miVenta 
									    FROM ps_seller tienda 
									    INNER JOIN ps_seller_product producto 
									    ON tienda.id_seller = producto.id_seller_product 
									    INNER JOIN ps_order_detail detalleCompra 
									    ON detalleCompra.product_id = producto.id_product
									    INNER JOIN ps_orders compra 
									    ON compra.id_order = detalleCompra.id_order 
									    WHERE (tienda.id_seller = '$var') 
									    AND (compra.date_upd BETWEEN '$fechaTermino' AND '$fechaInicial') 
									    GROUP BY compra.id_order 
									) AS miResultado";
					$resultado = $conexion->query($consulta);
					$conexion->set_charset("utf8");
					if ($resultado) {
						
						while ($fila = $resultado->fetch_array()) {
							if (!$fila[0]) {
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaTermino</td>";	
								$aux .= 	"<td>0</td>";				
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableDia .= "['$fechaTermino',0]";
							}
							else
							{
								$miDato = 0;
								$miDato = (int)$fila[0];
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaTermino</td>";
								$aux .= 	"<td>$fila[0]</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableDia .= "['$fechaTermino', $miDato]";
							}
							$cantidadComprasPorDia = $aux;
						}
					}
				}
				else
				{
					// echo "$fechaTermino";
					$fechaAux = $fechaTermino;
					//echo "$fechaAux";
					$fechaTermino = strtotime("-$i day", strtotime($fechaInicial));
					$fechaTermino = date('y-m-j', $fechaTermino);
					$consulta = "	SELECT COUNT(miVenta) 
									FROM ( 
									    SELECT COUNT(compra.id_order) AS miVenta 
									    FROM ps_seller tienda 
									    INNER JOIN ps_seller_product producto 
									    ON tienda.id_seller = producto.id_seller_product 
									    INNER JOIN ps_order_detail detalleCompra 
									    ON detalleCompra.product_id = producto.id_product
									    INNER JOIN ps_orders compra 
									    ON compra.id_order = detalleCompra.id_order 
									    WHERE (tienda.id_seller = '$var') 
									    AND (compra.date_upd BETWEEN '$fechaTermino' AND '$fechaAux') 
									    GROUP BY compra.id_order 
									) AS miResultado";
					$resultado = $conexion->query($consulta);
					$conexion->set_charset("utf8");
					if ($resultado) {
						
						while ($fila = $resultado->fetch_array()) {
							if (!$aux) {
								$aux .= "<tr>";
								$aux .= 	"<td>0</td>";
								$aux .= 	"<td>0</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableDia .= ",['$fechaTermino',0]";
							}
							else
							{
								$miDato = 0;
								$miDato = (int)$fila[0];
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaTermino</td>";
								$aux .= 	"<td>$fila[0]</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableDia .= ",['$fechaTermino', $miDato]";
							}
							$cantidadComprasPorDia = $aux;
						}
					}
				}

			}
			//echo "$cantidadComprasPorDia";


			/* cargar cantidad ventas por hora*/
			$var = $_POST['cbxTienda'];
			$aux = "";
			$auxDataTableHora = "";
			$fechaTermino = "";
			$fechaInicial = "";
			// $fechaInicial = date('Y-m-j h:m');
			
			for ($dias = 1; $dias < 121; $dias++) { 
				$i = $dias;
				if ($dias == 1) {
					$fechaInicial = date('Y-m-j  h:m');
					$fechaTermino = strtotime("-$i hour", strtotime($fechaInicial));
					$fechaTermino = date('Y-m-j  h:m', $fechaTermino);
					$consulta = "	SELECT COUNT(miVenta) 
									FROM ( 
									    SELECT COUNT(compra.id_order) AS miVenta 
									    FROM ps_seller tienda 
									    INNER JOIN ps_seller_product producto 
									    ON tienda.id_seller = producto.id_seller_product 
									    INNER JOIN ps_order_detail detalleCompra 
									    ON detalleCompra.product_id = producto.id_product
									    INNER JOIN ps_orders compra 
									    ON compra.id_order = detalleCompra.id_order 
									    WHERE (tienda.id_seller = '$var') 
									    AND (compra.date_upd BETWEEN '$fechaTermino' AND '$fechaInicial') 
									    GROUP BY compra.id_order 
									) AS miResultado";
					$resultado = $conexion->query($consulta);
					$conexion->set_charset("utf8");
					if ($resultado) {
						
						while ($fila = $resultado->fetch_array()) {
							if (!$fila[0]) {
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaTermino</td>";	
								$aux .= 	"<td>0</td>";				
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableHora .= "['$fechaTermino',0]";
							}
							else
							{
								$miDato = 0;
								$miDato = (int)$fila[0];
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaTermino</td>";
								$aux .= 	"<td>$fila[0]</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableHora .= "['$fechaTermino', $miDato]";
							}
							$cantidadComprasPorDia = $aux;
						}
					}
				}
				else
				{
					// echo "$fechaTermino";
					$fechaAux = $fechaTermino;
					//echo "$fechaAux";
					$fechaTermino = strtotime("-$i hour", strtotime($fechaInicial));
					$fechaTermino = date('Y-m-j  h:m', $fechaTermino);
					$consulta = "	SELECT COUNT(miVenta) 
									FROM ( 
									    SELECT COUNT(compra.id_order) AS miVenta 
									    FROM ps_seller tienda 
									    INNER JOIN ps_seller_product producto 
									    ON tienda.id_seller = producto.id_seller_product 
									    INNER JOIN ps_order_detail detalleCompra 
									    ON detalleCompra.product_id = producto.id_product
									    INNER JOIN ps_orders compra 
									    ON compra.id_order = detalleCompra.id_order 
									    WHERE (tienda.id_seller = '$var') 
									    AND (compra.date_upd BETWEEN '$fechaTermino' AND '$fechaAux') 
									    GROUP BY compra.id_order 
									) AS miResultado";
					$resultado = $conexion->query($consulta);
					$conexion->set_charset("utf8");
					if ($resultado) {
						
						while ($fila = $resultado->fetch_array()) {
							if (!$aux) {
								$aux .= "<tr>";
								$aux .= 	"<td>0</td>";
								$aux .= 	"<td>0</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableHora .= ",['$fechaTermino',0]";
							}
							else
							{
								$miDato = 0;
								$miDato = (int)$fila[0];
								$aux .= "<tr>";
								$aux .= 	"<td>$fechaTermino</td>";
								$aux .= 	"<td>$fila[0]</td>";					
								$aux .= "</tr>";

								/*llenar data table con datos*/
								$auxDataTableHora .= ",['$fechaTermino', $miDato]";
							}
							$cantidadComprasPorDia = $aux;
						}
					}
				}

			}
		}

	}

 ?>

 <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Página estadisticas</title>


    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <!-- cargar cantidad de ventas por mes -->
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Cantidad de ventas');
        data.addRows([
          <?php echo "$auxDataTable"; ?>
        ]);

        // Set chart options
        var options = {'title':'Cantidad de ventas ultimos 12 meses'};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('grVentasPorMes'));
        chart.draw(data, options);
      }
    </script>

    <!-- cargar ventas por día -->
    <script type="text/javascript">
    	google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

		     	var data = new google.visualization.DataTable();
		        data.addColumn('string', 'Topping');
		        data.addColumn('number', 'Cantidad de ventas');
		        data.addRows([
		          <?php echo "$auxDataTableDia"; ?>
		        ]);
				

		      var options = {
		        title: 'Cantidad de ventas ultimos 30 días',
		        chartArea: {width: '50%',hight: '80%'},
		        hAxis: {
		          title: 'Total ventas',
		          minValue: 0
		        },
		        vAxis: {
		          title: 'fecha'
		        }
		      };

		      var chart = new google.visualization.BarChart(document.getElementById('grVentasPorDia'));

		      chart.draw(data, options);
		    }
    </script>

    <!-- cargar cantidad de ventas por hora -->
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Cantidad de ventas');
        data.addRows([
          <?php echo "$auxDataTableHora"; ?>
        ]);

        // Set chart options
 		var options = {
		        title: 'Cantidad de ventas ultimos 30 días',
		        chartArea: {width: '50%',hight: '80%'},
		        hAxis: {
		          title: 'Total ventas',
		          minValue: 0
		        },
		        vAxis: {
		          title: 'fecha'
		        }
		      };

		      var chart = new google.visualization.BarChart(document.getElementById('grVentasPorHora'));

		      chart.draw(data, options);
		    }
    </script>

    <!-- cargar grafico cantidad hombres y mujeres en el marketplace -->
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Sexo'],
          <?php echo "$auxDataTableClientes"; ?>
        ]);

        var options = {
          title: 'Porcentaje de hombres y mujeres',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('grHombreMujer'));
        chart.draw(data, options);
      }
    </script>

    <!-- cargar grafico relacion empresas clientes -->
    <script type="text/javascript">
    	google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);

		function drawBasic() {

			 var data = google.visualization.arrayToDataTable([
			   ['', 'Cantidad', { role: 'style' }],
			   <?php echo "$auxEmpresaCliente"; ?>
			 ]);
		     	// var data = new google.visualization.DataTable();
		      //   data.addColumn('string', 'Topping');
		      //   data.addColumn('number', 'N° de registros');
		      //   data.addColumn({ role: 'style' })
		      //   data.addRows([
		      //     
		      //   ]);
				

		      var options = {
		        title: 'Cantidad de empresas y clientes registrados',
		        chartArea: {width: '50%',hight: '80%'},
		        hAxis: {
		          title: 'Total',
		          minValue: 0
		        }
		      };

		      var chart = new google.visualization.BarChart(document.getElementById('grEmpresaCliente'));

		      chart.draw(data, options);
		    }
    </script>
  </head>
  <body>
    
  	<div class="container mt-4">
  		<h1 align="center">Captura Datos tienda</h1>
  		<div class="row">
  			<div class="col">
  				<h3>Elige tu tienda	</h3>
  				<form action="index.php" method="POST">
  					<select class="custom-select" id="cbxTienda" name="cbxTienda">
					  <?php 
					  	echo "$echoSelect";
					   ?>
					</select>
					<button class="btn btn-primary mt-2" type="submit" id="btnTienda" name="btnTienda">Generar Estadisticas</button>
  				</form>

  				<!-- <h3 class="mt-3">Elige tu categoría	</h3>
  				<form action="" method="">
  					<select class="custom-select">
					  <?php 
					  	// echo "$echoCategorias";
					   ?>
					</select>
  				</form> -->

  				<h3 class="mt-5">Cantidad Clientes</h3>
				<!-- Tabla Listado de subscriptores-->
			      <div class="card mb-3">
			        <div class="card-header">
			          <div class="d-flex justify-content-between">
			            <p><i class="fa fa-table"></i> Mis Subscriptores</p>
			            
			          </div>
			        </div>
			        <div class="card-body">
			          <div class="table-responsive">
			            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			              <thead>
							    <tr>
							      <th scope="col">Nombre</th>
							      <th scope="col">Apellido</th>
							      <th scope="col">Email</th>
							      <th scope="col">Fecha Nacimiento</th>
							      <th scope="col">Direccion</th>
							      <th scope="col">Ciudad</th>
							      <th scope="col">Pais</th>
							      <th scope="col">Sexo</th>
							    </tr>
							  </thead>
			              <tbody align="center"><!-- contenido arrojado por la base de datos -->
			                <?php echo "$echoDatoClientes"; ?>
							  
			              </tbody>
			            </table>
			          </div>
			        </div>
			        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
			      </div>

				<div id="grHombreMujer" style="width: 900px; height: 500px;"></div>
				<div id="grEmpresaCliente" style="width: 100%; height: 500px;"></div>

  				<h3 class="mt-5">Articulos jamas comprados</h3>
  				<table class="table mt-2">
				  <thead>
				    <tr>
				      <th scope="col">ID</th>
				      <th scope="col">Nombre</th>
				      <th scope="col">Stock</th>
				    </tr>
				  </thead>
				  <tbody>
				    <?php echo "$echoProductosNoComprados"; ?>
				    <tr>
				      <th scope="row">total</th>
				      <td> <?php echo "$contador"; ?></td>
				    </tr>
				  </tbody>
				</table>

				<h3 class="mt-5">Articulos Más Comprados</h3>
  				<table class="table mt-2">
				  <thead>
				    <tr>
				      <th scope="col">Cantidad</th>
				      <th scope="col">ID</th>
				      <th scope="col">Nombre</th>
				      <th scope="col">Fecha</th>
				    </tr>
				  </thead>
				  <tbody>
				    <?php echo "$echoProdMasComprados"; ?>
				  </tbody>
				</table>

				<h3 class="mt-5">Articulos Menos Comprados</h3>
  				<table class="table mt-2">
				  <thead>
				    <tr>
				      <th scope="col">ID</th>
				      <th scope="col">Nombre</th>
				      <th scope="col">Stock</th>
				      <th scope="col">Cantidad de visitas</th>
				    </tr>
				  </thead>
				  <tbody>
				    <?php echo "$echoProdMenosComprados"; ?>
				  </tbody>
				</table>


				<h3 class="mt-5">Tabla resumen de la tienda</h3>
  				<table class="table mb-5">
				  <tbody>
				    <tr>
				      <th scope="col">Total Productos disponibles</th>
				      <td><?php echo $echoCantidadProductos; ?></td>
				    </tr>
				    <tr>
				      <th scope="col">Productos comprados: (considerando unidades vendidas)</th>
				      <td><?php echo "$echoCantidadCompras"; ?></td>
				    </tr>
				    <tr>
				      <th scope="col">Precio promedio (precio base):</th>
				      <td><?php echo "$echoPromedioPrecio"; ?></td>
				    </tr>
				    <tr>
				      <th scope="col">Total productos nunca comprados:</th>
				      <td><?php echo "$contador"; ?></td>
				    </tr>
				  </tbody>
				</table>


				<h3 class="mt-5">Cargar Grafica ventas ultimos 12 meses</h3>
    			<div id="grVentasPorMes" class="mt-5 container" ></div>


    			<h3 class="mt-5">Cargar Cantidad de ventas por dia</h3>
    			<div id="grVentasPorDia"  style="height: 1000px"></div>

    			<h3 class="mt-5">Grafico Ventas por hora</h3>
    			<div id="grVentasPorHora" style="min-height: 1000px"></div>

  			</div>
  		</div>
  	</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </body>
</html>