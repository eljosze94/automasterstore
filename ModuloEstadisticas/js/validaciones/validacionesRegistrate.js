
    $(document).ready(function(){//funcion para mostrar barra de progreso, se carga de manera automática
    var current = 1,current_step,next_step,steps;
    steps = $("fieldset").length;
    $(".next").click(function(){
      if (validaUsuario() && current == 1) {//verifica si se cumple la validacion de los campos, en caso contrario cancela la accion
        current_step = $(this).parent();
        next_step = $(this).parent().next();
        next_step.show();
        current_step.hide();
        setProgressBar(++current);
      }
      else if (current == 2 && validaDatoEmpresa()) {//verifica si se cumple la validacion de los campos, en caso contrario cancela la accion
        current_step = $(this).parent();
        next_step = $(this).parent().next();
        next_step.show();
        current_step.hide();
        setProgressBar(++current);
      }
      else
      {
      	return false;
      }
      
    });
    $(".previous").click(function(){
      current_step = $(this).parent();
      next_step = $(this).parent().prev();
      next_step.show();
      current_step.hide();
      setProgressBar(--current);
    });
    setProgressBar(current);
    // Change progress bar action
    function setProgressBar(curStep){
      var percent = parseFloat(100 / steps) * curStep;
      percent = percent.toFixed();
      $(".progress-bar")
        .css("width",percent+"%")
        .html(percent+"%");   
    }
  });


function soloNumeros(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function soloLetras(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta Letras
    patron =/^[A-Za-z]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function validaRut()
{
    var rut = document.getElementById("rut");
    
    if(rut.value.length>8 || rut.value.length<1)
	{
		document.getElementById('merror').innerHTML = 'Rut no valido';
		document.getElementById('merrorE3').innerHTML = '*';
		rut.focus();
		return false;
	}
    
}

function validarListaUsuario()
{
    var lista = document.getElementById("tipoUsuario");
    
    if(lista.value === "0")
    {
        document.getElementById('merror').innerHTML = 'Debe seleccionar un tipo de Usuario';
        document.getElementById('merrorE5').innerHTML = '*';
        return false;
    }
}

function validarListaEvento()
{
    var lista = document.getElementById("tipoNoticia");
    
    if(lista.value === "0")
    {
        document.getElementById('merror').innerHTML = 'Debe seleccionar un opcion';
        document.getElementById('merrorE1').innerHTML = '*';
        return false;
    }
}

function validarComuna()
{
	var comuna = document.getElementById("comuna");
	
	if(comuna.value === "0")
	{
	    document.getElementById('merror').innerHTML = 'Debe seleccionar una Comuna';
		document.getElementById('merrorE2').innerHTML = '*';
        return false;
	}
}

function validarComuna2()
{
	var comuna = document.getElementById("comuna");
	
	if(comuna.value === "0")
	{
	    document.getElementById('merror').innerHTML = 'Debe seleccionar una Comuna';
		document.getElementById('merrorE3').innerHTML = '*';
        return false;
	}
}

function validaDatoEmpresa(){
	var rut = document.getElementById("rutE");
	if(rut.value == "")
	{
		alert("Rut es obligatorio");
		rut.focus();
		return false;
	}
	return true;
}
function validaUsuario()
{
	
	var nombre = document.getElementById("nombre");
	var rut = document.getElementById("rut");
	var email = document.getElementById("email");
	var clave = document.getElementById("password");
	var cargo = document.getElementById("cargo");
	var telefono = document.getElementById("telefono");
	
	if(nombre.value == "")
	{
		alert("Nombre es obligatorio");
		nombre.focus();
		return false;
	}
	
	if(nombre.value.length>30)
	{
		alert("Máximo 30 carateres");
		nombre.focus();
		return false;
	}
	
	
	if(rut.value == "")
	{
		alert("Rut es obligatorio");
		rut.focus();
		return false;
	}
	
	
	if(rut.value.length>11)
	{
		alert("Máximo 10 carateres");
		rut.focus();
		return false;
	}
	
	if(email.value == "")
	{
		alert("Email es obligatorio");
		email.focus();
		return false;
	}
	
	
	if(email.value.length>30)
	{
		alert("Máximo 30 carateres");
		email.focus();
		return false;
	}
	
	if(clave.value == "")
	{
		alert("Clave es obligatoria");
		clave.focus();
		return false;
	}
	
	
	if(clave.value.length>31)
	{
		alert("Máximo 30 carateres");
		clave.focus();
		return false;
	}
	
	if(cargo.value == "")
	{
		alert("Cargo es obligatorio");
		cargo.focus();
		return false;
	}
	
	
	if(cargo.value.length>31)
	{
		alert("Máximo 30 carateres");
		cargo.focus();
		return false;
	}
	
	if(telefono.value == "")
	{
		alert("Telefono es obligatorio");
		telefono.focus();
		return false;
	}
	
	
	
	if(telefono.value.length>9)
	{
		alert("Máximo 9 carateres");
		telefono.focus();
		return false;
	}
	
	return true;
}

function validaEstacionamiento()
{
    document.getElementById('merrorE1').innerHTML = '';
    document.getElementById('merrorE2').innerHTML = '';
    document.getElementById('merrorE3').innerHTML = '';
    document.getElementById('merrorE4').innerHTML = '';
    document.getElementById('merrorE5').innerHTML = '';
    
	var ubicacion = document.getElementById("ubicacion");
	var horarioUso1 = document.getElementById("horarioUso1");
	var horarioUso2 = document.getElementById("horarioUso2");
	var valorUso = document.getElementById("valorUso");
	var cordenada = document.getElementById("direccion");
	
	if(ubicacion.value === "")
	{
	    document.getElementById('merror').innerHTML = 'Direccion es obligatoria';
	    document.getElementById('merrorE1').innerHTML = '*';
		ubicacion.focus();
		return false;
	}
	
	if(ubicacion.value.length>30)
	{
	    document.getElementById('merror').innerHTML = 'Maximo de 30 caracteres';
	    document.getElementById('merrorE1').innerHTML = '*';
		ubicacion.focus();
		return false;
	}
	
	if(horarioUso1.value === "")
	{
	    document.getElementById('merror').innerHTML = 'Horario es obligatorio';
	    document.getElementById('merrorE3').innerHTML = '*';
		horarioUso1.focus();
		return false;
	}
	
	if(horarioUso2.value === "")
	{
		document.getElementById('merror').innerHTML = 'Horario es obligatorio';
		document.getElementById('merrorE3').innerHTML = '*';
		horarioUso2.focus();
		return false;
	}
	
	if(valorUso.value === "")
	{
	    document.getElementById('merror').innerHTML = 'Valor uso es obligatorio';
		document.getElementById('merrorE4').innerHTML = '*';
		valorUso.focus();
		return false;
	}
	
	if(valorUso.value.length>11)
	{
	    document.getElementById('merror').innerHTML = 'Maximo 10 caracteres';
	    document.getElementById('merrorE4').innerHTML = '*';
		valorUso.focus();
		return false;
	}
	
	if(valorUso.value < 0)
	{
	    document.getElementById('merror').innerHTML = 'No se adminten valores negativos';
	    document.getElementById('merrorE4').innerHTML = '*';
	    valorUso.focus();
		return false;
	}
	
	if(cordenada.value === "")
	{
	    document.getElementById('merror').innerHTML = 'Cordenana no puede quedar vacia';
	    document.getElementById('merrorE5').innerHTML = '*';
		ubicacion.focus();
		return false;
	}
	
	return validarComuna();
}

function validaPuntosInteres()
{
    document.getElementById('merrorE1').innerHTML = '*';
    document.getElementById('merrorE2').innerHTML = '*';
    document.getElementById('merrorE3').innerHTML = '*';
    document.getElementById('merrorE4').innerHTML = '*';
    document.getElementById('merrorE5').innerHTML = '*';
    
	var id = document.getElementById("id");
	var nombre = document.getElementById("nombre");
	var direccion = document.getElementById("direccion");
	var clasificacion = document.getElementById("tipoPunto");
	var horario1 = document.getElementById("horario1");
	var horario2 = document.getElementById("horario2");
	
	if(nombre.value === "")
	{
	    document.getElementById('merror').innerHTML = 'Nombre es obligatorio';
		document.getElementById('merrorE1').innerHTML = '*';
		nombre.focus();
		return false;
	}
	
	if(nombre.value.length>30)
	{
	    document.getElementById('merror').innerHTML = 'Maximo 30 caracteres';
		document.getElementById('merrorE1').innerHTML = '*';
		nombre.focus();
		return false;
	}
	
	if(direccion.value === "")
	{
	    document.getElementById('merror').innerHTML = 'Direccion es obligatoria';
		document.getElementById('merrorE2').innerHTML = '*';
		direccion.focus();
		return false;
	}
	
	if(direccion.value.length>30)
	{
	    document.getElementById('merror').innerHTML = 'Maximo 30 caracteres';
		document.getElementById('merrorE2').innerHTML = '*';
		direccion.focus();
		return false;
	}
	
	if(clasificacion.value === "0")
	{
	    document.getElementById('merror').innerHTML = 'Debe seleccionar una clasificacion';
		document.getElementById('merrorE4').innerHTML = '*';
        return false;
	}

	if(horario1.value === "")
	{
		document.getElementById('merror').innerHTML = 'Horario es obligatorio';
		document.getElementById('merrorE5').innerHTML = '*';
		horario1.focus();
		return false;
	}
	
	if(horario2.value === "")
	{
		document.getElementById('merror').innerHTML = 'Horario es obligatorio';
		document.getElementById('merrorE5').innerHTML = '*';
		horario2.focus();
		return false;
	}
	
	return validarComuna2();
}

function validaEventos()
{
    document.getElementById('merrorE1').innerHTML = '';
    document.getElementById('merrorE2').innerHTML = '';
    document.getElementById('merrorE3').innerHTML = '';
    document.getElementById('merrorE4').innerHTML = '';
    document.getElementById('merrorE5').innerHTML = '';
    document.getElementById('merrorE6').innerHTML = '';
    document.getElementById('merrorE7').innerHTML = '';
    document.getElementById('merrorE8').innerHTML = '';
    
	var id = document.getElementById("id");
	var titulo = document.getElementById("titulo");
	var detalles = document.getElementById("detalles");
	var direccion = document.getElementById("direccion");
	var comuna = document.getElementById("comuna");
	var hora = document.getElementById("hora");
	var fecha = document.getElementById("fecha");
	
	validarListaEvento();
	
	validarCategoria();
	
	if(titulo.value === "")
	{
		document.getElementById('merror').innerHTML = 'Titulo es obligatorio';
		document.getElementById('merrorE3').innerHTML = '*';
		titulo.focus();
		return false;
	}
	
	if(titulo.value.length>100)
	{
		document.getElementById('merror').innerHTML = 'Maximo 100 caracteres';
		document.getElementById('merrorE3').innerHTML = '*';
		apellido.focus();
		return false;
	}
	
	if(detalles.value === "")
	{
		document.getElementById('merror').innerHTML = 'Detalles son obligatorios';
		document.getElementById('merrorE4').innerHTML = '*';
		detalles.focus();
		return false;
	}
	
	if(detalles.value.length>400)
	{
		document.getElementById('merror').innerHTML = 'Maximo 100 caracteres';
		document.getElementById('merrorE4').innerHTML = '*';
		detalles.focus();
		return false;
	}
	
	if(direccion.value === "")
	{
		document.getElementById('merror').innerHTML = 'Direccion es obligatoria';
		document.getElementById('merrorE5').innerHTML = '*';
		direccion.focus();
		return false;
	}
	
	if(direccion.value.length>30)
	{
		document.getElementById('merror').innerHTML = 'Maximo 30 caracteres';
		document.getElementById('merrorE5').innerHTML = '*';
		direccion.focus();
		return false;
	}
	
	if(comuna.value === "0")
	{
	    document.getElementById('merror').innerHTML = 'Debe seleccionar una Comuna';
		document.getElementById('merrorE6').innerHTML = '*';
        return false;
	}
	
	if(hora.value === "")
	{
		document.getElementById('merror').innerHTML = 'Hora es obligatoria';
		document.getElementById('merrorE7').innerHTML = '*';
		hora.focus();
		return false;
	}
	
	if(hora.value.length>8)
	{
		document.getElementById('merror').innerHTML = 'Maximo 8 caracteres';
		document.getElementById('merrorE7').innerHTML = '*';
		hora.focus();
		return false;
	}
	
	if(fecha.value === "")
	{
		document.getElementById('merror').innerHTML = 'Fecha es obligatoria';
		document.getElementById('merrorE8').innerHTML = '*';
		fecha.focus();
		return false;
	}
	
	if(fecha.value.length>10)
	{
		document.getElementById('merror').innerHTML = 'Maximo 10 caracteres';
		document.getElementById('merrorE8').innerHTML = '*';
		fecha.focus();
		return false;
	}
}

function validaLogin()
{
    var rut = document.getElementById("rut");
	var clave = document.getElementById("clave");
	var tipoUsuario = document.getElementById("tipoUsuario");
	
	if(rut.value === "")
	{
		alert("Rut es obligatorio");
		rut1.focus();
		return false;
	}
	
	if(rut.value.length>8 || rut.value.length<7)
	{
		alert("Extension rut minimo 7 caracteres y maximo 8");
		rut1.focus();
		return false;
	}
	
	if(clave.value === "")
	{
		alert("Clave es obligatoria");
		clave.focus();
		return false;
	}
	
	if(clave.value.length>30 || clave.value.length<5)
	{
		alert("Extension clave minima 5 caracteres y maximo 30");
		clave.focus();
		return false;
	}

}

