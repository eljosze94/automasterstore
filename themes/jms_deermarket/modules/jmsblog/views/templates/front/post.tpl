{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{$post.title|escape:'html':'UTF-8'}{/capture}
<div class="single-blog">
	<div class="blog-post">
		{assign var=catparams value=['category_id' => $post.category_id, 'slug' => $post.category_alias]}	
		{if $post.link_video && $jmsblog_setting.JMSBLOG_SHOW_MEDIA}
			<div class="post-video">
				{$post.link_video}
			</div>
		
		{/if}
		<h1 class="title">{$post.title|escape:'html':'UTF-8'}</h1>
		<ul class="post-meta">
			{if $jmsblog_setting.JMSBLOG_SHOW_CATEGORY}
				<li>
					<span>
						{l s='En:' mod='jmsblog'} 
						<a href="{jmsblog::getPageLink('jmsblog-category', $catparams)}">
							{$post.category_name|escape:'html':'UTF-8'}
						</a>
					</span>
				</li>
			{/if}
			<li>
				<span>{$post.created|escape:'html':'UTF-8'|date_format:"%B %e, %Y"}</span>
			</li>
			{if $jmsblog_setting.JMSBLOG_SHOW_COMMENTS}
				<li>
					<span>{$comments|@count}{l s=' Comentarios' mod='jmsblog'}</span>
				</li>
			{/if}
			{if $jmsblog_setting.JMSBLOG_SHOW_VIEWS}
				<li>
					<span>{$post.views|escape:'html':'UTF-8'} {l s='Visitas' mod='jmsblog'}</span>
				</li>
			{/if}
		</ul>
		<div class="post-fulltext">
			{$post.fulltext}	
		</div>
	</div>
	{if $jmsblog_setting.JMSBLOG_SHOW_SOCIAL_SHARING == 1}
		<div class="social-sharing">
		{literal}
		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "a6f949b3-864b-44c5-b0ec-4140186ad958", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		{/literal}
		<span class='st_sharethis_large' displayText='ShareThis'></span>

		{if $jmsblog_setting.JMSBLOG_SHOW_FACEBOOK}
			<span class='st_facebook_large' displayText='Facebook'></span>
		{/if}
		
		{if $jmsblog_setting.JMSBLOG_SHOW_TWITTER}
			<span class='st_twitter_large' displayText='Tweet'></span>
		{/if}
		
		{if $jmsblog_setting.JMSBLOG_SHOW_GOOGLEPLUS}
			<span class='st_googleplus_large' displayText='Google +'></span>
		{/if}
		
		{if $jmsblog_setting.JMSBLOG_SHOW_LINKEDIN}
			<span class='st_linkedin_large' displayText='LinkedIn'></span>
		{/if}
		
		{if $jmsblog_setting.JMSBLOG_SHOW_PINTEREST}
			<span class='st_pinterest_large' displayText='Pinterest'></span>
		{/if}
		
		{if $jmsblog_setting.JMSBLOG_SHOW_EMAIL}
			<span class='st_email_large' displayText='Email'></span>
		{/if}
		</div>
	{/if}

	{if $jmsblog_setting.JMSBLOG_COMMENT_ENABLE}	
		<div id="comments">
			{if $jmsblog_setting.JMSBLOG_FACEBOOK_COMMENT == 0}
				{if $msg == 1}
				<div class="success">
					{l s='Your comment submited' mod='jmsblog'} ! {if $jmsblog_setting.JMSBLOG_AUTO_APPROVE_COMMENT == 0} {l s='Please waiting approve from Admin' mod='jmsblog'}.{/if}
				</div>
				{/if}
				{if $cerrors|@count gt 0}
					<ul>
					{foreach from=$cerrors item=cerror}
						<li class="error">{$cerror}</li>
					{/foreach}	
					</ul>
				{/if}
				
				{if $comments}
					<ol class="comment-list ">
						<h1 class="page-heading">{l s='Comentarios' mod='jmsblog'}</h1>
						{foreach from=$comments item=comment key = k}
						<li>
							<article>
							<div class="left-content">
								<div class="user-image">
									<img class="attachment-widget wp-post-image img-responsive" src="{$image_baseurl|escape:'html':'UTF-8'}user.png" />
								</div>
							</div>
								<div class="comment-info">
									<a class="comment-date" href="#comments">
										<time>{$comment.time_add|date_format:"%e %B %Y"}</time>
									</a>
									<p class="customer-name">
										<a href="{$comment.customer_site|escape:'html':'UTF-8'}" class="url">{$comment.customer_name|escape:'html':'UTF-8'}</a>
									</p>
									<p class="comement-text">{$comment.comment|escape:'html':'UTF-8'}</p>
								</div>
							</article>
						</li>
						{/foreach}
					</ol>
				{/if}
				
				{if $jmsblog_setting.JMSBLOG_ALLOW_GUEST_COMMENT || (!$jmsblog_setting.JMSBLOG_ALLOW_GUEST_COMMENT && $logged)}	
				<div class="commentForm">
					<form id="commentForm" enctype="multipart/form-data" method="post" action="index.php?fc=module&module=jmsblog&controller=post&post_id={$post.post_id|escape:'html':'UTF-8'}&action=submitComment">
						<div class="row">
							<div class="col-sm-12">
								<h1 class="page-heading">{l s='Deja un cometario' mod='jmsblog'}</h1>
								<p class="h-info">{l s='Tu dirección de email no será publicada' mod='jmsblog'}.</p>
							</div>
						</div>	
						
							
								<div class="form-group">
									<label for="comment_name">{l s='Nombre' mod='jmsblog'}<sup class="required">*</sup></label>
									<input id="customer_name" class="form-control" name="customer_name" type="text" value="{$customer.firstname} {$customer.lastname}" required />
								</div>	
							
							
								<div class="form-group">
									<label for="comment_title">{l s='Email' mod='jmsblog'}<sup class="required">*</sup></label>
									<input id="comment_title" class="form-control" name="email" type="text" value="{$customer.email}" required />
								</div>
							
						
						<div class="form-group">
							<label for="comment_title">{l s='Website' mod='jmsblog'}</label>
							<input id="customer_site" class="form-control" name="customer_site" type="text" value=""/>
						</div>
						<div class="form-group">
							<label for="content">{l s='Comentario' mod='jmsblog'}<sup class="required">*</sup></label>
							<textarea id="comment" class="form-control" name="comment" rows="2" required></textarea>
						</div>
						<div id="new_comment_form_footer">
							<input id="item_id_comment_send" name="post_id" type="hidden" value="{$post.post_id|escape:'html':'UTF-8'}" />
							<input id="item_id_comment_reply" name="post_id_comment_reply" type="hidden" value="" />
							<p class="">
								<button id="submitComment" class="btn btn-default" name="submitComment" type="submit">{l s='Comentario' mod='jmsblog'}</button>
							</p>
						</div>
					</form>
					<script>
					$("#commentForm").validate({
					  rules: {		
						customer_name: "required",		
						email: {
						  required: true,
						  email: true
						}
					  }
					});
					</script>
				</div>
				{/if}
				{if !$jmsblog_setting.JMSBLOG_ALLOW_GUEST_COMMENT && !$logged}
					{l s='Porfavor inicia sesión para comentar' mod='jmsblog'}
				{/if}
			{else}
				{include file="{$module_dir}comment_facebook.tpl"}		
			{/if}
		</div>
	{/if}

</div>