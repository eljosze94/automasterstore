{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block user information module HEADER -->
	<div class="btn-group compact-hidden user-info">
		{if $logged}
			<a class="btn-xs dropdown-toggle login account" data-toggle="dropdown" href="{$link->getPageLink('my-account', true)}" title="{l s='View my customer account' mod='blockuserinfo'}" rel="nofollow">
					<span class="text">
						{$cookie->customer_firstname}
<i aria-hidden="true" class="fa fa-angle-down"></i>
					</span>
				</a>
				<ul role="menu" class="dropdown-menu">
					<li>
						<a href="{$link->getPageLink('my-account', true)}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow">
							{l s='Mi Cuenta' mod='blockuserinfo'}
						</a>
					</li>
					<li>
						<a href="{$link->getPageLink('compare', true)}" title="{l s='Compare page' mod='blockuserinfo'}" rel="nofollow">
							{l s='Comparación' mod='blockuserinfo'}
						</a>
					</li>
					<li>
						<a href="{$link->getPageLink('order', true)}" title="{l s='View my customer account' mod='blockuserinfo'}" rel="nofollow">
							{l s='Pagar' mod='blockuserinfo'}
						</a>
					</li>
					{if $logged}
						<li>
							<a href="{$link->getPageLink('index', true, NULL, "mylogout")}" title="{l s='Log me out' mod='blockuserinfo'}" rel="nofollow">
								{l s='Salir' mod='blockuserinfo'}
							</a>
						</li>
					{/if}
				</ul>
		{else}
			<a title="Login & Register" href="{$link->getPageLink('my-account', true)}" rel="nofollow" class="btn-xs">
				<span class="text">
					{l s='Iniciar Sesión' mod='blockuserinfo'}
					
				</span>
			</a>
		{/if}
	</div>
