{*
 * @package Jms Vertical Megamenu
 * @version 1.0
 * @Copyright (C) 2009 - 2015 Joommasters.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @Website: http://www.joommasters.com
*}
<a class="ver-btn dropdown-toggle" id="ver-btn1" data-toggle="dropdown">
	<i class="fa fa-bars"></i>
	{l s='Categorías' mod='jmsvermegamenu'}
	<i class="fa fa-caret-down" aria-hidden="true"></i>
</a> 
<div id="mobile-vermegamenu" class="ver-menu-mobile dropdown-menu" role="menu">
	{$vermenu_html|escape:''}
</div>