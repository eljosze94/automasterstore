{*
 * @package Jms Drop Megamenu
 * @version 1.0
 * @Copyright (C) 2009 - 2015 Joommasters.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @Website: http://www.joommasters.com
*}
<a class="ver-btn dropdown-toggle" id="ver-btn" data-toggle="dropdown">
	<i class="fa fa-bars"></i>
	{l s='categorías' mod='jmsvermegamenu'}
	<i class="fa fa-caret-down" aria-hidden="true"></i>
</a> 
<div class="ver-menu-box dropdown-menu" role="menu">
	{$vermenu_html|escape:''}
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
    	jQuery('.jms-vermegamenu').jmsVerMegaMenu({    			
    		event: '{$JMSVMM_MOUSEEVENT}',
    		duration:'{$JMSVMM_DURATION}'
    	});		
	});	
</script>