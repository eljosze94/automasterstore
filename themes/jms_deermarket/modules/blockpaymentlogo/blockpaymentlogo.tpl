{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<a href="{$link->getCMSLink($cms_payement_logo)|escape:'html'}">
<ul class="payment-logo">
		<li>
			<img src="{$img_dir}pay1.png" alt="Visa"/>
		</li>
		<li>
			<img src="{$img_dir}pay2.png" alt="Mastercard" />
		</li>
		<li>
			<img src="{$img_dir}pay3.png" alt="Paypal" />
		</li>
		<li>
			<img src="{$img_dir}pay4.png" alt="Maestro" />
		</li>
		<li>
			<img src="{$img_dir}pay5.png" alt="Maestro" />
		</li>
		<li>
			<img src="{$img_dir}pay6.png" alt="Maestro" />
		</li>
</ul>
</a>