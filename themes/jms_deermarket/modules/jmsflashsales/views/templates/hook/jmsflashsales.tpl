{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $jpb_homepage == 1}
<script type="text/javascript">
	jQuery(function ($) {
	    "use strict";
		var flashsalesCarousel = $(".flashsales-carousel");		
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		flashsalesCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:3
				},
				991:{
					items:2
				},
				361:{
					items:2
				},
				320:{
					items:1
				}
			},
			rtl: rtl,
			margin: 0,
		    nav: true,
		    dots: false,
			autoplay:false,
		    navigationText: ["", ""],
		    slideSpeed: 200	
		});
	});
</script>
<div class="addon-title">
	<div class="flex-box">
		<h3>
			{l s="Week's Hot Deals" mod="jmsflashsale"}
		</h3>
		<div class="desc-text">
			{l s="Unbox new offers every single week" mod="jmsflashsale"}
		</div>
		<div class="flashsales-countdown">
			{$expiretime|escape:'htmlall':'UTF-8'}
		</div>
		<a href="#" title="view all product" class="btn-effect">{l s="View all" mod="jmsflashsale"}</a>
	</div>
</div>
{elseif $jpb_homepage == 2}
<script type="text/javascript">
	jQuery(function ($) {
	    "use strict";
		var flashsalesCarousel = $(".flashsales-carousel");		
		var rtl = false;
		if ($("body").hasClass("rtl")) rtl = true;				
		flashsalesCarousel.owlCarousel({
			responsiveClass:true,
			responsive:{			
				1199:{
					items:4
				},
				991:{
					items:3
				},
				768:{
					items:3
				},
				360:{
					items:2
				},
				0:{
					items:1
				}
			},
			rtl: rtl,
			margin: 0,
		    nav: true,
		    dots: false,
			autoplay:false,
		    navigationText: ["", ""],
		    slideSpeed: 200	
		});
	});
</script>
<div class="addon-title">
	<div class="flex-box">
		<h3>
			{l s="Spaecial Deals" mod="jmsflashsale"}
		</h3>
		<div class="flashsales-countdown">
			{$expiretime|escape:'htmlall':'UTF-8'}
		</div>
	</div>
</div>
{elseif $jpb_homepage == 4}
	<script type="text/javascript">
		jQuery(function ($) {
		    "use strict";
			var flashsalesCarousel = $(".flashsales-carousel");		
			var rtl = false;
			if ($("body").hasClass("rtl")) rtl = true;				
			flashsalesCarousel.owlCarousel({
				responsiveClass:true,
				responsive:{			
					1199:{
						items:1
					},
					991:{
						items:1
					},
					768:{
						items:1
					},
					318:{
						items:1
					}
				},
				rtl: rtl,
				margin: 0,
			    nav: true,
			    dots: false,
				autoplay:false,
			    navigationText: ["", ""],
			    slideSpeed: 200	
			});
		});
	</script>
	<div class="addon-title">
		<div class="flex-box">
			<h3>
				{l s="Hot Deals" mod="jmsflashsale"}
			</h3>
			<div class="flashsales-countdown">
				{$expiretime|escape:'htmlall':'UTF-8'}
			</div>
			<a href="#" class="btn-view-all btn-hover">{l s="View All" mod="jmsflashsale"}</a>
		</div>
	</div>
{/if}

<div class="jmsflashsales">
	{if $jpb_homepage==1}<div class="row">{/if}
	<div class="flashsales-carousel item-hover">	
	{foreach from=$products item=product key=k}	
	{if $jpb_homepage==4}
		<div class="product-box" itemscope itemtype="http://schema.org/Product">
		<div class="preview product-colors" data-id-product="{$product.id_product}">
			<a href="{$product.link|escape:'html':'UTF-8'}" class="product-image {if $jpb_phover == 'image_swap'}image_swap{else}image_blur{/if}" data-id-product="{$product.id_product}" itemprop="url">
				<img class="img-responsive product-img1" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:html:'UTF-8'}" itemprop="image" />
			</a>
			{if isset($product.new) && $product.new == 1}
					<span class="label label-new">
						{l s='New' mod='jmsflashsale'}
					</span>
			{/if}
			{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
					<span class="label label-sale">
						{l s='Sale' mod='jmsflashsale'}
					</span>
			{/if}
			{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
		</div>
		<div class="product-info">
			<div class="product-cat">{$product.category}</div>
			<a href="{$product.link|escape:'html'}" itemprop="url" class="product-name">{$product.name|escape:'html':'UTF-8'}</a>
			<div class="content_price clearfix" itemscope itemtype="http://schema.org/Offer">
				{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
					{hook h="displayProductPriceBlock" product=$product type="old_price"}
						<span class="price old">
							{displayWtPrice p=$product.price_without_reduction}
						</span>
				{/if}
				{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
					<span class="price new" itemprop="price">
						{if !$priceDisplay}
							{convertPrice price=$product.price}
						{else}
							{convertPrice price=$product.price_tax_exc}
						{/if}
					</span>
				{/if}
				<meta itemprop="priceCurrency" content="{$currency->iso_code|escape:'htmlall':'UTF-8'}" />
			</div>
			{if ($product.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
				{if ($product.quantity > 0 OR $product.allow_oosp)}
					<a class="product-btn cart-button ajax_add_to_cart_button btn-effect" data-id-product="{$product.id_product}" href="{$link->getPageLink('cart')|escape:'html':'UTF-8'}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" title="{l s='Add to cart' mod='jmsproductfilter'}">
							<span class="icon-basket"></span>
							<span class="text">{l s='Add To Cart' mod='jmsflashsale'}</span>
							<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
							<span class="fa fa-check" aria-hidden="true"></span>
					</a>							
				{else}
					<a href="#" class="product-btn cart-button btn-default ajax_add_to_cart_button disable btn-effect" title="{l s='Out of Stock' mod='jmsflashsale'}">
						<span class="icon-basket"></span>
						<span class="text">{l s='Add To Cart' mod='jmsflashsale'}</span>
						<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
						<span class="fa fa-check" aria-hidden="true"></span>
					</a>
				{/if}									
			{/if}
		</div>
	</div>
	{else}
	<div class="product-box" itemscope itemtype="http://schema.org/Product">
		<div class="preview product-colors" data-id-product="{$product.id_product}">
			<a href="{$product.link|escape:'html':'UTF-8'}" class="product-image {if $jpb_phover == 'image_swap'}image_swap{else}image_blur{/if}" data-id-product="{$product.id_product}" itemprop="url">
				<img class="img-responsive product-img1" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:html:'UTF-8'}" itemprop="image" />
			</a>
			{if isset($product.new) && $product.new == 1}
					<span class="label label-new">
						{l s='New' mod='jmsflashsale'}
					</span>
			{/if}
			{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
					<span class="label label-sale">
						{l s='Sale' mod='jmsflashsale'}
					</span>
			{/if}
			{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
			{if ($product.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
				{if ($product.quantity > 0 OR $product.allow_oosp)}
					<a class="product-btn cart-button ajax_add_to_cart_button" data-id-product="{$product.id_product}" href="{$link->getPageLink('cart')|escape:'html':'UTF-8'}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" title="{l s='Add to cart' mod='jmsproductfilter'}">
							<span class="icon-basket"></span>
							<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
							<span class="fa fa-check" aria-hidden="true"></span>
					</a>							
				{else}
					<a href="#" class="product-btn cart-button btn-default ajax_add_to_cart_button disable" title="{l s='Out of Stock' mod='jmsflashsale'}">
						<span class="icon-basket"></span>
						<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
						<span class="fa fa-check" aria-hidden="true"></span>
					</a>
				{/if}									
			{/if}
		</div>
		<div class="product-info">
			<div class="product-cat">
				{assign var='categoryLink' value=$link->getcategoryLink({$product.id_category_default}, $product.category)}
				<a href="{$categoryLink}" title="{$product.category_default}">{$product.category}</a>
			</div>
			<a href="{$product.link|escape:'html'}" itemprop="url" class="product-name">{$product.name|escape:'html':'UTF-8'}</a>
			<div class="content_price clearfix" itemscope itemtype="http://schema.org/Offer">
				{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
					{hook h="displayProductPriceBlock" product=$product type="old_price"}
						<span class="price old">
							{displayWtPrice p=$product.price_without_reduction}
						</span>
				{/if}
				{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
					<span class="price new" itemprop="price">
						{if !$priceDisplay}
							{convertPrice price=$product.price}
						{else}
							{convertPrice price=$product.price_tax_exc}
						{/if}
					</span>
				{/if}
				<meta itemprop="priceCurrency" content="{$currency->iso_code|escape:'htmlall':'UTF-8'}" />
			</div>
		</div>
		<div class="action-btn">
			<div class="box">
				<a class="add-to-bookmark addToWishlist product-btn" href="#" onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|escape:'html'}', false, 1); return false;" data-id-product="{$product.id_product|escape:'html'}" title="{l s='Add to Wishlist' mod='jmsflashsale'}">
					<i class="icon-heart"></i>
					{l s='Wishlist' mod='jmsflashsale'}
				</a>
				<a href="#" data-link="{$product.link|escape:'html':'UTF-8'}" class="quick-view product-btn hidden-xs ">
					<i class="icon-eye"></i>
					{l s='Quick view' mod='jmsflashsale'}
				</a>
			</div>
		</div>
	</div>
	{/if}			
	
	{/foreach}
	</div>
	{if $jpb_homepage==1}</div>{/if}
</div>