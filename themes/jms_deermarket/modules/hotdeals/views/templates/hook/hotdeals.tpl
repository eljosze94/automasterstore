{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="jms-hotdeal">
	<div class="hotdeal-carousel11">
			{foreach from=$products item=product key=k name=hotdeals}
			<div class="item">
				<div class="product-box clearfix">
					<div class="preview"> 
						<a href="{$product.link|escape:'html'}" class="preview-image product_img_link"><img class="img-responsive " src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="" /></a>
						<div class="countdown" id="countdown-{$hotdeals[$k].id_hotdeals|escape:'html'}">{$hotdeals[$k].deals_time|escape:'html'}</div>
					</div>
					<div class="product-info">	
						{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
						{if $smarty.capture.displayProductListReviews}
							<div class="hook-reviews">
							{hook h='displayProductListReviews' product=$product}
							</div>
						{/if}
						<a href="{$product.link|escape:'html'}">{$product.name|truncate:25:'...'|escape:'html':'UTF-8'}</a>
						<div class="content_price clearfix" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
								<span class="price new" itemprop="price">
									{if !$priceDisplay}
										{convertPrice price=$product.price}
									{else}
										{convertPrice price=$product.price_tax_exc}
									{/if}
								</span>
							{/if}
							{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
								{hook h="displayProductPriceBlock" product=$product type="old_price"}
								<span class="price old">
									{displayWtPrice p=$product.price_without_reduction}
								</span>
							{/if}
						</div>
						<span class="description_short">
							{displayWtPrice p=$product.description_short|truncate:120:'...'}
						</span>	
						
					</div>
				</div>
			</div>
			{/foreach}
	</div>
	{if ($viewsAll == 1)} 
		<div class="view_all">
			<h6 class="view"><a href="{$link->getModuleLink('hotdeals','allproduct')}">
				{l s='View all product sale' mod='hotdeals'}
			</a></h6>
		</div>
	{/if}
</div>