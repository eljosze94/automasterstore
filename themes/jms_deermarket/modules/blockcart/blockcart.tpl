{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- MODULE Block cart -->
<div class="btn-group compact-hidden cart_block" id="cart_block">
			<a href="#" class="dropdown-toggle cart-icon" data-toggle="dropdown">
				<i class="fa fa-shopping-cart" aria-hidden="true"></i>
				<span class="ajax_cart_quantity">{$cart_qties}</span>
			</a>
			<span class="cart_block_total ajax_block_cart_total">{$total}</span>
	<div class="dropdown-menu shoppingcart-box" role="menu">
        <span class="ajax_cart_no_product" {if $cart_qties != 0}style="display:none"{/if}>{l s='No hay productos' mod='blockcart'}</span>
        {if !$PS_CATALOG_MODE}
			<ul class="list products cart_block_list" id="cart_block_list">
			{if $products}
			{foreach from=$products item='product' name='myLoop'}
				{assign var='productId' value=$product.id_product}
				{assign var='productAttributeId' value=$product.id_product_attribute}
				<li id="cart_block_product_{$product.id_product}_{if $product.id_product_attribute}{$product.id_product_attribute}{else}0{/if}_{if $product.id_address_delivery}{$product.id_address_delivery}{else}0{/if}" class="{if $smarty.foreach.myLoop.first}first_item item{elseif $smarty.foreach.myLoop.last}last_item item{else}item{/if}">
					<div class="cart-wrap clearfix">
						<a class="preview-image" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}"><img alt="{$product.name|escape:'html':'UTF-8'}" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')}" class="preview"></a>
						<div class="description"> 
							<a href="{$link->getProductLink($product, $product.link_rewrite, $product.category, null, null, $product.id_shop, $product.id_product_attribute)|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a>
							<div class="price-quantity">
								<span class="price">{if $priceDisplay == $smarty.const.PS_TAX_EXC}{displayWtPrice p="`$product.total`"}{else}{displayWtPrice p="`$product.total_wt`"}{/if}</span>
							</div>
							<div class="quantity-formated">{l s='x' mod='blockcart'} <span class="quantity">{$product.cart_quantity}</span></div>
						</div>
						<span class="remove_link">
							{if !isset($customizedDatas.$productId.$productAttributeId) && (!isset($product.is_gift) || !$product.is_gift)}
								<a class="ajax_cart_block_remove_link" href="{$link->getPageLink('cart', true, NULL, "delete=1&id_product={$product.id_product|intval}&ipa={$product.id_product_attribute|intval}&id_address_delivery={$product.id_address_delivery|intval}&token={$static_token}")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='remove this product from my cart' mod='blockcart'}">
									<span class="fa fa-times"></span>
								</a>
							{/if}
						</span>
					</div>
				</li>
			{/foreach}
			{/if}	
			</ul>
			<div class="checkout-info">
				<!--<div class="shipping clearfix">
					<span class="label">{l s='Shipping' mod='blockcart'}:</span>
					<span id="cart_block_shipping_cost" class="ajax_cart_shipping_cost pull-right">{$shipping_cost}</span>
				</div>-->
				<div class="total clearfix">
					<span class="label">{l s='Total' mod='blockcart'}:</span>
					<span class="cart_block_total ajax_block_cart_total pull-right">{$total}</span>
				</div>
				<div class="cart-button">
					<a id="button_order_cart" class="btn-effect" href="{$link->getPageLink("$order_process", true)|escape:"html":"UTF-8"}" title="{l s='Check out' mod='blockcart'}" rel="nofollow">
						{l s='Check out' mod='blockcart'}
					</a> 
				</div>
			</div>
		{/if}
	</div>
</div>
{strip}
{addJsDef CUSTOMIZE_TEXTFIELD=$CUSTOMIZE_TEXTFIELD}
{addJsDef img_dir=$img_dir|escape:'quotes':'UTF-8'}
{addJsDef generated_date=$smarty.now|intval}
{addJsDef ajax_allowed=$ajax_allowed|boolval}
{addJsDef hasDeliveryAddress=(isset($cart->id_address_delivery) && $cart->id_address_delivery)}

{addJsDefL name=customizationIdMessage}{l s='Customization #' mod='blockcart' js=1}{/addJsDefL}
{addJsDefL name=removingLinkText}{l s='remove this product from my cart' mod='blockcart' js=1}{/addJsDefL}
{addJsDefL name=freeShippingTranslation}{l s='Free shipping!' mod='blockcart' js=1}{/addJsDefL}
{addJsDefL name=freeProductTranslation}{l s='Free!' mod='blockcart' js=1}{/addJsDefL}
{addJsDefL name=delete_txt}{l s='Delete' mod='blockcart' js=1}{/addJsDefL}
{addJsDefL name=toBeDetermined}{l s='To be determined' mod='blockcart' js=1}{/addJsDefL}
{/strip}
<!-- /MODULE Block cart -->
