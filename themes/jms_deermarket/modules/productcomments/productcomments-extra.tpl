 {*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script type="text/javascript">
$(function(){
	$('a[href=#idTab5]').click(function(){
		$('*[id^="idTab"]').addClass('block_hidden_only_for_screen');
		$('div#idTab5').removeClass('block_hidden_only_for_screen');

		$('ul#more_info_tabs a[href^="#idTab"]').removeClass('selected');
		$('a[href="#idTab5"]').addClass('selected');
	});
});
</script>
{if ((($nbComments >= 0 && ($is_logged || $allow_guests)) || ($nbComments >= 0)))}
<p class="rating">	
	<span class="nbcomments">
		{l s='Este producto tiene %s Preguntas.'|sprintf:$nbComments mod='productcomments'}
	</span>	
	{if (($is_logged OR $allow_guests))}
	<button id="new_comment_tab_btn" class="open-comment-form btn-danger btn-xs" href="#new_comment_form">
		{l s=' ¿Tienes alguna pregunta?' mod='productcomments'}
	</button>
	{/if}
</p>	
{/if}
<!--  /Module ProductComments -->