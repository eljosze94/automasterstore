{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block Newsletter module-->
	<div class="addon-title">
		<h3>
			{l s='REGISTRATE PARA NUESTRO NEWSLETTER' mod='blocknewsletter'}
		</h3>
		<div class="desc-text">
			{l s='Suscríbete a nuestro newsletter, recibe notificaciones sobre nuevas actualizaciones.' mod='blocknewsletter'}
		</div>
	</div>
	<div id="newsletter_block_left" class="block newsletter_block_left">	
				<div class="block_content">
					<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
						<div class="form-group{if isset($msg) && $msg } {if $nw_error}form-error{else}form-ok{/if}{/if}" >
							<input class="inputNew form-control grey newsletter-input" type="text" name="email" size="18" value="{if isset($msg) && $msg}{$msg}{elseif isset($value) && $value}{$value}{else}{l s='Escriba su correo electronico' mod='blocknewsletter'}{/if}" />
							<button type="submit" name="submitNewsletter" class="btn-effect">
								{l s='Suscribirse' mod='blocknewsletter'}
							</button>
							<input type="hidden" name="action" value="0" />
						</div>
					</form>
					<div class="newsletter_alert"></div>
				</div>
				{hook h="displayBlockNewsletterBottom" from='blocknewsletter'}	
	</div>
<!-- /Block Newsletter module-->
{strip}
{if isset($msg) && $msg}
{addJsDef msg_newsl=$msg|@addcslashes:'\''}
{/if}
{if isset($nw_error)}
{addJsDef nw_error=$nw_error}
{/if}
{addJsDefL name=placeholder_blocknewsletter}{l s='Escriba su correo electronico' mod='blocknewsletter' js=1}{/addJsDefL}
{if isset($msg) && $msg}
	{addJsDefL name=alert_blocknewsletter}{l s='Newsletter : %1$s' sprintf=$msg js=1 mod="blocknewsletter"}{/addJsDefL}
{/if}
{/strip}