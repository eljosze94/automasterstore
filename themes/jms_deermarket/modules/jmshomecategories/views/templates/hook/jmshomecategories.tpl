<!-- JMS MODULE Home categories -->
<div class="home_categories">
    {if isset($categories) AND $categories}
            <div class="categories-carousel">
            {foreach from=$categories item=category name=homeCategories}
                {assign var='categoryLink' value=$link->getcategoryLink($category.id_category, $category.link_rewrite)}
					<div class="categories-wrapper">
						<img src="{$img_cat_dir}{$category.id_category}_thumb.jpg" alt="{$category.name}" title="{$category.name}" class="img-responsive"/>
						<div class="flex-wrapper">
							<div class="category-info">
								<span class="cat-name">{$category.name}</span>
								<span class="product-count">{$category.product_count}{l s="Products" mod="jmshomecategories"}</span>
								<a href="{$categoryLink}" class="btn-shop-now btn-hover">Shop Now</a>
							</div>
						</div>
					</div>
            {/foreach}
            </div>
    {else}
        <p>{l s='No categories' mod='homecategories'}</p>
  {/if}
</div>
<!-- /JMS MODULE Home categories -->