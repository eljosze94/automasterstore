{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script type="text/javascript">
jQuery(function ($) {
    "use strict";
    var instagramCarousel = $(".instagram-images");
    var rtl = false;
    if ($("body").hasClass("rtl")) rtl = true;

     instagramCarousel.owlCarousel({
        responsiveClass:true,
        responsive:{            
            1199:{
                items:5
            },
            991:{
                items:3
            },
            768:{
                items:3
            },
            480:{
                items:2
            },
            0:{
                items:1
            }
        },
        rtl: rtl,
        margin: 0,
        nav: false,
        dots: false,
        autoplay: false,
        slideSpeed: 800,
		loop:true
    });
});
</script>
<div class="instagram-images">
    {foreach from=$insta item=insta_item}
        <a href="{$insta_item.link}" target="_blank" class="insta-item">
            <img src=" {$insta_item.images.standard_resolution.url}">
            <div class="flex-box">
                <div class="insta-box">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                    <div class="text">
                        {l s='FOLLOW ON INSTAGRAM' mod='jmspagebuilder'}
                    </div> 
                </div>
            </div>
        </a>
    {/foreach}
</div>
