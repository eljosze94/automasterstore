{if $addon_title}
    <div class="addon-title">
        <h3>{$addon_title|escape:'htmlall':'UTF-8'}</h3>
    </div>
{/if}
    {if isset($categories) AND $categories}
    <div class="cat-box">
        <ul class="categories-list">
            {foreach from=$categories item=category key=k}
                {assign var='categoryLink' value=$link->getcategoryLink($category.id_category, $category.link_rewrite)}
                    <li>
                        <a class="cat-name" href="{$categoryLink}">{$category.name}</a>
                    </li>
            {/foreach}
        </ul>
        {if $id_view_all}
            {assign var='viewAllLink' value=$link->getcategoryLink($id_view_all, '#')}
            <a class="view-all-btn" href="{$viewAllLink}">
            {l s='VER TODO' mod='jmspagebuilder'}
            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
        </a>
        {/if}
    </div>
    {else}
        <p>{l s='No categories' mod='jmspagebuilder'}</p>
    {/if}