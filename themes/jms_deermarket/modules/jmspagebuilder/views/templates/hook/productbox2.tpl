{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="product-box" itemscope itemtype="http://schema.org/Product">
	<div class="preview product-colors" data-id-product="{$product.id_product}">
		<a href="{$product.link|escape:'html':'UTF-8'}" class="product-image {if $jpb_phover == 'image_swap'}image_swap{else}image_blur{/if}" data-id-product="{$product.id_product}" itemprop="url">
			<img class="img-responsive product-img1" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:html:'UTF-8'}" itemprop="image" />
		</a>
	</div>
	<div class="product-info">
		<a href="{$product.link|escape:'html'}" itemprop="url" class="product-name">
			{$product.name|escape:'html':'UTF-8'}
		</a>
		{if !$PS_CATALOG_MODE}
			<div class="content_price clearfix" itemscope itemtype="http://schema.org/Offer">
				{if $product.show_price AND !isset($restricted_country_mode)}
					<span class="price new" itemprop="price">
						{if !$priceDisplay}
							{convertPrice price=$product.price}
						{else}
							{convertPrice price=$product.price_tax_exc}
						{/if}
					</span>
				{/if}
				{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
					{hook h="displayProductPriceBlock" product=$product type="old_price"}
						<span class="price old">
							{displayWtPrice p=$product.price_without_reduction}
						</span>
				{/if}
				<meta itemprop="priceCurrency" content="{$currency->iso_code|escape:'htmlall':'UTF-8'}" />
			</div>
		{/if}
	</div>
</div>