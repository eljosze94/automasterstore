{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script type="text/javascript">
jQuery(function ($) {
    "use strict";
	var productCarousel = $(".product-carousel");		
	var items = {if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if},
	    itemsDesktop = {if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if},
	    itemsDesktopSmall = {if $cols_md}{$cols_md|escape:'htmlall':'UTF-8'}{else}3{/if},
	    itemsTablet = {if $cols_sm}{$cols_sm|escape:'htmlall':'UTF-8'}{else}2{/if},
	    itemsMobile = {if $cols_xs}{$cols_xs|escape:'htmlall':'UTF-8'}{else}1{/if};
	var rtl = false;
	if ($("body").hasClass("rtl")) rtl = true;				
	productCarousel.owlCarousel({
		responsiveClass:true,
		responsive:{			
			1199:{
				items:itemsDesktop
			},
			991:{
				items:itemsDesktopSmall
			},
			768:{
				items:itemsTablet
			},
			318:{
				items:itemsMobile
			}
		},
		rtl: rtl,
	    nav: {if $navigation == '1'}true{else}false{/if},
	    dots: {if $pagination == '1'}true{else}false{/if},
		autoplay:{if $autoplay == '1'}true{else}false{/if},
	    rewindNav: {if $rewind == '1'}true{else}false{/if},
	    navigationText: ["", ""],
	    slideBy: {if $slidebypage == '1'}'page'{else}1{/if},
	    slideSpeed: 200	
	});
});
$(document).ready(function() {
    var small_img1 =  $(".img-banner-second").children('.small_img1'),
        small_img2 =  $(".img-banner-second").children('.small_img2'),
        small_img3 =  $(".img-banner-second").children('.small_img3');
    var content_first =  $(".html-content-second").children('.content-first'),
        content_second =  $(".html-content-second").children('.content-second'),
        content_third =  $(".html-content-second").children('.content-third');
        small_img1.click(function (e) {   
            if ($(this).hasClass("fix-border")) $(this).removeClass("fix-border");
            if ($(this).not(".active")) $(this).addClass("active");
            if (small_img3.hasClass("fix-border")) small_img3.removeClass("fix-border");
            if (content_first.not(".active")) content_first.addClass("active");
            if (small_img2.hasClass("active")) small_img2.removeClass("active");
            if (content_second.hasClass("active")) content_second.removeClass("active");
            if (small_img3.hasClass("active")) small_img3.removeClass("active");
            if (content_third.hasClass("active")) content_third.removeClass("active");
        });
        small_img2.click(function (e) {   
            $(this).addClass("active");
            if (content_second.not(".active")) content_second.addClass("active");
            if (small_img1.not(".fix-border")) small_img1.addClass("fix-border");
            if (small_img3.not(".fix-border")) small_img3.addClass("fix-border");
            if (small_img1.hasClass("active")) small_img1.removeClass("active");
            if (content_first.hasClass("active")) content_first.removeClass("active");
            if (small_img3.hasClass("active")) small_img3.removeClass("active");
            if (content_third.hasClass("active")) content_third.removeClass("active");
        });
        small_img3.click(function (e) {   
            $(this).addClass("active");
            if ($(this).hasClass("fix-border")) $(this).removeClass("fix-border");
            if (small_img1.hasClass("fix-border")) small_img1.removeClass("fix-border");
            if (content_third.not(".active")) content_third.addClass("active");
            if (small_img1.hasClass("active")) small_img1.removeClass("active");
            if (content_first.hasClass("active")) content_first.removeClass("active");
            if (small_img2.hasClass("active")) small_img2.removeClass("active");
            if (content_second.hasClass("active")) content_second.removeClass("active");
            e.preventDefault()
        });
});
</script>

<div class="flex-box top">
	<div class="left-content">
		{if $addon_title}
			<div class="addon-title">
				{if $icon_class}
					<i class="{$icon_class|escape:'htmlall':'UTF-8'}"></i>
				{/if}
				<h3>{$addon_title|escape:'htmlall':'UTF-8'}</h3>
				<a class="cat-btn" title="Show Categories">
					<i class="fa fa-angle-double-down" aria-hidden="true"></i>
				</a>
			</div>
		{/if}
		<div class="categories-list">
			<ul>
				{foreach from=$categories item=category }
		            {assign var='categoryLink' value=$link->getcategoryLink($category.id_category, $category.link_rewrite)}
					<li><a href="{$categoryLink}" title="{$category.name}">{$category.name}</a></li>
				{/foreach}
			</ul>
			{if $allcat_id}
			 	{assign var='allcategoryLink' value=$link->getcategoryLink($allcat_id, $allcat_link_rewrite)}
			 	<a href="{$allcategoryLink}" title="allcat" class="view-all-btn btn-hover">
			 		{l s='View All' mod='jmspagebuilder'} 
			 		<i class="fa fa-angle-right" aria-hidden="true"></i>
				</a>
			{/if}
		</div>
	</div>
	<div class="right-content">
		<div class="flex-box">
			<div class="html-content html-content-second">
				{if $banner_html1}
					<div class="content-first item">
							{$banner_html1}
					</div>
				{/if}
				{if $banner_html2}
					<div class="content-second item">
						{$banner_html2}
					</div>
				{/if}
				{if $banner_html3}
					<div class="content-third item">
						{$banner_html3}
					</div>
				{/if}
			</div>
			{if $banner1 || $banner2 || $banner3 }
			<div class="img-banner img-banner-second">
				{if $banner1}
					<div class="item small_img1">
						<img src="{$root_url|escape:'html':'UTF-8'}{$banner1|escape:'html':'UTF-8'}" alt="Image" />
					</div>
				{/if}
				{if $banner2}
					<div class="item small_img2">
						<img src="{$root_url|escape:'html':'UTF-8'}{$banner2|escape:'html':'UTF-8'}" alt="Image" />
					</div>
				{/if}
				{if $banner3}
					<div class="item small_img3">
						<img src="{$root_url|escape:'html':'UTF-8'}{$banner3|escape:'html':'UTF-8'}" alt="Image" />
					</div>
				{/if}
			</div>
			{/if}
		</div>
	</div>
</div>
{assign var="box_template" "{$addon_tpl_dir}productbox.tpl"}
<div class="product-carousel item-hover">	
	{foreach from = $products_slides item = products_slide}
		<div class="item">
			{foreach from = $products_slide item = product}
				{include file={$box_template} product=$product}	
			{/foreach}
		</div>
	{/foreach}
</div>