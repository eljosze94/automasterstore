{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA

*}
{if $widget_setting.JBW_SB_SHOW_CATEGORYMENU}
<aside class="blog-widget widget-categories">
	<h3 class="widget-title"><span>{l s='Categorias' mod='jmsblogwidget'}</span></h3>
	<ul>
	{$category_menu}
	</ul>
</aside>
{/if}


{if $widget_setting.JBW_SB_SHOW_RECENT}
<aside class="blog-widget">
	<h3 class="widget-title"><span>{l s='Publicaciones recientes' mod='jmsblogwidget'}</span></h3>
	<ul class="post-list">
		{foreach from=$latestpost key=k item=post}
			{assign var="show_view" value=$post.views + 1}
			{assign var=params value=['post_id' => $post.post_id, 'category_slug' => $post.category_alias, 'slug' => $post.alias]}
			<li>
				<div class="entry-thumb">
					<a href="{jmsblog::getPageLink('jmsblog-post', $params)}" class="post-img">
					{if $post.image}
						<img src="{$image_baseurl|escape:'html'}thumb_{$post.image|escape:'html'}" class="img-responsive" />
					{else}	
						<img src="{$image_baseurl|escape:'html'}no-img.jpg" class="img-responsive" />
					{/if}
					</a>
				</div>
				<div class="entry-title">
					<a href="{jmsblog::getPageLink('jmsblog-post', $params)}">
						{$post.title|escape:'html'}
					</a>
				</div>
				<div class="date">
					<ul>
						<li>
							<span class="label" style="color: #000;" >{l s='Publicado: ' mod='jmsblogwidget'}</span>{$post.created|escape:'html':'UTF-8'|date_format:"%b %d, %Y"}
						</li>
						<li>
							<span class="label view" style="color: #000;">{l s='Visitas: ' mod='jmsblogwidget'}</span>{$show_view|escape:'html'}
						</li>
<br>
					</ul>	
				</div>
			</li>
		{/foreach}
	</ul>
	
</aside>
{/if}

{if $widget_setting.JBW_SB_SHOW_POPULAR}
<aside class="blog-widget">
	<h3 class="widget-title"><span>{l s='Publicaciones populares' mod='jmsblogwidget'}</span></h3>
	<ul class="post-list">
		{foreach from=$popularpost key=k item=post}	
		{assign var="show_view" value=$post.views + 1}	
		{assign var=params value=['post_id' => $post.post_id, 'category_slug' => $post.category_alias, 'slug' => $post.alias]}
			<li>
				<div class="entry-thumb">
					<a href="{jmsblog::getPageLink('jmsblog-post', $params)}" class="post-img">
						{if $post.image}
							<img src="{$image_baseurl|escape:'html'}thumb_{$post.image|escape:'html'}" class="img-responsive" />
						{else}	
							<img src="{$image_baseurl|escape:'html'}no-img.jpg" class="img-responsive" />
						{/if}
					</a>
				</div>
				<div class="entry-title">
					<a href="{jmsblog::getPageLink('jmsblog-post', $params)}">
						{$post.title|escape:'html'}
					</a>
				</div>
				{* <div class="date">{$post.created|escape:'html':'UTF-8'|date_format:"%b %d, %Y"}</div> *}
<div class="date">
					<ul>
						<li>
							<span class="label" style="color: #000;" >{l s='Publicado: ' mod='jmsblogwidget'}</span>{$post.created|escape:'html':'UTF-8'|date_format:"%b %d, %Y"}
						</li>
						<li>
							<span class="label view" style="color: #000;">{l s='Visitas: ' mod='jmsblogwidget'}</span>{$show_view|escape:'html'}
						</li>
<br>
					</ul>	
				</div>
			</li>
		{/foreach}
	</ul>
</aside>
{/if}
	
{if $widget_setting.JBW_SB_SHOW_LATESTCOMMENT}
<aside class="blog-widget">
	<h3 class="widget-title"><span>{l s='Últimos comentarios' mod='jmsblogwidget'}</span></h3>
	{foreach from=$latestcomment key=k item=comment}
		<article class="comment-item clearfix">
			<img src="{$image_baseurl|escape:'html'}user.png" class="img-responsive">
			<div class="comment-info">
				<h6>{$comment.customer_name}</h6>
				<p>{$comment.comment}</p>
			</div>
		</article>
	{/foreach}
</aside>
{/if}

{if $widget_setting.JBW_SB_SHOW_ARCHIVES}
<aside class="blog-widget widget-categories">
	<h3 class="widget-title">
		<span>{l s='Archivos' mod='jmsblogwidget'}</span>
	</h3>
	<ul>
	{foreach from=$archives item=archive}
		{assign var=aparams value=['archive' => $archive.postmonth]}
		<li><a href="{jmsblog::getPageLink('jmsblog-archive', $aparams)}">{$archive.postmonth}</a></li>
	{/foreach}
	</ul>
</aside>
{/if}
