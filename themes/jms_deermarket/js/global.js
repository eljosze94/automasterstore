/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


$(document).ready(function(){
	
	$(".fa-home").css('margin', 0);
	
	
    if (navigator.userAgent.match(/Android/i))
    {
        var viewport = document.querySelector('meta[name="viewport"]');
        viewport.setAttribute('content', 'initial-scale=1.0,maximum-scale=1.0,user-scalable=0,width=device-width,height=device-height');
        window.scrollTo(0, 1);
    }
    if (typeof quickView !== 'undefined' && quickView)
        quick_view();

    if (typeof page_name != 'undefined' && !in_array(page_name, ['index', 'product']))
    {
        $(document).on('change', '.selectProductSort', function(e){
            if (typeof request != 'undefined' && request)
                var requestSortProducts = request;
            var splitData = $(this).val().split(':');
            var url = '';
            if (typeof requestSortProducts != 'undefined' && requestSortProducts)
            {
                url += requestSortProducts ;
                if (typeof splitData[0] !== 'undefined' && splitData[0])
                {
                    url += ( requestSortProducts.indexOf('?') < 0 ? '?' : '&') + 'orderby=' + splitData[0] + (splitData[1] ? '&orderway=' + splitData[1] : '');
                    if (typeof splitData[1] !== 'undefined' && splitData[1])
                        url += '&orderway=' + splitData[1];
                }
                document.location.href = url;
            }
        });

        $(document).on('change', 'select[name="n"]', function(){
            $(this.form).submit();
        });

        $(document).on('change', 'select[name="currency_payment"]', function(){
            setCurrency($(this).val());
        });
    }

    $(document).on('change', 'select[name="manufacturer_list"], select[name="supplier_list"]', function(){
        if (this.value != '')
            location.href = this.value;
    });
    
    // Close Alert messages
    $(".alert.alert-danger").on('click', this, function(e){
        if (e.offsetX >= 16 && e.offsetX <= 39 && e.offsetY >= 16 && e.offsetY <= 34)
            $(this).fadeOut();
    });
});


/*fixed menu*/
$(document).ready(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 0 && $(window).width() > 991 && !$("body").hasClass("home3")) {
			$('.fixed-top').addClass('navbar-fixed-top');
            $('#main-header').addClass('change-style');
        }
        else if (scroll > 990 && $(window).width() > 1599 && $("body").hasClass("home3")) {
            $('#main-header').addClass('change-style');
        }
        else if (scroll > 850 && $(window).width() == 1599 && $("body").hasClass("home3")) {
            $('#main-header').addClass('change-style');
        }
        else if (scroll > 700 && $(window).width() == 1366 && $("body").hasClass("home3")) {
            $('#main-header').addClass('change-style');
        }
        else if (scroll > 0 && $(window).width() > 1599 && !$("body").hasClass("index")) {
            $('#main-header').addClass('change-style');
        }
        else {
            $('.fixed-top').removeClass('navbar-fixed-top');
             $('#main-header').removeClass('change-style');
        }
    });
});

function quick_view()
{
    $(document).on('click', '.quick-view:visible, .quick-view-mobile:visible', function(e){
        e.preventDefault();
        var url = $(this).attr('data-link');
        var anchor = '';

        if (url.indexOf('#') != -1)
        {
            anchor = url.substring(url.indexOf('#'), url.length);
            url = url.substring(0, url.indexOf('#'));
        }

        if (url.indexOf('?') != -1)
            url += '&';
        else
            url += '?';

        if (!!$.prototype.fancybox)
            $.fancybox({
                'padding':  0,
                'width':    1087,
                'height':   480,
                'type':     'iframe',
                'href':     url + 'content_only=1' + anchor
            });
    });
}


function bindUniform() {
	if (!!$.prototype.uniform)
		$("select.form-control,input[type='radio'],input[type='checkbox']").not(".not_uniform").uniform();
}

function view_as() { 
    var viewGrid = $(".view-grid"),
        viewList = $(".view-list"),
        productList = $(".product_list");
    viewGrid.click(function (e) {       
        productList.removeClass("products-list-in-row");
        productList.addClass("products-list-in-column");
        $(this).addClass('active');
        viewList.removeClass("active");
        e.preventDefault()
    });
    viewList.click(function (e) {       
        productList.removeClass("products-list-in-column");
        productList.addClass("products-list-in-row");
        viewGrid.removeClass("active");
        $(this).addClass('active');        
        e.preventDefault()
    })
}

jQuery(function ($) {
    "use strict";
    $(".view-grid").addClass('active');
    view_as();
});


jQuery(function ($) {
    "use strict";
    $.initQuantity = function ($control) {
        $control.each(function () {
            var $this = $(this),
                data = $this.data("inited-control"),
                $plus = $(".input-group-addon:last", $this),
                $minus = $(".input-group-addon:first", $this),
                $value = $(".form-control", $this);
            if (!data) {
                $control.attr("unselectable", "on").css({
                    "-moz-user-select": "none",
                    "-o-user-select": "none",
                    "-khtml-user-select": "none",
                    "-webkit-user-select": "none",
                    "-ms-user-select": "none",
                    "user-select": "none"
                }).bind("selectstart", function () {
                    return false
                });
                $plus.click(function () {
                    var val =
                        parseInt($value.val()) + 1;
                    $value.val(val);
                    return false
                });
                $minus.click(function () {
                    var val = parseInt($value.val()) - 1;
                    $value.val(val > 0 ? val : 1);
                    return false
                });
                $value.blur(function () {
                    var val = parseInt($value.val());
                    $value.val(val > 0 ? val : 1)
                })
            }
        })
    };
    $.initQuantity($(".quantity-control"));
    $.initSelect = function ($select) {
        $select.each(function () {
            var $this = $(this),
                data = $this.data("inited-select"),
                $value = $(".value", $this),
                $hidden = $(".input-hidden", $this),
                $items = $(".dropdown-menu li > a", $this);
            if (!data) {
                $items.click(function (e) {
                    if ($(this).closest(".sort-isotope").length >
                        0) e.preventDefault();
                    var data = $(this).attr("data-value"),
                        dataHTML = $(this).html();
                    $this.trigger("change", {
                        value: data,
                        html: dataHTML
                    });
                    $value.html(dataHTML);
                    if ($hidden.length) $hidden.val(data)
                });
                $this.data("inited-select", true)
            }
        })
    };
    $.initSelect($(".btn-select"))
});
	jQuery(function ($) {
    "use strict";
    var thumbCarousel = $(".thumb-carousel");
    var rtl = false;
    if ($("body").hasClass("rtl")) rtl = true;
    thumbCarousel.owlCarousel({
        responsiveClass:true,
        responsive:{            
            1199:{
                items:4
            },
            360:{
                items:3
            },
            0:{
                items:2
            },
        },
        rtl: rtl,
        margin: 12,
        nav: true,
        dots: false,
        autoplay: false,
        slideSpeed: 800,
    });
});

// Products Category
jQuery(function ($) {
    "use strict";
    var productCarousel = $(".productscategory-carousel");
    var rtl = false;
    if ($("body").hasClass("rtl")) rtl = true;

    productCarousel.owlCarousel({
        responsiveClass:true,
        responsive:{            
            1367:{
                items:4
            },
            992:{
                items:3
            },
            768:{
                items:3
            },
            0:{
                items:1
            }
        },
        rtl: rtl,
        margin: 15,
        nav: false,
        dots: false,
        autoplay: false,
        slideSpeed: 800,
    });
});
// Accessories
jQuery(function ($) {
    "use strict";
    var productCarousel = $(".accessories-carousel");
    var rtl = false;
    if ($("body").hasClass("rtl")) rtl = true;

     productCarousel.owlCarousel({
        responsiveClass:true,
        responsive:{            
            1367:{
                items:3
            },
            991:{
                items:3
            },
            768:{
                items:3
            },
            481:{
                items:2
            },
            320:{
                items:1
            }
        },
        rtl: rtl,
        margin: 15,
        nav: false,
        dots: false,
        autoplay: false,
        slideSpeed: 800,
    });
});
/* Button Setting Home 1 */
$(document).ready(function() {
	$('#btn_close').click(function(event) {
		$('.megamenu-box').removeClass('open');
		$('.home4').removeClass('cover');
		$('.popup-newslleter').removeClass('open');
	});
	$('#btn_newslleter').click(function(event) {
		$('.megamenu-box').toggleClass('open');
		$('.popup-newslleter').toggleClass('open');
	});
    $('.cat-btn').click(function(event) {
        $('.left-content').toggleClass('open');
    });
    $('#show-cat').click(function(event) {
        $('.mobile-style').toggleClass('open');
    });
    $('#show-cat1').click(function(event) {
        $('.mobile-style1').toggleClass('open');
    });
    $('#show-cat2').click(function(event) {
        $('.mobile-style2').toggleClass('open');
    });
     $('#show-cat3').click(function(event) {
        $('.mobile-style3').toggleClass('open');
    });
    $('#show-cat4').click(function(event) {
        $('.mobile-style4').toggleClass('open');
    });
});
/* ------ */
$(document).ready(function() {

    $('#back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 800);

    });
	
});
/*=======================================
=            Jms Ajax Search            =
=======================================*/
$(document).ready(function() {

    $('#jms_ajax_search .btn-xs').click(function(e) {

        e.preventDefault();

        $('#jms_ajax_search .dropdown-menu').toggleClass('open');

    });
    $(".jms-adv-search input").click(function() {  
        return false;
    });
});
/* Other */
$(document).ready(function() {
    $('.small_img1').addClass("active");
    $(".content-first").addClass("active");
   
	$("li a.deal-thumb").hover(
		function() {	
		  var new_src = $(this).attr('href').replace('large','thickbox');
		  var bigpig = $(this).parents('.preview').find('.big-image');
		  if (bigpig.attr('src') != new_src)
		  {
			 bigpig.attr({
						'src' : new_src
					});
		  }
		}
	);

	$(".small-images li").hover(
	  function() {
		$( this ).addClass( "active" );
	  }, function() {
		$( this ).removeClass( "active" );
	  }
	);
});
/* Hot deals */

/* ------- */
