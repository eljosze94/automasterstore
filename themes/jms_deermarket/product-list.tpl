{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($products) && $products}
	<div id="product_list" class="product_list products-list-in-column">
		<div class="list-box">
			{foreach from=$products item=product name=products}
		<div class="item ajax_block_product" itemscope itemtype="http://schema.org/Product">
			<div class="product-box" itemscope itemtype="http://schema.org/Product">
	<div class="preview product-colors" data-id-product="{$product.id_product}">
		<a href="{$product.link|escape:'html':'UTF-8'}" class="product-image {if $jpb_phover == 'image_swap'}image_swap{else}image_blur{/if}" data-id-product="{$product.id_product}" itemprop="url">
			<img class="img-responsive product-img1" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:html:'UTF-8'}" itemprop="image" />
		</a>
		{if $product.specific_prices}
                                {assign var='specific_prices' value=$product.specific_prices}
									{if $specific_prices.reduction_type == 'percentage' && ($specific_prices.from == $specific_prices.to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $specific_prices.from))}
                                    <span class="price-percent-reduction">-{$specific_prices.reduction*100|floatval}%</span>
									{/if}
                            {/if}
		<div class="action-btn">
				<a class="add-to-bookmark addToWishlist product-btn btn-hover" href="#" onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|escape:'html'}', false, 1); return false;" data-id-product="{$product.id_product|escape:'html'}" title="{l s='Agregar a lista de deseos' mod='jmspagebuilder'}">
					<i class="fa fa-heart" aria-hidden="true"></i>
				</a>
				{if isset($comparator_max_item) && $comparator_max_item}
					<a class="add_to_compare product-btn btn-hover" href="{$product.link|escape:'html':'UTF-8'} " data-id-product="{$product.id_product}" title="{l s='Agregar a lista de comparación' mod='jmspagebuilder'}">
						<i class="fa fa-exchange" aria-hidden="true"></i>
						<i class="fa fa-check" aria-hidden="true"></i>
					</a>
				{/if}
		</div>
		<a href="#" data-link="{$product.link|escape:'html':'UTF-8'}" class="quick-view product-btn hidden-xs btn-hover">
			{l s='VISTA RÁPIDA' mod='jmspagebuilder'}
		</a>
	</div>
	<div class="product-info">
		<a href="{$product.link|escape:'html'}" itemprop="url" class="product-name">
			{$product.name|escape:'html':'UTF-8'}
		</a>
		{if !$PS_CATALOG_MODE}
			<div class="content_price clearfix" itemscope itemtype="http://schema.org/Offer">
				{if $product.show_price AND !isset($restricted_country_mode)}
					<span class="price new" itemprop="price">
						{if !$priceDisplay}
							{convertPrice price=$product.price}
						{else}
							{convertPrice price=$product.price_tax_exc}
						{/if}
					</span>
				{/if}
				{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
					{hook h="displayProductPriceBlock" product=$product type="old_price"}
						<span class="price old">
							{displayWtPrice p=$product.price_without_reduction}
						</span>
				{/if}
				<meta itemprop="priceCurrency" content="{$currency->iso_code|escape:'htmlall':'UTF-8'}" />
			</div>
		{/if}
		{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
				{if ($product.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
					{if ($product.quantity > 0 OR $product.allow_oosp)}
						<a class="product-btn cart-button ajax_add_to_cart_button btn-effect" data-id-product="{$product.id_product}" href="{$link->getPageLink('cart')|escape:'html':'UTF-8'}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" title="{l s='Agregar al carro' mod='jmsproductfilter'}">
								<span class="fa fa-shopping-cart" aria-hidden="true"></span>
								<span class="text">{l s='AGREGAR AL CARRO' mod='jmspagebuilder'}</span>
								<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
								<i class="fa fa-check" aria-hidden="true"></i>
						</a>							
					{else}
						<a href="#" class="product-btn cart-button btn-default ajax_add_to_cart_button disable btn-effect" title="{l s='Out of Stock' mod='jmspagebuilder'}">
							<span class="fa fa-shopping-cart" aria-hidden="true"></span>
							<span class="text">{l s='AGREGAR AL CARRO' mod='jmspagebuilder'}</span>
							<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
							<i class="fa fa-check" aria-hidden="true"></i>
						</a>
					{/if}									
				{/if}
	</div>
	<div class="list-info">
		<div class="box">
			<a href="{$product.link|escape:'html'}" itemprop="url" class="product-name">{$product.name|escape:'html':'UTF-8'}</a>
						<span class="review">
							{hook h='displayProductListReviews' product=$product}
						</span>
						<div class="content_price clearfix" itemscope itemtype="http://schema.org/Offer">
							{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
								<span class="price new" itemprop="price">
									{if !$priceDisplay}
										{convertPrice price=$product.price}
									{else}
										{convertPrice price=$product.price_tax_exc}
									{/if}
								</span>
							{/if}
							{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
								{hook h="displayProductPriceBlock" product=$product type="old_price"}
									<span class="price old">
										{displayWtPrice p=$product.price_without_reduction}
									</span>
							{/if}
							<meta itemprop="priceCurrency" content="{$currency->iso_code|escape:'htmlall':'UTF-8'}" />
						</div>
						<div class="product-desc">
							{$product.description_short}
						</div>
						{if $product.features}
							<ul class="product-features">
								{foreach from=$product.features item=feature}
										<li>
											<i class="fa fa-check" aria-hidden="true"></i>
											{$feature.value}
										</li>
								{/foreach}
							</ul>
						{/if}
						{if ($product.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
					{if ($product.quantity > 0 OR $product.allow_oosp)}
						<a class="product-btn cart-button ajax_add_to_cart_button btn-effect" data-id-product="{$product.id_product}" href="{$link->getPageLink('cart')|escape:'html':'UTF-8'}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" title="{l s='Agregar al carro' mod='jmsproductfilter'}">
								<span class="fa fa-shopping-cart" aria-hidden="true"></span>
								<span class="text">{l s='AGREGAR AL CARRO' mod='jmspagebuilder'}</span>
								<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
								<i class="fa fa-check" aria-hidden="true"></i>
						</a>							
					{else}
						<a href="#" class="product-btn cart-button btn-default ajax_add_to_cart_button disable btn-effect" title="{l s='Out of Stock' mod='jmspagebuilder'}">
							<span class="fa fa-shopping-cart" aria-hidden="true"></span>
							<span class="text">{l s='AGREGAR AL CARRO' mod='jmspagebuilder'}</span>
							<span class="fa fa-refresh fa-spin" aria-hidden="true"></span>
							<i class="fa fa-check" aria-hidden="true"></i>
						</a>
					{/if}									
				{/if}
		</div>
	</div>
</div>
		</div>
	{/foreach}
		</div>
	</div>
{addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}
{addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
{addJsDef comparator_max_item=$comparator_max_item}
{addJsDef comparedProductsIds=$compared_products}
{/if}
