{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Your addresses'}{/capture}
<div class="box">
	<h1 class="page-subheading">{l s='Your addresses'}</h1>
	<p class="info-title">
		{if isset($id_address) && (isset($smarty.post.alias) || isset($address->alias))}
			{l s='Modify address'}
			{if isset($smarty.post.alias)}
				"{$smarty.post.alias}"
			{else}
				{if isset($address->alias)}"{$address->alias|escape:'html':'UTF-8'}"{/if}
			{/if}
		{else}
			{l s='To add a new address, please fill out the form below.'}
		{/if}
	</p>
	{include file="$tpl_dir./errors.tpl"}
	<p class="required"><sup>*</sup>{l s='Required field'}</p>
	<form action="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" method="post" class="std" id="add_address">
		<!--h3 class="page-subheading">{if isset($id_address)}{l s='Your address'}{else}{l s='New address'}{/if}</h3-->
		{assign var="stateExist" value=false}
		{assign var="postCodeExist" value=false}
		{assign var="dniExist" value=false}
		{assign var="homePhoneExist" value=false}
		{assign var="mobilePhoneExist" value=false}
		{assign var="atLeastOneExists" value=false}
		{foreach from=$ordered_adr_fields item=field_name}
			{if $field_name eq 'company'}
				<div class="form-group">
					<label for="company">{l s='Company'}{if isset($required_fields) && in_array($field_name, $required_fields)} <sup>*</sup>{/if}</label>
					<input class="form-control validate" data-validate="{$address_validation.$field_name.validate}" type="text" id="company" name="company" value="{if isset($smarty.post.company)}{$smarty.post.company}{else}{if isset($address->company)}{$address->company|escape:'html':'UTF-8'}{/if}{/if}" />
				</div>
			{/if}
			{if $field_name eq 'vat_number'}
				<div id="vat_area">
					<div id="vat_number">
						<div class="form-group">
							<label for="vat-number">{l s='VAT number'}{if isset($required_fields) && in_array($field_name, $required_fields)} <sup>*</sup>{/if}</label>
							<input type="text" class="form-control validate" data-validate="{$address_validation.$field_name.validate}" id="vat-number" name="vat_number" value="{if isset($smarty.post.vat_number)}{$smarty.post.vat_number}{else}{if isset($address->vat_number)}{$address->vat_number|escape:'html':'UTF-8'}{/if}{/if}" />
						</div>
					</div>
				</div>
			{/if}
			{if $field_name eq 'dni'}
			{assign var="dniExist" value=true}
			<div class="required form-group dni">
				<label for="dni">{l s='Identification number'} <sup>*</sup></label>
				<input class="form-control" data-validate="{$address_validation.$field_name.validate}" type="text" name="dni" id="dni" value="{if isset($smarty.post.dni)}{$smarty.post.dni}{else}{if isset($address->dni)}{$address->dni|escape:'html':'UTF-8'}{/if}{/if}" />
				<span class="form_info">{l s='DNI / NIF / NIE'}</span>
			</div>
			{/if}
			{if $field_name eq 'firstname'}
				<div class="required form-group">
					<label for="firstname">{l s='First name'} <sup>*</sup></label>
					<input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" name="firstname" id="firstname" value="{if isset($smarty.post.firstname)}{$smarty.post.firstname}{else}{if isset($address->firstname)}{$address->firstname|escape:'html':'UTF-8'}{/if}{/if}" />
				</div>
			{/if}
			{if $field_name eq 'lastname'}
				<div class="required form-group">
					<label for="lastname">{l s='Last name'} <sup>*</sup></label>
					<input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="lastname" name="lastname" value="{if isset($smarty.post.lastname)}{$smarty.post.lastname}{else}{if isset($address->lastname)}{$address->lastname|escape:'html':'UTF-8'}{/if}{/if}" />
				</div>
			{/if}
			{if $field_name eq 'address1'}
				<div class="required form-group">
					<label for="address1">{l s='Address'} <sup>*</sup></label>
					<input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="address1" name="address1" value="{if isset($smarty.post.address1)}{$smarty.post.address1}{else}{if isset($address->address1)}{$address->address1|escape:'html':'UTF-8'}{/if}{/if}" />
				</div>
			{/if}
			{if $field_name eq 'address2'}
				<div class="required form-group">
					<label for="address2">{l s='Address (Line 2)'}{if isset($required_fields) && in_array($field_name, $required_fields)} <sup>*</sup>{/if}</label>
					<input class="validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="address2" name="address2" value="{if isset($smarty.post.address2)}{$smarty.post.address2}{else}{if isset($address->address2)}{$address->address2|escape:'html':'UTF-8'}{/if}{/if}" />
				</div>
			{/if}
			{if $field_name eq 'postcode'}
				{assign var="postCodeExist" value=true}
				<div class="required postcode form-group unvisible">
					<label for="postcode">{l s='Zip/Postal Code'} <sup>*</sup></label>
					<input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($address->postcode)}{$address->postcode|escape:'html':'UTF-8'}{/if}{/if}" />
				</div>
			{/if}
			{if $field_name eq 'city'}
				<div class="required form-group">
					<label for="city">{l s='City'} <sup>*</sup></label>
					<input class="is_required validate form-control" data-validate="{$address_validation.$field_name.validate}" type="text" name="city" id="city" value="{if isset($smarty.post.city)}{$smarty.post.city}{else}{if isset($address->city)}{$address->city|escape:'html':'UTF-8'}{/if}{/if}" maxlength="64" />
				</div>
				{* if customer hasn't update his layout address, country has to be verified but it's deprecated *}
			{/if}
			{if $field_name eq 'Country:name' || $field_name eq 'country' || $field_name eq 'Country:iso_code'}
				<div class="required form-group">
					<label for="id_country">{l s='Country'} <sup>*</sup></label>
					<select id="id_country" class="form-control" name="id_country">{$countries_list}</select>
				</div>
			{/if}
			{if $field_name eq 'State:name'}
				{assign var="stateExist" value=true}
				<div class="required id_state form-group">
					<label for="id_state">{l s='State'} <sup>*</sup></label>

					<select name="id_state" id="id_state" class="form-control">
						<option value="">-</option>
					</select>
				</div>
			{/if}
			{if $field_name eq 'phone'}
				{assign var="homePhoneExist" value=true}
				<div class="form-group phone-number">
					<label for="phone">{l s='Home phone'}{if isset($one_phone_at_least) && $one_phone_at_least} <sup>**</sup>{/if}</label>
					<input class="{if isset($one_phone_at_least) && $one_phone_at_least}is_required{/if} validate form-control" data-validate="{$address_validation.phone.validate}" type="tel" id="phone" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{if isset($address->phone)}{$address->phone|escape:'html':'UTF-8'}{/if}{/if}"  />
				</div>
				<div class="clearfix"></div>
			{/if}
			{if $field_name eq 'phone_mobile'}
				{assign var="mobilePhoneExist" value=true}
				<div class="{if isset($one_phone_at_least) && $one_phone_at_least}required {/if}form-group">
					<label for="phone_mobile">{l s='Mobile phone'}{if isset($one_phone_at_least) && $one_phone_at_least} <sup>**</sup>{/if}</label>
					<input class="validate form-control" data-validate="{$address_validation.phone_mobile.validate}" type="tel" id="phone_mobile" name="phone_mobile" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{else}{if isset($address->phone_mobile)}{$address->phone_mobile|escape:'html':'UTF-8'}{/if}{/if}" />
				</div>
			{/if}
			{if ($field_name eq 'phone_mobile') || ($field_name eq 'phone_mobile') && !isset($atLeastOneExists) && isset($one_phone_at_least) && $one_phone_at_least}
				{assign var="atLeastOneExists" value=true}
				<p class="inline-infos required">** {l s='You must register at least one phone number.'}</p>
			{/if}
		{/foreach}
		{if !$postCodeExist}
			<div class="required postcode form-group unvisible">
				<label for="postcode">{l s='Zip/Postal Code'} <sup>*</sup></label>
				<input class="is_required validate form-control" data-validate="{$address_validation.postcode.validate}" type="text" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($address->postcode)}{$address->postcode|escape:'html':'UTF-8'}{/if}{/if}" />
			</div>
		{/if}
		{if !$stateExist}
			<div class="required id_state form-group unvisible">
				<label for="id_state">{l s='State'} <sup>*</sup></label>
				<select name="id_state" id="id_state" class="form-control">
					<option value="">-</option>
                                        <option value="ACHAO">ACHAO</option>
						<option value="ALGARROBO">ALGARROBO</option>
						<option value="ALHUE" >ALHUE</option>
						<option value="ALTO BIOBIO" >ALTO BIOBIO</option>
						<option value="ALTO DEL CARMEN" >ALTO DEL CARMEN</option>
						<option value="ALTO HOSPICIO" >ALTO HOSPICIO</option>
						<option value="ALTO JAHUEL" >ALTO JAHUEL</option>
						<option value="ANCUD" >ANCUD</option>
						<option value="ANDACOLLO" >ANDACOLLO</option>
						<option value="ANGOL" >ANGOL</option>
						<option value="ANTARTICA" >ANTARTICA</option>
						<option value="ANTOFAGASTA" >ANTOFAGASTA</option>
						<option value="ANTUCO" >ANTUCO</option>
						<option value="ARAUCO" >ARAUCO</option>
						<option value="ARICA" >ARICA</option>
						<option value="ARTIFICIO" >ARTIFICIO</option>
						<option value="AYSEN" >AYSEN</option>
						<option value="BALMACEDA" >BALMACEDA</option>
						<option value="BAQUEDANO" >BAQUEDANO</option>
						<option value="BATUCO" >BATUCO</option>
						<option value="BUIN" >BUIN</option>
						<option value="BULNES" >BULNES</option>
						<option value="CABILDO" >CABILDO</option>
						<option value="CABO DE HORNOS" >CABO DE HORNOS</option>
						<option value="CABRERO" >CABRERO</option>
						<option value="CALAMA" >CALAMA</option>
						<option value="CALBUCO" >CALBUCO</option>
						<option value="CALDERA" >CALDERA</option>
						<option value="CALERA DE TANGO" >CALERA DE TANGO</option>
						<option value="CALLE LARGA" >CALLE LARGA</option>
						<option value="CAMARONES" >CAMARONES</option>
						<option value="CAMINA" >CAMINA</option>
						<option value="CANELA" >CANELA</option>
						<option value="CANETE" >CANETE</option>
						<option value="CARAHUE" >CARAHUE</option>
						<option value="CARTAGENA" >CARTAGENA</option>
						<option value="CASABLANCA" >CASABLANCA</option>
						<option value="CASTRO" >CASTRO</option>
						<option value="CATEMU" >CATEMU</option>
						<option value="CAUQUENES" >CAUQUENES</option>
						<option value="CERRILLOS" >CERRILLOS</option>
						<option value="CERRO NAVIA" >CERRO NAVIA</option>
						<option value="CHAÑARAL" >CHAÑARAL</option>
						<option value="CHANCO" >CHANCO</option>
						<option value="CHEPICA" >CHEPICA</option>
						<option value="CHICUREO" >CHICUREO</option>
						<option value="CHIGUAYANTE" >CHIGUAYANTE</option>
						<option value="CHILE CHICO" >CHILE CHICO</option>
						<option value="CHILLAN" >CHILLAN</option>
						<option value="CHILLAN VIEJO" >CHILLAN VIEJO</option>
						<option value="CHIMBARONGO" >CHIMBARONGO</option>
						<option value="CHOLCHOL" >CHOLCHOL</option>
						<option value="CHOLGUAN" >CHOLGUAN</option>
						<option value="CHONCHI" >CHONCHI</option>
						<option value="CISNES" >CISNES</option>
						<option value="COBQUECURA" >COBQUECURA</option>
						<option value="COCHAMO" >COCHAMO</option>
						<option value="COCHRANE" >COCHRANE</option>
						<option value="CODEGUA" >CODEGUA</option>
						<option value="COELEMU" >COELEMU</option>
						<option value="COIHUECO" >COIHUECO</option>
						<option value="COINCO" >COINCO</option>
						<option value="COLBUN" >COLBUN</option>
						<option value="COLCHANE" >COLCHANE</option>
						<option value="COLINA" >COLINA</option>
						<option value="COLLIPULLI" >COLLIPULLI</option>
						<option value="COLTAUCO" >COLTAUCO</option>
						<option value="COMBARBALA" >COMBARBALA</option>
						<option value="CONCEPCIÓN" >CONCEPCIÓN</option>
						<option value="CONCHALÍ" >CONCHALÍ</option>
						<option value="CONCÓN" >CONCÓN</option>
						<option value="CONSTITUCIÓN" >CONSTITUCIÓN</option>
						<option value="CONTULMO" >CONTULMO</option>
						<option value="COPIAPÓ" >COPIAPÓ</option>
						<option value="COQUIMBO" >COQUIMBO</option>
						<option value="CORONEL" >CORONEL</option>
						<option value="CORRAL" >CORRAL</option>
						<option value="COYHAIQUE" >COYHAIQUE</option>
						<option value="CUMPEO" >CUMPEO</option>
						<option value="CUNCO" >CUNCO</option>
						<option value="CURACAUTÍN" >CURACAUTÍN</option>
						<option value="CURACAVÍ" >CURACAVÍ</option>
						<option value="CURACO DE VÉLEZ" >CURACO DE VÉLEZ</option>
						<option value="CURANILAHUE" >CURANILAHUE</option>
						<option value="CURARREHUE" >CURARREHUE</option>
						<option value="CUREPTO" >CUREPTO</option>
						<option value="CURICÓ" >CURICÓ</option>
						<option value="DALCAHUE" >DALCAHUE</option>
						<option value="DIEGO DE ALMAGRO" >DIEGO DE ALMAGRO</option>
						<option value="DONIHUE" >DONIHUE</option>
						<option value="EL BOSQUE" >EL BOSQUE</option>
						<option value="EL CARMEN" >EL CARMEN</option>
						<option value="EL MELÓN" >EL MELÓN</option>
						<option value="EL MONTE" >EL MONTE</option>
						<option value="EL QUISCO" >EL QUISCO</option>
						<option value="EL SALVADOR" >EL SALVADOR</option>
						<option value="EL TABO" >EL TABO</option>
						<option value="EMPEDRADO" >EMPEDRADO</option>
						<option value="ENTRE LAGOS" >ENTRE LAGOS</option>
						<option value="ERCILLA" >ERCILLA</option>
						<option value="ESTACIÓN CENTRAL" >ESTACIÓN CENTRAL</option>
						<option value="FLORIDA" >FLORIDA</option>
						<option value="FREIRE" >FREIRE</option>
						<option value="FREIRINA" >FREIRINA</option>
						<option value="FRESIA" >FRESIA</option>
						<option value="FRUTILLAR" >FRUTILLAR</option>
						<option value="FUTALEUFÚ" >FUTALEUFÚ</option>
						<option value="FUTRONO" >FUTRONO</option>
						<option value="GALVARINO" >GALVARINO</option>
						<option value="GENERAL LAGOS" >GENERAL LAGOS</option>
						<option value="GORBEA" >GORBEA</option>
						<option value="GRANEROS" >GRANEROS</option>
						<option value="GUAITECAS" >GUAITECAS</option>
						<option value="HIJUELAS" >HIJUELAS</option>
						<option value="HORNOPIRÉN" >HORNOPIRÉN</option>
						<option value="HUALAIHUE" >HUALAIHUE</option>
						<option value="HUALANE" >HUALANE</option>
						<option value="HUALPÉN" >HUALPÉN</option>
						<option value="HUALQUI" >HUALQUI</option>
						<option value="HUARA" >HUARA</option>
						<option value="HUASCO" >HUASCO</option>
						<option value="HUECHURABA" >HUECHURABA</option>
						<option value="HUEPIL" >HUEPIL</option>
						<option value="ILLAPEL" >ILLAPEL</option>
						<option value="INDEPENDENCIA" >INDEPENDENCIA</option>
						<option value="IQUIQUE" >IQUIQUE</option>
						<option value="ISLA DE MAIPO" >ISLA DE MAIPO</option>
						<option value="ISLA NEGRA" >ISLA NEGRA</option>
						<option value="JUAN FERNÁNDEZ" >JUAN FERNÁNDEZ</option>
						<option value="LA CALERA" >LA CALERA</option>
						<option value="LA CISTERNA" >LA CISTERNA</option>
						<option value="LA CRUZ" >LA CRUZ</option>
						<option value="LA ESTRELLA" >LA ESTRELLA</option>
						<option value="LA FLORIDA" >LA FLORIDA</option>
						<option value="LA GRANJA" >LA GRANJA</option>
						<option value="LA HIGUERA" >LA HIGUERA</option>
						<option value="LA JUNTA" >LA JUNTA</option>
						<option value="LA LIGUA" >LA LIGUA</option>
						<option value="LA PINTANA" >LA PINTANA</option>
						<option value="LA REINA" >LA REINA</option>
						<option value="LA SERENA" >LA SERENA</option>
						<option value="LA UNION" >LA UNION</option>
						<option value="LAGO RANCO" >LAGO RANCO</option>
						<option value="LAGO VERDE" >LAGO VERDE</option>
						<option value="LAGUNA BLANCA" >LAGUNA BLANCA</option>
						<option value="LAJA" >LAJA</option>
						<option value="LAMPA" >LAMPA</option>
						<option value="LANCO" >LANCO</option>
						<option value="LAS CABRAS">LAS CABRAS</option>
						<option value="LAS CONDES">LAS CONDES</option>
						<option value="LAS CRUCES">LAS CRUCES</option>
						<option value="LAUTARO">LAUTARO</option>
						<option value="LEBU">LEBU</option>
						<option value="LICANTEN">LICANTEN</option>
						<option value="LIMACHE">LIMACHE</option>
						<option value="LINARES">LINARES</option>
						<option value="LITUECHE">LITUECHE</option>
						<option value="LLAILLAY">LLAILLAY</option>
						<option value="LLANQUIHUE">LLANQUIHUE</option>
						<option value="LLOLLEO">LLOLLEO</option>
						<option value="LO BARNECHEA">LO BARNECHEA</option>
						<option value="LO ESPEJO">LO ESPEJO</option>
						<option value="LO MIRANDA">LO MIRANDA</option>
						<option value="LO PRADO">LO PRADO</option>
						<option value="LOLOL">LOLOL</option>
						<option value="LONCOCHE">LONCOCHE</option>
						<option value="LONGAVI">LONGAVI</option>
						<option value="LONQUEN">LONQUEN</option>
						<option value="LONQUIMAY">LONQUIMAY</option>
						<option value="LONTUE">LONTUE</option>
						<option value="LOS ALAMOS">LOS ALAMOS</option>
						<option value="LOS ANDES">LOS ANDES</option>
						<option value="LOS ANGELES">LOS ANGELES</option>
						<option value="LOS LAGOS">LOS LAGOS</option>
						<option value="LOS MUERMOS">LOS MUERMOS</option>
						<option value="LOS SAUCES">LOS SAUCES</option>
						<option value="LOS VILOS">LOS VILOS</option>
						<option value="LOTA">LOTA</option>
						<option value="LUMACO">LUMACO</option>
						<option value="MACHALI">MACHALI</option>
						<option value="MACUL">MACUL</option>
						<option value="MAFIL">MAFIL</option>
						<option value="MAIPU">MAIPU</option>
						<option value="MALLOA">MALLOA</option>
						<option value="MALLOCO">MALLOCO</option>
						<option value="MARCHIHUE">MARCHIHUE</option>
						<option value="MARIA ELENA">MARIA ELENA</option>
						<option value="MARIA PINTO">MARIA PINTO</option>
						<option value="MARIQUINA">MARIQUINA</option>
						<option value="MAULE">MAULE</option>
						<option value="MAULLIN">MAULLIN</option>
						<option value="MEJILLONES">MEJILLONES</option>
						<option value="MELIPEUCO">MELIPEUCO</option>
						<option value="MELIPILLA">MELIPILLA</option>
						<option value="MININCO">MININCO</option>
						<option value="MOLINA">MOLINA</option>
						<option value="MONTE PATRIA">MONTE PATRIA</option>
						<option value="MOSTAZAL">MOSTAZAL</option>
						<option value="MULCHEN">MULCHEN</option>
						<option value="NACIMIENTO">NACIMIENTO</option>
						<option value="NANCAGUA">NANCAGUA</option>
						<option value="NATALES">NATALES</option>
						<option value="NAVIDAD">NAVIDAD</option>
						<option value="NEGRETE">NEGRETE</option>
						<option value="NINHUE">NINHUE</option>
						<option value="NIQUEN">NIQUEN</option>
						<option value="NOGALES">NOGALES</option>
						<option value="NOS">NOS</option>
						<option value="NUEVA IMPERIAL">NUEVA IMPERIAL</option>
						<option value="NUÑOA">NUÑOA</option>
						<option value="OHIGGINS">OHIGGINS</option>
						<option value="OLIVAR">OLIVAR</option>
						<option value="OLMUE">OLMUE</option>
						<option value="OSORNO">OSORNO</option>
						<option value="OVALLE">OVALLE</option>
						<option value="PADRE HURTADO">PADRE HURTADO</option>
						<option value="PADRE LAS CASAS">PADRE LAS CASAS</option>
						<option value="PAIHUANO">PAIHUANO</option>
						<option value="PAILLACO">PAILLACO</option>
						<option value="PAINE">PAINE</option>
						<option value="PAIPOTE">PAIPOTE</option>
						<option value="PALENA">PALENA</option>
						<option value="PALMILLA">PALMILLA</option>
						<option value="PANGUIPULLI">PANGUIPULLI</option>
						<option value="PANQUEHUE">PANQUEHUE</option>
						<option value="PAPUDO">PAPUDO</option>
						<option value="PAREDONES">PAREDONES</option>
						<option value="PARGUA">PARGUA</option>
						<option value="PARRAL">PARRAL</option>
						<option value="PEDRO AGUIRRE CERDA">PEDRO AGUIRRE CERDA</option>
						<option value="PELARCO">PELARCO</option>
						<option value="PELEQUEN">PELEQUEN</option>
						<option value="PELLUHUE">PELLUHUE</option>
						<option value="PEMUCO">PEMUCO</option>
						<option value="PEÑABLANCA">PEÑABLANCA</option>
						<option value="PEÑAFLOR">PEÑAFLOR</option>
						<option value="PEÑALOLEN">PEÑALOLEN</option>
						<option value="PENCAHUE">PENCAHUE</option>
						<option value="PENCO">PENCO</option>
						<option value="PERALILLO">PERALILLO</option>
						<option value="PERQUENCO">PERQUENCO</option>
						<option value="PETORCA">PETORCA</option>
						<option value="PEUMO">PEUMO</option>
						<option value="PICA">PICA</option>
						<option value="PICHIDEGUA">PICHIDEGUA</option>
						<option value="PICHILEMU">PICHILEMU</option>
						<option value="PINTO">PINTO</option>
						<option value="PIRQUE">PIRQUE</option>
						<option value="PITRUFQUEN">PITRUFQUEN</option>
						<option value="PLACILLA">PLACILLA</option>
						<option value="PLACILLA (VINA DEL MAR)">PLACILLA (VINA DEL MAR)</option>
						<option value="PORTEZUELO">PORTEZUELO</option>
						<option value="PORVENIR">PORVENIR</option>
						<option value="POZO ALMONTE">POZO ALMONTE</option>
						<option value="PRIMAVERA">PRIMAVERA</option>
						<option value="PROVIDENCIA">PROVIDENCIA</option>
						<option value="PUCHUNCAVI">PUCHUNCAVI</option>
						<option value="PUCÓN">PUCÓN</option>
						<option value="PUDAHUEL">PUDAHUEL</option>
						<option value="PUEBLO SECO">PUEBLO SECO</option>
						<option value="PUENTE ALTO">PUENTE ALTO</option>
						<option value="PUERTO AGUIRRE">PUERTO AGUIRRE</option>
						<option value="PUERTO CHACABUCO">PUERTO CHACABUCO</option>
						<option value="PUERTO MONTT">PUERTO MONTT</option>
						<option value="PUERTO OCTAY">PUERTO OCTAY</option>
						<option value="PUERTO VARAS">PUERTO VARAS</option>
						<option value="PUMANQUE">PUMANQUE</option>
						<option value="PUNITAQUI">PUNITAQUI</option>
						<option value="PUNTA ARENAS">PUNTA ARENAS</option>
						<option value="PUQUELDON">PUQUELDON</option>
						<option value="PUREN">PUREN</option>
						<option value="PURRANQUE">PURRANQUE</option>
						<option value="PUTAENDO">PUTAENDO</option>
						<option value="PUTRE">PUTRE</option>
						<option value="PUYEHUE">PUYEHUE</option>
						<option value="QUEILEN">QUEILEN</option>
						<option value="QUELLON">QUELLON</option>
						<option value="QUEMCHI">QUEMCHI</option>
						<option value="QUEPE">QUEPE</option>
						<option value="QUILACO">QUILACO</option>
						<option value="QUILICURA">QUILICURA</option>
						<option value="QUILLECO">QUILLECO</option>
						<option value="QUILLON">QUILLON</option>
						<option value="QUILLOTA">QUILLOTA</option>
						<option value="QUILPUE">QUILPUE</option>
						<option value="QUINCHAO">QUINCHAO</option>
						<option value="QUINTA DE TILCOCO">QUINTA DE TILCOCO</option>
						<option value="QUINTA NORMAL">QUINTA NORMAL</option>
						<option value="QUINTERO">QUINTERO</option>
						<option value="QUIRIHUE">QUIRIHUE</option>
						<option value="QUIRIQUINA">QUIRIQUINA</option>
						<option value="RANCAGUA">RANCAGUA</option>
						<option value="RANQUIL">RANQUIL</option>
						<option value="RAUCO">RAUCO</option>
						<option value="RECOLETA">RECOLETA</option>
						<option value="REÑACA">REÑACA</option>
						<option value="RENAICO">RENAICO</option>
						<option value="RENCA">RENCA</option>
						<option value="RENGO">RENGO</option>
						<option value="REQUINOA">REQUINOA</option>
						<option value="RETIRO">RETIRO</option>
						<option value="RINCONADA">RINCONADA</option>
						<option value="RIO BUENO">RIO BUENO</option>
						<option value="RIO CLARO">RIO CLARO</option>
						<option value="RIO HURTADO">RIO HURTADO</option>
						<option value="RIO IBANEZ">RIO IBANEZ</option>
						<option value="RIO NEGRO">RIO NEGRO</option>
						<option value="RIO VERDE">RIO VERDE</option>
						<option value="ROMERAL">ROMERAL</option>
						<option value="ROSARIO">ROSARIO</option>
						<option value="SAAVEDRA">SAAVEDRA</option>
						<option value="SAGRADA FAMILIA">SAGRADA FAMILIA</option>
						<option value="SALAMANCA">SALAMANCA</option>
						<option value="SAN ANTONIO">SAN ANTONIO</option>
						<option value="SAN BERNARDO">SAN BERNARDO</option>
						<option value="SAN CARLOS">SAN CARLOS</option>
						<option value="SAN CLEMENTE">SAN CLEMENTE</option>
						<option value="SAN ESTEBAN">SAN ESTEBAN</option>
						<option value="SAN FABIAN">SAN FABIAN</option>
						<option value="SAN FELIPE">SAN FELIPE</option>
						<option value="SAN FERNANDO">SAN FERNANDO</option>
						<option value="SAN GREGORIO">SAN GREGORIO</option>
						<option value="SAN IGNACIO">SAN IGNACIO</option>
						<option value="SAN JAVIER">SAN JAVIER</option>
						<option value="SAN JOAQUIN">SAN JOAQUIN</option>
						<option value="SAN JOSE DE MAIPO">SAN JOSE DE MAIPO</option>
						<option value="SAN JUAN DE LA COSTA">SAN JUAN DE LA COSTA</option>
						<option value="SAN MIGUEL">SAN MIGUEL</option>
						<option value="SAN NICOLAS">SAN NICOLAS</option>
						<option value="SAN PABLO">SAN PABLO</option>
						<option value="SAN PEDRO DE ATACAMA">SAN PEDRO DE ATACAMA</option>
						<option value="SAN PEDRO DE LA PAZ">SAN PEDRO DE LA PAZ</option>
						<option value="SAN PEDRO DE MELIPILLA">SAN PEDRO DE MELIPILLA</option>
						<option value="SAN RAFAEL">SAN RAFAEL</option>
						<option value="SAN RAMON">SAN RAMON</option>
						<option value="SAN ROSENDO">SAN ROSENDO</option>
						<option value="SAN SEBASTIAN">SAN SEBASTIAN</option>
						<option value="SAN VICENTE DE TAGUA TAGUA">SAN VICENTE DE TAGUA TAGUA</option>
						<option value="SANTA BARBARA">SANTA BARBARA</option>
						<option value="SANTA CRUZ">SANTA CRUZ</option>
						<option value="SANTA JUANA">SANTA JUANA</option>
						<option value="SANTA MARIA">SANTA MARIA</option>
						<option value="SANTIAGO CENTRO">SANTIAGO CENTRO</option>
						<option value="SANTO DOMINGO">SANTO DOMINGO</option>
						<option value="SIERRA GORDA">SIERRA GORDA</option>
						<option value="TALAGANTE">TALAGANTE</option>
						<option value="TALCA">TALCA</option>
						<option value="TALCAHUANO">TALCAHUANO</option>
						<option value="TALTAL">TALTAL</option>
						<option value="TEMUCO">TEMUCO</option>
						<option value="TENO">TENO</option>
						<option value="TEODORO SCHMIDT">TEODORO SCHMIDT</option>
						<option value="TIERRA AMARILLA">TIERRA AMARILLA</option>
						<option value="TIL TIL">TIL TIL</option>
						<option value="TIMAUKEL">TIMAUKEL</option>
						<option value="TIRUA">TIRUA</option>
						<option value="TOCOPILLA">TOCOPILLA</option>
						<option value="TOLTEN">TOLTEN</option>
						<option value="TOME">TOME</option>
						<option value="TORRES DEL PAINE">TORRES DEL PAINE</option>
						<option value="TORTEL">TORTEL</option>
						<option value="TRAIGUEN">TRAIGUEN</option>
						<option value="TUCAPEL">TUCAPEL</option>
						<option value="VALDIVIA">VALDIVIA</option>
						<option value="VALLENAR">VALLENAR</option>
						<option value="VALPARAISO">VALPARAISO</option>
						<option value="VICHUQUEN">VICHUQUEN</option>
						<option value="VICTORIA">VICTORIA</option>
						<option value="VICUÑA">VICUÑA</option>
						<option value="VILCUN">VILCUN</option>
						<option value="VILLA ALEGRE">VILLA ALEGRE</option>
						<option value="VILLA ALEMANA">VILLA ALEMANA</option>
						<option value="VILLARRICA">VILLARRICA</option>
						<option value="VIÑA DEL MAR">VIÑA DEL MAR</option>
						<option value="VITACURA">VITACURA</option>
						<option value="YERBAS BUENAS">YERBAS BUENAS</option>
						<option value="YUMBEL">YUMBEL</option>
						<option value="YUNGAY">YUNGAY</option>
						<option value="ZAPALLAR">ZAPALLAR</option>
				</select>
			</div>
		{/if}
		{if !$dniExist}
			<div class="required dni form-group unvisible">
				<label for="dni">{l s='Identification number'} <sup>*</sup></label>
				<input class="is_required form-control" data-validate="{$address_validation.dni.validate}" type="text" name="dni" id="dni" value="{if isset($smarty.post.dni)}{$smarty.post.dni}{else}{if isset($address->dni)}{$address->dni|escape:'html':'UTF-8'}{/if}{/if}" />
				<span class="form_info">{l s='DNI / NIF / NIE'}</span>
			</div>
		{/if}
		<div class="form-group">
			<label for="other">{l s='Additional information'}</label>
			<textarea class="validate form-control" data-validate="{$address_validation.other.validate}" id="other" name="other" cols="26" rows="3" >{if isset($smarty.post.other)}{$smarty.post.other}{else}{if isset($address->other)}{$address->other|escape:'html':'UTF-8'}{/if}{/if}</textarea>
		</div>
		{if !$homePhoneExist}
			<div class="form-group phone-number">
				<label for="phone">{l s='Home phone'}</label>
				<input class="{if isset($one_phone_at_least) && $one_phone_at_least}is_required{/if} validate form-control" data-validate="{$address_validation.phone.validate}" type="tel" id="phone" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{if isset($address->phone)}{$address->phone|escape:'html':'UTF-8'}{/if}{/if}"  />
			</div>
		{/if}
		<div class="clearfix"></div>
		{if !$mobilePhoneExist}
			<div class="{if isset($one_phone_at_least) && $one_phone_at_least}required {/if}form-group">
				<label for="phone_mobile">{l s='Mobile phone'}{if isset($one_phone_at_least) && $one_phone_at_least} <sup>**</sup>{/if}</label>
				<input class="validate form-control" data-validate="{$address_validation.phone_mobile.validate}" type="tel" id="phone_mobile" name="phone_mobile" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{else}{if isset($address->phone_mobile)}{$address->phone_mobile|escape:'html':'UTF-8'}{/if}{/if}" />
			</div>
		{/if}
		{if isset($one_phone_at_least) && $one_phone_at_least && !$atLeastOneExists}
			<p class="inline-infos required">{l s='You must register at least one phone number.'}</p>
		{/if}
		<div class="required form-group" id="adress_alias">
			<label for="alias">{l s='Please assign an address title for future reference.'} <sup>*</sup></label>
			<input type="text" id="alias" class="is_required validate form-control" data-validate="{$address_validation.alias.validate}" name="alias" value="{if isset($smarty.post.alias)}{$smarty.post.alias}{elseif isset($address->alias)}{$address->alias|escape:'html':'UTF-8'}{elseif !$select_address}{l s='My address'}{/if}" />
		</div>
		<p class="submit2">
			{if isset($id_address)}<input type="hidden" name="id_address" value="{$id_address|intval}" />{/if}
			{if isset($back)}<input type="hidden" name="back" value="{$back}" />{/if}
			{if isset($mod)}<input type="hidden" name="mod" value="{$mod}" />{/if}
			{if isset($select_address)}<input type="hidden" name="select_address" value="{$select_address|intval}" />{/if}
			<input type="hidden" name="token" value="{$token}" />
			<button type="submit" name="submitAddress" id="submitAddress" class="btn btn-default button">
				<span>{l s='Save'}</span>
			</button>
		</p>
	</form>
</div>
<ul class="footer_links clearfix">
	<li>
		<a class="btn btn-default button" href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}">
			<span>{l s='Back to your addresses'}</span>
		</a>
	</li>
</ul>
{strip}
{if isset($smarty.post.id_state) && $smarty.post.id_state}
	{addJsDef idSelectedState=$smarty.post.id_state|intval}
{elseif isset($address->id_state) && $address->id_state}
	{addJsDef idSelectedState=$address->id_state|intval}
{else}
	{addJsDef idSelectedState=false}
{/if}
{if isset($smarty.post.id_country) && $smarty.post.id_country}
	{addJsDef idSelectedCountry=$smarty.post.id_country|intval}
{elseif isset($address->id_country) && $address->id_country}
	{addJsDef idSelectedCountry=$address->id_country|intval}
{else}
	{addJsDef idSelectedCountry=false}
{/if}
{if isset($countries)}
	{addJsDef countries=$countries}
{/if}
{if isset($vatnumber_ajax_call) && $vatnumber_ajax_call}
	{addJsDef vatnumber_ajax_call=$vatnumber_ajax_call}
{/if}
{/strip}
