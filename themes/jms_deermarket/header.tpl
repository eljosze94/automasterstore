{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<!-- Google Tag Manager -->
		{literal}
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-M48NFX3');</script>
		{/literal}
		<!-- End Google Tag Manager -->
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> -->
                <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{$content_dir}themes/{$themename}/css/bootstrap.min.css" />
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				{if $css_uri == 'lteIE9'}
					<!--[if lte IE 9]>
					{foreach from=$css_files[$css_uri] key=css_uriie9 item=mediaie9}
					<link rel="stylesheet" href="{$css_uriie9|escape:'html':'UTF-8'}" type="text/css" media="{$mediaie9|escape:'html':'UTF-8'}" />
					{/foreach}
					<![endif]-->
				{else}
					<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
				{/if}
			{/foreach}
		{/if}
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			{$js_def}
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
			{/foreach}
		{/if}
		{$HOOK_HEADER}
		<link rel="stylesheet" type="text/css" href="{$content_dir}themes/{$themename}/css/theme-responsive.css" />
		<script type="text/javascript" src="{$content_dir}themes/{$themename}/js/jquery.viewportchecker.js"></script>
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($jpb_homeclass) && $jpb_homeclass}{$jpb_homeclass}{/if} {if isset($jpb_grid) && $jpb_grid == 0}layout-list{/if} {if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} show-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}{if $jpb_rtl} rtl{/if}">
	<!-- Google Tag Manager (noscript) -->
	{literal}
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M48NFX3"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	{/literal}
	<!-- End Google Tag Manager (noscript) -->
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
		<div id="page">
			{if $jpb_mobilemenu}
				<div class="menu-wrap">
					<nav id="off-canvas-menu">
						<div id="off-canvas-title">{l s='MENU'}</div>
						{hook h='displayTopColumn'}
					</nav>				
					<button id="close-button" class="close-button">Close Menu</button>
				</div>
			{/if}
			<div class="main-site">
			{if isset($HOOK_TOP) && !$content_only}{$HOOK_TOP}{/if}
			{if $page_name != 'index'}
				<div class="bg-image-area container">
					<div class="content_scene_img">
						<div class="breadcrumb-box">
							{include file="$tpl_dir./breadcrumb.tpl"}
						</div>
					</div>
				</div>
			{/if}

			
			{if $page_name!='index'}
			<section class="container page-content">
				<div class="row">
					
					{if $page_name!='index' && !$hide_left_column && !empty($HOOK_LEFT_COLUMN)}
					<!-- Left -->
						{if !$content_only}
							<aside class="col-xs-12 col-sm-12 col-md-2 col-lg-3 col-xs-12 content-aside small-column">
								<div class="content-aside-inner"> 
									{$HOOK_LEFT_COLUMN}
                                                                         <!-- <div style="padding:10px;"> <img src="/img/" alt="" style="width:128px;height:128px;"></div> --> 
                                                                         
								</div>	
							</aside>	
						{/if}					
					{/if}
					<!-- Center -->
					{$show_left = !$hide_left_column && !empty($HOOK_LEFT_COLUMN)}
					{$show_right = !$hide_right_column && !empty($HOOK_RIGHT_COLUMN)}
					{if $page_name!='index'}
						{if $show_left && $show_right}
							<section class="container content-center" id="center_column">
						{elseif ($show_left && !$show_right) || (!$show_left && $show_right)}
							<section class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xs-12 {if $page_name == 'product'}col-md-push-3{/if} large-column" id="center_column">
						{else}
							<section class="container content-center without-column" id="center_column">							
						{/if}
					
					{else}					
					<section class="container content-centers" id="center_column">
					{/if}
			{/if}
	{/if}
